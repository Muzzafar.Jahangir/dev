#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate License paypal - Uruguay

  Background: 
    Given User access to Uruguay store
    And I Close Cookie 
    
   @TCID07  @TCIDSanity @TCIDUruguay @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Uruguay
    When I Click on Corporate License Button US Region
    Then I Click on Next button
	And I enter Uruguay data
	And I select the State
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	@TCID08 @TCIDSanity @TCIDUruguay @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Uruguay
    When I Click on Corporate License Button US Region
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Uruguay data
	And I select the State
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement