#Created by Muhammad Naveed Khan and Naqash Zafar on April 24, 2019
Feature: Business License CC - Albania

  Background: 
    Given User access to Albania store
    
    @TCIDSanity @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic @TCID123
  Scenario: Verify purchase business license of Team Viewer for Albania
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Albania data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	    @TCIDSanity @TCIDAlbania @TCIDAlbaniaBusinessLicenseAddOnsWithCC 
  Scenario: Verify purchase business license of Team Viewer for Albania with CC
  	#When I Change Langauage and Open WebShop
    When I Click on Business License Button
     And I Close Cookie
     Then I Refresh Browser
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Albania data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	And I Get Order Id and Payment Method From Success Page
 	

 	
 	