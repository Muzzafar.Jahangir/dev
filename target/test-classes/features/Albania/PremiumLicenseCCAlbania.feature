#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Premium License CC - Albania

  Background: 
    Given User access to Albania store
    And I Close Cookie 
    
    @TCIDSanity @TCIDAlbania @TCIDPremiumLicenseCC @TCIDPremiumLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Albania
    When I Click on Premium License Button
    Then I Click on Next button
	And I enter Albania data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDAlbania @TCIDPremiumLicenseCC @TCIDPremiumLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Albania
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Albania data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement