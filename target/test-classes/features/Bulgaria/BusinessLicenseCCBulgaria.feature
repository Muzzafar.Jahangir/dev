#Created by Muhammad Naveed Khan and Naqash Zafar on April 24, 2019
Feature: Business License CC - Bulgaria

  Background: 
    Given User access to Bulgaria store
    And I Close Cookie 
    
     @TCIDSanity @TCIDBulgaria @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Bulgaria
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Bulgaria data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	@TCIDSanity @TCIDBulgaria @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Bulgaria
    When I Click on Business License Button
      And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Bulgaria data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	
	     @TCIDSanity @TCIDBulgariaVAT @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Bulgaria using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Bulgaria VAT
	And I enter Bulgaria data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	@TCIDSanity @TCIDBulgariaVAT @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Bulgaria using VAT
    When I Click on Business License Button
      And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Bulgaria VAT
	And I enter Bulgaria data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	