#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Premium Licence Invoice - Bulgaria

  Background: 
    Given User access to Bulgaria store
    And I Close Cookie 
    
   @TCIDSanity @TCIDBulgaria @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Bulgaria
    When I Click on Premium License Button
    Then I Click on Next button
	And I enter Bulgaria data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDBulgaria @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Bulgaria
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Bulgaria data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDBulgariaVAT @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Bulgaria using VAT
    When I Click on Premium License Button
    Then I Click on Next button
    And I enter Bulgaria VAT
	And I enter Bulgaria data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDBulgariaVAT @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Bulgaria using VAT
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter Bulgaria VAT
	And I enter Bulgaria data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement