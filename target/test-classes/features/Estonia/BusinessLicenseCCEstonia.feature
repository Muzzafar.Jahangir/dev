#Created by Muhammad Naveed Khan and Naqash Zafar on April 24, 2019
Feature: Business License CC - Estonia

  Background: 
    Given User access to Estonia store
    And I Close Cookie 
    
   @TCIDSanity @TCIDEstonia @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Estonia
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Estonia data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	   @TCIDSanity @TCIDEstonia @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Estonia
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Estonia data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	   
   @TCIDSanity @TCIDEstoniaVAT @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Estonia using VAT
    When I Click on Business License Button 
    Then I Click on Next button
    And I enter Estonia VAT
	And I enter Estonia data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	   @TCIDSanity @TCIDEstoniaVAT @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Estonia using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Estonia VAT
	And I enter Estonia data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	

 	
 	