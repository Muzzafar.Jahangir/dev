#Created by Muhammad Naveed Khan on August 13, 2018
Feature: Request a Catalog

  Background: 
    Given User Access to application

 @TCID2222
  Scenario: Verify CheckOut Process for Team Viewer
    When I Click on Multi User button
    And I add concurrent users
    And I add mobile devices
 	And I Click on Next button
 	And I enter data
 	And I enter Email
 	And I select payment
 	And I select check box
 	And I Place Order
 	And I verify Order Placement