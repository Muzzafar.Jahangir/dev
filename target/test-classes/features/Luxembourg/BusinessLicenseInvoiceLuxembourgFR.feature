#Created by Muhammad Naveed Khan and Naqash Zafar on May 3, 2019
Feature: Business Licence Invoice - Lexumbourg FR

  Background: 
    Given User access to Luxembourg FR store
    And I Close Cookie
    
  @TCIDSanity  @TCIDLexumbourg @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Luxembourg FR
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Luxembourg data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 
 @TCIDSanity  @TCIDLexumbourg @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Luxembourg FR
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Luxembourg data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	  @TCIDSanity  @TCIDLexumbourg @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Luxembourg FR using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Luxembourg VAT
	And I enter Luxembourg data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 
 @TCIDSanity  @TCIDLexumbourg @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Luxembourg FR using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Luxembourg VAT
	And I enter Luxembourg data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement

 	
 	