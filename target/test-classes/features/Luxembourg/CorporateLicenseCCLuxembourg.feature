#Created by Muhammad Naveed Khan and Naqash Zafar on May 3, 2019
Feature: Corporate License CC - Lexumbourg

  Background: 
    Given User access to Luxembourg store
    And I Close Cookie 
    
    @TCIDSanity @TCIDLexumbourg @TCIDCorporateLicenseCC @TCIDCorporateLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Luxembourg
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Luxembourg data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	    @TCIDSanity @TCIDLexumbourg @TCIDCorporateLicenseCC @TCIDCorporateLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Luxembourg
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Luxembourg data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	    @TCIDSanity @TCIDLexumbourg @TCIDCorporateLicenseCC @TCIDCorporateLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Luxembourg using VAT
    When I Click on Corporate License Button
    Then I Click on Next button
     And I enter Luxembourg VAT
	And I enter Luxembourg data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	    @TCIDSanity @TCIDLexumbourg @TCIDCorporateLicenseCC @TCIDCorporateLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Luxembourg using VAT
    When I Click on Corporate License Button 
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
     And I enter Luxembourg VAT
	And I enter Luxembourg data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement