 #Created by Muhammad Naveed Khan and Naqash Zafar on May 30 , 2019
Feature: Verify cancellation of Update TV13B0001 To TV14B0001

  Background: 
    Given User access to D365
     
    
  @CancellationOfUpdate-6076 @TCIDSanity
  Scenario: Verify cancellation of Update TV13B0001 To TV14B0001
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create TV13B0001 Perpetual order in ERP
 	And I get values of tax subtotal and totals for Internal Created Order
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I update TV13B0001 To TV14B0001
 	And I get values of tax subtotal and totals for Internal Created Order
	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I validate TV13B0001 To TV14B0001 update
 	And I Cancel Perpetual Contract On ERP
 	And I confirm sales order and invoice in ERP
 	And I validate TV13B0001 line is activated back
 	And I open journal invoice for voucher checking
 
 	
 	