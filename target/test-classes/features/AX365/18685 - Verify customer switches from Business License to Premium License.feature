#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Switch Business License To Premium License In ERP

  Background: 
    Given User access to D365
     
    
  @TCIDSwitchBtoP @TCIDSanity
  Scenario: Verify Business License is successfully switched to Premium License in Dynamics 365
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Business license order in ERP
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I switch from Business To Premium
 	And I complete order in ERP
 	And I again confirm sales order and invoice in ERP for Switch Order
 	And I validate Business To Premium switching
 	And I Cancel Switched Contract On ERP
 	
 	