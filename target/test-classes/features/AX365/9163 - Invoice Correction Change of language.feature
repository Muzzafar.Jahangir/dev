#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Invoice Correction for Change Language

  Background: 
    Given User access to D365
     
    
  @TCIDInvoiceCorrectionForLanguageChange-9163 @TCIDSanity
  Scenario: Verify Invoice Correction for Change Of Language
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Business license order in ERP
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I correct the invoice and changing the language
 	And I resend the invoice for implementing the changes
 	And I verify the language of invoice after resending
 	And I open journal invoice for voucher checking and lasernet printing
 	And I verify the language of invoice after resending
 	
 	
 	
 	