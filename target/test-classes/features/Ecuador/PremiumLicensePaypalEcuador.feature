#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Premium License paypal - Ecuador

  Background: 
    Given User access to Ecuador store
    And I Close Cookie 
    
   @TCIDSanity @TCIDEcuador @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Ecuador
    When I Click on Premium License Button US Region
    Then I Click on Next button
	And I enter Ecuador data
	And I select the State
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDEcuador @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Ecuador
    When I Click on Premium License Button US Region
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Ecuador data
	And I select the State
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement