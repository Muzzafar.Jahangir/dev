#Created by Muhammad Naveed Khan and Naqash Zafar on April 24, 2019
Feature: Business Licence CC - Ecuador

  Background: 
    Given User access to Ecuador store
    And I Close Cookie 
    
    @TCIDSanity @TCIDEcuador @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Ecuador
    When I Click on Business License Button US Region
    Then I Click on Next button
	And I enter Ecuador data
	And I select the State
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 
  @TCIDSanity @TCIDEcuador @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Ecuador
    When I Click on Business License Button US Region
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Ecuador data
	And I select the State
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement

 	
 	