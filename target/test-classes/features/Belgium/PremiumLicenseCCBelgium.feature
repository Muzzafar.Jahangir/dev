#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Premium License CC - Belgium

  Background: 
    Given User access to Belgium store
    And I Close Cookie 
    
    @TCIDSanity @TCIDBelgium @TCIDPremiumLicenseCC @TCIDPremiumLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Belgium
    When I Click on Premium License Button
    Then I Click on Next button
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDBelgium @TCIDPremiumLicenseCC @TCIDPremiumLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Belgium
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	    @TCIDSanity @TCIDBelgium @TCIDPremiumLicenseCC @TCIDPremiumLicenseBasic @TCIDBelgiumVAT
  Scenario: Verify purchase Premium license of Team Viewer for Belgium using VAT
    When I Click on Premium License Button
    Then I Click on Next button
    And I enter Belgium VAT
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDBelgium @TCIDPremiumLicenseCC @TCIDPremiumLicenseAddOns @TCIDBelgiumVAT
  Scenario: Verify purchase Premium license of Team Viewer for Belgium using VAT
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter Belgium VAT
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement