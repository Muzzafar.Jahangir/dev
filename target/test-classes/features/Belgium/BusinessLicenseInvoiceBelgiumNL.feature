#Created by Muhammad Naveed Khan and Naqash Zafar on April 23, 2019
Feature: Business Licence Invoice - Belgium NL

  Background: 
    Given User access to Belgium NL store
    And I Close Cookie 
    
    @TCIDSanity @TCIDBelgium @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Belgium NL
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
  @TCIDSanity @TCIDBelgium @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Belgium NL
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	    @TCIDSanity @TCIDBelgium @TCIDBusinessLicense @TCIDBusinessLicenseBasic @TCIDBelgiumVAT
  Scenario: Verify purchase business license of Team Viewer for Belgium NL using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Belgium VAT
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
  @TCIDSanity @TCIDBelgium @TCIDBusinessLicense @TCIDBusinessLicenseAddOns @TCIDBelgiumVAT
  Scenario: Verify purchase business license of Team Viewer for Belgium NL using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Belgium VAT
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement