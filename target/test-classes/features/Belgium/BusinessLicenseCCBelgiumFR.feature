#Created by Muhammad Naveed Khan and Naqash Zafar on April 24, 2019
Feature: Business Licence CC - Belgium FR

  Background: 
    Given User access to Belgium FR store
    And I Close Cookie 
    
     @TCIDSanity @TCIDBelgium @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Belgium FR
    When I Click on Business License Button
   # Then I Click on Next button
#	And I enter Belgium data
# 	And I select Credit Card as Payment Method
 #	And I enter Credit Card Data
 #	And I select check box
 #	And Validating the Calculations
 #	And I Place Order
 #	And I verify Order Placement
 	
 	  @TCIDSanity @TCIDBelgium @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Belgium FR
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement


     @TCIDSanity @TCIDBelgium @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic @TCIDBelgiumVAT
  Scenario: Verify purchase business license of Team Viewer for Belgium FR using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Belgium VAT
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	  @TCIDSanity @TCIDBelgium @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns @TCIDBelgiumVAT
  Scenario: Verify purchase business license of Team Viewer for Belgium FR using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Belgium VAT
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	