#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Premium License PayPal - Belgium NL

  Background: 
    Given User access to Belgium NL store
    And I Close Cookie 
    
 @TCIDSanity @TCIDBelgium @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Belgium NL
    When I Click on Premium License Button
    Then I Click on Next button
	And I enter Belgium data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	 @TCIDSanity @TCIDBelgium @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Belgium NL
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Belgium data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	 @TCIDSanity @TCIDBelgium @TCIDBusinessLicense @TCIDBusinessLicenseBasic @TCIDBelgiumVAT
  Scenario: Verify purchase Premium license of Team Viewer for Belgium NL using VAT
    When I Click on Premium License Button
    Then I Click on Next button
    And I enter Belgium VAT
	And I enter Belgium data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	 @TCIDSanity @TCIDBelgium @TCIDBusinessLicense @TCIDBusinessLicenseAddOns @TCIDBelgiumVAT
  Scenario: Verify purchase Premium license of Team Viewer for Belgium NL using VAT
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter Belgium VAT
	And I enter Belgium data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement