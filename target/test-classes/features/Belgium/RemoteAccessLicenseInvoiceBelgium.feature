#Created by Muhammad Naveed Khan and Naqash Zafar on April, 26 2019
Feature: Remote Access License Invoice - Belgium

  Background: 
    Given User access to Belgium store
    And I Close Cookie
   @TCIDSanity @TCIDBelgium @TCIDRemoteLicense
  Scenario: Verify CheckOut Process for Team Viewer for Belgium EN
	  
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	 And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement

 	
@TCIDBelgiumVAT @TCIDSanity @TCIDBelgium @TCIDRemoteLicense
  Scenario: Verify CheckOut Process for Team Viewer for Belgium EN using VAT
	  
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
    And I enter Belgium VAT
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	 And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement	
 	
 	

 	
 	