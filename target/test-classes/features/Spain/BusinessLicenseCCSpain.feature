#Created by Muhammad Naveed Khan and Naqash Zafar on May 3, 2019
Feature: Business Licence CC - Spain

  Background: 
    Given User access to Spain store
    And I Close Cookie
    
   @TCIDWeekTest @TCIDSanity  @TCIDSpain @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Spain
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Spain data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 @TCIDSanity  @TCIDSpain @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Spain
    When I Click on Business License Button
     And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Spain data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	   @TCIDSanity @TCIDSpainVAT @TCIDSpain @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Spain using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Spain VAT
	And I enter Spain data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 @TCIDSanity  @TCIDSpain @TCIDSpainVAT @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Spain using VAT
    When I Click on Business License Button
     And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Spain VAT
	And I enter Spain data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	