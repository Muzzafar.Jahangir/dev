#Created by Muhammad Naveed Khan and Naqash Zafar on May 3, 2019
Feature: Business Licence CC - Italy IT

  Background: 
    Given User access to Italy IT store
    And I Close Cookie
    
   @TCIDSanity  @TCIDItaly @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Italy IT
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Italy data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	 @TCIDSanity  @TCIDItaly @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Italy IT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Italy data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement

 	
 	   @TCIDSanity  @TCIDItalyVAT @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Italy IT using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Italy VAT
	And I enter Italy data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	 @TCIDSanity  @TCIDItalyVAT @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Italy IT using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Italy VAT
	And I enter Italy data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement