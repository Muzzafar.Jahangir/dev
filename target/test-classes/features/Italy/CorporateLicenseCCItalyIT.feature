#Created by Muhammad Naveed Khan and Naqash Zafar on May 3, 2019
Feature: Corporate License CC - Italy IT

  Background: 
    Given User access to Italy IT store
    And I Close Cookie 
    
    @TCIDSanity @TCIDItaly @TCIDCorporateLicenseCC @TCIDCorporateLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Italy IT
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Italy data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	    @TCIDSanity @TCIDItaly @TCIDCorporateLicenseCC @TCIDCorporateLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Italy IT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Italy data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	    @TCIDSanity @TCIDItalyVAT @TCIDCorporateLicenseCC @TCIDCorporateLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Italy IT using VAT
    When I Click on Corporate License Button
    Then I Click on Next button
    And I enter Italy VAT
	And I enter Italy data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	    @TCIDSanity @TCIDItalyVAT @TCIDCorporateLicenseCC @TCIDCorporateLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Italy IT using VAT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter Italy VAT
	And I enter Italy data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement