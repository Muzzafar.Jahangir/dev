#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate License CC - Puerto Rico

  Background: 
    Given User access to Puerto Rico store
    And I Close Cookie 
    
    @TCIDSanity @TCIDPuertoRico @TCIDCorporateLicenseCC @TCIDCorporateLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Puerto Rico
    When I Click on Corporate License Button US Region
    Then I Click on Next button
	And I enter Puerto Rico data
	And I select the State
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	    @TCIDSanity @TCIDPuertoRico @TCIDCorporateLicenseCC @TCIDCorporateLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Puerto Rico
    When I Click on Corporate License Button US Region
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Puerto Rico data
	And I select the State
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement