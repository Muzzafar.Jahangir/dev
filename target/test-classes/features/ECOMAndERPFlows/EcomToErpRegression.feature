Feature:   Verify invoice calculation when order is placed from webshop for Corporate License using Invoice payment method no VAT

  Background: 
    Given User access to Netherlands store for staging
 	
 	@EcomToErpRegression
  Scenario:  Verify invoice calculation when order is placed from webshop for Corporate License using Invoice payment method no VAT

     And I Close Cookie
    Then I Refresh Browser
    And I add All Premium/Corporate License AddOns
    And I Click on Next button
    And I enter Netherlands data
    And I get quantity and Prices of Products
    And I select invoice as payment method
    And I select check box
 #   And Validating the Calculations
    And I Place Order
 #   And I verify Order Placement