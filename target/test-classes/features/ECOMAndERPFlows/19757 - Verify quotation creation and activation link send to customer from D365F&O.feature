#Created by Aamir Khan
Feature: Verify quotation creation and activation link send to customer from D365F&O

  Background: 
    Given User access to Netherlands store for staging to Request Quote
   
  
  @TCIDPaymentBusinesslicense-19757
  Scenario: Verify CheckOut Process for Team Viewer for Netherlands
	 
	 	 
    When I Close Cookie
	And I Click Premium Product
	
	#ECOM FOR PLACING QUOTATION
	
	And I add All Premium Quote AddOns
	Then I Click on Next button
	And I enter Netherlands data										
	And I get quantity and Prices of Products for All adds on
	And I Place Order
	And I verify Quote submission

	############################
	#EMAIL FOR COPYING LINK AND QUOTAION ID
	
	And I Switch To Email For Quotation ID and Activation Link
	And I Login To Email												
	And I Click On Email and get Quotation ID
	And I select Activation Link
	And I sign out from email

	
	############################
	
	#ERP FOR QUOTATION STATUS
   
 	And I Switch To ERP for Order Verification  
 	And I Login ERP										
 	And I Navigate to Quotation

	############################
	
	#ECOM FOR PLACING ORDER OF QUOTATION AND SIGN UP FOR MCO PORTAL
	
	And I switch to Check out Page
	And I Click for Netherlands Check out link
	And I Close Cookie
#	And Validating the Calculations	
								
	And I select invoice as payment method
	And I select check box
	And I Place Order
	And I verify Order Placement
	And I Get Order Id and Payment Method From Success Page
	And I click Activiate License Email button
	And I SignUp for MCO Portal
   


	 ############################	
	
	#MAGENTO FOR SALES ORDER ID
	
 	And I Switch to Magento 
 	And I Login to Magento                              
 	And I Copy Sales Order ID
 	
 	############################
 	
 	#ERP FOR VERIFICATION OF SALES ORDER
 	And I Switch To ERP for Order Verification  
 	And I Login ERP		
 	And I open the sales order
  	And I verify quantity of purchased products in ERP
 	And I verify amount of purchased products in ERP
 	And I verify customer information in General Tab
 	And I verify customer information in SetUp Tab
 	And I verify customer information in Price And Information Tab
 	And I verify customer information in Financials And Dimensions Tab
 	And I open new tab and get the number of Main Account and Sales Tax using Sales Tax Code and Sales Tax Group
# 	And I get values of tax subtotal and totals for verification of ECOM to ERP flow
 	And I confirm sales order and invoice in ERP
 	And I verify contract is invoiced
 	And I open invoice under journal for getting invoice number
 									
 	################################
 	#MCO PORTAL TO LOOK FOR PRODCUT
 	
 	And I switch and login to MCO Portal
 	And I Switch to MCO Portal to verify Premium product


	
