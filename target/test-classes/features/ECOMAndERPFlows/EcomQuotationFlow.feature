#Created by Aamir Khan
Feature: Verify quotation creation and activation link send to customer from D365F&O  
  
  Background: 
    Given User access to Netherlands store to Request Quote
   
  
  @EcomQuotationFlow
  Scenario: Verify CheckOut Process for Team Viewer for Netherlands
	  	 
    When I Close Cookie
	And I Click Premium Product

	And I add All Premium Quote AddOns
	Then I Click on Next button
	And I enter Netherlands data										
	And I get quantity and Prices of Products for All adds on
	And I Place Order
	And I verify Quote submission