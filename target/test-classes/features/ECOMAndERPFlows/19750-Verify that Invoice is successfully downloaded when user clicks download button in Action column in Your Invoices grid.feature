#Created by Aamir Khan
Feature: Remote Access License Invoice TEST1- Netherlands

  Background: 
    Given User access to Netherlands store for staging
   
  
  @TCIDPaymentBusinesslicense-19750
  Scenario: Verify CheckOut Process for Team Viewer for Netherlands
	 
	 
	 When I Click on Business License Button
    And I Close Cookie
    Then I Refresh Browser

   	Then I Click on Next button
	And I enter Netherlands data
	And I get quantity and Prices of Products for All adds on
	And I select invoice as payment method
	And I select check box
	And Validating the Calculations
	And I Place Order
	And I verify Order Placement
	And I Get Order Id and Payment Method From Success Page
	And I click Activiate License Email button
	And I SignUp for MCO Portal
   
 	And I Switch To ERP for Order Verification  
 	And I Login ERP
 	And I Search Channel Reference ID in ERP
 	And I open the sales order and verify products purchased in ERP
 	And I verify quantity of purchased products in ERP
 	And I verify amount of purchased products in ERP
 	And I verify customer information in General Tab
 	And I verify customer information in SetUp Tab
 	And I verify customer information in Price And Information Tab
 	And I verify customer information in Financials And Dimensions Tab
 	And I open new tab and get the number of Main Account and Sales Tax using Sales Tax Code and Sales Tax Group
 	And I get values of tax subtotal and totals for verification of ECOM to ERP flow
 	And I confirm sales order and invoice in ERP
 	And I verify contract is invoiced
 	And I open invoice under journal for getting invoice number
 	
 	And I resend the invoice for implementing the changes
 	
 	And I switch and login to MCO Portal
 	And I Switch to MCO Portal to verify product
 	And I download pdf
 	And I verify Invoice and Product on PDF

	
	
 	

 	
 	
 	

 	
 	