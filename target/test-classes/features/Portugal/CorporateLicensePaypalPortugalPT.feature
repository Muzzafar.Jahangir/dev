#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate License PayPal - Portugal

  Background: 
    Given User Access to Portugal PT store
    And I Close Cookie 
    
     @TCIDSanity @TCIDPortugal @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Portugal PT
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Portugal data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	 @TCID10101  @TCIDSanity @TCIDPortugal @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Portugal PT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Portugal data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	   @TCIDPortugalVAT  @TCIDSanity @TCIDPortugal @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Portugal PT using VAT
    When I Click on Corporate License Button
    Then I Click on Next button
     And I enter Portugal VAT
	And I enter Portugal data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	@TCIDPortugalVAT @TCIDSanity @TCIDPortugal @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Portugal PT using VAT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
     And I enter Portugal VAT
	And I enter Portugal data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement