#Created by Muhammad Naveed Khan and Naqash Zafar on April 23, 2019
Feature: Business Licence Invoice - Portugal PT

  Background: 
    Given User Access to Portugal PT store
    And I Close Cookie 
    
    @TCIDSanity @TCIDPortugal @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Portugal PT
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Portugal data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
    @TCIDSanity @TCIDPortugal @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Portugal PT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Portugal data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	    @TCIDSanity @TCIDPortugalVAT @TCIDPortugal @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Portugal PT using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Portugal VAT
	And I enter Portugal data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
    @TCIDSanity @TCIDPortugalVAT @TCIDPortugal @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Portugal PT using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Portugal VAT
	And I enter Portugal data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement