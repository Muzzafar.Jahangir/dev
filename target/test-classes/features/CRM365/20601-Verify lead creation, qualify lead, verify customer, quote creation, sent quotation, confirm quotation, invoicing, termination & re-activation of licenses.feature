#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify user can create a lead and prospect in CRM for Subscription Order

  Background: 
    Given User access to CRM
     
    
  @TCID20601 @TCIDSanity
  Scenario: Verify user can create a lead and prospect in CRM
  When I Login CRM
  And I create new Lead
  And I qualify qoute
  And I validate values in account
  And I validate values in contact and create quote
  And I create sales qoutation of Business license order in ERP for CRM Flow
  And I add add-ons, BLPA001 and ITBB0001 product
  And I send and verify quotation
  And I confirm and verify quotation in CRM
  And I check salesorder created and verified
  And I complete order in ERP
 And I confirm sales order and invoice in ERP for CRM
 And I verify contract is invoiced
 And I terminate contract and ReActivate
  
  
  
 	
 	
 	
 	
 	
 	 