#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate License Invoice - Netherlands

  Background: 
    Given User access to Netherlands store
    And I Close Cookie 
    
     @TCIDSanity @TCIDNetherlands @TCIDBusinessLicense @TCIDBusinessLicenseBasic @TCID2
  Scenario: Verify purchase Corporate license of Team Viewer for Netherlands
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Netherlands data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	 @TCID10101  @TCIDSanity @TCIDNetherlands @TCIDBusinessLicense @TCIDBusinessLicenseAddOns @TCID6
  Scenario: Verify purchase Corporate license of Team Viewer for Netherlands
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Netherlands data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	     @TCIDSanity @TCIDNetherlandsVAT @TCIDNetherlands @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Netherlands using VAT
    When I Click on Corporate License Button
    Then I Click on Next button
     And  I enter Netherlands VAT
	And I enter Netherlands data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	 @TCID10101 @TCIDNetherlandsVAT  @TCIDSanity @TCIDNetherlands @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Netherlands using VAT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
     And  I enter Netherlands VAT
	And I enter Netherlands data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement