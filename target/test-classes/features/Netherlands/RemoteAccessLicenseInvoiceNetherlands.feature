#Created by Muhammad Naveed Khan and Naqash Zafar on April, 26 2019
Feature: Remote Access License Invoice - Netherlands

  Background: 
    Given User access to Netherlands store
    
  @TCIDSanity @TCIDNetherlands @TCIDRemoteLicense @RemoteAccessNetherlands01
  Scenario: Verify CheckOut Process for Team Viewer for Netherlands EN
	  
    When I Click on Remote Access Subscription Button
    And I Close Cookie
    Then I Click on Next button for Staging
	And I enter Netherlands data
 	And I select invoice as payment method
 	And I select check box
 	 And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	@TCIDNetherlandsVAT @TCIDSanity @TCIDRemoteLicense @TCID8
  Scenario: Verify CheckOut Process for Team Viewer for Netherlands EN using VAT
	And  I Close Cookie  
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
   # And  I enter Netherlands VAT
	And  I enter Netherlands data
 	And  I select invoice as payment method
 	And  I select check box
 	And  Validating the Calculations
 	And  I Place Order
 	And  I verify Order Placement
 	
 	
 	

 	
 	