$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ECOMAndERPFlows/EcomInvoiceFlow.feature");
formatter.feature({
  "line": 1,
  "name": "Verify invoice calculation when order is placed from webshop for Corporate License using Invoice payment method no VAT",
  "description": "",
  "id": "verify-invoice-calculation-when-order-is-placed-from-webshop-for-corporate-license-using-invoice-payment-method-no-vat",
  "keyword": "Feature"
});
formatter.before({
  "duration": 698388756,
  "status": "passed"
});
formatter.before({
  "duration": 2300724,
  "status": "passed"
});
formatter.before({
  "duration": 773908,
  "status": "passed"
});
formatter.before({
  "duration": 3216835,
  "status": "passed"
});
formatter.before({
  "duration": 1068911,
  "status": "passed"
});
formatter.before({
  "duration": 7057876,
  "status": "passed"
});
formatter.before({
  "duration": 490805,
  "status": "passed"
});
formatter.before({
  "duration": 23727953,
  "status": "passed"
});
formatter.before({
  "duration": 1318114,
  "status": "passed"
});
formatter.before({
  "duration": 646807,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "User access to Netherlands store for staging",
  "keyword": "Given "
});
formatter.match({
  "location": "TVStartingSteps.NLAppStaging()"
});
formatter.result({
  "duration": 14827454627,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Verify invoice calculation when order is placed from webshop for Corporate License using Invoice payment method no VAT",
  "description": "",
  "id": "verify-invoice-calculation-when-order-is-placed-from-webshop-for-corporate-license-using-invoice-payment-method-no-vat;verify-invoice-calculation-when-order-is-placed-from-webshop-for-corporate-license-using-invoice-payment-method-no-vat",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@EcomInvoiceFlow"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "I Close Cookie",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I Refresh Browser",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I add All Premium/Corporate License AddOns",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I Click on Next button",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I enter Netherlands data",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I get quantity and Prices of Products",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I select invoice as payment method",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I select check box",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I Place Order",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.Click_Cookie()"
});
formatter.result({
  "duration": 11215388297,
  "status": "passed"
});
formatter.match({
  "location": "AdOnPageSteps.refreshBrowser()"
});
formatter.result({
  "duration": 16380169710,
  "status": "passed"
});
formatter.match({
  "location": "AdOnPageSteps.add_All_PemiumCorporateLicense_Addson()"
});
formatter.result({
  "duration": 8468969290,
  "status": "passed"
});
formatter.match({
  "location": "AdOnPageSteps.a_click_test()"
});
formatter.result({
  "duration": 24034029088,
  "status": "passed"
});
formatter.match({
  "location": "CheckOutPageSteps.enter_NL_EN_data()"
});
formatter.result({
  "duration": 8365435429,
  "status": "passed"
});
formatter.match({
  "location": "CheckOutPageSteps.getQuantityAndPrices()"
});
formatter.result({
  "duration": 3265854542,
  "status": "passed"
});
formatter.match({
  "location": "CheckOutPageSteps.select_payment()"
});
formatter.result({
  "duration": 4306760547,
  "status": "passed"
});
formatter.match({
  "location": "CheckOutPageSteps.select_checkbox()"
});
formatter.result({
  "duration": 6143793890,
  "status": "passed"
});
formatter.match({
  "location": "CheckOutPageSteps.place_order()"
});
formatter.result({
  "duration": 41163792440,
  "status": "passed"
});
formatter.uri("ECOMAndERPFlows/EcomQuotationFlow.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Created by Aamir Khan"
    }
  ],
  "line": 2,
  "name": "Verify quotation creation and activation link send to customer from D365F\u0026O",
  "description": "",
  "id": "verify-quotation-creation-and-activation-link-send-to-customer-from-d365f\u0026o",
  "keyword": "Feature"
});
formatter.before({
  "duration": 902609,
  "status": "passed"
});
formatter.before({
  "duration": 292003,
  "status": "passed"
});
formatter.before({
  "duration": 130802,
  "status": "passed"
});
formatter.before({
  "duration": 559706,
  "status": "passed"
});
formatter.before({
  "duration": 170102,
  "status": "passed"
});
formatter.before({
  "duration": 1002911,
  "status": "passed"
});
formatter.before({
  "duration": 92901,
  "status": "passed"
});
formatter.before({
  "duration": 15730861,
  "status": "passed"
});
formatter.before({
  "duration": 337504,
  "status": "passed"
});
formatter.before({
  "duration": 113801,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "User access to Netherlands store to Request Quote",
  "keyword": "Given "
});
formatter.match({
  "location": "TVStartingSteps.NLAppStagingForQuote()"
});
formatter.result({
  "duration": 25387845960,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Verify CheckOut Process for Team Viewer for Netherlands",
  "description": "",
  "id": "verify-quotation-creation-and-activation-link-send-to-customer-from-d365f\u0026o;verify-checkout-process-for-team-viewer-for-netherlands",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 8,
      "name": "@EcomQuotationFlow"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "I Close Cookie",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "I Click Premium Product",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I add All Premium Quote AddOns",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I Click on Next button",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "I enter Netherlands data",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I get quantity and Prices of Products for All adds on",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I Place Order",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "I verify Quote submission",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.Click_Cookie()"
});
formatter.result({
  "duration": 11047083117,
  "status": "passed"
});
formatter.match({
  "location": "HomePageSteps.clickPremiumProduct()"
});
formatter.result({
  "duration": 7337380978,
  "status": "passed"
});
formatter.match({
  "location": "AdOnPageSteps.add_All_Pemium_Quote_Addson()"
});
formatter.result({
  "duration": 154699219343,
  "status": "passed"
});
formatter.match({
  "location": "AdOnPageSteps.a_click_test()"
});
formatter.result({
  "duration": 20245033747,
  "status": "passed"
});
formatter.match({
  "location": "CheckOutPageSteps.enter_NL_EN_data()"
});
formatter.result({
  "duration": 8858389593,
  "status": "passed"
});
formatter.match({
  "location": "CheckOutPageSteps.getQuantityAndPricesForBusinessLicense()"
});
formatter.result({
  "duration": 15510094471,
  "status": "passed"
});
formatter.match({
  "location": "CheckOutPageSteps.place_order()"
});
formatter.result({
  "duration": 41170181609,
  "status": "passed"
});
formatter.match({
  "location": "ConfirmationPageSteps.verifyQuotePermission()"
});
formatter.result({
  "duration": 1120107223,
  "status": "passed"
});
});