#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Premium License paypal - Estonia

  Background: 
    Given User access to Estonia store
    And I Close Cookie 
    
   @TCIDSanity @TCIDEstonia @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Estonia
    When I Click on Premium License Button
    Then I Click on Next button
	And I enter Estonia data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDEstonia @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Estonia
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Estonia data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	    
   @TCIDSanity @TCIDEstoniaVAT @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Estonia using VAT
    When I Click on Premium License Button
    Then I Click on Next button
    And I enter Estonia VAT
	And I enter Estonia data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDEstoniaVAT @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Estonia using VAT
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter Estonia VAT
	And I enter Estonia data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement