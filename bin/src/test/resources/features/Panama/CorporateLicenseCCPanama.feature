#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate License CC - Panama

  Background: 
    Given User access to Panama store
    And I Close Cookie 
    
    @TCIDSanity @TCIDPanama @TCIDCorporateLicenseCC @TCIDCorporateLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Panama
    When I Click on Corporate License Button US Region
    Then I Click on Next button
	And I enter Panama data
	And I select the State
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	    @TCIDSanity @TCIDPanama @TCIDCorporateLicenseCC @TCIDCorporateLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Panama
    When I Click on Corporate License Button US Region
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Panama data
	And I select the State
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement