#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate Licence Invoice - Panama

  Background: 
    Given User access to Panama store
    And I Close Cookie 
    
     @TCIDSanity @TCIDPanama @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Panama
    When I Click on Corporate License Button US Region
    Then I Click on Next button
	And I enter Panama data
	And I select the State
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	  @TCIDSanity @TCIDPanama @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Panama
    When I Click on Corporate License Button US Region
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Panama data
	And I select the State
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement