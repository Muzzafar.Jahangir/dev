#Created by Muhammad Naveed Khan and Naqash Zafar on April 24, 2019
Feature: Business License CC - Poland

  Background: 
    Given User access to Poland store
    And I Close Cookie 
    
   @TCIDSanity @TCIDPoland @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Poland
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Poland data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	  @TCIDSanity @TCIDPoland @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Poland
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Poland data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	  @TCIDPolandVAT @TCIDSanity @TCIDPoland @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Poland using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Poland VAT
	And I enter Poland data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	  @TCIDPolandVAT @TCIDSanity @TCIDPoland @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Poland using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Poland VAT
	And I enter Poland data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	

 	
 	