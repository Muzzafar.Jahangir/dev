#Created by Muhammad Naveed Khan and Naqash Zafar on April 23, 2019
Feature: Business License Invoice - Bulgaria

  Background: 
    Given User access to Bulgaria store
    And I Close Cookie 
    
    @TCID0007 @TCIDSanity @TCIDBulgaria @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Bulgaria
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Bulgaria data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 
 
  @TCIDSanity @TCIDBulgaria @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Bulgaria
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Bulgaria data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	 @TCIDSanity @TCIDBulgaria @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Bulgaria using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Bulgaria VAT
	And I enter Bulgaria data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 
 
  @TCIDSanity @TCIDBulgariaVAT @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Bulgaria using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Bulgaria VAT
	And I enter Bulgaria data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement