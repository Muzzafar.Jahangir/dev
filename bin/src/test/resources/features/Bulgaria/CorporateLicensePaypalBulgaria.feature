#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate Licence paypal - Bulgaria

  Background: 
    Given User access to Bulgaria store
    And I Close Cookie 
    
     @TCIDSanity @TCIDBulgaria @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Bulgaria
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Bulgaria data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	  @TCIDSanity @TCIDBulgaria @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Bulgaria
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Bulgaria data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	     @TCIDSanity @TCIDBulgariaVAT @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Bulgaria using VAT
    When I Click on Corporate License Button
    Then I Click on Next button
    And I enter Bulgaria VAT
	And I enter Bulgaria data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	  @TCIDSanity @TCIDBulgariaVAT @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Bulgaria using VAT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter Bulgaria VAT
	And I enter Bulgaria data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement