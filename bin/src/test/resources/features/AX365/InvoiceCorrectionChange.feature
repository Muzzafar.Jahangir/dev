#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Switch Business License To Corporate License In ERP

  Background: 
    Given User access to D365
     
    
  @TCIDSwitchBtoC @TCIDSanity
  Scenario: Verify Business License is successfully switched to Corporate License in Dynamics 365
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Business license order in ERP
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I correct the invoice and changing the invoice address and billing address
 	
 	
 	