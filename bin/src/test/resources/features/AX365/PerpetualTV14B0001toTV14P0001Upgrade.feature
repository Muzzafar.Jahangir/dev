#Created by Muhammad Naveed Khan and Naqash Zafar on May 30 , 2019
Feature: Verify Upgrade TV14B0001 To TV14P0001

  Background: 
    Given User access to D365
     
    
  @TCIDPerpTV14BtoTV14P @TCIDSanity
  Scenario: Verify Upgrade TV14B0001 To TV14P0001 in Dynamics 365
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create TV14B0001 Perpetual order in ERP
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I upgrade TV14B0001 To TV14P0001
	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I validate TV14B0001 To TV14P0001 upgrade
 	And I Cancel Perpetual Contract On ERP
 
 	
 	