#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Business License Lasernet Printer Validations in ERP

  Background: 
    Given User access to D365
     
    
  @TCIDDBLLaserNetPrintValidations-17245 @TCIDSanity
  Scenario: Verify Business License Lasernet Printer Validations in ERP
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Business license order in ERP
 	And I get values of tax subtotal and totals for Internal Created Order
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I open journal invoice for voucher checking and lasernet printing
 	And I verify voucher with tax and total value on lasernet preview
 	 