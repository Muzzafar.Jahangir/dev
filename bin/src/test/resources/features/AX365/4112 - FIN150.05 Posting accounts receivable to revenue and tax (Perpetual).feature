#Created by Muhammad Naveed Khan and Naqash Zafar on May 25 , 2019
Feature: Verify User can invoice from Customer Service Portal for Perpetual Order

  Background: 
    Given User access to D365
     
    
  @TCIDPer-4112 @TCIDSanity
  Scenario: Verify User can invoice from Customer Service Portal for Perpetual Order
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create TV14B0001 Perpetual order in ERP
 	And I complete order in ERP
 	And I Switch To Customer Service Portal
 	And I confirm sales order and invoice in ERP
 	And I verify contract is invoiced 
 	
 
 	
 	