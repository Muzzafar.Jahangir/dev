#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Switch Premium License To Corporate License In ERP

  Background: 
    Given User access to D365
     
    
  @TCIDSwitchPtoC @TCIDSanity
  Scenario: Verify Premium License is successfully switched to Corporate License in Dynamics 365
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Premium license order in ERP
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I switch from Premium To Corporate
 	And I complete order in ERP
 	And I again confirm sales order and invoice in ERP for Switch Order
 	And I validate Business To Corporate switching
 	And I Cancel Switched Contract On ERP
 	
 	