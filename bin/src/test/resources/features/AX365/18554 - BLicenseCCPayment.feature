#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Business License Payment with Credit Card is successful - 18554

  Background: 
    Given User access to D365
     
    
  @TCIDPaymentwithCC-18554 @TCIDSanity
  Scenario: Verify Business License Payment with Credit Card is successful - 18554
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Business license order in ERP
 	And I complete order in ERP for Credit Card Payment
 	And I confirm sales order and invoice the Credit Card Order in ERP
 	And I verify contract is invoiced 
 	And I open journal invoice for Credit Card PAID Status
 	
 	
 	
 	