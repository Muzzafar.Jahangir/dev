#Created by Muhammad Naveed Khan and Naqash Zafar on May 30 , 2019
Feature: Verify VAT Number applied contract correction -  8998

  Background: 
    Given User access to D365
     
    
  @TCIDVATApply-8998 @TCIDSanity
  Scenario: Verify VAT Number applied contract correction -  8998
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Business license order in ERP
 	And I get values of tax subtotal and totals for Contract Correction
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I correct Contract and Add Tax Exempt Number
 	And I confirm sales order and invoice in ERP
 	And I open journal invoice for checking the VAT number amount applied
 	
 	
 	
 	
 	 