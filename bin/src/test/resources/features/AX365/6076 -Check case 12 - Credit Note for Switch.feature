#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Cancel Contract In ERP

  Background: 
    Given User access to D365
     
    
  @CancellationOfSwitchOrder-6076 @TCIDDemoD365 @TCIDSanity @CancelContract
  Scenario: Verify switch contract is successfully cancelled in Dynamics 365
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Business license order in ERP
 	And I get values of tax subtotal and totals for Internal Created Order
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I switch from Business To Premium
 	And I again get values of tax subtotal and totals for Internal Created Order
 	And I complete order in ERP
 	And I again confirm sales order and invoice in ERP for Switch Order
 	And I validate Business To Premium switching
 	And I Cancel Switched Contract On ERP
 	And I again confirm sales order and invoice in ERP for Switch Order
 	And I open journal invoice for voucher checking of switch order
 	
 	