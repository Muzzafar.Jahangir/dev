#Created by Muhammad Naveed Khan and Naqash Zafar on May 30 , 2019
Feature: Verify Business License Correct Contract -  6002

  Background: 
    Given User access to D365
     
    
  @TCIDBLCORRECTINVOICE-6002 @TCIDSanity
  Scenario: Verify Business License with Correct Contract -  6002
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Business license order in ERP
 	And I get values of tax subtotal and totals for Contract Correction
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I correct Contract
 	And I confirm sales order and invoice in ERP
 	And I validate correct contract
 	And I open journal invoice and check lasernet printing for correct contract
 	And I verify voucher with tax and total value on lasernet preview for Correct Contract
 	
 	
 	
 	
 	 