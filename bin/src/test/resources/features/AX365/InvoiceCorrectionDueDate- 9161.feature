#Created by Muhammad Naveed Khan and Naqash Zafar on 7 July , 2019
Feature: Verify Invoice Correction for Invoice Due Date

  Background: 
    Given User access to D365
     
    
  @TCIDInvoiceCorrectionForDUEDATE-9161 @TCIDSanity
  Scenario: Verify Invoice Correction for Due Date
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Business license order in ERP
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I correct the invoice and change the due date
 	And I resend the invoice for implementing the changes
 	And I verify due date after resending the invoice
 	And I open journal invoice for voucher checking and lasernet printing
 	And  I verify due date after resending the invoice
 	
 	
 	
 	