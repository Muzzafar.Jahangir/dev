#Created by Muhammad Naveed Khan and Naqash Zafar on April, 26 2019
Feature: Remote Access License CC - Belgium FR

  Background: 
    Given User access to Belgium FR store
    And I Close Cookie
    
  @TCIDSanity @TCIDBelgium @TCIDRemoteLicense
  Scenario: Verify Checkout Process for Team Viewer for Belgium FR
 	When I Click on Remote Access Subscription Button
  	 Then I Click on Next button
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	 And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
   @TCIDBelgiumVAT @TCIDSanity @TCIDBelgium @TCIDRemoteLicense
  Scenario: Verify Checkout Process for Team Viewer for Belgium FR using VAT
 	When I Click on Remote Access Subscription Button
  	 Then I Click on Next button
  	 And I enter Belgium VAT
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	 And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	

 	
 	