#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Premium License CC - Uruguay

  Background: 
    Given User access to Uruguay store
    And I Close Cookie 
    
  @TCID09  @TCIDURUGUAYTEST1 @TCIDSanity @TCIDUruguay @TCIDPremiumLicenseCC @TCIDPremiumLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Uruguay
    When I Click on Premium License Button US Region
    Then I Click on Next button
	And I enter Uruguay data
	And I select the State
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	@TCID10   @TCIDSanity @TCIDUruguay @TCIDPremiumLicenseCC @TCIDPremiumLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Uruguay
    When I Click on Premium License Button US Region
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Uruguay data
	And I select the State
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement