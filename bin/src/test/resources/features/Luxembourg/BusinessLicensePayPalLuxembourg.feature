#Created by Muhammad Naveed Khan and Naqash Zafar on May 3, 2019
Feature: Business Licence PayPal - Lexumbourg

  Background: 
    Given User access to Luxembourg store
    And I Close Cookie
    
  @TCIDSanity  @TCIDLexumbourg @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Luxembourg
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Luxembourg data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	  @TCIDSanity  @TCIDLexumbourg @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Luxembourg
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Luxembourg data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	  @TCIDSanity  @TCIDLexumbourg @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Luxembourg using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Luxembourg VAT
	And I enter Luxembourg data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	  @TCIDSanity  @TCIDLexumbourg @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Luxembourg using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Luxembourg VAT
	And I enter Luxembourg data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	

 	
 	