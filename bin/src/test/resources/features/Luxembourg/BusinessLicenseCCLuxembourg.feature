#Created by Muhammad Naveed Khan and Naqash Zafar on May 3, 2019
Feature: Business Licence CC - Lexumbourg

  Background: 
    Given User access to Luxembourg store
    And I Close Cookie
    
    @TCIDWeekTest @TCIDSanity  @TCIDLexumbourg @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Lexumbourg
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Luxembourg data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 @TCIDSanity  @TCIDLexumbourg @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Lexumbourg
    When I Click on Business License Button
     And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Luxembourg data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	@TCIDSanity  @TCIDLexumbourg @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
 	 Scenario: Verify purchase business license of Team Viewer for Lexumbourg using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Luxembourg VAT
	And I enter Luxembourg data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
    @TCIDSanity  @TCIDLexumbourg @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Lexumbourg using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Luxembourg VAT
	And I enter Luxembourg data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	