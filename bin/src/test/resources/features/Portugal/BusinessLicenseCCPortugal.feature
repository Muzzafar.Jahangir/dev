#Created by Muhammad Naveed Khan and Naqash Zafar on April 24, 2019
Feature: Business Licence CC - Portugal

  Background: 
    Given User Access to Portugal store
    And I Close Cookie 
    
   @TCIDSanity @TCIDPortugal @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Portugal
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Portugal data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 
 
   @TCIDSanity @TCIDPortugal @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Portugal
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Portugal data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
   @TCIDSanity @TCIDPortugal @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Portugal using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Portugal VAT
	And I enter Portugal data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 
 
   @TCIDSanity @TCIDPortugal @TCIDPortugalVAT  @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Portugal using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
     And I enter Portugal VAT
	And I enter Portugal data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	