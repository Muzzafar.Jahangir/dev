#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Premium License Invoice - Portugal

  Background: 
    Given User Access to Portugal store
    And I Close Cookie 
    
 @TCIDSanity @TCIDPortugal @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Portugal
    When I Click on Premium License Button
    Then I Click on Next button
	And I enter Portugal data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	 @TCIDSanity @TCIDPortugal @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Portugal
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Portugal data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	   
 @TCIDSanity @TCIDPortugal @TCIDPortugalVAT @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Portugal using VAT
    When I Click on Premium License Button
    Then I Click on Next button
    And I enter Portugal VAT
	And I enter Portugal data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	 @TCIDSanity @TCIDPortugalVAT @TCIDPortugal @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Portugal using VAT
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter Portugal VAT
	And I enter Portugal data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement