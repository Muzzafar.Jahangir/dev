#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate License CC - Portugal

  Background: 
    Given User Access to Portugal store
    And I Close Cookie 
    
    @TCIDSanity @TCIDPortugal @TCIDCorporateLicenseCC @TCIDCorporateLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Portugal
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Portugal data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	    @TCIDSanity @TCIDPortugal @TCIDCorporateLicenseCC @TCIDCorporateLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Portugal
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Portugal data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	    @TCIDSanity @TCIDPortugalVAT @TCIDPortugal @TCIDCorporateLicenseCC @TCIDCorporateLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Portugal using VAT 
    When I Click on Corporate License Button
    Then I Click on Next button
     And I enter Portugal VAT
	And I enter Portugal data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	    @TCIDSanity @TCIDPortugalVAT @TCIDPortugal @TCIDCorporateLicenseCC @TCIDCorporateLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Portugal using VAT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
     And I enter Portugal VAT
	And I enter Portugal data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement