#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate Licence Paypal - Albania

  Background: 
    Given User access to Albania store
    And I Close Cookie 
    
     @TCIDSanity @TCIDAlbania @TCIDBusinessLicense @TCIDBusinessLicenseBasic @TCIDAlbaniaPaypal
  Scenario: Verify purchase Corporate license of Team Viewer for Albania
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Albania data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	  @TCIDSanity @TCIDAlbania @TCIDBusinessLicense @TCIDBusinessLicenseAddOns @TCIDAlbaniaPaypalTest  @TCIDAlbaniaPaypal
  Scenario: Verify purchase Corporate license of Team Viewer for Albania
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Albania data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement