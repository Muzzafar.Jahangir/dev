#Created by Muhammad Naveed Khan and Naqash Zafar on May 24, 2019
Feature: Business License PayPal - Albania

  Background: 
    Given User access to Albania store
 
    
     @TCIDSanity @TCIDAlbania @TCIDBusinessLicense @TCIDBusinessLicenseBasic @TCIDAlbaniaTest
  Scenario: Verify purchase business license of Team Viewer for Albania
    When I Click on Business License Button
      And I Close Cookie
    Then I Click on Next button
	And I enter Albania data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 
 	
 	
 	 @TCIDSanity @TCIDAlbania @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Albania
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Albania data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement