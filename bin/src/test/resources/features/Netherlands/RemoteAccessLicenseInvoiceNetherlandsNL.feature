#Created by Muhammad Naveed Khan on August 13, 2018
Feature: Remote Access License Invoice - Netherlands NL

  Background: 
    Given User access to Netherlands NL store
    And I Close Cookie
  @TCIDSanity @TCIDNetherlands @TCIDRemoteLicense
  Scenario: Verify CheckOut Process for Team Viewer for Netherlands NL
	  
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
	And I enter Netherlands data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	  @TCIDNetherlandsVAT @TCIDSanity @TCIDRemoteLicense
  Scenario: Verify CheckOut Process for Team Viewer for Netherlands NL using VAT
	  
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
    And  I enter Netherlands VAT
	And I enter Netherlands data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	

 	
 	