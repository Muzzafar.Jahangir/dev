#Created by Muhammad Naveed Khan and Naqash Zafar on April 23, 2019
Feature: Business Licence Invoice - Netherlands

  Background: 
    Given User access to Netherlands store
   
    
  @TCIDSanity  @TCIDNetherlands @TCIDBusinessLicense @TCIDBusinessLicenseBasic @TCID002
  Scenario: Verify purchase business license of Team Viewer for Netherlands
    When I Click on Business License Button
      And I Close Cookie
   	#Then I Click on Next button
   	Then I Refresh Browser
   	Then I Click on Next button
	And I enter Netherlands data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	  @TCIDSanity  @TCIDNetherlands @TCIDBusinessLicense @TCIDBusinessLicenseAddOns @TCID5
  Scenario: Verify purchase business license of Team Viewer for Netherlands
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Netherlands data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	

 	  @TCIDSanity @TCIDNetherlandsVAT  @TCIDNetherlands @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Netherlands using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And  I enter Netherlands VAT
	And I enter Netherlands data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	  @TCIDSanity @TCIDNetherlandsVAT  @TCIDNetherlands @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Netherlands using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And  I enter Netherlands VAT
	And I enter Netherlands data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	