#Created by Muhammad Naveed Khan and Naqash Zafar on April, 26 2019
Feature: Remote Access License CC - Netherlands

  Background: 
    Given User access to Netherlands store
    And I Close Cookie
    
  @TCIDSanity @TCIDNetherlands @TCIDRemoteLicense
  Scenario: Verify CheckOut Process for Team Viewer for Netherlands EN
	  
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
	And I enter Netherlands data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	 And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	 @TCIDNetherlandsVAT @TCIDSanity @TCIDRemoteLicense
  Scenario: Verify CheckOut Process for Team Viewer for Netherlands EN using VAT
	  
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
    And  I enter Netherlands VAT
	And I enter Netherlands data
	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	 And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	

 	
 	