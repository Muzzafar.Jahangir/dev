#Created by Muhammad Naveed Khan and Naqash Zafar on April 24, 2019
Feature: Business Licence CC - Netherlands

  Background: 
    Given User access to Netherlands store
    And I Close Cookie
    
    @TCIDSanity  @TCIDNetherlands @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Netherlands
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Netherlands data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	And I Activate License
	And I Enter Credentials For Login on TV Activation Link
	#And I Login Email
	And I Access Gmail
	And I Login Gmail
	And I archive Emails
 	
 	
 @TCIDNLCCADOns @TCIDSanity  @TCIDNetherlands @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Netherlands 
    When I Click on Business License Button
     And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Netherlands data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	    @TCIDSanity  @TCIDNetherlands @TCIDNetherlandsVAT @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Netherlands using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And  I enter Netherlands VAT
	And I enter Netherlands data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 @TCIDSanity  @TCIDNetherlands @TCIDNetherlandsVAT @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Netherlands using VAT
    When I Click on Business License Button
     And I add All the Business License AddOns
    Then I Click on Next button
    And  I enter Netherlands VAT
	And I enter Netherlands data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	