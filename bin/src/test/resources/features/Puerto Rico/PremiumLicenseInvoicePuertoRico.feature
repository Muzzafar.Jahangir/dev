#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Premium License Invoice - Puerto Rico

  Background: 
    Given User access to Puerto Rico store
    And I Close Cookie 
    
 @TCIDSanity @TCIDPuertoRico @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Puerto Rico
    When I Click on Premium License Button US Region
    Then I Click on Next button
	And I enter Puerto Rico data
	And I select the State
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	 @TCIDSanity @TCIDPuertoRico @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Puerto Rico
    When I Click on Premium License Button US Region
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Puerto Rico data
	And I select the State
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement