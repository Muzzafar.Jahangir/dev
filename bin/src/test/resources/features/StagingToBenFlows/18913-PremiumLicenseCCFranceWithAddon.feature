#Created by Muhammad Naveed Khan and Naqash Zafar on Aug 05, 2019
Feature: Premium License CC - France

  Background: 
    Given User access to France store for Staging
 	
 	@PremiumLicenseFlowForERPFrance-18913
  Scenario: Verify Order Checkout Process For Premium License With Addson via Credit Card
   	When I Click on Premium License Button
    And I Close Cookie
    Then I Refresh Browser
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter France data
	And I get quantity and Prices of Products
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	And I Get Order Id and Payment Method From Success Page
 	And I Switch To ERP for Order Verification
 	When I Login ERP
 	And I Search Channel Reference ID in ERP
 	And I open the sales order and verify products purchased in ERP
 	And I verify quantity of purchased products in ERP
 	And I verify amount of purchased products in ERP
 	And I verify customer information on header bar in ERP
 	And I verify payment status on header bar
 	And I verify sales order is completed
 	
 	
 	
 	
 