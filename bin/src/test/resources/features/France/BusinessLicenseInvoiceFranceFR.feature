#Created by Muhammad Naveed Khan and Naqash Zafar on May 2, 2019
Feature: Business Licence Invoice - France FR

  Background: 
    Given User access to France FR store
    And I Close Cookie
    
  @TCIDSanity  @TCIDFrance @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for France FR
    When I Click on Business License Button
    Then I Click on Next button
	And I enter France data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 
 @TCIDSanity  @TCIDFrance @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for France FR
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter France data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	

 	  @TCIDSanity  @TCIDFranceVAT @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for France FR using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter France VAT
	And I enter France data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 
 @TCIDSanity  @TCIDFranceVAT @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for France FR using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter France VAT
	And I enter France data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	