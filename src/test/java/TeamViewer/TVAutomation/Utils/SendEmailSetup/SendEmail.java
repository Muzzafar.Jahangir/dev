package TeamViewer.TVAutomation.Utils.SendEmailSetup;

import java.io.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


import TeamViewer.TVAutomation.DataProvider.TestData;
import TeamViewer.TVAutomation.Pages.AdOnPage;
import TeamViewer.TVAutomation.Pages.HomePage;
import TeamViewer.TVAutomation.Pages.SystemEmailLogin;
import TeamViewer.TVAutomation.Utils.PropertyReader;

public class SendEmail {

/*	public static void main(String[] args) throws IOException, MessagingException {
		sendEmailBody(TestData.Email.EMAIL_RESULT_SUBJECT, "cucumber-json-report-SanityForTeamViewer");
	}*/

	public static void sendEmailBody(String subject, String jsonFileName) throws MessagingException, IOException {

		PropertyReader propertyReader = new PropertyReader();
		String emailSendFrom = propertyReader.readPropertyConfigEmail(TestData.Email.EMAIL_FROM);
		String emailSendTo = propertyReader.readPropertyConfigEmail(TestData.Email.EMAIL_TO);
		String emailSendCC = propertyReader.readPropertyConfigEmail(TestData.Email.EMAIL_CC);
		String smtpHostServer = propertyReader.readPropertyConfigEmail(TestData.Email.EMAIL_SMTP_HOST_SERVER);
		String smtpMailServer = propertyReader.readPropertyConfigEmail(TestData.Email.EMAIL_SMTP_MAIL_SERVER);
		
	 
		Properties props = System.getProperties();


		props.put("mail.smtp.starttls.enable", "true");
	//	props.put("mail.smtp.ssl.enable", "true");

		props.put("mail.smtp.host", "173.194.66.16");
		
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");

		props.put("mail.smtp.user", "teamviewer.automation@gmail.com");

		props.put("mail.smtp.password","abcd@1234");
		
		props.put("mail.smtp.socketFactory.port","465");
		
		props.put("mail.smtp.port", "465");

		props.put("mail.smtp.auth", "true");

		Session session = Session.getDefaultInstance(props);

		MimeMessage message = new MimeMessage(session);

		message.setFrom(new InternetAddress(TestData.Email.EMAIL_FROM));
		message.setReplyTo(InternetAddress.parse(emailSendFrom, false));
		
		
		BodyPart messageBodyPart = new MimeBodyPart();
//		messageBodyPart.setText("Hi,"+"\n" +"\n"+"Please find attached, test automation report executed against Order No: "+ SystemEmailLogin.finalOrderID +"\n"+"\n"+"Best regards,"+"\n"+"Visionet Test Automation Team");
		messageBodyPart.setText("Hi,"+"\n" +"\n"+"Please find attached, test automation report."+"\n"+"\n"+"Best regards,"+"\n"+"Visionet Test Automation Team");
		
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		messageBodyPart = new MimeBodyPart();

		String filename = "target" + File.separator + "extent-reports" + File.separator + "TeamViewerAutomationTestExecutionReport.html";
                  
		DataSource source = new FileDataSource(filename);
         	messageBodyPart.setDataHandler(new DataHandler(source));
         	messageBodyPart.setFileName("TVAutomationReport.html");
         	multipart.addBodyPart(messageBodyPart);


		//Old Subject
//		subject = subject + " | " + browser.toUpperCase() + " | "
//				+ new SimpleDateFormat("MMM dd, yyyy hh:mm a").format(new Date());
		
		
		//New Subject
			subject = subject + "-" + AdOnPage.licenseName +"-"+HomePage.countryName+"-"+ new SimpleDateFormat("MMM dd, yyyy hh:mm a").format(new Date());
		
		message.setSubject(subject, "UTF-8");
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailSendTo, false));
		message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(emailSendCC));
		
		//message.setContent(CompileHTML.resultSet(jsonFileName), "text/html");
		message.setContent(multipart);

		Transport transport = session.getTransport("smtp");
		try{
		transport.connect("teamviewer.automation@gmail.com","abcd@1234");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	
		transport.sendMessage(message,message.getAllRecipients());

	
		//transport.close();
		
	}
}





//package TeamViewer.TVAutomation.Utils.SendEmailSetup;
//
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Properties;
//
//import javax.activation.DataHandler;
//import javax.activation.DataSource;
//import javax.activation.FileDataSource;
//import javax.mail.BodyPart;
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.Multipart;
//import javax.mail.Session;
//import javax.mail.Transport;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeBodyPart;
//import javax.mail.internet.MimeMessage;
//import javax.mail.internet.MimeMultipart;
//
//import org.apache.commons.mail.EmailAttachment;
//
//import TeamViewer.TVAutomation.DataProvider.TestData;
//import TeamViewer.TVAutomation.Pages.AdOnPage;
//import TeamViewer.TVAutomation.Pages.HomePage;
//import TeamViewer.TVAutomation.Pages.SystemEmailLogin;
//import TeamViewer.TVAutomation.Utils.PropertyReader;
//
//public class SendEmail {
//
///*	public static void main(String[] args) throws IOException, MessagingException {
//		sendEmailBody(TestData.Email.EMAIL_RESULT_SUBJECT, "cucumber-json-report-SanityForTeamViewer");
//	}*/
//
//	public static void sendEmailBody(String subject, String jsonFileName) throws MessagingException, IOException {
//
//		PropertyReader propertyReader = new PropertyReader();
//		String emailSendFrom = propertyReader.readPropertyConfigEmail(TestData.Email.EMAIL_FROM);
//		String emailSendTo = propertyReader.readPropertyConfigEmail(TestData.Email.EMAIL_TO);
//		String emailSendCC = propertyReader.readPropertyConfigEmail(TestData.Email.EMAIL_CC);
//		String smtpHostServer = propertyReader.readPropertyConfigEmail(TestData.Email.EMAIL_SMTP_HOST_SERVER);
//		String smtpMailServer = propertyReader.readPropertyConfigEmail(TestData.Email.EMAIL_SMTP_MAIL_SERVER);
//		
//	 
//		Properties props = System.getProperties();
//		props.put(smtpMailServer, smtpHostServer);
//		Session session = Session.getInstance(props, null);
//
//		MimeMessage message = new MimeMessage(session);
//
//		message.setFrom(new InternetAddress(emailSendFrom, TestData.Email.EMAIL_SEND_FROM_LABEL));
//		message.setReplyTo(InternetAddress.parse(emailSendFrom, false));
//		
//		
//		BodyPart messageBodyPart = new MimeBodyPart();
//		messageBodyPart.setText("Hi,"+"\n" +"\n"+"Please find attached, test automation report executed against Order No: "+ SystemEmailLogin.finalOrderID +"\n"+"\n"+"Best regards,"+"\n"+"Visionet Test Automation Team");
//		
//		Multipart multipart = new MimeMultipart();
//		multipart.addBodyPart(messageBodyPart);
//		 messageBodyPart = new MimeBodyPart();
//		 String filename = "target\\extent-reports\\TeamViewerAutomationTestExecutionReport.html";
//         DataSource source = new FileDataSource(filename);
//         messageBodyPart.setDataHandler(new DataHandler(source));
//         messageBodyPart.setFileName("TVAutomationReport.html");
//         multipart.addBodyPart(messageBodyPart);
//
//		//Old Subject
////		subject = subject + " | " + browser.toUpperCase() + " | "
////				+ new SimpleDateFormat("MMM dd, yyyy hh:mm a").format(new Date());
//		
//		
//		//New Subject
//			subject = subject + "-" + AdOnPage.licenseName +"-"+HomePage.countryName+"-"+ new SimpleDateFormat("MMM dd, yyyy hh:mm a").format(new Date());
//		
//		message.setSubject(subject, "UTF-8");
//		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailSendTo, false));
//		message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(emailSendCC));
//		
//		//message.setContent(CompileHTML.resultSet(jsonFileName), "text/html");
//		message.setContent(multipart);
//
//		Transport.send(message);
//		
//	}
//}
