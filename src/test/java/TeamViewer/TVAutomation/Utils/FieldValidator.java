package TeamViewer.TVAutomation.Utils;

import org.openqa.selenium.WebElement;

public class FieldValidator {
	private WebElement webElement;
	private String key;
	private String value;

	public FieldValidator() {

	}

	public FieldValidator(WebElement webElement, String key, String value) {
		this.webElement = webElement;
		this.key = key;
		this.value = value;
	}

	public WebElement getWebElement() {
		return webElement;
	}

	public void setWebElement(WebElement webElement) {
		this.webElement = webElement;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
