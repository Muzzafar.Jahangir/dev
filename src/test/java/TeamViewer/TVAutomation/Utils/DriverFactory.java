package TeamViewer.TVAutomation.Utils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;


public class DriverFactory {
	
	
	protected static WebDriver driver;
	public static Path download_folder;

	public DriverFactory() {
		initialize();
		//initializeLogging();
	}

	public void initialize() {
		if (driver == null)
			if (new PropertyReader().readProperty("runAt").equals("local")) {
				createNewLocalDriverInstance();
			} else if (new PropertyReader().readProperty("runAt").equals("remote")) {
				createNewRemoteDriverInstance();
			}
	}

	private void createNewLocalDriverInstance() {
		String browser = new PropertyReader().readProperty("browser");
		/*if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
            driver = new ChromeDriver();
*/


//       ChromeOptions chromeOptions = new ChromeOptions();
//       chromeOptions.addArguments("--headless");
//      chromeOptions.addArguments("--no-sandbox");
//
//       WebDriver driver = new ChromeDriver(chromeOptions);


		if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", "chromedriver");
           // download_folder = Paths.get(System.getProperty("user.home") + "/Downloads");
           download_folder = Paths.get("/usr/bin/chromedriver");
            
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("profile.default_content_setting_values.notifications", 2);
            prefs.put("download.default_directory", download_folder.toAbsolutePath());
            prefs.put("download.prompt_for_download", false);
            prefs.put("download.directory_upgrade", true);
            prefs.put("plugins.plugins_disabled", new String[]{"Chrome PDF Viewer"});

            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            options.addArguments("disable-infobars");
            options.addArguments("--headless");
	        options.addArguments("--no-sandbox");

            options.setExperimentalOption("prefs", prefs);
            HashMap<String, Object> options1 = new HashMap<String, Object>();
            options1.put("prefs", prefs);
            driver = new ChromeDriver(options);
             
		} else if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
			driver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			capabilities.setCapability("requireWindowFocus", true);
			driver = new InternetExplorerDriver();
		} else {
			throw new IllegalArgumentException("The Browser Type is Undefined");
		}
	}

	private void createNewRemoteDriverInstance() {
        String browser = new PropertyReader().readProperty("browser");
        if (browser.equalsIgnoreCase("chrome")) {
            ChromeOptions options = new ChromeOptions();

            String browserExePath = "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe";//new PropertyReader().readProperty("browserExePath");
            String driverExePath = "C:/chrome_driver/chromedriver.exe";//new PropertyReader().readProperty("driverExePath");

            options.setBinary(browserExePath);
            System.setProperty("webdriver.chrome.driver", driverExePath);
//            driver = new ChromeDriver(options);
 
            Map<String, Object> prefs = new HashMap<>();
            prefs.put("profile.default_content_setting_values.notifications", 2);
 
            options.setExperimentalOption("prefs", prefs);
            driver = new ChromeDriver(options);

        } else if (browser.equalsIgnoreCase("firefox")) {
            System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
            driver = new FirefoxDriver();
        } else if (browser.equalsIgnoreCase("ie")) {
            System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
            DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
            capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            capabilities.setCapability("requireWindowFocus", true);
            driver = new InternetExplorerDriver();
        } else {
            throw new IllegalArgumentException("The Browser Type is Undefined");
        }
	}

	public WebDriver getDriver() {
		return driver;
	}
	
	
	public static void quitDriver() {
		driver.quit();
		driver = null;
	}
}
