package TeamViewer.TVAutomation.Utils;

import static TeamViewer.TVAutomation.Utils.DataPool.readExcelData;
import static org.junit.Assert.*;

import java.awt.AWTException;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.mail.Authenticator;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

import TeamViewer.TVAutomation.DataProvider.TestData;

public class UtilityMethods extends DriverFactory {

	private static final Wait<WebDriver> wait = new WebDriverWait(driver, 15);
	public static String parentWindowHandler;

	public static void Close_1TabOfBrowser() throws InterruptedException {
		waitForPageLoadAndPageReady();
		waitForPageLoadAndPageReady();
		driver.close();
	}
	
	public static void CloseCurrentTabAndOpenNewTab(String url) throws InterruptedException{
		
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		driver.switchTo().window(tabs.get(1));
		driver.navigate().to(url);

	//	driver.switchTo().window(tabs.get(0)).close();
		
		
	}

	public static void RefreshBrowser() throws InterruptedException {

		waitForPageLoadAndPageReady();
		driver.navigate().refresh();
		waitForPageLoadAndPageReady();
	}

	public static void Clear_Cache() {
		driver.manage().deleteAllCookies();
	}

	public static void pageDown() throws AWTException {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 250);");
	}

	public static void pageUp() throws AWTException {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, -250);");
	}

	public static void hoverElementAndClick(WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element);
		action.click().build().perform();
	}

	public static String selectByVisibleTextAndReturnSelectedOption(String locator, String text)
			throws InterruptedException {
		Select select = new Select(driver.findElement(By.cssSelector(locator)));
		select.selectByVisibleText(text);
		return select.getFirstSelectedOption().getText();
	}

	public static String getSelectedOption(WebElement element) throws InterruptedException {
		WebElement selectedOption = new Select(element).getFirstSelectedOption();
		return selectedOption.getText();
	}

	public static void scrollToWebElement(WebElement scrollTo) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", scrollTo);
	}

	public static void clickLinkSidebar(WebElement sidebarLink) throws Exception {
		sidebarLink.click();
		waitForPageLoad();
	}

	public static String getTimeStampMMddHHmmss() {
		return new SimpleDateFormat("MMddHHmmss").format(new Date());
	}

	public static String getTimeStampddMMYYYY() {
		return new SimpleDateFormat("ddMMYYYY").format(new Date());
	}

	public static String getTimeStampYYYYHHmmss() {
		return new SimpleDateFormat("YYYYHHmmss").format(new Date());
	}

	public static String getDateWithFormatmmddyyyy() {
		return new SimpleDateFormat("MM/dd/yyyy").format(new Date());
	}

	public static void waitForPageLoad() {

		Wait<WebDriver> wait = new WebDriverWait(driver, 300);
		wait.until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver driver) {
				((JavascriptExecutor) driver).executeScript("return document.readyState");
				return String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
						.equals("complete");
			}
		});
	}

	public static void waitForClickable(WebElement element) {
		new WebDriverWait(driver, 50).until(ExpectedConditions.elementToBeClickable(element));
	}

	public static WebElement waitForElementToBeClickable(WebElement element) {
		return new WebDriverWait(driver, 100).until(ExpectedConditions.elementToBeClickable(element));
	}

	public static void waitForVisibility(WebElement element) {
		new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(element));
	}

	public static void waitForElementVisibility(WebElement element) {
		new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(element));
	}

	public static void clickOkAtPopUpAlert() {
		Alert alert = driver.switchTo().alert();
		//System.out.println("alert.getText() = " + alert.getText());

		alert.accept();
		checkPageIsReady();
	}

	public static void checkPageIsReady() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String s = "";
		while (!s.equals("complete")) {
			s = (String) js.executeScript("return document.readyState");
			try {
				wait1Seconds();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void switchToParentWindow(String parentWindowHandler) {
		driver.switchTo().window(parentWindowHandler);
	}

	public static String switchToSubWindowAndReturnParentWindowHandler() {

		String parentWindowHandler = driver.getWindowHandle();
		String subWindowHandler = null;

		Set<String> handles = driver.getWindowHandles();

		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler);
		return parentWindowHandler;
	}

	public static void refreshPage() {
		driver.navigate().refresh();
		waitForPageLoadAndPageReady();
	}

	public static void moveToMainMenuAndClickSubMenuItem(WebElement mainMenu, WebElement subMenu)
			throws InterruptedException {
		UtilityMethods.waitForClickable(mainMenu);
		Actions action = new Actions(driver);
		waitForEnable1Sec(mainMenu);
		action.moveToElement(mainMenu).build().perform();
		UtilityMethods.waitForVisibility(subMenu);
		action.moveToElement(subMenu).click().perform();
	}

	public static void moveToMainMenuandsubMenu(WebElement mainMenu, WebElement subMenu) throws InterruptedException {
		UtilityMethods.waitForVisibility(mainMenu);
		Actions action = new Actions(driver);
		waitForEnable1Sec(mainMenu);
		action.moveToElement(mainMenu).build().perform();
		waitForEnable1Sec(subMenu);
		action.moveToElement(subMenu).build().perform();
	}

	public static void waitForElementPresentInSec(List<WebElement> list, int waitForSeconds)
			throws InterruptedException {
		for (int second = 0;; second++) {
			if (second >= waitForSeconds)
				fail("timeout");
			try {
				if (!list.isEmpty())
					break;
			} catch (NoSuchElementException e) {
			}
			wait1Seconds();
		}
	}

	public static WebElement waitForElementToBePresent(WebElement element) throws InterruptedException {
		new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(element));
		return element;
	}

	public static void moveToMenuAndClick(WebElement menu) throws InterruptedException {
		Actions action = new Actions(driver);

		UtilityMethods.waitForVisibility(menu);

		waitForEnable1Sec(menu);
		action.moveToElement(menu);
		menu.click();
	}

	public static String dateGeneration() {
		Date dNow = new Date();
		SimpleDateFormat nameFormat = new SimpleDateFormat("ddMMyyyyhhmmss");
		return nameFormat.format(dNow);

	}

	public static WebElement fluientWaitforElement(WebElement element, int timoutSec, int pollingSec) {
		FluentWait<WebDriver> fWait = new FluentWait<WebDriver>(driver).withTimeout(timoutSec, TimeUnit.SECONDS)
				.pollingEvery(pollingSec, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class, TimeoutException.class)
				.ignoring(StaleElementReferenceException.class);
		for (int i = 0; i < 2; i++) {
			try {

				fWait.until(ExpectedConditions.visibilityOf(element));
				fWait.until(ExpectedConditions.elementToBeClickable(element));
			} catch (Exception e) {
				//System.out.println("Element Not found trying again - " + element.toString().substring(70));
				e.printStackTrace();
			}
		}
		return element;
	}

	public static void dragDrop(WebElement dragElement, WebElement dropLocation) {
		Actions action = new Actions(driver);
		action.dragAndDrop(dragElement, dropLocation).perform();

	}

	public static void moveToElement(WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();

	}

	public static String waitForTextToBePresentInElementAndReturnText(WebElement element, String text)
			throws InterruptedException {
		new WebDriverWait(driver, 10).until(ExpectedConditions.textToBePresentInElement(element, text));
		return text;
	}

	public static void waitForTextToBePresentInElement(WebElement element, String text) throws InterruptedException {
		new WebDriverWait(driver, 20)

				.until(ExpectedConditions.textToBePresentInElement(element, text));
	}

	public static void waitForInvisibilityOfElementWithText(String locator, String text) throws InterruptedException {
		new WebDriverWait(driver, 10)
				.until(ExpectedConditions.invisibilityOfElementWithText(By.cssSelector(locator), text));
	}

	public static boolean waitForInvisibilityOfElementWithTextAndReturn(String locator, String text)
			throws InterruptedException {
		return new WebDriverWait(driver, 10)
				.until(ExpectedConditions.invisibilityOfElementWithText(By.cssSelector(locator), text));
	}

	public static void selectFromDropDown(WebElement dropdown, String optionsCssPath, String textToSelect)
			throws InterruptedException, AWTException {
		dropdown.click();
		List<WebElement> options = driver.findElements(By.cssSelector(optionsCssPath));
		wait1Seconds();
		for (WebElement option : options) {
			if (option.getText().equals(textToSelect)) {
				option.click();
				break;
			}
		}
	}

	public static String getDateWithFormatMMMddyyyy() {
		return new SimpleDateFormat("MMM dd, yyyy").format(new Date());
	}

	public static void waitForEnable1Sec(WebElement element) throws InterruptedException {
		do {
			wait1Seconds();
		} while (element.isEnabled() == false);
	}

	public static void waitForListEnable2Sec(List<?> list) throws InterruptedException {

		do {
			waitForPageLoadAndPageReady();

		} while (list.size() < 0);
	}

	public static boolean verifyElementNotPresent(WebElement element) throws Exception {
		try {
			element.isDisplayed();
			//System.out.println("Element Present");
			return false;

		} catch (NoSuchElementException e) {
			//System.out.println("Element absent");
			return true;
		}
	}

	public static void invisibilityOfElement(String element) {
		new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(element)));
	}

	public static void waitForElementPresentInSec(WebElement element, int waitForSeconds) throws InterruptedException {
		for (int second = 0;; second++) {
			if (second >= waitForSeconds)
				fail("timeout");
			try {
				if (element.isDisplayed())
					break;
			} catch (NoSuchElementException e) {
			}
			wait1Seconds();
		}
	}

	public static boolean ifElementPresentInSec(WebElement element, int waitForSeconds) throws InterruptedException {
		boolean found = false;
		for (int second = 0;; second++) {
			if (second >= waitForSeconds)
				fail("timeout");
			try {
				if (element.isDisplayed()) {
					found = true;
				}
				break;
			} catch (NoSuchElementException e) {
				found = false;
			}
			wait1Seconds();
		}
		return found;
	}

	public static boolean ifElementPresentInSeconds(By by, int waitForSeconds) throws InterruptedException {
		boolean found = false;
		for (int second = 0;; second++) {
			if (second >= waitForSeconds)
				fail("timeout");

			if (driver.findElement(by).isDisplayed()) {
				found = true;
				break;
			}

			wait1Seconds();
		}
		return found;
	}

	public static boolean ifElementPresent(WebElement element) {
		boolean found;
		try {
			element.isDisplayed();
			found = true;

		} catch (NoSuchElementException e) {
			found = false;
		}
		return found;
	}

	public static boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public static void scrollToElementAndClick(WebElement element) throws Exception {
		waitForPageLoadAndPageReady();
		UtilityMethods.wait1Seconds();
		Actions actions = new Actions(driver);
		actions.moveToElement(element);
		actions.perform();
		element.click();
	}

	public static void scrollToElement(WebElement element) throws Exception {
		waitForPageLoadAndPageReady();
		UtilityMethods.wait1Seconds();
		Actions actions = new Actions(driver);
		actions.moveToElement(element);
		actions.perform();
	}

	public static void waitForPageLoadAndPageReady() {
		waitForPageLoad();
		checkPageIsReady();
	}

	public static void clickOkatAlertIfPresent() {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			//System.out.println("alert was present");
			clickOkAtPopUpAlert();
		} catch (TimeoutException eTO) {
			//System.out.println("alert was not present");
		}
	}

	public static String getTodaysDateMMddyyyy() {
		return new SimpleDateFormat("MM/dd/yyyy").format(new Date(System.currentTimeMillis()));
	}

	public static String getTodaysDateMMMddyyyy() {
		return new SimpleDateFormat("MMM dd, yyyy").format(new Date(System.currentTimeMillis()));
	}

	public static void waitForTextPresentInElement(String textToVerify, WebElement element, int waitForSeconds)
			throws InterruptedException {
		for (int second = 0;; second++) {
			if (second >= waitForSeconds)
				fail("timeout");
			try {
				if (textToVerify.equals(element.getText()))
					break;
			} catch (Exception e) {
			}
			wait1Seconds();
		}
	}

	public static void waitForElementEnabled(WebElement element, int waitForSeconds) throws InterruptedException {
		for (int second = 0;; second++) {
			if (second >= waitForSeconds)
				fail("timeout");
			try {
				if (element.isEnabled())
					break;
			} catch (Exception e) {
			}
			wait1Seconds();
		}
	}

	public static void waitForElementDisplayed(WebElement element, int waitForSeconds) throws InterruptedException {
		for (int second = 0;; second++) {
			if (second >= waitForSeconds)
				fail("timeout");
			try {
				if (element.isDisplayed())
					break;
			} catch (Exception e) {
			}
			wait1Seconds();
		}
	}

	public static String convertDateFormatToString(String date, String input_pattern, String output_pattern)
			throws ParseException {
		return new SimpleDateFormat(output_pattern).format(new SimpleDateFormat(input_pattern).parse(date));
	}

	public static String getTextJavaScript(WebElement element) {
		org.openqa.selenium.JavascriptExecutor js = (org.openqa.selenium.JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		return (String) js.executeScript("return $(arguments[0]).text();", element);
	}

	public static String getTextJavaScriptInnerHTML(WebElement element) {
		org.openqa.selenium.JavascriptExecutor js = (org.openqa.selenium.JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		return (String) js.executeScript("return $(arguments[0]).innerHTML;", element);
	}

	public static String getInnerText(WebElement element) {
		return element.getAttribute("innerText");
	}

	public static String getTextContent(WebElement element) {
		return element.getAttribute("textContent");
	}

	public static void waitTillSomeTextAppearInElementUsingAttribute(final WebElement element, int timeOutInSeconds) {
		(new WebDriverWait(driver, timeOutInSeconds)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return element.getAttribute("value").length() != 0;
			}
		});
	}

	public static void waitTillSomeTextAppearInElementUsingText(final WebElement element, int timeOutInSeconds) {
		(new WebDriverWait(driver, timeOutInSeconds)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return element.getText().length() != 0;
			}
		});
	}

	public static void waitTillTextAppearInElementUsingPlaceholderAttribute(final WebElement element,
			int timeOutInSeconds, String text) {
		final String textToAppearInValue = text;
		(new WebDriverWait(driver, timeOutInSeconds)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return element.getAttribute("placeholder").equals(textToAppearInValue);
			}
		});
	}

	public static void waitTillTextAppearInElementUsingAttribute(final WebElement element, int timeOutInSeconds,
			String text) {
		final String textToAppearInValue = text;
		(new WebDriverWait(driver, timeOutInSeconds)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				//System.out.println("text: " + textToAppearInValue);
				return element.getAttribute("value").equals(textToAppearInValue);
			}
		});
	}

	public static String getDateWithFormatMMMdyyyy() {
		return new SimpleDateFormat("MMM d, yyyy").format(new Date());
	}

	public static boolean patternMatches_10_Digits(String s) {
		return s.matches("\\d{10}");
	}

	public static void waitForElementToDisappear(By by, int timeinSec) throws InterruptedException {

		for (int i = 0; i <= timeinSec; ++i) {
			if (driver.findElements(by).isEmpty()) {
				break;
			} else {
				UtilityMethods.wait1Seconds();
			}
			if (i == timeinSec)
				fail("element not disappeared within " + timeinSec + " seconds");
		}
	}

	public static void waitForNotifyPopupToDisappear_IFExists() throws InterruptedException {
		if (isElementPresent(By.cssSelector("span.noty_text"))) {
			waitForElementToDisappear(By.cssSelector("span.noty_text"), 10);
		}
	}

	public static void waitForInvisibilityOfElement(String locator) throws InterruptedException {
		new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(locator)));
	}

	public static void waitForVisibilityOfNotifyPopup() throws InterruptedException {
		for (int second = 0;; second++) {
			if (second >= 10)
				fail("timeout");
			try {
				if (isElementPresent(By.cssSelector("span.noty_text")))
					break;
			} catch (Exception e) {
			}
			wait1Seconds();
		}
	}

	public static void waitForInvisibilityOfNotifyPopup() throws InterruptedException {
		for (int second = 0;; second++) {
			if (second >= 10)
				fail("timeout");
			try {
				if (!isElementPresent(By.cssSelector("span.noty_text")))
					break;
			} catch (Exception e) {
			}
			wait1Seconds();
		}
	}

	public static void waitForVisibilityThenInvisibilityOfNotifyPopup() throws InterruptedException {
		waitForVisibilityOfNotifyPopup();
		waitForInvisibilityOfNotifyPopup();
	}

	public static String getTimeStampYYYYddHHmmss() {
		return new SimpleDateFormat("YYYYddHHmmss").format(new Date());
	}

	public static String getDateWithFormat_M_d_yy() {
		return new SimpleDateFormat("M/d/yy").format(new Date());
	}

	public static void waitForElementPresenceByCss(String element) {
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(element)));
	}

	public static void waitForElementPresenceByXpath(String element) {
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(element)));
	}

	public static boolean waitForElementNotPresentByXpath(String element) {
		return new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(element)));
	}

	public static String wait30SecTextToBePresentInElementAndReturnText(WebElement element, String text)
			throws InterruptedException {
		new WebDriverWait(driver, 30).until(ExpectedConditions.textToBePresentInElement(element, text));
		return text;
	}

	public static void wait3MinForElementInvisibility(String element) {
		new WebDriverWait(driver, 180).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(element)));
	}

	public static void scrollToSideBarLinkAndClickElement(WebElement sideBarLink) throws Exception {
		waitForPageLoadAndPageReady();
		scrollToWebElement(sideBarLink);
		clickLinkSidebar(sideBarLink);
		waitForPageLoadAndPageReady();
	}

	public static void moveToElementWithWaits(WebElement element) throws Exception {
		waitForPageLoadAndPageReady();
		waitForPageLoadAndPageReady();
		waitForVisibility(element);
		waitForClickable(element);
		UtilityMethods.ifElementPresentInSec(element, 10);
		UtilityMethods.moveToElement(element);

	}

	public static void moveToElementWithWaitsVisibility(WebElement element) throws Exception {
		waitForPageLoadAndPageReady();
		waitForPageLoadAndPageReady();
		waitForVisibility(element);
		UtilityMethods.ifElementPresentInSec(element, 10);
		UtilityMethods.moveToElement(element);
	}

	public static void moveToDisabledElementWithWaits(WebElement element) throws Exception {
		wait1Seconds();
		waitForPageLoadAndPageReady();
		waitForVisibility(element);
		UtilityMethods.ifElementPresentInSec(element, 10);
		UtilityMethods.moveToElement(element);
	}

	public static String isButtonDisabled(WebElement button) throws Exception {
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.moveToDisabledElementWithWaits(button);
		return button.getAttribute("disabled");
	}

	public static void switchToNewTab() throws InterruptedException {

		ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public static void switchToFirstTab() throws InterruptedException {

		ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(0));
		UtilityMethods.waitForPageLoadAndPageReady();
	}
	public static void switchToSecondTab() throws InterruptedException {

		ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public static void pageRefreshUntilElementVisible(By by) throws InterruptedException {
		int count = 0;
		while (!UtilityMethods.isElementPresent(by)) {
			UtilityMethods.waitForPageLoadAndPageReady();
			if (UtilityMethods.isElementPresent(by) || count > 15) {
				break;
			}

			UtilityMethods.refreshPage();
			//System.out.println(" gmail page refresh count : " + count);
			count = count + 1;
		}
	}

	public static boolean ifEmailPresentBySubject(String user, String password, String subject) {
		boolean isEmailPresent = false;
		Properties properties = new Properties();
		try {

			// server setting
			properties.put("mail.imap.host", "imap.gmail.com");
			properties.put("mail.imap.port", "993");

			// SSL setting
			properties.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			properties.setProperty("mail.imap.socketFactory.fallback", "false");
			properties.setProperty("mail.imap.socketFactory.port", String.valueOf("993"));

			Session session = Session.getInstance(properties);
			// create the POP3 store object and connect with the pop server
			Store store = session.getStore("imap");
			store.connect(user, password);

			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);

			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();
			//System.out.println("messages.length " + messages.length);
			for (int i = 0, n = messages.length; i < n; i++) {
				Message message = messages[i];
				//System.out.println("---------------------------------");
				//System.out.println("Email Number " + (i + 1));
				//System.out.println("Subject: " + message.getSubject());
				//System.out.println("From: " + message.getFrom()[0]);
				//System.out.println("Body: " + message.getContent().toString());
				isEmailPresent = subject.equalsIgnoreCase(message.getSubject()) ? true : false;
			}

			// close the store and folder objects
			emailFolder.close(false);
			store.close();

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isEmailPresent;
	}

	public static Boolean getShadesOFSelectedCOlor(WebElement element, String color) throws InterruptedException {
		Boolean result = false;
		//System.out.println("color " + element.getAttribute("text").toLowerCase());
		if (color.equalsIgnoreCase(" blue")) {
			if (element.getAttribute("text").toLowerCase().contains("blue")
					|| element.getAttribute("text").toLowerCase().contains("deep")
					|| element.getAttribute("text").toLowerCase().contains("ocean")
					|| element.getAttribute("text").toLowerCase().contains("moonshine")
					|| element.getAttribute("text").toLowerCase().contains("navy")
					|| element.getAttribute("text").toLowerCase().contains("spinnaker")
					|| element.getAttribute("text").toLowerCase().contains("hydrangea")
					|| element.getAttribute("text").toLowerCase().contains("marlin")
					|| element.getAttribute("text").toLowerCase().contains("wipeout")
					|| element.getAttribute("text").toLowerCase().contains("cornflower")
					|| element.getAttribute("text").toLowerCase().contains("pool")
					|| element.getAttribute("text").toLowerCase().contains("harbor")) {
				result = true;
			}
		} else if (color.equalsIgnoreCase(" black")) {
			if (element.getAttribute("text").toLowerCase().contains("black")) {
				result = true;
			}
		}
		return result;
	}

	public static void pageRefreshUntilElementVisible(By by, int seconds) throws InterruptedException {
		for (int second = 0;; second++) {
			if (second >= seconds)
				fail("timeout");
			try {
				UtilityMethods.waitForPageLoadAndPageReady();
				if (isElementPresent(by))
					break;
			} catch (Exception e) {
			}
			UtilityMethods.refreshPage();
			UtilityMethods.waitForPageLoadAndPageReady();
		}
	}

	public static void switchToNewPopUpWindow() throws InterruptedException {

		// Store your parent window
		parentWindowHandler = driver.getWindowHandle();
		String subWindowHandler = null;

		// get all window handles
		Set<String> handles = driver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();

		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}

		// switch to popup window
		driver.switchTo().window(subWindowHandler);
		UtilityMethods.waitForPageLoadAndPageReady();

		// perform operations on popup window
		//System.out.println("on popup window .. driver.getTitle() = " + driver.getTitle());

		// switch back to parent window
		//driver.switchTo().window(parentWindowHandler);
		//System.out.println("back to parent window .. driver.getTitle() = " + driver.getTitle());
	}

	public static void enterDataInTextBox(WebElement element, String data_to_enter) {
		element.clear();
		element.sendKeys(data_to_enter);
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public static void clickByLink(String link) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(link)));
		waitForElementPresentInSec(driver.findElement(By.linkText(link)), 10);
		moveToElementWithWaits(driver.findElement(By.linkText(link)));
		driver.findElement(By.linkText(link)).click();
		waitForPageLoadAndPageReady();
	}

	public static WebElement getElementByLink(String link) throws Exception {
		moveToElementWithWaits(driver.findElement(By.linkText(link)));
		waitForElementPresentInSec(driver.findElement(By.linkText(link)), 30);
		return driver.findElement(By.linkText(link));
	}

	public static void clickElement(WebElement element) throws InterruptedException {
		UtilityMethods.waitForElementPresentInSec(element, 30);
		element.click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public static String getTextNotifyPopup() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement popupElement = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span.noty_text")));
		return popupElement.getText();
	}

	public static boolean getElementNotVisibleByLink(String link) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 8);
		return wait.until(ExpectedConditions.invisibilityOfElementLocated(By.linkText(link)));
	}

	public static void pageRefreshUntilElementIsVisible(By by, int seconds) throws InterruptedException {
		for (int second = 0;; second++) {
			if (second >= seconds)
				fail("timeout");
			try {
				UtilityMethods.waitForPageLoadAndPageReady();
				if (driver.findElement(by).isDisplayed() == true) {
					UtilityMethods.waitForPageLoadAndPageReady();
					//System.out.println("yes");
					break;
				}
			} catch (Exception e) {
			}
			UtilityMethods.refreshPage();
			UtilityMethods.waitForPageLoadAndPageReady();

		}
	}

	public static void moveToElementWithJSExecutor(WebElement element) throws InterruptedException {
		waitForPageLoadAndPageReady();
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		waitForPageLoadAndPageReady();
	}

	public static String appendStringWithDashes(String value) {
		return value.substring(0, 4) + "-" + value.substring(4, 8) + "-" + value.substring(value.length() - 2);
	}

	public static void verifyLinkDisplayed(String link) throws Exception {
		ifElementPresentInSec(getElementByLink(link), 10);
		assertTrue(getElementByLink(link).isDisplayed());
	}

	public static void verifyElementDisplayed(WebElement element) throws Exception {
		assertTrue(ifElementPresentInSec(element, 10));
	}

	public static String getTextFileContents(String fileName) throws IOException {
		String attachmentFileText;
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			attachmentFileText = sb.toString();
		}
		return attachmentFileText;
	}

	public static void waitForFileExists(String fileName) {
		Boolean waitForFile = true;
		while (waitForFile) {
			File f = new File(fileName);
			if (f.exists() && !f.isDirectory()) {
				waitForFile = false;
			}
		}
	}

	public static void acceptAlert() {
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	// verify field on page
	public static void verifyPageElement(String elementPath, WebElement element) throws Throwable {

		waitForElementPresenceByCss(elementPath);
		waitForEnable1Sec(element);
		waitForVisibility(element);
	}

	public static boolean isElementDisplayed(By by, int timeoutInSeconds) {
		WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		return driver.findElement(by).isDisplayed();
	}

	public static WebElement fluentWait(final By locator) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(30, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

		WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(locator);
			}
		});

		return foo;
	};

	public static WebElement waitForElementVisible(WebElement element) {
		return new WebDriverWait(driver, 15).until(ExpectedConditions.visibilityOf(element));
	}

	public static WebElement waitForElementClickable(WebElement element) {
		return new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(element));
	}

	public static WebElement waitForElementVisibleAndClickable(WebElement element) {
		waitForElementVisible(element);
		return waitForElementClickable(element);
	}

	public static Boolean elementNotPresent(WebElement element) {
		return new WebDriverWait(driver, 5).until(ExpectedConditions.invisibilityOf(element));
	}

	public static void waitForTextPresent(WebElement webElement, String quantity) {
		wait.until(ExpectedConditions.textToBePresentInElement(webElement, quantity));
	}

	public static void deleteCookies() {
		Set<Cookie> allCookies = driver.manage().getCookies();
		for (Cookie cookie : allCookies) {
			driver.manage().deleteCookieNamed(cookie.getName());
		}
		driver.manage().deleteAllCookies();
	}

	public static String removeWhiteSpace(String string) {
		return string.replaceAll("\\s+", "");
	}

	public static void clickElementOnlyIfDisplayed(By selector) {
		Boolean isElementPresent = driver.findElements(selector).size() > 0;
		if (isElementPresent) {
			driver.findElement(selector).click();
		}
	}

	public static boolean is8DigitNumeric(String s) {
		return s.matches("^[0-9]{8}$");
	}

	public static boolean isCurrencyPattern(String s) {
		return s.matches("^\\$?[0-9][0-9\\,]*(\\.\\d{1,2})?$|^\\$?[\\.]([\\d][\\d]?)$");
	}

	public static String getLastWord(String s) {
		String[] wordList = s.trim().split("\\s+");
		String lastWord = wordList[wordList.length - 1];
		return lastWord;
	}

	public static String getNthWord(String fullString, int nth) {
		String[] temp = fullString.split(" ");
		if (nth - 1 < temp.length)
			return temp[nth - 1];
		return null;
	}

	public static String getFirstLineOfText(String string) {
		return string.substring(0, string.indexOf("\n"));
	}

	public static String getLastFourCharacters(String myString) {
		return myString.substring(myString.length() - 4);
	}

	public static String multiplyByNum_StringToFloat00(String floatNumber, int numMultiply) {
		int result = ((int) Double.parseDouble(floatNumber)) * numMultiply;
		DecimalFormat decimalFormat = new DecimalFormat("0.00");
		return decimalFormat.format(Double.parseDouble(String.valueOf(result)));
	}

	public static void verifyElementNotVisible(By locator) {
		assertTrue(new WebDriverWait(driver, 4).until(ExpectedConditions.invisibilityOfElementLocated(locator)));
	}

	public static void printListDouble(List<Double> list) {
		for (int i = 0; i < list.size(); i++) {
			//System.out.println("list.get(i) = " + list.get(i));
		}
	}

	public static <S, T extends Iterable<S>> void printList(T list) {
		for (Object element : list) {
			//System.out.println(element);
		}
	}

	public static <T extends Comparable<? super T>> boolean isSortedAscending(Iterable<T> iterable) {
		Iterator<T> iter = iterable.iterator();
		if (!iter.hasNext()) {
			return true;
		}
		T t = iter.next();
		while (iter.hasNext()) {
			T t2 = iter.next();
			if (t.compareTo(t2) > 0) {
				return false;
			}
			t = t2;
		}
		return true;
	}

	public static boolean isSortedAscending(List<Double> list) {
		boolean sorted = true;
		for (int i = 1; i < list.size(); i++) {
			if (list.get(i - 1) > list.get(i)) {
				sorted = false;
				break;
			}
		}
		return sorted;
	}

	public static boolean isSortedDescending(List<Double> list) {
		boolean sorted = true;
		for (int i = 1; i < list.size(); i++) {
			if (list.get(i - 1) < list.get(i)) {
				sorted = false;
				break;
			}
		}
		return sorted;
	}

	public static boolean isStringListSorted(List<String> list) {
		String previous = "";
		for (String current : list) {
			if (current.compareTo(previous) < 0)
				return false;
			previous = current;
		}
		return true;
	}

	public static WebElement findDynamicElement(By by, int timeOut) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		return element;
	}

	public static void waitTillUrlChanges(String url, String urlEndsWithText, int seconds) throws InterruptedException {
		for (int second = 0;; second++) {
			if (second >= seconds)
				fail("timeout");
			try {
				UtilityMethods.wait1Seconds();
				if (url.contains(urlEndsWithText))
					break;
			} catch (Exception e) {
			}
			UtilityMethods.wait1Seconds();
		}
	}

	public static boolean verifyPDFContents(String strURL, String reqTextInPDF) {

		boolean flag = false;

		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;

		try {
			URL url = new URL(strURL);
			BufferedInputStream file = new BufferedInputStream(url.openStream());
			PDFParser parser = new PDFParser(file);

			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(1);

			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
		} catch (MalformedURLException e2) {
			System.err.println("URL string could not be parsed " + e2.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open PDF Parser. " + e.getMessage());
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}

		//System.out.println("+++++++++++++++++");
		//System.out.println("parsedText = " + parsedText);
		//System.out.println("+++++++++++++++++");

		if (parsedText.contains(reqTextInPDF)) {
			flag = true;
		}

		return flag;
	}

	public static String getBalance(String myString) {
		String splitStringIntoSpaces[] = myString.split(" ");
		return myString.substring(1, splitStringIntoSpaces[0].length());
	}

	public static void readGMails(String user, String password, String emailSubject) {
		try {

			String MAIL_POP_HOST = "pop.gmail.com";
			String MAIL_STORE_TYPE = "pop3";

			String POP_PORT = "995";

			// create properties field
			Properties properties = new Properties();
			properties.put("mail.pop3.host", MAIL_POP_HOST);
			properties.put("mail.pop3.port", POP_PORT);
			properties.put("mail.pop3.starttls.enable", "true");
			properties.put("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

			Session emailSession = Session.getDefaultInstance(properties, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(user, password);
				}
			});
			// create the POP3 store object and connect with the pop server
			Store store = emailSession.getStore(MAIL_STORE_TYPE);

			store.connect(MAIL_POP_HOST, user, password);

			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);

			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();
			//System.out.println("messages.length " + messages.length);

			for (int i = 0, n = messages.length; i < n; i++) {
				Message message = messages[i];

				if (message.getSubject().equals(emailSubject)) {
					//System.out.println("---------------------------------");
					//System.out.println("Email Number " + (i + 1));
					//System.out.println("Subject: " + message.getSubject());
					//System.out.println("From: " + message.getFrom()[0]);
					//System.out.println("Body: " + message.getContent().toString());
				}
			}

			// close the store and folder objects
			emailFolder.close(false);
			store.close();

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void delete(String user, String password) {
		try {
			String MAIL_POP_HOST = "pop.gmail.com";
			String MAIL_STORE_TYPE = "pop3";

			String POP_PORT = "995";

			// create properties field
			Properties properties = new Properties();
			properties.put("mail.pop3.host", MAIL_POP_HOST);
			properties.put("mail.pop3.port", POP_PORT);
			properties.put("mail.pop3.starttls.enable", "true");
			properties.put("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

			Session emailSession = Session.getDefaultInstance(properties, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(user, password);
				}
			});
			// create the POP3 store object and connect with the pop server
			Store store = emailSession.getStore(MAIL_STORE_TYPE);

			store.connect(MAIL_POP_HOST, user, password);

			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_WRITE);

			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();

			//System.out.println("messages.length---" + messages.length);
			for (int i = 0; i < messages.length; i++) {
				Message message = messages[i];
				message.setFlag(Flags.Flag.DELETED, true);

			}
			// expunges the folder to remove messages which are marked deleted
			// emailFolder.close(true);
			emailFolder.expunge();
			store.close();

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public static void deletePreviousGMails(String userName, String password) {
		Properties properties = new Properties();

		// server setting
		properties.put("mail.imap.host", "imap.gmail.com");
		properties.put("mail.imap.port", "993");

		// SSL setting
		properties.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.setProperty("mail.imap.socketFactory.fallback", "false");
		properties.setProperty("mail.imap.socketFactory.port", String.valueOf("993"));

		Session session = Session.getInstance(properties);

		try {
			// connects to the message store
			Store store = session.getStore("imap");
			store.connect(userName, password);

			// opens the inbox folder
			Folder folderInbox = store.getFolder("INBOX");
			folderInbox.open(Folder.READ_WRITE);

			// fetches new messages from server
			Message[] arrayMessages = folderInbox.getMessages();

			for (int i = 0; i < arrayMessages.length; i++) {
				Message message = arrayMessages[i];
				message.setFlag(Flags.Flag.DELETED, true);
				//System.out.println("Marked DELETE for message: " + message.getSubject());

			}

			boolean expunge = true;
			folderInbox.close(expunge);

			// disconnect
			store.close();
		} catch (NoSuchProviderException ex) {
			//System.out.println("No provider.");
			ex.printStackTrace();
		} catch (MessagingException ex) {
			//System.out.println("Could not connect to the message store.");
			ex.printStackTrace();
		}
	}

	public static void getSelectedOptionByText(WebElement element, String text) throws InterruptedException {
		new Select(element).selectByVisibleText(text);
	}

	public static void hoverElement(WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element);
	}

	public static void clickElemetJavaSciptExecutor(WebElement webElement) throws InterruptedException {
		UtilityMethods.waitForElementEnabled(webElement, 100);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", webElement);
	}

	/**
	 * Verify mandatory fields
	 */
	public static String verifyMandatoryFeilds(List<FieldValidator> fieldValidators) throws InterruptedException {
		String errorString = "";
		if (fieldValidators != null) {
			for (int i = 0; i < fieldValidators.size(); i++) {
				FieldValidator fieldValues = fieldValidators.get(i);
				WebElement webElement = fieldValues.getWebElement();
				String value = fieldValues.getValue();
				String key = fieldValues.getKey();
				if (webElement.getAttribute("aria-required") != null
						&& (webElement.getAttribute("aria-required").equalsIgnoreCase("true")
								|| webElement.getAttribute("aria-required").equalsIgnoreCase(""))
						&& (value.length() == 0)) {
					errorString = TestData.MANDATORY_FIELD_ERROR_MESSAGE_1 + " " + key
							+ TestData.MANDATORY_FIELD_ERROR_MESSAGE_2;
					break;
				}
			}
		}
		return errorString;
	}

	/**
	 * Method to get the Test Data from Excel File
	 * 
	 * @param rowToMatch
	 * @param sheetName
	 * @param columnValue
	 * @return
	 */
	public static String getTestData(String rowToMatch, String sheetName, String columnValue) {
		String testDataString = null;
		try {
			testDataString = readExcelData(sheetName, rowToMatch).get(columnValue);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return testDataString != null ? testDataString.toString() : "";
	}

	/**
	 * This method is being used for equal comparison for the provided actual
	 * and expected values parameters
	 * 
	 * @param expected
	 *            Expected Value
	 * @param actual
	 *            the value to check against expected
	 */
	public static void validateAssertEqual(Object expected, Object actual) {
		assertEquals(TestData.InformationMessage.DOUBLE_QUOTES + expected + TestData.InformationMessage.DOUBLE_QUOTES
				+ TestData.InformationMessage.ASSERT_EQUAL_MESSAGE + TestData.InformationMessage.DOUBLE_QUOTES + actual
				+ TestData.InformationMessage.DOUBLE_QUOTES_END, expected, actual);
	}

	/**
	 * This method is being used for not equal comparison for the provided
	 * actual and expected values
	 * 
	 * @param expected
	 *            Expected Value
	 * @param actual
	 *            the value to check against expected
	 */
	public static void validateAssertNotEqual(String expected, String actual) {
		assertNotEquals(TestData.InformationMessage.DOUBLE_QUOTES + expected + TestData.InformationMessage.DOUBLE_QUOTES
				+ TestData.InformationMessage.ASSERT_NOT_EQUAL_MESSAGE + TestData.InformationMessage.DOUBLE_QUOTES
				+ actual + TestData.InformationMessage.DOUBLE_QUOTES_END, expected, actual);
	}

	/**
	 * This method is being used to validate the expected condition as TRUE
	 * 
	 * @param message
	 *            Custom message
	 * @param condition
	 *            The given condition
	 */
	public static void validateAssertTrue(String message, boolean condition) {
		assertTrue(message, condition);
	}

    public static void validateAssertTrue(boolean condition) {
        assertTrue(condition);
    }

    public static void validateAssertFalse(String message, boolean condition) {
        assertFalse(message, condition);
    }

    public static void clickEmailPromptIfAppears() throws InterruptedException {
        UtilityMethods.waitForPageLoadAndPageReady();
    	 UtilityMethods.wait3Seconds();UtilityMethods.wait3Seconds();
    	//Boolean isPresent = driver.findElements(By.xpath(".//div[@id='emailprompt']/a[@class='dialog_close icon-close-gray']")).size() > 0;
        /*if (driver.findElement(By.xpath("//*[@id='emailprompt']/a")).isDisplayed()){
            driver.findElement(By.xpath("//*[@id='emailprompt']/a")).click();
        }*/
    }

    
    
    public static void wait30Seconds() throws InterruptedException{
        Thread.sleep(30000);
    }
    
    public static void wait40Seconds() throws InterruptedException{
        Thread.sleep(40000);
    }
    
    public static void wait150Seconds() throws InterruptedException{
        Thread.sleep(150000);
    }
    
    public static void wait60Seconds() throws InterruptedException{
        Thread.sleep(60000);
    }
    
    public static void wait15Seconds() throws InterruptedException{
        Thread.sleep(15000);
    }
    
    public static void wait20Seconds() throws InterruptedException{
        Thread.sleep(20000);
    }
    
    public static void wait10Seconds() throws InterruptedException{
        Thread.sleep(10000);
    }
    
    public static void wait300Seconds() throws InterruptedException{
        Thread.sleep(300000);
    }
    
    public static void wait420Seconds() throws InterruptedException{
        Thread.sleep(420000);
    }
    
    
    public static void wait240Seconds() throws InterruptedException{
        Thread.sleep(240000);
    }
    public static void wait5Seconds() throws InterruptedException{
        Thread.sleep(5000);
    }
    
    public static void wait120Seconds() throws InterruptedException{
        Thread.sleep(120000);
    }
    
    
    public static void wait7Seconds() throws InterruptedException{
        Thread.sleep(7000);
    }
    
    
    public static void wait3Seconds() throws InterruptedException{
        Thread.sleep(3000);
    }

    public static void wait600Seconds() throws InterruptedException{
        Thread.sleep(600000);
    }
    
    public static void wait1200Seconds() throws InterruptedException{
        Thread.sleep(1200000);
    }
    public static void wait1Seconds() throws InterruptedException {
        Thread.sleep(1000);
    }

    public static void wait2Seconds() throws InterruptedException {
        Thread.sleep(2000);
    }
    public static boolean is20DigitNumeric(String s) {
        return s.matches("^[0-9]{20}$");
    }

    public static void getLatestWindowFocused()
    {
        String mostRecentWindowHandle = null;
        for(String winHandle:driver.getWindowHandles()){
            mostRecentWindowHandle = winHandle;
        }
        driver.switchTo().window(mostRecentWindowHandle);
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("window.focus();");
        js = null;
    }
    
    public static void switchTOIframeCRM() throws Throwable{
    	
    	waitForPageLoadAndPageReady();
    	try{
    	new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("contentIFrame1"))));
	//	switchToSubWindowAndReturnParentWindowHandler();
		driver.switchTo().frame(driver.findElement(By.id("contentIFrame1")));
		
    	}
    	catch(Exception e){
    		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("contentIFrame0"))));
    //		switchToSubWindowAndReturnParentWindowHandler();
    		driver.switchTo().frame(driver.findElement(By.id("contentIFrame0")));
    		
    	}
    	wait5Seconds();
    	
    }
}