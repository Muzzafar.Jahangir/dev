
package TeamViewer.TVAutomation.Stepdefinitions;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.support.PageFactory;


import static TeamViewer.TVAutomation.Utils.DataPool.readExcelData;

import java.io.File;
import java.util.HashMap;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;

import TeamViewer.TVAutomation.DataProvider.TestData;
import TeamViewer.TVAutomation.Pages.ConfirmationPage;
import TeamViewer.TVAutomation.Strings.StringsToVerify;
import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
//import cucumber.api.java.en.Then;


public class ConfirmationPageSteps extends DriverFactory {

	private ConfirmationPage testPage;

	@Before
	public void setUp() throws Exception {
		testPage = new ConfirmationPage(driver);

	}

	@And("^I verify Order Placement$")
	public void verify_order_placed_successfully() throws Throwable {
		UtilityMethods.waitForPageLoadAndPageReady();
		WebDriverWait wait = new WebDriverWait(driver,40);
		wait.until(ExpectedConditions.visibilityOf(testPage.Confirm));
		if(testPage.Confirm.isDisplayed()){
			
			
			Reporter.addStepLog("ORDER HAS BEEN PLACED SUCCESSFULLY!");
			UtilityMethods.validateAssertEqual("Success Header Text is Showing", "Success Header Text is Showing");
			System.out.println("ORDER HAS BEEN PLACED SUCCESSFULLY!");
			
		}
		else{
			
			Reporter.addStepLog("ORDER IS NOT PLACED. PLEASE CHECK!");
			System.out.println("ORDER IS NOT PLACED. PLEASE CHECK!");
			UtilityMethods.validateAssertEqual("Header Text Should Show", "Header Text is not Showing");
			
		}

//		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_JP_DATA);	
//		row.get(TestData.ORDER_SUCCESS.toString().trim());
//
//
//		//UtilityMethods.validateAssertEqual(testPage.getConfirm(), StringsToVerify.MESSAGE_CONFIRMATION);
//		UtilityMethods.validateAssertEqual(testPage.getConfirm(), row.get(TestData.ORDER_SUCCESS.toString().trim()));


	}	
	
	@And("^I verify Quote submission$")
	
	public void verifyQuotePermission() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		WebDriverWait wait = new WebDriverWait(driver,40);
		wait.until(ExpectedConditions.visibilityOf(testPage.ConfirmQuote));
		if(testPage.ConfirmQuote.isDisplayed()){
			
		
			Reporter.addStepLog("QUOTE HAS BEEN SUBMITTER SUCCESSFULLY!");
			UtilityMethods.validateAssertEqual("Success Header Text is Showing", "Success Header Text is Showing");
			System.out.println("QUOTE HAS BEEN SUBMITTER SUCCESSFULLY!");
			
		}
		else{
			
			Reporter.addStepLog("QUOTE IS NOT PLACED. PLEASE CHECK!");
			System.out.println("QUOTE IS NOT PLACED. PLEASE CHECK!");
			UtilityMethods.validateAssertEqual("Header Text Should Show", "Header Text is not Showing");
			
		}


	}

	@And("^I Get Order Id and Payment Method From Success Page$")
	
	public void getOrderId() throws Throwable{
		testPage.gettingOrderIdFromSuccessPAGE();


	}
	
	@And("^I click Activiate License Email button$")
	public void clickActivateLicenseButton() throws Throwable{
		
		testPage.clickActivateLicenseButton();
		
	}


}
