package TeamViewer.TVAutomation.Stepdefinitions;


import TeamViewer.TVAutomation.Pages.AX365Page;
import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AX365PageSteps extends DriverFactory {

	private AX365Page d365page;


	@Before
	public void setUp() throws Exception {

		d365page = new AX365Page(driver);

	}


	@And("^I switch from Business To Premium$")
	public void switchBusinessToPremium() throws Throwable{


		d365page.switchFromBusinessToPremium();

	}

	@And("^I switch from Business To Corporate$")
	public void switchBusinessToCorporate() throws Throwable{


		d365page.switchFromBusinessToCorporate();

	}

	@And("^I create TV14B0001 Perpetual order in ERP$")
	public void openCustomerDetailsAndTV14B0001Order() throws Throwable{


		d365page.openingCustomerDetailsAndTV14B001Order();

	}


	@And("^I create TV13B0001 Perpetual order in ERP$")
	public void openCustomerDetailsAndTV13B0001Order() throws Throwable{


		d365page.openingCustomerDetailsAndTV13B001Order();

	}

	@And("^I create TV14P0001 Perpetual order in ERP for CRM Flow$")
	public void addPerpetualOrderForCRMCustomer() throws Throwable{


		d365page.addPerpetualOrderForCRMCustomer();

	}

	@And("^I create TV14P0001 Perpetual order in ERP$")
	public void openCustomerDetailsAndTV14P0001Order() throws Throwable{


		d365page.openingCustomerDetailsAndTV14P001Order();

	}

	@And("^I create TV14C0001 Perpetual order in ERP$")
	public void openCustomerDetailsAndTV14C0001Order() throws Throwable{


		d365page.openingCustomerDetailsAndTV14C0001Order();

	}
	@And("^I upgrade TV14B0001 To TV14P0001$")
	public void switchTV14B0001ToTV14P0001() throws Throwable{


		d365page.upgradeFromTV14B0001ToTV14P0001();

	}

	@And("^I upgrade TV14B0001 To TV14C0001$")
	public void switchTV14B0001ToTV14C0001() throws Throwable{


		d365page.upgradeFromTV14B0001ToTV14C0001();

	}

	@And("^I update TV13B0001 To TV14B0001$")
	public void updateTV13B0001ToTV14B0001() throws Throwable{


		d365page.updateFromTV13B0001ToTV14B0001();

	}

	@And("^I migrate TV14B0001 To TVB0001$")
	public void migarateTV14B0001ToTVB0001() throws Throwable{


		d365page.migrateFromTV14B0001ToTVB0001();

	}

	@And("^I migrate TV14P0001 To TVP0001$")
	public void migarateTV14P0001ToTVP0001() throws Throwable{


		d365page.migrateFromTV14P0001ToTVP0001();

	}
	
	@And("^I migrate TV14B0001 To TVP0001$")
	public void migrateFromTV14B0001ToTVP0001() throws Throwable{
		
		d365page.migrateFromTV14B0001ToTVP0001();
		
	}
	
	@And("^I migrate TV14P0001 To TVC0001$")
	public void migarateTV14P0001ToTVC0001() throws Throwable{


		d365page.migrateFromTV14P0001ToTVC0001();

	}

	@And("^I migrate TV14C0001 To TVC0001$")
	public void migarateTV14C0001ToTVC0001() throws Throwable{


		d365page.migrateFromTV14C0001ToTVC0001();

	}
	@And("^I upgrade TV14P0001 To TV14C0001$")
	public void switchTV14P0001ToTV14C0001() throws Throwable{


		d365page.upgradeFromTV14P0001ToTV14C0001();

	}


	@And("^I switch from Premium To Corporate$")
	public void switchPremiumToCorporate() throws Throwable{


		d365page.switchFromPremiumToCorporate();

	}


	@And("^I Search Customer in ERP$")
	public void searchCustomerOrder() throws Throwable{


		d365page.searchForCustomer();

	}
	
	@And("^I Search Channel Reference ID in ERP$")
	public void searchForChannelReferenceID() throws Throwable{


		d365page.searchForChannelReferenceID();

	}

	@And("^I create Business license order in ERP$")
	public void openCustomerDetailsAndBusinessOrder() throws Throwable{


		d365page.openingCustomerDetailsAndBusinessOrder();

	}

	@And("^I create direct business license order$")
	public void createDirectBusinessLicenseOrder() throws Throwable{

		d365page.createDirectBusinessLicenseOrder();
	}

	@And("^I create sales qoutation of Business license order in ERP for CRM Flow$")
	public void openCustomerDetailsAndBusinessOrderForCRMFlow() throws Throwable{


		d365page.openingCustomerDetailsAndBusinessOrderforCRMFlow();

	}
	
	@And("^I add BLPA001 and ITBB0001 product$")
	public void addProducts() throws Throwable{
		
		d365page.addBLPA001AndITBB0001Product();
		
	}

	@And("^I send and verify quotation$")
	public void sendTheQoutationAndCompleteTheOrder() throws Throwable{


		d365page.sendQoutationAndVerifyTheOrder();

	}
	
	@And("^I confirm and verify quotation in CRM$")
	public void confirmationAndVerficationOfQuote() throws Throwable{
		
		d365page.confirmationAndVerificationOfQuotation();
		
	}

	@And("^I check salesorder created and verified$")
	public void checkSalesOrderAfterQoutationConfirmed() throws Throwable{


		d365page.checkSalesOrderAfterQoutationConfirmed();

	}

	@And("^I create Premium license order in ERP$")
	public void openCustomerDetailsAndPremiummOrder() throws Throwable{


		d365page.openingCustomerDetailsAndPremiumOrder();

	}

	@And("^I create Corporate license order in ERP$")
	public void openCustomerDetailsAndCorporateOrder() throws Throwable{


		d365page.openingCustomerDetailsAndCorporateOrder();

	}

	@And("^I complete order in ERP$")
	public void completeOrderInERP() throws Throwable{


		d365page.completeTheOrder();

	}
	
	@And("^I complete order in ERP for Credit Card Payment$")
	public void completeOrderForCreditCardPayment() throws Throwable{


		d365page.completeTheOrderForCreditCardPayment();

	}

	@And("^I verify discount on corporate is applied$")
	public void verifyDiscountOnCorporate() throws Throwable{


		d365page.verifyDiscountOnCorporate();

	}


	@When("^I Login ERP$")
	public void LoginERP() throws Throwable{

		d365page.doLoginInERP();

	}
	
	@When("^I Login To Email$")
	public void LoginQuotationEmail() throws Throwable{

		d365page.doLoginInQuotationEmail();

	}
	
	@And("^I Click On Email and get Quotation ID$")
	public void clickQuotationEmail() throws Throwable{

		d365page.clickQuotationEmail();

	}
	
	@And("^I select Activation Link$")
	public void clickActivationLinkInPDF() throws Throwable{

		d365page.clickActivationLinkInPDF();

	}
	
	@And("^I sign out from email$")
	public void signOutFromEmail() throws Throwable{
		
		d365page.signOutFromEmail();
		
	}

	@And("^I Search Order in ERP$")
	public void searchOrder() throws Throwable{


		d365page.searchOrderInERPViaChannelFilter();

	}

	@And("^I open the sales order and verify products purchased in ERP$")
	public void verifySearchedOrder() throws Throwable{


		d365page.verifyPurchasesInERP();

	}
	
	@And("^I verify visa card$")
	public void verifyVisaCard() throws Throwable{
		
		d365page.verifyVisaCardInERP();
		
	}
	
	@And("^I verify american card$")
	public void verifyAmericanCard() throws Throwable{
		
		d365page.verifyAmericanCardInERP();
		
	}
	
	@And("^I verify master card$")
	public void verifyMasterCard() throws Throwable{
		
		d365page.verifyMasterCardInERP();
		
	}
	

	@And("^I verify quantity of purchased products in ERP$")
	public void verifyQuantityOfProducts() throws Throwable{
		System.out.println();
		System.out.println();
		System.out.println("***************************************************");
		
		d365page.verifyQuantityOfPurchasedProducts();

	}
	
	@And("^I verify amount of purchased products in ERP$")
	public void verifyAmountOfPurchasedProducts() throws Throwable{


		d365page.verifyAmountOfPurchasedProducts();

	}
	
	@And("^I verify customer information in General Tab$")
	public void customerInformationOnHeaderBar() throws Throwable{


		d365page.verifyCustomerInformationInGeneralTab();

	}
	
	@And("^I verify customer information in SetUp Tab$")
	public void customerInformationInSetUpTab() throws Throwable{


		d365page.verifyCustomerInformationInSetUp();

	}
	
	@And("^I verify customer information in Price And Information Tab$")
	public void customerInformationInPriceAndDiscountTab() throws Throwable{


		d365page.verifyCustomerInformationInPriceAndDiscountTab();

	}
	
	@And("^I verify customer information in Financials And Dimensions Tab$")
	public void customerInformationInRetailAndDimensionsTab() throws Throwable{


		d365page.verifyCustomerInformationInFinancialDimensionsTab();

	}
	
	@And("^I open new tab and get the number of Main Account and Sales Tax using Sales Tax Code and Sales Tax Group")
	public void getNumberOfMainAccountAndSalesTax() throws Throwable{


		d365page.getNumberOfMainAccountAndSalesTax();

	}
	
	@And("^I verify payment status on header bar$")
	public void paymentStatusOnHeaderBar() throws Throwable{


		d365page.paymentStatusOnHeaderBar();

	}
	
	@And("^I verify sales order is completed$")
	public void checkOrderIsCompleted() throws Throwable{


		d365page.checkOrderIsCompleted();

	}

	@And("^I Switch To Customer Service Portal$")
	public void switchtoCustomerServicePortal() throws Throwable{


		d365page.switchToCustomerServicePortal();

	}

	@And("^I Cancel Simple Contract On ERP$")
	public void cancelContractOrders() throws Throwable{


		d365page.cancelContractFunctionalityForSimpleOrder();

	}

	@And("^I Cancel Switched Contract On ERP$")
	public void cancelContractOrderAfterSwitch() throws Throwable{


		d365page.cancelContractFunctionalityForSwitchOrder();

	}

	@And("^I Cancel Perpetual Contract On ERP$")
	public void cancelSwitchedContractOrders() throws Throwable{


		d365page.cancelContractFunctionalityForPerpetualOrder();

	}

	@And("^I Cancel Migration Contract On ERP$")
	public void cancelMigrateContractOrders() throws Throwable{


		d365page.cancelContractFunctionalityForMigratedOrder();

	}

	@And("^I confirm sales order and invoice in ERP$")
	public void confirmSaleOrder() throws Throwable{

		d365page.confirmSalesOrderandInvoice();


	}
	
	@And("^I invoice in ERP for CRM$")
	public void confirmSaleOrderCRM() throws Throwable{

		d365page.invoiceSalesOrderAndVerify();


	}
	
	@And("^I validate values against invoice$")
	public void validationOverInvoice() throws Throwable{

		d365page.validationOverInvoice();
	}
	
	@And("^I Switch To ECOM$")
	public void switchToECOMAndAddPayment() throws Throwable{

		d365page.switchToECOM();

	}
	
	@And("^I Switch to different country store$")
	public void switchToDifferentCountryStoreAndAddPayment() throws Throwable{

		d365page.switchToDifferentCountryStoreAndAddPayment();

	}
	
	@And("^I open customers page and search relevant customer$")
	public void openCustomerPage() throws Throwable{

		d365page.switchToERP();

	}
	
	@And("^I Open New Tab and Run The Job For Posting Of Status$")
	public void switchToERPForJob() throws Throwable{

		d365page.runTheJob();

	}
	
	@And("^I Click forgot password link$")
	public void clickForgotPasswordLink() throws Throwable {
		
		d365page.clickForgotPasswordLink();
		
	}
	
	@And("^I Verify email Pop up$")
	public void verifyPopUpEmail() throws Throwable {
		
		d365page.verifyPopUpEmail();
		
	}
	
	
	@And("^I Switch To ERP for Order Verification$")
	public void switchToERPForOrderVerification() throws Throwable{

		d365page.switchToERPForVerification();

	}
	
	@And("^I switch to Check out Page$")
	public void switchToCheckOutPageOfQuotation() throws Throwable{

		d365page.switchToCheckOutPageOfQuotation();

	}
	
	@And("^I Switch To Email For Quotation ID and Activation Link$")
	public void switchToEmailForQuotationIDAndActivationLink() throws Throwable{

		d365page.switchToEmailForQuotationIDAndActivationLink();

	}
	
	@And("^I Switch To ERP again$")
	public void switchToERPAgain() throws Throwable{

		d365page.switchToERPAgain();

	}
	
	@And("^I Open Customer Transactions And Validate the Settlements$")
	public void validatePaymentStatus() throws Throwable{

		d365page.openCustomerTransactions();

	}
	
	@And("^I Search For Settled Invoice$")
	public void validateOffSetAccount() throws Throwable{

		d365page.openSettledInvoice();

	}
	
	@And("^I Verify Offset account for PayPal$")
	public void validateOffSetAccountForPayPaL() throws Throwable{

		d365page.verifyOffsetAccountNumberForPaypal();

	}
	
	@And("^I Verify Offset account for CreditCard$")
	public void validateOffSetAccountForCreditCard() throws Throwable{

		d365page.verifyOffsetAccountNumberForCreditCard();

	}
	
	@And("^I confirm sales order and invoice the Credit Card Order in ERP$")
	public void confirmSaleOrderForCC() throws Throwable{

		d365page.confirmSalesOrderandInvoiceForCCPayment();


	}

	@And("^I again confirm sales order and invoice in ERP for Switch Order$")
	public void confirmSaleOrderAgainForSwitchedOrder() throws Throwable{

		d365page.confirmSalesOrderandInvoiceAgainForSwitchOrder();


	}

	@And("^I verify contract is invoiced$")
	public void verifyContractIsInvoiced() throws Throwable{

		d365page.verifyContractIsInvoiced();


	}
	
	@And("^I add Premium Product$")
	public void addPremiumProductOnSalesOrder() throws Throwable{


		d365page.addPremiumProductOnSalesOrder();

	}
	
	@And("^I add add-ons in Sales Order$")
	public void addAddOnInSalesOrder() throws Throwable{
		
		d365page.addAddOnInSalesOrder();
		
	}
	
	@And("^I terminate contract and ReActivate$")
	public void terminateContractAndReActivate() throws Throwable{

		d365page.terminatContract();


	}


	@And("^I validate correct contract$")
	public void validateTheSubTotalAmountAfterTaxAppliedInContractCorrection() throws Throwable{

		d365page.validateValuesAfterCorrectContractForDiscount();


	}



	@And("^I open journal invoice for voucher checking and lasernet printing$")
	public void clickLasernetPrint() throws Throwable{

		d365page.lasernetPrintOpen();


	}
	
	@And("^I open Data Recognition form and search order$")
	public void openRecognitionForm() throws Throwable{

		d365page.openRecognitionForm();


	}

	@And("^I run the journal job$")
	public void runJournalJob() throws Throwable{

		d365page.runJournalJob();


	}
	
	@And("^I open journal$")
	public void openJournal() throws Throwable{

		d365page.openJournal();


	}
	
	@Then("^I validate values and post and validate journal$")
	public void validateJournal() throws Throwable{

		d365page.validateAndPostJournal();


	}
	
	

	@And("^I open journal invoice for corporate discount checking$")
	public void checkDiscountOnLasernetPrintingForCorporate() throws Throwable{

		d365page.checkDiscountOnInvoiceForCorporate();


	}
	
	@And("^I open invoice under journal for getting invoice number$")
	public void checkInvoiceNumber() throws Throwable{

		d365page.getInvoiceNumber();


	}

	@And("^I correct the invoice and changing the customer delivery address$")
	public void correctTheInvoice4DeliveryAddress() throws Throwable{

		d365page.correctTheInvoiceForDeliveryAddress();


	}
	
	@And("^I correct the invoice and changing the language$")
	public void correctTheInvoiceChangeLanguage() throws Throwable{

		d365page.correctTheInvoiceForChangeLanguage();


	}
	
	@And("^I correct the invoice and changing the customer invoice address$")
	public void correctTheInvoice4InvoiceAddress() throws Throwable{

		d365page.correctTheInvoiceForInvoiceAddress();


	}
	

	@And("^I correct the invoice and change the due date$")
	public void correctTheInvoiceForDueDate() throws Throwable{

		d365page.correctTheInvoiceFordueDate();


	}

	@And("^I open journal invoice for voucher checking$")
	public void clickLasernetPrintForVoucherChecking() throws Throwable{

		d365page.lasernetPrintOpenAndVerifyCancelContract();

	}
	
	@And("^I open voucher to verify sales tax and Sale order revenue value$")
	public void checkSalesTaxAndSalesOrderRevenueValue() throws Throwable{

		d365page. openVoucherAndVerifyTheSalesTaxAndSalesOrderRevenue();

	}
	
	@And("^I open journal invoice$")
	public void openJournalToCheckPaidStatus() throws Throwable{

		d365page.openJournalToCheckPaidStatus();


	}
	
	@And("^I open customer and check payment settlement against invoice of CC$")
	public void openJournalAndCheckInvoiceSettlement() throws Throwable{

		d365page.checkCustomerSettlementForCreditCard();


	}
	
	@And("^I open journal invoice for checking the VAT number amount applied$")
	public void openVoucherAndCheckSubTotal() throws Throwable{

		d365page.openVoucherAndCheckSubTotal();


	}
	
	@And("^I open journal invoice for voucher checking of switch order$")
	public void clickLasernetPrintForVoucherCheckingOfSwitchedOrder() throws Throwable{

		d365page.lasernetPrintOpenAndVerifyCancelContractForSwitchedOrder();


	}
	
	@And("^I open journal invoice and check lasernet printing for correct contract$")
	public void clickLasernetPrintAfterContractCorrection() throws Throwable{

		d365page.lasernetPrintOpenForContractCorrection();


	}



	@And("^I verify voucher with tax and total value on lasernet preview$")
	public void verifyLasernetPrinterValuesOfTaxandTotal() throws Throwable{

		d365page.verifyTaxAndTotalOnlasernetPrinter();


	}


	@And("^I verify delivery address after resending the invoice$")
	public void verifyDeliveryAddressOnResending() throws Throwable{

		d365page.verifyDeliveryAndInvoiceAddressOnResending();
	}
	
	@And("^I verify due date after resending the invoice$")
	public void verifyDueDateOnResending() throws Throwable{

		d365page.verifyDueDateOnResending();
	}
	
	@And("^I verify the language of invoice after resending$")
	public void verifyLanguageChangeOnResending() throws Throwable{

		d365page.verifyLanguageChangeInInvoice();
	}
	
	
	@And("^I resend the invoice for implementing the changes$")
	public void resendTheInvoiceAndVerifyTheDeliveryAddress() throws Throwable{

		d365page.lasernetPrintOpenAndResendTheInvoice();


	}

	@And("^I verify discounted price available in lasernet preview for corporate$")
	public void verifyLasernetPrinterDiscountedPrice() throws Throwable{

		d365page.verifyDiscountedPriceOnPrinter();


	}



	@And("^I verify voucher with tax and total value on lasernet preview for Correct Contract$")
	public void verifyLasernetPrinterValuesOfTaxandTotalForContractCorrection() throws Throwable{

		d365page.verifyTaxAndTotalOnlasernetPrinterForContractCorrection();


	}
	
	@And("^I verify Invoice and Product on PDF$")
	public void verifyInvoiceandProductOnMCODownloadPDF() throws Throwable{

		d365page.verifyInvoiceandProductOnMCODownloadPDF();


	}



	@And("^I terminate contract in ERP$")
	public void terminateSaleOrder() throws Throwable{

		d365page.terminateSalesOrder();

	}

	@And("^I verify terminatation of contract in ERP$")
	public void verifyTerminateSaleOrder() throws Throwable{

		d365page. verifyterminateofSalesOrder();

	}


	@And("^I validate Business To Premium switching$")
	public void validateBusinessToPremiumSwitching() throws Throwable{

		d365page.validateBusinessToPremiumSwitchedOrNot();


	}

	@And("^I validate Business To Corporate switching$")
	public void validateBusinessToCorporateSwitching() throws Throwable{

		d365page.validateBusinessToCorporateSwitchedOrNot();

	}

	@And("^I validate TV14B0001 To TV14P0001 upgrade$")
	public void validateTV14B0001ToTV14P0001Upgrade() throws Throwable{

		d365page.validateTV14B0001ToTV14P0001OrNot();

	}

	@And("^I validate TV14B0001 To TV14C0001 upgrade$")
	public void validateTV14B0001ToTV14C0001Upgrade() throws Throwable{

		d365page.validateTV14B0001ToTV14C0001OrNot();

	}

	@And("^I validate TV13B0001 To TV14B0001 update$")
	public void validateTV13B0001ToTV14B0001Update() throws Throwable{

		d365page.validateTV13B0001ToTV14B0001OrNot();

	}

	@And("^I validate TV13B0001 line is activated back$")
	public void validateTV13B0001isActive() throws Throwable{

		d365page.validateTV13B0001isActiveBack();

	}

	@And("^I validate TV14B0001 To TVB0001 migration$")
	public void validateTV14B0001ToTVB0001Migrate() throws Throwable{

		d365page.validateTV14B0001ToTVB0001OrNot();

	}
	
	@And("^I validate TV14B0001 To TVP0001 migration$")
	public void validateTV14B0001ToTVP0001Migrate() throws Throwable{

		d365page.validateTV14B0001ToTVP0001OrNot();

	}

	@And("^I validate TV14P0001 To TVC0001 migration$")
	public void validateTV14P0001ToTVC0001Migrate() throws Throwable{

		d365page.validateTV14P0001ToTVC0001OrNot();

	}


	@And("^I validate TV14C0001 To TVC0001 migration$")
	public void validateTV14C0001ToTVC0001Migrate() throws Throwable{

		d365page.validateTV14C0001ToTVC0001OrNot();

	}
	@And("^I Verify Totals of Order from ECOM to ERP$")
	public void checkOrderTotals() throws Throwable{

		d365page.checkOrderTotalsInERP();


	}

	@And("^I get values of tax subtotal and totals for Internal Created Order$")
	public void getOrderAmountValues() throws Throwable{

		d365page.getOrderValuesForLasernetInvoiceInERP();

	}
	
	@And("^I get values of tax subtotal and totals for verification of ECOM to ERP flow$")
	public void getOrderAmountValuesFOREcomToErp() throws Throwable{

		d365page.getOrderValuesFromTotalsPageForEcomToErpFLOW();

	}
	
	@And("^I again get values of tax subtotal and totals for Internal Created Order$")
	public void againGetOrderAmountValues() throws Throwable{

		d365page.againget_OrderValuesForLasernetInvoiceInERP();


	}

	@And("^I get values of tax subtotal and totals for Contract Correction$")
	public void getOrderAmountValuesForContractCorrection() throws Throwable{

		d365page.getOrderValuesForContractCancellation();


	}

	@And("^I correct Contract and Add Discount$")
	public void addDiscountInCorrectContract() throws Throwable{

		d365page.addingDiscountInContractCorrection();

	}
	
	@And("^I correct Contract and Add Tax Exempt Number$")
	public void addVATInCorrectContract() throws Throwable{

		d365page.addingTaxExemptNumberInContractCorrection();

	}
	
	@And("^I click PAC license$")
	public void clickPACLicense() throws Throwable{

		d365page.clickPACLicense();

	}
	
	@And("^I open activation link in of PAC License$")
	public void openActivationLink() throws Throwable{

		d365page.openActivationLink();

	}
	
	@And("^I Switch to MCO Portal to verify product$")
	public void switchMCOPortalToVerifyProduct() throws Throwable{

		d365page.switchMCOPortalToVerifyProduct();

	}
	
	@And("^I Switch to MCO Portal to verify Premium product$")
	public void switchMCOPortalToVerifyPremiumProduct() throws Throwable{

		d365page.switchMCOPortalToVerifyPremiumProduct();

	}
	
	@And("^I download pdf$")
	public void downloadPDFandValidateValues() throws Throwable{

		d365page.downloadPDFandValidateValues();

	}
	
	@And("^I Navigate to Quotation$")
	public void verifyQuotationStatus() throws Throwable{
		
		d365page.verifyQuotationStatus();
		
	}
	
	@And("^I Switch to Magento$")
	public void switchToMagento() throws Throwable{

		d365page.switchToMagento();

	}
	
	@And("^I Login to Magento$")
	public void LoginMagento() throws Throwable{

		d365page.doLoginInMagento();

	}
	
	@And("^I Copy Sales Order ID$")
	public void copySalesOrderID() throws Throwable{
		
		d365page.copySalesOrderID();
		
	}
	
	@And("^I open the sales order$")
	public void openSalesOrder() throws Throwable{
		
		d365page.openSalesOrder();
		
	}
	
	@And("^I confirm sales order in ERP and verify in CRM$")
	public void confirmSaleOrderCRMAndVerfying() throws Throwable{

		d365page.confirmSalesOrderAndVerify();

	}
	
	@And("^I Validate values in Sales order confirmations journal$")
	public void validatingValuesInConfirmationAndJournalInvoice() throws Throwable{

		d365page.validatingValuesInConfirmationAndJournalInvoice();

	}
	
	@And("^I open journal invoice to validate values$")
	public void openJournalToCheckPostedStatus() throws Throwable{

		d365page.openJournalToCheckPostedStatus();

	}
	
	
	@And("^I validate values In CRM of invoice$")
	public void validateValuesInCRMOfInvoice() throws Throwable{

		d365page.validateValuesInCRMOfInvoice();

	}
	
	@And("^I go to contract correction$")
	public void contractCorrectionOfSalesOrder() throws Throwable{
		
		d365page.contractCorrectionOfSalesOrder();
	}
	
	@And("^I validate values against invoice for CRM$")
	public void validationOverInvoiceForCRM() throws Throwable{

		d365page.validationOverInvoiceForCRM();
	}
	
	@And("^I validate values in CRM$")
	public void validateInoviceInCRM() throws Throwable{

		d365page.validateInoviceInCRM();
	}
	
	@And("^I create sales qoutation of Perpetual Business 14 order in ERP for CRM Flow$")
 	public void openingCustomerDetailsAndTV14B0001Order() throws Throwable{


 		d365page.openingCustomerDetailsAndCreatingTV14B0001Order();

 	}
	
	 @And("^I add Mobile add-ons$")
	 	public void addMobileAddOns() throws Throwable{
	 		
	 		d365page.addMobileAddOns();
	 		
	 	}
	 
	 @And("^I create sales qoutation of Premium license order in ERP for CRM Flow$")
	 	public void openCustomerDetailsAndPremiumOrderForCRMFlow() throws Throwable{


	 		d365page.openingCustomerDetailsAndPremiumOrderforCRMFlow();

	 	}
	 
	 @And("^I verify credit note line$")
	 public void verifyCreditNote() throws Throwable{
		 
		 d365page.verifyCreditNote();
	 }
	
	 @And("^I Modify Contract$")
	 public void modifyContract() throws Throwable{
		 
		 d365page.modifyContract();
		 
	 }
	 
	 @And("^I Cancel the Contract For a line$") 
	 public void cancelContractLine() throws Throwable{
		 
		 d365page.cancelContractLine();
		 
	 } 
	 @And("^I Cancel the contract$") 
	 public void cancelContract() throws Throwable{
		 
		 d365page.cancelContract();
		 
	 }

	 @And("^I Transfer the order$")
	 public void transferOrder() throws Throwable{
		 
		 d365page.transferOrder();		 
	 }
	 
	 @And("^I Consolidate both orders$")
	 public void consolidateOrders() throws Throwable{
		
		 d365page.consolidateOrders();
		 
	 }
	 
}
