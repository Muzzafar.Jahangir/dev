package TeamViewer.TVAutomation.Stepdefinitions;
import java.util.Set;

//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.PageFactory;

//import TeamViewer.TVAutomation.Pages.HomePage;
import TeamViewer.TVAutomation.Pages.PayPalLoginPage;
import TeamViewer.TVAutomation.Utils.DriverFactory;
//import TeamViewer.TVAutomation.Utils.UtilityMethods;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;

public class PayPalLoginPageSteps extends DriverFactory {


	private PayPalLoginPage testPage;



	@Before
	public void setUp() throws Exception {
		testPage = new PayPalLoginPage(driver);

	}

	@And("^I select PayPal Method$")
	public void selectPayPalMethod() throws Throwable{


		testPage.selectPayPalPayment();

	}




	@And("^I enter Paypal Credentials$")
	public void doLoginInPayPal() throws Throwable{

		String parentHandle = driver.getWindowHandle();
		Set<String> handles = driver.getWindowHandles();

		for(String windowHandle : handles){

			if(!windowHandle.equals(parentHandle)){

				driver.switchTo().window(windowHandle);

				testPage.enterCredentialsInPayPal();

				driver.switchTo().window(parentHandle);
			}

		}




	}

}