
package TeamViewer.TVAutomation.Stepdefinitions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import com.cucumber.listener.Reporter;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.PropertyReader;
import TeamViewer.TVAutomation.Utils.UtilityMethods;
//import cucumber.api.java.Before;
import cucumber.api.java.en.Given;

public class TVStartingSteps extends DriverFactory {


	@Given("^User access to Japan store$")
	public void JPENApp() throws InterruptedException {
		
		String url = new PropertyReader().readProperty("JPappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM JAPAN STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to Korea store$")
	public void KRENApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("KRappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM KOREA STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	
	@Given("^User access to Albania store$")
	public void ALApp() throws InterruptedException {
		String url = new PropertyReader().readProperty("ALappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		//driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM ALBANIA STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}

	@Given("^User access to Netherlands store$")
	public void NLApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("NLappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM NETHERLAND STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to Netherlands NL store$")
	public void NLNLApp() throws InterruptedException {
		String url = new PropertyReader().readProperty("NLNLappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM NETHERLAND STORE USING NETHERLAND LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User Access to Portugal PT store$")
	public void PTPTApp() throws InterruptedException {
		String url = new PropertyReader().readProperty("PTPTappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM PORTUGAL STORE USING PORTUGAL LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User Access to Portugal store$")
	public void PTApp() throws InterruptedException {
		String url = new PropertyReader().readProperty("PTappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM PORTUGAL STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}

	@Given("^User access to Belgium store$")
	public void BEApp() throws InterruptedException  {
		String url = new PropertyReader().readProperty("BEappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM BELGIUM STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();
	}
	

	@Given("^User access to Belgium store for staging$")
	public void BEAppStaging() throws InterruptedException  {
		String url = new PropertyReader().readProperty("BEappURLStaging");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM BELGIUM STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();
	}
	
	@Given("^User access to Netherlands store for staging$")
	public void NLAppStaging() throws InterruptedException  {
		String url = new PropertyReader().readProperty("NLappURLStaging");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		String[] path = url.split(".com");
		
		System.out.println("URL is: "+path[1]);
		Reporter.addStepLog("URL is: "+path[1]);
		UtilityMethods.wait5Seconds();
	}
	
	@Given("^User access to Ireland store for staging$")
	public void IRAppStaging() throws InterruptedException  {
		String url = new PropertyReader().readProperty("IRappURLStaging");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		
		System.out.println("USER IS ORDERING FROM NETHERLANDS STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait5Seconds();
	}
	
	@Given("^User access to UK store for staging$")
	public void UKAppStaging() throws InterruptedException  {
		String url = new PropertyReader().readProperty("UKappURLStaging");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		
		System.out.println("USER IS ORDERING FROM NETHERLANDS STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait5Seconds();
	}
	
	@Given("^User access to Argentina store for staging$")
	public void ArgentinaAppStaging() throws InterruptedException  {
		String url = new PropertyReader().readProperty("ARappURLStaging");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		
		System.out.println("USER IS ORDERING FROM NETHERLANDS STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait5Seconds();
	}
	
	@Given("^User access to Netherlands store to Request Quote$")
	public void NLAppStagingForQuote() throws InterruptedException  {
		String url = new PropertyReader().readProperty("NLappURLStagingQuotation");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		String[] path = url.split(".com");
		
		System.out.println("URL is: "+path[1]);
		Reporter.addStepLog("URL is: "+path[1]);
		System.out.println("USER IS ORDERING FROM NETHERLANDS STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();
	}
	
	@Given("^User access to UK store for staging to Request Quote$")
	public void UKAppStagingForQuote() throws InterruptedException  {
		String url = new PropertyReader().readProperty("UKappURLStagingQuotation");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		
		System.out.println("USER IS ORDERING FROM NETHERLANDS STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();
	}

	@Given("^User access to Spain store for staging$")
	public void ESAppStaging() throws InterruptedException  {
		String url = new PropertyReader().readProperty("ESappURLuat");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM SPAIN STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();
	}

	@Given("^User access to Belgium NL store$")
	public void BENLApp() throws InterruptedException {
		String url = new PropertyReader().readProperty("BENLappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM BELGIUM STORE USING NETHERLANDS LANGUAGE");
		UtilityMethods.wait20Seconds();

	}

	@Given("^User access to Belgium FR store$")
	public void BEFRApp() throws InterruptedException {
		String url = new PropertyReader().readProperty("BEFRappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM BELGIUM STORE USING FRENCH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to Estonia store$")
	public void EEApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("EEappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM ESTONIA STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to Bulgaria store$")
	public void BGApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("BGappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM BULGARIA STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to Panama store$")
	public void PAApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("PAappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM PANAMA STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to Uruguay store$")
	public void UYApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("UYappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM URUGUAY STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	
	@Given("^User access to Puerto Rico store$")
	public void PRApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("PRappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM PUERTO RICO STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	
	@Given("^User access to Puerto Rico ES store$")
	public void PRESApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("PRESappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM PUERTO RICO STORE USING SPANISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to Ecuador store$")
	public void ECApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("ECappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM ECUADOR STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to Poland store$")
	public void PLApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("PLappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM POLAND STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	

	@Given("^User access to France store$")
	public void FRApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("FRappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get(url);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ORDERING FROM FRANCE STORE USING ENGLISH LANGUAGE");
		System.out.println("USER IS ORDERING FROM FRANCE STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to France store for Staging$")
	public void FRAppStaging() throws InterruptedException {

		String url = new PropertyReader().readProperty("FRappURLForStaging");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ORDERING FROM FRANCE STORE USING ENGLISH LANGUAGE");
		System.out.println("USER IS ORDERING FROM FRANCE STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to France FR store$")
	public void FRFRApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("FRFRappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM FRANCE STORE USING FRENCH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}

	
	@Given("^User access to Spain store$")
	public void ESApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("ESappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM SPAIN STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	

	@Given("^User access to Spain ES store$")
	public void ESESApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("ESESappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM SPAIN STORE USING SPANISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to Luxembourg store$")
	public void LUApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("LUappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM LUXEMBOURG STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}

	@Given("^User access to Luxembourg FR store$")
	public void FRLUApp() throws InterruptedException {

		String url = new PropertyReader().readProperty("FRLUappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM LUXEMBOURG STORE USING FRENCH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to Italy store$")
	public void ITapp() throws InterruptedException {

		String url = new PropertyReader().readProperty("ITappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM ITALY STORE USING ENGLISH LANGUAGE");
		UtilityMethods.wait20Seconds();

	}
	
	@Given("^User access to Italy IT store$")
	public void ITITapp() throws InterruptedException {

		String url = new PropertyReader().readProperty("ITITappURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		System.out.println("USER IS ORDERING FROM ITALY STORE USING ITALIAN LANGUAGE");
		UtilityMethods.wait20Seconds();
	}
	
	@Given("^User access to D365$")
	public void D365app() throws InterruptedException {

		String url = new PropertyReader().readProperty("D365URL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.get(url);
		
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ACCESSING D365 ");
		System.out.println("USER IS ACCESSING D365");
		
		
		
		
		
	}
	

	@Given("^User access to CRM$")
	public void CRMapp() throws InterruptedException {

		String url = new PropertyReader().readProperty("CRMURL");
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get(url);
		
	//	Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ACCESSING CRM ");
		System.out.println("USER IS ACCESSING CRM");
		
		
		
		
		
	}
	

}



