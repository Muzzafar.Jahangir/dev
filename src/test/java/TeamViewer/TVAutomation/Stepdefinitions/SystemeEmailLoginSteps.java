package TeamViewer.TVAutomation.Stepdefinitions;

//import TeamViewer.TVAutomation.Pages.CheckOutPage;
import TeamViewer.TVAutomation.Pages.SystemEmailLogin;
import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.PropertyReader;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.cucumber.listener.Reporter;

import java.io.File;
//import java.awt.AWTException;
import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;

import TeamViewer.TVAutomation.Utils.UtilityMethods;


public class SystemeEmailLoginSteps  extends DriverFactory {


	private SystemEmailLogin systemLoginPage;

	@Before
	public void setUp() throws Exception {
		systemLoginPage = new SystemEmailLogin(driver);

	}

	@And("^I Login Email$")
	public void loginSystemsEmail() throws Throwable{

		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS NAVIGATING TO EMAIL");
		System.out.println("USER IS NAVIGATING TO EMAIL ");
		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		String url = new PropertyReader().readProperty("EmailURL");
		driver.navigate().to(url);

		UtilityMethods.waitForPageLoadAndPageReady();

		 UtilityMethods.wait10Seconds();
		
		systemLoginPage.loginToEmail();

		UtilityMethods.waitForPageLoadAndPageReady();

		systemLoginPage.clickEmailItem();

		systemLoginPage.clickEmailItemActivation();

	}
	
	@And("^I Login Email for Invoice order$")
	public void loginSystemsEmailForInvoiceOrder() throws Throwable{



		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS NAVIGATING TO EMAIL");
		System.out.println("USER IS NAVIGATING TO EMAIL ");
		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		String url = new PropertyReader().readProperty("EmailURL");
		driver.navigate().to(url);

		UtilityMethods.waitForPageLoadAndPageReady();

		 UtilityMethods.wait10Seconds();
		
		systemLoginPage.loginToEmail();

		UtilityMethods.waitForPageLoadAndPageReady();

		systemLoginPage.clickInvoiceReminderEmailItem();

	

	}

	@And("^I clear Inbox$")
	public void clearInbox() throws Throwable{

		systemLoginPage.deleteEmailItemsInInbox();
	}



//	@And("^I add Trusted Device$")
//	public void addTrustedDevice() throws Throwable{
//
//		systemLoginPage.clickDeviceAuthorizationItem();
//	}




}

