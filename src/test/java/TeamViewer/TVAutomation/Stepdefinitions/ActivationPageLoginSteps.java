package TeamViewer.TVAutomation.Stepdefinitions;
import static TeamViewer.TVAutomation.Utils.DataPool.readExcelData;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import TeamViewer.TVAutomation.DataProvider.TestData;
import TeamViewer.TVAutomation.Pages.ActivationPageLogin;
import TeamViewer.TVAutomation.Pages.ActivationPageSignUp;
import TeamViewer.TVAutomation.Pages.CheckOutPage;
import TeamViewer.TVAutomation.Strings.StringsToVerify;
import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;

public class ActivationPageLoginSteps extends DriverFactory {


	private ActivationPageLogin testPage;
	private ActivationPageSignUp testPage1;

	@Before
	public void setUp() throws Exception {
		testPage = new ActivationPageLogin(driver);
		testPage1 = new ActivationPageSignUp(driver);

	}
	
	@And("^I SignUp for TV account$")
	public void signUpForTvAccount() throws Throwable{
		testPage1.clickActivation();
		testPage1.enterSignUpData();
	}
	
	@And("^I SignUp for MCO Portal$")
	public void signUpForMCOPortal() throws Throwable{

		testPage1.enterSignUpDataForMCOPortal();
	
	}
	
	@And("^I switch and login to MCO Portal$")
	public void switchAndLoginMCOPortal() throws Throwable{
		
		testPage1.switchAndLoginMCOPortal();
		
	}

	
		
		@And("^I Validate License is Added$")
		public void licenseAddedValidation() throws Throwable{
			testPage.validateLicense();
			

		}
		
		
		@And("^I delete Account of TeamViewer$")
		public void deleteTvAccount() throws Throwable{
		
			testPage.deleteAccount();
			

		}
	}
