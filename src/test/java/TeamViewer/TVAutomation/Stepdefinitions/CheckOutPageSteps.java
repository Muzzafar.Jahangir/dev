
package TeamViewer.TVAutomation.Stepdefinitions;


import static TeamViewer.TVAutomation.Utils.DataPool.readExcelData;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

import org.apache.maven.plugin.logging.SystemStreamLog;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;

import TeamViewer.TVAutomation.DataProvider.TestData;
import TeamViewer.TVAutomation.Pages.CheckOutPage;
import TeamViewer.TVAutomation.Strings.StringsToVerify;
import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;

public class CheckOutPageSteps extends DriverFactory {

	
	public static String emailID = "";

	private CheckOutPage checkoutPageObject;

	@Before
	public void setUp() throws Exception {
		checkoutPageObject = new CheckOutPage(driver);

	}

	@And("^I add voucher$")
	public void add_voucher() throws Throwable{

		checkoutPageObject.clickVouch();

	}


	@And("^I select check box$")
	public void select_checkbox() throws Throwable{

		checkoutPageObject.clickCheckBox();
	}

	@And("^I select Credit Card as Payment Method$")
	public void select_creditcardRadioButton() throws Throwable{

		checkoutPageObject.clickCreditCardRadioButton();


	}
	
	
	@And("^I select paypal as payment method$")
	public void select_PayPalRadioButton() throws Throwable{

		checkoutPageObject.clickPayPalButton();

	}
	
	@And("^I click On Proceed Next Button$")
	public void clickProceedButton() throws Throwable{

		checkoutPageObject.clickProceedButton();

	}
	
	
	@And("^I enter paypal credentials for ERP Flow$")
	public void enterPayPalCredentialsForERP() throws Throwable{

		checkoutPageObject.enterPayPalCredentialsForERP();

	}
	
	@And("^I select invoice as payment method$")
	public void select_payment() throws Throwable{

		checkoutPageObject.clickInvoicePaymentButton();

	}


	@And("^I Place Order$")
	public void place_order() throws Throwable{

		checkoutPageObject.clickPO();

	}
	
	@And("^I Scroll Down$")
	public void scrollDown() throws Throwable{
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,2000)");
		
	}

	

	@Then("^I verify duplicate email error appeared$")
	public void verify_duplicate_email_error_appeared() throws Throwable {
		UtilityMethods.validateAssertEqual(checkoutPageObject.getDuplicateEmailError(), StringsToVerify.MESSAGE_DUPLICATE_EMAIL);
	}



	@Then("^I verify invalid VAT error appeared$")
	public void verify_invalid_VAT_error_appeared() throws Throwable {
		UtilityMethods.validateAssertEqual(checkoutPageObject.getInValidVoucherError(), StringsToVerify.MESSAGE_INVALID_VAT);


	}

	@And("^I Apply Voucher$")
	public void apply_voucher() throws Throwable{
		checkoutPageObject.clickApply();

	}

	@Then("^I verify valid Voucher appeared$")
	public void verify_valid_voucher_appeared() throws Throwable {
		UtilityMethods.validateAssertEqual(checkoutPageObject.getValidVoucher(), StringsToVerify.MESSAGE_VALID_VOUCHER);
	}


	@Then("^I verify invalid voucher error appeared$")
	public void verify_invalid_voucher_error_appeared() throws Throwable {
		UtilityMethods.validateAssertEqual(checkoutPageObject.getInValidVoucherError(), StringsToVerify.MESSAGE_INVALID_VOUCHER);
	}




	@And("^I select the State$")
	public void select_State() throws Throwable {

		Select select = new Select(checkoutPageObject.States);
		select.selectByIndex(4);

	}


	@And("^I enter Japan data$")
	public void enter_JP_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_JP_DATA);	
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}



	

	@And("^I enter Korea data$")
	public void enter_KR_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_KR_DATA);	
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}

	
	@And("^I enter Poland data$")
	public void enter_PL_EN_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_PL_DATA);	
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}
	@And("^I enter Poland VAT$")
	public void enter_PL_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_PL_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}

	
	@And("^I enter Panama data$")
	public void enter_PA_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_PA_DATA);	
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}
	@And("^I enter Puerto Rico data$")
	public void enter_PR_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_PR_DATA);	
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}

	@And("^I enter Netherlands data$")
	public void enter_NL_EN_data() throws Throwable {
		
		Reporter.addStepLog("USER IS ENTERING BILLING DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_NL_DATA);

		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		
		
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				//row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		//checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		try{
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		}
		catch (Exception e)
		{
			checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address1,
					row.get(TestData.ADDRESS_COLUMN).toString().trim());
		}
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
		
		
		Reporter.addStepLog("EMAIL: "+row.get(TestData.EMAIL_COLUMN).toString().trim());
		Reporter.addStepLog("FIRST NAME: "+row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		Reporter.addStepLog("COMPANY NAME: "+row.get(TestData.COMPANY_COLUMN).toString().trim());
		Reporter.addStepLog("ADDRESS: "+row.get(TestData.ADDRESS_COLUMN).toString().trim());
		Reporter.addStepLog("CITY NAME: "+row.get(TestData.CITY_COLUMN).toString().trim());
		Reporter.addStepLog("ZIP CODE: "+row.get(TestData.ZIP_COLUMN).toString().trim());
		Reporter.addStepLog("PHONE NUMBER: "+row.get(TestData.PHONE_COLUMN).toString().trim());
		
	}
	
	@And("^I enter Ireland data$")
	public void enter_IR_EN_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING BILLING DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_IR_DATA);

		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		
		
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				//row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		//checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		try{
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		}
		catch (Exception e)
		{
			checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address1,
					row.get(TestData.ADDRESS_COLUMN).toString().trim());
		}
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
		
		Reporter.addStepLog("EMAIL: "+row.get(TestData.EMAIL_COLUMN).toString().trim());
		Reporter.addStepLog("FIRST NAME: "+row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		Reporter.addStepLog("COMPANY NAME: "+row.get(TestData.COMPANY_COLUMN).toString().trim());
		Reporter.addStepLog("ADDRESS: "+row.get(TestData.ADDRESS_COLUMN).toString().trim());
		Reporter.addStepLog("CITY NAME: "+row.get(TestData.CITY_COLUMN).toString().trim());
		Reporter.addStepLog("ZIP CODE: "+row.get(TestData.ZIP_COLUMN).toString().trim());
		Reporter.addStepLog("PHONE NUMBER: "+row.get(TestData.PHONE_COLUMN).toString().trim());
		
	}
	
	@And("^I enter UK data$")
	public void enter_UK_EN_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING BILLING DETAILS NOW");
		System.out.println("USER IS ENTERING BILLING DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_UK_DATA);

		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		
		
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				//row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		//checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		try{
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		}
		catch (Exception e)
		{
			checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address1,
					row.get(TestData.ADDRESS_COLUMN).toString().trim());
		}
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	
		Reporter.addStepLog("EMAIL: "+row.get(TestData.EMAIL_COLUMN).toString().trim());
		Reporter.addStepLog("FIRST NAME: "+row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		Reporter.addStepLog("COMPANY NAME: "+row.get(TestData.COMPANY_COLUMN).toString().trim());
		Reporter.addStepLog("ADDRESS: "+row.get(TestData.ADDRESS_COLUMN).toString().trim());
		Reporter.addStepLog("CITY NAME: "+row.get(TestData.CITY_COLUMN).toString().trim());
		Reporter.addStepLog("ZIP CODE: "+row.get(TestData.ZIP_COLUMN).toString().trim());
		Reporter.addStepLog("PHONE NUMBER: "+row.get(TestData.PHONE_COLUMN).toString().trim());
		
	}
	
	@And("^I enter Argentina's data$")
	public void enter_AR_EN_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING BILLING DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_AR_DATA);

		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		
		
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				//row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		//checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		try{
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		}
		catch (Exception e)
		{
			checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address1,
					row.get(TestData.ADDRESS_COLUMN).toString().trim());
		}
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
		UtilityMethods.wait1Seconds();
		new Select(driver.findElement(By.name("region_id"))).selectByIndex(1);
		
		Reporter.addStepLog("EMAIL: "+row.get(TestData.EMAIL_COLUMN).toString().trim());
		Reporter.addStepLog("FIRST NAME: "+row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		Reporter.addStepLog("COMPANY NAME: "+row.get(TestData.COMPANY_COLUMN).toString().trim());
		Reporter.addStepLog("ADDRESS: "+row.get(TestData.ADDRESS_COLUMN).toString().trim());
		Reporter.addStepLog("CITY NAME: "+row.get(TestData.CITY_COLUMN).toString().trim());
		Reporter.addStepLog("ZIP CODE: "+row.get(TestData.ZIP_COLUMN).toString().trim());
		Reporter.addStepLog("PHONE NUMBER: "+row.get(TestData.PHONE_COLUMN).toString().trim());
		
	}
	
	@And("^I enter Netherlands VAT$")
	public void enter_NL_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_NL_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}
	
	

	@And("^I enter Albania data$")
	public void enter_AL_EN_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_AL_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}

	@And("^I enter Belgium data$")
	public void enter_BE_data() throws Throwable {
	//	Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_BE_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}
	
	@And("^I enter Belgium VAT$")
	public void enter_BG_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_BE_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}
	
	@And("^I enter Ireland VAT$")
	public void enter_IR_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_IR_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}
	
	@And("^I enter UK VAT$")
	public void enter_UK_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_UK_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}
	
	@And("^I enter Argentina VAT$")
	public void enter_ARG_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_AR_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}
	
	
	@And("^I enter Bulgaria data$")
	public void enter_BG_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_BG_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}
	
	@And("^I enter Bulgaria VAT$")
	public void enter_BL_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_BG_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}
	
	
	
	@And("^I enter Estonia data$")
	public void enter_EE_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_EE_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}
	
	@And("^I enter Estonia VAT$")
	public void enter_EE_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_EE_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}
	
	@And("^I enter Uruguay data$")
	public void enter_UY_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_UY_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}
	
	@And("^I enter Portugal data$")
	public void enter_PT_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_PT_DATA);	
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}
	@And("^I enter Portugal VAT$")
	public void enter_PT_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_PT_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}
	
	
	@And("^I enter Ecuador data$")
	public void enter_EC_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_EC_DATA);	
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}
	
	@And("^I Enter Details in ECOM to process$")
	public void enterDetailsInEcomToprocess() throws Throwable {
		
		checkoutPageObject.enterECOMDataForPayment();
		
	}
	
	
	@And("^I get url and verify$")
	public void getURLtoVerify() throws Throwable {
		
		checkoutPageObject.getURLandVerify();
		
	}
	
	@Then("^I got error Pop up$")
	public void gettingPopUpError() throws Throwable{
		
		checkoutPageObject.verifyErrorPopUp();
		
	}
	
	@And("^I get quantity and Prices of Products$")
	public void getQuantityAndPrices() throws Throwable {
		
		
		checkoutPageObject.getPricesAndQuantityOfProducts();
		
	}
	
	@And("^I get quantity and Prices of Products for All adds on$")
	public void getQuantityAndPricesForBusinessLicense() throws Throwable {
		
		
		checkoutPageObject.getPricesAndQuantityOfProductsForBusinessLicense();
		
	}
	
	@And("^I get quantity and Prices of Products Japan$")
	public void getQuantityAndPricesForJapan() throws Throwable {
		
		
		checkoutPageObject.getPricesAndQuantityOfProductsForJapan();
		
	}
	
	@And("^I get quantity and Prices of US Countries$")
	public void getQuantityAndPricesForUSCountries() throws Throwable {
		
		
		checkoutPageObject.getPricesAndQuantityOfProductsForUSCountries();
		
	}
	
	@And("^I enter France data$")
	public void enter_FR_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_FR_DATA);	
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}
	
	@And("^I enter France VAT$")
	public void enter_FR_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_FR_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}
	
	@And("^I enter Spain data$")
	public void enter_ES_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_ES_DATA);	
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}
	
	
	@And("^I enter Spain VAT$")
	public void enter_ES_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_ES_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}
	
	
	@And("^I enter Luxembourg data$")
	public void enter_LU_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_LU_DATA);	
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}
	
	@And("^I enter Luxembourg VAT$")
	public void enter_LU_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_LU_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}
	
	@And("^I enter Italy data$")
	public void enter_IT_data() throws Throwable {
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING THE BILLINGS DETAILS NOW");
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_IT_DATA);	
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Email,
				row.get(TestData.EMAIL_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Company,
				row.get(TestData.COMPANY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Address,
				row.get(TestData.ADDRESS_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.City,
				row.get(TestData.CITY_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.ZIP,
				row.get(TestData.ZIP_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.Phone,
				row.get(TestData.PHONE_COLUMN).toString().trim());
	}
	
	@And("^I enter Italy VAT$")
	public void enter_IT_VAT() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_IT_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.vatNumber,
				row.get(TestData.VAT_COLUMN).toString().trim());
		
		checkoutPageObject.vatNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait5Seconds();
	}
	
	
	@And("^I validate payment is successful$")
	public void validatePaymentIsSuccessful() throws Throwable{
		
		checkoutPageObject.verifyPaymentIsSuccessful();
	}
	
	@And("^I enter Visa Card Data$")
	public void add_CreditCardDataforNetherlands() throws Throwable{

		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(checkoutPageObject.CC_Iframe));

		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_NL_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_Number,
				row.get(TestData.CREDIT_CARD_NUMBER).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_CVV,
				row.get(TestData.CREDIT_CARD_CVV).toString().trim());

//		checkoutPageObject.CC_Month.click();
//		Select select = new Select(checkoutPageObject.CC_Month);
//		select.selectByIndex(10);
//
//		checkoutPageObject.CC_Year.click();
//		Select select1 = new Select (checkoutPageObject.CC_Year);
//		select1.selectByIndex(10);
		
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_Month,
				row.get(TestData.CREDIT_CARD_MONTH).toString().trim());
		
		checkoutPageObject.CC_Month.click();
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_Year,
				row.get(TestData.CREDIT_CARD_YEAR).toString().trim());


		driver.switchTo().defaultContent();

		UtilityMethods.wait3Seconds();
		
		try{
			
			if(checkoutPageObject.proceedPayment.isDisplayed()){
				
				checkoutPageObject.proceedPayment.click();
			}
		}
		catch(Exception e){
			
			
			System.out.println("PROCEED PAYMENT BUTTON DOESN'T EXIST");
		}

	}
	
	@And("^I enter American Card Data$")
	public void add_AmericanCardDataforNetherlands() throws Throwable{

		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(checkoutPageObject.CC_Iframe));

		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_NL_DATA_AMERICAN);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_Number,
				row.get(TestData.CREDIT_CARD_NUMBER).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_CVV,
				row.get(TestData.CREDIT_CARD_CVV).toString().trim());

//		checkoutPageObject.CC_Month.click();
//		Select select = new Select(checkoutPageObject.CC_Month);
//		select.selectByIndex(10);
//
//		checkoutPageObject.CC_Year.click();
//		Select select1 = new Select (checkoutPageObject.CC_Year);
//		select1.selectByIndex(10);

		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_Month,
				row.get(TestData.CREDIT_CARD_MONTH).toString().trim());
		
		checkoutPageObject.CC_Month.click();
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_Year,
				row.get(TestData.CREDIT_CARD_YEAR).toString().trim());
		
		driver.switchTo().defaultContent();

		UtilityMethods.wait3Seconds();
		
		try{
			
			if(checkoutPageObject.proceedPayment.isDisplayed()){
				
				checkoutPageObject.proceedPayment.click();
			}
		}
		catch(Exception e){
			
			
			System.out.println("PROCEED PAYMENT BUTTON DOESN'T EXIST");
		}

	}
	
	@And("^I enter Master Card Data$")
	public void add_MasterCardDataforNetherlands() throws Throwable{

//		WebDriverWait wait = new WebDriverWait(driver, 15);
	//	wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(checkoutPageObject.CC_Iframe));
		new WebDriverWait(driver,20).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("wirecard-integrated-payment-page-frame")));
		UtilityMethods.wait5Seconds();
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_NL_DATA_MASTER);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_Number,
				row.get(TestData.CREDIT_CARD_NUMBER).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_CVV,
				row.get(TestData.CREDIT_CARD_CVV).toString().trim());


		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_Month,
				row.get(TestData.CREDIT_CARD_MONTH).toString().trim());
		
		checkoutPageObject.CC_Month.click();
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_Year,
				row.get(TestData.CREDIT_CARD_YEAR).toString().trim());

		driver.switchTo().defaultContent();

		UtilityMethods.wait3Seconds();
		
		try{
			
			if(checkoutPageObject.proceedPayment.isDisplayed()){
				
				checkoutPageObject.proceedPayment.click();
			}
		}
		catch(Exception e){
			
			
			System.out.println("PROCEED PAYMENT BUTTON DOESN'T EXIST");
		}
	

	}
	
	@And("^I enter Credit Card Data for Invoice payment on staging$")
	public void add_CreditCardData() throws Throwable{

		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(checkoutPageObject.CC_Iframe));

		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_NL_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_FirstName,
				row.get(TestData.FIRST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_LastName,
				row.get(TestData.LAST_NAME_COLUMN).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_Number,
				row.get(TestData.CREDIT_CARD_NUMBER).toString().trim());
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.CC_CVV,
				row.get(TestData.CREDIT_CARD_CVV).toString().trim());

		checkoutPageObject.CC_Month.click();
		Select select = new Select(checkoutPageObject.CC_Month);
		select.selectByIndex(10);

		checkoutPageObject.CC_Year.click();
		Select select1 = new Select (checkoutPageObject.CC_Year);
		select1.selectByIndex(10);

		driver.switchTo().defaultContent();

		UtilityMethods.wait3Seconds();
		
		try{
			
			if(checkoutPageObject.proceedPayment.isDisplayed()){
				
				checkoutPageObject.proceedPayment.click();
				System.out.println("USER HAS CLICKED ON PROCESSING PAYMENT BUTTON");
			}
		}
		catch(Exception e){
			
			
			System.out.println("PROCEED PAYMENT BUTTON DOESN'T EXIST");
		}

	}
	
	@And("^I enter PayPal Credentials$")
	public void enterPayPalCredentials() throws Throwable{
		
		checkoutPageObject.enterPayPalCredentials();
		
	}

//	@And("^I Validate the Calculations for Korea$")
//	public void validateTaxIsCorrectforKorea() throws Throwable {
//		
//		checkoutPageObject.calculateTheTaxAndTotalsforKorea();
//
//	}
	


	@And("^Validating the Calculations$")
	public void validateTaxIsCorrect() throws Throwable {
		
		checkoutPageObject.calculateTheTaxAndTotals();

	}


	@And("^I enter NL Voucher$")
	public void enter_Voucher() throws Throwable {
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET, TestData.CREATE_JP_DATA);
		checkoutPageObject.enterTextInTextBox(checkoutPageObject.VoucherNumber,
				row.get(TestData.VOUCHER_COLUMN).toString().trim());

	}



	




}
