
package TeamViewer.TVAutomation.Stepdefinitions;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import TeamViewer.TVAutomation.Pages.AdOnPage;
import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class AdOnPageSteps extends DriverFactory {

	private AdOnPage addOnPageObject;

	@Before
	public void setUp() throws Exception {
		addOnPageObject = new AdOnPage(driver);

	}
	
	@Then("^I Click on Next button$")
	public void a_click_test() throws Throwable{
		
		addOnPageObject.clicknext();
		
	}
	
	@Then("^I Refresh Browser$")
	public void refreshBrowser() throws Throwable{
	
	String refreshingURL = driver.getCurrentUrl().substring(0,71);
	//above, truncating the URL to cart page
	System.out.println(refreshingURL);
	UtilityMethods.wait1Seconds();	
	UtilityMethods.wait10Seconds();
	driver.navigate().to(refreshingURL);
	WebDriverWait wait = new WebDriverWait(driver,20);
	wait.until(ExpectedConditions.visibilityOf(addOnPageObject.Next));

		
	}
	
	
	@Then("^I Route Back$")
	public void routeBack() throws Throwable{
		
		addOnPageObject.routeBack();
		
	}
	
	
	
	@Then("^I Click on Next button for Staging$")
	public void a_click_test_Staging() throws Throwable{
		
		addOnPageObject.clicknext_staging();
		
	}
	
	@And("^I add Support for Mobile Devices AddsOn For Business License$")
	public void add_MobileDeviceSupport_AddsOn() throws Throwable{
		
		addOnPageObject.addMobileDeviceSupportAddOnForBusiness();
	}
	
	@And("^I add All the Business License AddOns$")
	public void add_All_BusinessLicense_AddsOn() throws Throwable{
		
		addOnPageObject.addAllBusinessLicenseAddOns();
		
	
	}
	
	@And("^I add All Premium/Corporate License AddOns$")
	public void add_All_PemiumCorporateLicense_Addson()throws Throwable{
		
		addOnPageObject.addAllPremiumCorporateLicenseAddson();	
	
	}
	
	@And("^I add All Premium Quote AddOns$")
	public void add_All_Pemium_Quote_Addson()throws Throwable{
		
		addOnPageObject.addAllPremiumQuoteAddson();	
	
	}
	
	@And("^I add Monitor,Endpoint,Backup and PilotAddon$")
	public void addMonitorEndPointBackupandPilot()throws Throwable{
		
		addOnPageObject.addMonitorEndpointPilotAndBackup();	
	
	}
	
	
	
	
	
}