package TeamViewer.TVAutomation.Stepdefinitions;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import cucumber.api.java.en.And;

public class KillBrowser extends DriverFactory {

	
	@And("^I Kill Browser$")
	public void killBrowser() throws Throwable{
		
		driver.quit();
		
	}
	
	
}
