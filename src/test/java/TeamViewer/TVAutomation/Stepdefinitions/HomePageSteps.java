
package TeamViewer.TVAutomation.Stepdefinitions;


import org.openqa.selenium.By;

import TeamViewer.TVAutomation.Pages.HomePage;
import TeamViewer.TVAutomation.Utils.DriverFactory;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class HomePageSteps extends DriverFactory {

	private HomePage testPage;

	@Before
	public void setUp() throws Exception {
		testPage = new HomePage(driver);

	}




	@And("^I Close Cookie$")
	public void Click_Cookie() throws Throwable{

		testPage.clickCookie();

	}
	
	@And("^I Click for Netherlands Check out link$")
	public void ClickNetherlandCheckOutLink() throws Throwable{

		driver.findElement(By.xpath("//a[contains(@href,'https://uat.service.teamviewer.com/en-nl/quote/accept/index/qid')]")).click();

	}
	
	@And("^I Click for Ireland Check out link$")
	public void ClickIrelandCheckOutLink() throws Throwable{

		driver.findElement(By.xpath("//a[contains(@href,'https://uat.service.teamviewer.com/en-ie/quote/accept/index/qid')]")).click();

	}
	

	@When("^I Click on Remote Access Subscription Button$")
	public void a_click_test() throws Throwable{

		testPage.clickRemoteAccessSubscription();

	}
	
	@And("^I Change Langauage and Open WebShop$")
	public void changeLanguageandOpenWebShop() throws Throwable{

		testPage.changeLanguageandOpenWebShop();

	}
	
	@When("^I Click on Remote Access Subscription Button for Spain$")
	public void es_a_click_test() throws Throwable{

		testPage.clickRemoteAccessSubscriptionForSpain();

	}

	@When("^I Click on Business License Button$")
	public void clickBusinessLicenseButton() throws Throwable{

		testPage.clickBusinessLicenseButton();

	}
	
	@When("^I Click on Business License Button US Region$")
	public void clickBusinessLicenseButtonUS() throws Throwable{

		testPage.clickRemoteAccessSubscription();

	}
	
	@When("^I Click Premium Product$")
	public void clickPremiumProduct() throws Throwable{

		testPage.clickPremiumProduct();

	}


	@When("^I Click on Premium License Button$")
	public void clickPremiumLicenseButton() throws Throwable{

		testPage.clickPremiumLicenseButton();
	}	
	
	@When("^I Click on Premium License Button US Region$")
	public void clickPremiumLicenseButtonUS() throws Throwable{

		testPage.clickBusinessLicenseButton();
	}	

	@When("^I Click on Corporate License Button$")
	public void clickCorporateLicenseButton() throws Throwable{

		testPage.clickCorporateLicenseButton();
	}

	@When("^I Click on Corporate License Button US Region$")
	public void clickCorporateLicenseButtonUS() throws Throwable{

		testPage.clickPremiumLicenseButton();
	}






}
