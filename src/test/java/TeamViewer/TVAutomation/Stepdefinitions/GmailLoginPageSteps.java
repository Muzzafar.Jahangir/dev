package TeamViewer.TVAutomation.Stepdefinitions;

//import TeamViewer.TVAutomation.Pages.CheckOutPage;
import TeamViewer.TVAutomation.Pages.GmailLoginPage;
import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.PropertyReader;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

//import java.awt.AWTException;
import java.util.ArrayList;
//import java.util.HashMap;

//import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.Select;
//import org.openqa.selenium.support.ui.WebDriverWait;

//import TeamViewer.TVAutomation.DataProvider.TestData;
//import TeamViewer.TVAutomation.Pages.CheckOutPage;
//import TeamViewer.TVAutomation.Strings.StringsToVerify;
//import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;
//import cucumber.api.java.Before;
//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When

public class GmailLoginPageSteps  extends DriverFactory {

	
	  

		private GmailLoginPage gmailLoginPage;

		@Before
		public void setUp() throws Exception {
			gmailLoginPage = new GmailLoginPage(driver);

		}
		
		
		@Given("^I Access Gmail$")
		public void gmailLogin() throws Throwable {
			
			
			System.out.println("USER IS NAVIGATING TO GMAIL FOR LOGIN");
			
			
			((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
			
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(2));
			String url = new PropertyReader().readProperty("GmailURL");
			driver.navigate().to(url);
			UtilityMethods.waitForPageLoadAndPageReady();
			gmailLoginPage.clickSignIn();
		
		
		}

		@And("^I Login Gmail$")
		public void doLoginGmail() throws Throwable{
			UtilityMethods.waitForPageLoadAndPageReady();
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(3));
			UtilityMethods.wait5Seconds();
			gmailLoginPage.EnterEMail();
			gmailLoginPage.clickNextButton();
			UtilityMethods.wait5Seconds();
			gmailLoginPage.EnterPassword();
			gmailLoginPage.clickNextButton();
			UtilityMethods.wait7Seconds();
			gmailLoginPage.clickEmailItem();
			UtilityMethods.wait7Seconds();
			gmailLoginPage.clickEmailItemActivation();
			
		}
		
		@And("^I archive Emails$")
		public void archiveEmails() throws Throwable{
			
			gmailLoginPage.archiveEmails();
			
		}
}

