package TeamViewer.TVAutomation.Stepdefinitions;


import static TeamViewer.TVAutomation.Utils.DataPool.readExcelData;

import java.util.HashMap;

import TeamViewer.TVAutomation.DataProvider.TestData;
import TeamViewer.TVAutomation.Pages.AX365Page;
import TeamViewer.TVAutomation.Pages.CRMPage;
import TeamViewer.TVAutomation.Pages.CheckOutPage;
import TeamViewer.TVAutomation.Utils.DriverFactory;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class CRMPageSteps extends DriverFactory {
	
	private CRMPage crmpage;
	public static boolean argentinaFlag = false;
	public static boolean netherlandFlag =  false;
	public static boolean polandFlag =  false;
	public static boolean ukFlag =  false;
	
	
	@Before
	public void setUp() throws Exception {
		
		crmpage = new CRMPage(driver);

	}
	
	
	
	@When("^I Login CRM$")
	public void LoginCRM() throws Throwable{

		crmpage.doLoginInCRM();

	}
	
	@When("^I create new customer account$")
	public void createCustomeronCRM() throws Throwable{

		crmpage.createNewCustomeronCRM();

	}
	
	
	@And("^I create new Lead of NetherLands$")
	 public void createNewLead() throws Throwable{
		 
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET_CRM, TestData.CREATE_NL_DATA);
		crmpage.createNewLead(row.get(TestData.SALUTATION).toString().trim(),
				row.get(TestData.STREET).toString().trim(),
				row.get(TestData.ZIP_CODE).toString().trim(),
				row.get(TestData.CITY).toString().trim(),
				row.get(TestData.TAX_NUMBER).toString().trim(),
				row.get(TestData.TAX_NUMBER_VALUE).toString().trim(),
				row.get(TestData.SOURCE).toString().trim(),
				row.get(TestData.PRODUUCT_LINE).toString().trim(),
				row.get(TestData.BUSINESS_PHONE).toString().trim(),
				row.get(TestData.NAME_FIRST).toString().trim(),
				row.get(TestData.NAME_LAST).toString().trim(),
				row.get(TestData.TOPIC).toString().trim(),
				row.get(TestData.EMAIL).toString().trim(),
				row.get(TestData.COMPANY_NAME).toString().trim(),
				row.get(TestData.COUNTRYCRM).toString().trim());
		
	 }
	
	@And("^I create new Lead of UK$")
	 public void createNewLeadUK() throws Throwable{
		 
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET_CRM, TestData.CREATE_UK_DATA);
		crmpage.createNewLead(row.get(TestData.SALUTATION).toString().trim(),
				row.get(TestData.STREET).toString().trim(),
				row.get(TestData.ZIP_CODE).toString().trim(),
				row.get(TestData.CITY).toString().trim(),
				row.get(TestData.TAX_NUMBER).toString().trim(),
				row.get(TestData.TAX_NUMBER_VALUE).toString().trim(),
				row.get(TestData.SOURCE).toString().trim(),
				row.get(TestData.PRODUUCT_LINE).toString().trim(),
				row.get(TestData.BUSINESS_PHONE).toString().trim(),
				row.get(TestData.NAME_FIRST).toString().trim(),
				row.get(TestData.NAME_LAST).toString().trim(),
				row.get(TestData.TOPIC).toString().trim(),
				row.get(TestData.EMAIL).toString().trim(),
				row.get(TestData.COMPANY_NAME).toString().trim(),
				row.get(TestData.COUNTRYCRM).toString().trim());
		
	 }
	
	@And("^I create new Lead For Poland with VAT$")
	 public void createNewLeadPolandVAT() throws Throwable{
		 
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET_CRM, TestData.CREATE_PLT_DATA);
		crmpage.createNewLeadPolandVAT(row.get(TestData.SALUTATION).toString().trim(),
				row.get(TestData.STREET).toString().trim(),
				row.get(TestData.ZIP_CODE).toString().trim(),
				row.get(TestData.CITY).toString().trim(),
				row.get(TestData.TAX_NUMBER).toString().trim(),
				row.get(TestData.TAX_NUMBER_VALUE).toString().trim(),
				row.get(TestData.SOURCE).toString().trim(),
				row.get(TestData.PRODUUCT_LINE).toString().trim(),
				row.get(TestData.BUSINESS_PHONE).toString().trim(),
				row.get(TestData.NAME_FIRST).toString().trim(),
				row.get(TestData.NAME_LAST).toString().trim(),
				row.get(TestData.TOPIC).toString().trim(),
				row.get(TestData.EMAIL).toString().trim(),
				row.get(TestData.COMPANY_NAME).toString().trim(),
				row.get(TestData.COUNTRYCRM).toString().trim());
		
		
	 }
	
	@And("^I create new Lead For UK with VAT$")
	 public void createNewLeadUKVAT() throws Throwable{
		
		ukFlag = true;
		
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET_CRM, TestData.CREATE_UKT_DATA);
		crmpage.createNewLeadPolandVAT(row.get(TestData.SALUTATION).toString().trim(),
				row.get(TestData.STREET).toString().trim(),
				row.get(TestData.ZIP_CODE).toString().trim(),
				row.get(TestData.CITY).toString().trim(),
				row.get(TestData.TAX_NUMBER).toString().trim(),
				row.get(TestData.TAX_NUMBER_VALUE).toString().trim(),
				row.get(TestData.SOURCE).toString().trim(),
				row.get(TestData.PRODUUCT_LINE).toString().trim(),
				row.get(TestData.BUSINESS_PHONE).toString().trim(),
				row.get(TestData.NAME_FIRST).toString().trim(),
				row.get(TestData.NAME_LAST).toString().trim(),
				row.get(TestData.TOPIC).toString().trim(),
				row.get(TestData.EMAIL).toString().trim(),
				row.get(TestData.COMPANY_NAME).toString().trim(),
				row.get(TestData.COUNTRYCRM).toString().trim());
		
		
	 }
	
	@And("^I create new Lead of Argentina$")
		public void createNewLeadArgentina() throws Throwable{
			HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET_CRM, TestData.CREATE_AR_DATA);
			crmpage.createNewLead(row.get(TestData.SALUTATION).toString().trim(),
					row.get(TestData.STREET).toString().trim(),
					row.get(TestData.ZIP_CODE).toString().trim(),
					row.get(TestData.CITY).toString().trim(),
					row.get(TestData.TAX_NUMBER).toString().trim(),
					row.get(TestData.TAX_NUMBER_VALUE).toString().trim(),
					row.get(TestData.SOURCE).toString().trim(),
					row.get(TestData.PRODUUCT_LINE).toString().trim(),
					row.get(TestData.BUSINESS_PHONE).toString().trim(),
					row.get(TestData.NAME_FIRST).toString().trim(),
					row.get(TestData.NAME_LAST).toString().trim(),
					row.get(TestData.TOPIC).toString().trim(),
					row.get(TestData.EMAIL).toString().trim(),
					row.get(TestData.COMPANY_NAME).toString().trim(),
					row.get(TestData.COUNTRYCRM).toString().trim());
		
		
	 }
	
	@And("^I create new Lead for Ireland$")
	 public void createNewLeadIR() throws Throwable{
		 
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET_CRM, TestData.CREATE_IR_DATA);
		crmpage.createNewLead(row.get(TestData.SALUTATION).toString().trim(),
				row.get(TestData.STREET).toString().trim(),
				row.get(TestData.ZIP_CODE).toString().trim(),
				row.get(TestData.CITY).toString().trim(),
				row.get(TestData.TAX_NUMBER).toString().trim(),
				row.get(TestData.TAX_NUMBER_VALUE).toString().trim(),
				row.get(TestData.SOURCE).toString().trim(),
				row.get(TestData.PRODUUCT_LINE).toString().trim(),
				row.get(TestData.BUSINESS_PHONE).toString().trim(),
				row.get(TestData.NAME_FIRST).toString().trim(),
				row.get(TestData.NAME_LAST).toString().trim(),
				row.get(TestData.TOPIC).toString().trim(),
				row.get(TestData.EMAIL).toString().trim(),
				row.get(TestData.COMPANY_NAME).toString().trim(),
				row.get(TestData.COUNTRYCRM).toString().trim());
		
		// crmpage.createNewLead();
	 }
	
	@And("^I create new Lead For Argentina with VAT$")
	 public void createNewLeadArgentinaVAT() throws Throwable{
		 
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET_CRM, TestData.CREATE_ART_DATA);
		crmpage.createNewLeadPolandVAT(row.get(TestData.SALUTATION).toString().trim(),
				row.get(TestData.STREET).toString().trim(),
				row.get(TestData.ZIP_CODE).toString().trim(),
				row.get(TestData.CITY).toString().trim(),
				row.get(TestData.TAX_NUMBER).toString().trim(),
				row.get(TestData.TAX_NUMBER_VALUE).toString().trim(),
				row.get(TestData.SOURCE).toString().trim(),
				row.get(TestData.PRODUUCT_LINE).toString().trim(),
				row.get(TestData.BUSINESS_PHONE).toString().trim(),
				row.get(TestData.NAME_FIRST).toString().trim(),
				row.get(TestData.NAME_LAST).toString().trim(),
				row.get(TestData.TOPIC).toString().trim(),
				row.get(TestData.EMAIL).toString().trim(),
				row.get(TestData.COMPANY_NAME).toString().trim(),
				row.get(TestData.COUNTRYCRM).toString().trim());
		
		
	 }
	
	@And("^I create new Lead For Ireland with VAT$")
	 public void createNewLeadIRandVAT() throws Throwable{
		 
		HashMap<String, String> row = readExcelData(TestData.ADD_DATA_SHEET_CRM, TestData.CREATE_IR_DATA);
		crmpage.createNewLeadPolandVAT(row.get(TestData.SALUTATION).toString().trim(),
				row.get(TestData.STREET).toString().trim(),
				row.get(TestData.ZIP_CODE).toString().trim(),
				row.get(TestData.CITY).toString().trim(),
				row.get(TestData.TAX_NUMBER).toString().trim(),
				row.get(TestData.TAX_NUMBER_VALUE).toString().trim(),
				row.get(TestData.SOURCE).toString().trim(),
				row.get(TestData.PRODUUCT_LINE).toString().trim(),
				row.get(TestData.BUSINESS_PHONE).toString().trim(),
				row.get(TestData.NAME_FIRST).toString().trim(),
				row.get(TestData.NAME_LAST).toString().trim(),
				row.get(TestData.TOPIC).toString().trim(),
				row.get(TestData.EMAIL).toString().trim(),
				row.get(TestData.COMPANY_NAME).toString().trim(),
				row.get(TestData.COUNTRYCRM).toString().trim());
		
		// crmpage.createNewLead();
	 }
	
	@And("^I click new order in CRM and switch to ERP$")
	 public void createDirectOrder() throws Throwable{
		 
		 crmpage.clickNewOrderAndSwitchToERPFromCRM();
	 }
	
	
	 
	 @And("^I qualify qoute$")
	 public void qualifyCreateQoute() throws Throwable{
		 
		 crmpage.qualifyAndCreateNewQoute();
	 }
	 
	 @And("^I validate values in account$")
	 public void validateAccount() throws Throwable{
		 
		crmpage.validateAccount();
		 
	 }
	 
	 @And("^I validate values in contact and create quote$")
	 public void validateContact() throws Throwable{
		 
		 crmpage.validateContact();
		 
	 }

	 @And("^I create new customer account with country NLD and tax group STA$")
	 	public void createCustomeronCRM_NLD_STA() throws Throwable{


	 		HashMap<String, String> row = readExcelData(TestData.ADD_CUSTOMER_DATA_SHEET_CRM, TestData.CREATE_NL_DATA);
//	 		System.out.println(row.get(TestData.FIRST_NAME).toString()+row.get(TestData.STREET).toString()+row.get(TestData.ZIP_CODE).toString()+
//	 				row.get(TestData.CITY).toString());
			crmpage.createNewCustomeronCRM_NLD_STA(row.get(TestData.FIRST_NAME).toString().trim(),
					row.get(TestData.STREET).toString().trim(),
					row.get(TestData.CITY).toString().trim(),
					row.get(TestData.ZIP_CODE).toString().trim(),
					row.get(TestData.COUNTRY_NAME).toString().trim());
	
	 	}
	 
	 @And("^I create new customer account with country UK and tax group STA$")
	 	public void createCustomeronCRM_UK_STA() throws Throwable{

		 int sixe = driver.getWindowHandles().size();
		 System.out.println("Size of windows is "+sixe);
	 		HashMap<String, String> row = readExcelData(TestData.ADD_CUSTOMER_DATA_SHEET_CRM, TestData.CREATE_UK_DATA);
//	 		System.out.println(row.get(TestData.FIRST_NAME).toString()+row.get(TestData.STREET).toString()+row.get(TestData.ZIP_CODE).toString()+
//	 				row.get(TestData.CITY).toString());
			crmpage.createNewCustomeronCRM_NLD_STA(row.get(TestData.FIRST_NAME).toString().trim(),
					row.get(TestData.STREET).toString().trim(),
					row.get(TestData.CITY).toString().trim(),
					row.get(TestData.ZIP_CODE).toString().trim(),
					row.get(TestData.COUNTRY_NAME).toString().trim());
	
	 	}
	 
	 @And("^I create new customer account with country ARG and tax group STA$")
	 	public void createCustomeronCRM_ARG_STA() throws Throwable{

		 HashMap<String, String> row = readExcelData(TestData.ADD_CUSTOMER_DATA_SHEET_CRM, TestData.CREATE_AR_DATA);
	 		System.out.println(row.get(TestData.FIRST_NAME).toString()+row.get(TestData.STREET).toString()+row.get(TestData.ZIP_CODE).toString()+
	 				row.get(TestData.CITY).toString());
			crmpage.createNewCustomeronCRM_NLD_STA(row.get(TestData.FIRST_NAME).toString().trim(),
					row.get(TestData.STREET).toString().trim(),
					row.get(TestData.CITY).toString().trim(),
					row.get(TestData.ZIP_CODE).toString().trim(),
					row.get(TestData.COUNTRY_NAME).toString().trim());

	 	}
	 
	 @And("^I create new customer account with country IR and tax group STA$")
	 	public void createCustomeronCRM_IR_STA() throws Throwable{


	 		HashMap<String, String> row = readExcelData(TestData.ADD_CUSTOMER_DATA_SHEET_CRM, TestData.CREATE_IR_DATA);
	 		System.out.println(row.get(TestData.FIRST_NAME).toString()+row.get(TestData.STREET).toString()+row.get(TestData.ZIP_CODE).toString()+
	 				row.get(TestData.CITY).toString());
			crmpage.createNewCustomeronCRM_NLD_STA(row.get(TestData.FIRST_NAME).toString().trim(),
					row.get(TestData.STREET).toString().trim(),
					row.get(TestData.CITY).toString().trim(),
					row.get(TestData.ZIP_CODE).toString().trim(),
					row.get(TestData.COUNTRY_NAME).toString().trim());
	
	 	}
	 
	 @And("^I create new customer account with country IR and tax group RC$")
	 	public void createCustomeronCRM_IR_RC() throws Throwable{


		
		HashMap<String, String> row = readExcelData(TestData.ADD_CUSTOMER_DATA_SHEET_CRM, TestData.CREATE_IR_DATA);
//			System.out.println(row.get(TestData.FIRST_NAME).toString()+row.get(TestData.STREET).toString()+row.get(TestData.ZIP_CODE).toString()+
//			row.get(TestData.CITY).toString());
		crmpage.createNewCustomeronCRM_NLD_RC(row.get(TestData.FIRST_NAME).toString().trim(),
		row.get(TestData.STREET).toString().trim(),
		row.get(TestData.CITY).toString().trim(),
		row.get(TestData.ZIP_CODE).toString().trim(),
		row.get(TestData.COUNTRY_NAME).toString().trim());

	 	}
	 
	 @And("^I open accounts and search for STA accountID$")
	 public void openAndSearchSTAAccID() throws Throwable{
	 
		 crmpage.openAndSearchSTAAccID(CRMPage.accountNoLabel);
	 
	 }

	 @And("^I create new order$")
	 	public void createOrderERP() throws Throwable{

	 		crmpage.createNewOrderERP();

	 	}

	
	 @And("^I open accounts$")
	 	 public void openAndSearchAccID() throws Throwable{
	 	 
	 		 CRMPage.openAndSearchAccID();
	 	 
	 	 }
	 
	 @And("^I search for accountID of Argentina$")
 	 public void searchAccIDArgentina() throws Throwable{
 	 
		 argentinaFlag = true;
 		 CRMPage.searchACCId("20220978");
 	 
 	 }

	 @And("^I search for accountID of Netherlands$")
 	 public void searchAccIDNetherlands() throws Throwable{
 	 
		 netherlandFlag = true;
 	//	 crmpage.searchACCIdNetherlands("20220978");
 		CRMPage.searchACCId("20221172");
 	 
 	 }
	 
	 @And("^I search for accountID of UK$")
 	 public void searchAccIDUK() throws Throwable{
 	 
		  ukFlag = true;

		  if(AX365Page.transferFlow==true){
	 			CRMPage.openAndSearchAccID();
	 			CRMPage.searchACCId("20402488");
	 		
	 		 }
	 		 else{
	 			 
	 			CRMPage.searchACCId("20221735");
	 		 }
 	//	crmpage.searchACCIdNetherlands("20221654");
 	 
 	 }
	 
	 @And("^I search for accountID of Poland$")
 	 public void searchAccIDPoland() throws Throwable{
 	 
		 polandFlag = true;
 		 CRMPage.searchACCId("20220978");
 	 
 	 }
	 
	 
	 @And("^I open customer account and create opportunity$")
	 	 public void openCustomerAndCreateOpportunity() throws Throwable{
	 	 
	 		 crmpage.openCustomerAndCreateOpportunity();
	 	 
	 	 }

	 @And("^I generate quote from opportunity$")
	 	 public void generateQuoteFromOpportunity() throws Throwable{
	 		 
	 		 crmpage.generateQuoteFromOpportunity();
	 		 
	 	 }

	 @And("^I create new customer account with country NLD and tax group RC$")
	 	public void createCustomeronCRM_NLD_RC() throws Throwable{

	 	//	crmpage.createNewCustomeronCRM_NLD_RC();
	 		HashMap<String, String> row = readExcelData(TestData.ADD_CUSTOMER_DATA_SHEET_CRM, TestData.CREATE_NL_DATA);
//	 		System.out.println(row.get(TestData.FIRST_NAME).toString()+row.get(TestData.STREET).toString()+row.get(TestData.ZIP_CODE).toString()+
//	 				row.get(TestData.CITY).toString());
			crmpage.createNewCustomeronCRM_NLD_RC(row.get(TestData.FIRST_NAME).toString().trim(),
					row.get(TestData.STREET).toString().trim(),
					row.get(TestData.CITY).toString().trim(),
					row.get(TestData.ZIP_CODE).toString().trim(),
					row.get(TestData.COUNTRY_NAME).toString().trim());

	 	}
	 
	 @And("^I create new customer account with country UK and tax group RC$")
	 	public void createCustomeronCRM_UK_RC() throws Throwable{

	 
	 		HashMap<String, String> row = readExcelData(TestData.ADD_CUSTOMER_DATA_SHEET_CRM, TestData.CREATE_UK_DATA);

			crmpage.createNewCustomeronCRM_NLD_RC(row.get(TestData.FIRST_NAME).toString().trim(),
					row.get(TestData.STREET).toString().trim(),
					row.get(TestData.CITY).toString().trim(),
					row.get(TestData.ZIP_CODE).toString().trim(),
					row.get(TestData.COUNTRY_NAME).toString().trim());

	 	}


	 
	 
}
