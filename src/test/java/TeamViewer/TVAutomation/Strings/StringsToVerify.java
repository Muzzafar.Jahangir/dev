/**
 * Created by Farhan on July 25, 2017.
 */

package TeamViewer.TVAutomation.Strings;

public class StringsToVerify {

	// CHECK OUT PAGE
	public final static String MESSAGE_INVALID_VOUCHER = "This is not a valid coupon code. Please check the code you entered and try again.";
	public final static String MESSAGE_DUPLICATE_EMAIL = "This email address has already been used to purchase a license. Please contact our Sales department to edit your existing contract.";
	public final static String MESSAGE_INVALID_VAT = "Invalid VAT number";
	public final static String MESSAGE_VALID_VOUCHER = "Click here to remove voucher";
	public final static String MESSAGE_CONFIRMATION = "THANKS FOR YOUR PURCHASE!";
}