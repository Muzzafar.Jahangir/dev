package TeamViewer.TVAutomation.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;

public class TermsOfUse extends DriverFactory {

	@FindBy(xpath = "//h2[@class='pageheading_title pageheading_title-static pageheading_title-static-article' and text()='Terms of Use']")
	public WebElement pageHeader;

	@FindBy(xpath = "//div[@id='collapsible-12']/p[1]")
	public WebElement prohibitedUsesText;

	@FindBy(xpath = "//div[@id='collapsible-10']/p[1]")
	public WebElement siteContentAndUsesText;

	@FindBy(xpath = "//div[@id='collapsible-11']/p[1]")
	public WebElement intelectualPropertyText;

	@FindBy(xpath = "//div[@id='collapsible-13']/p[1]")
	public WebElement disclaimersAndLimitationOfLiabilityText;

	@FindBy(xpath = "//div[@id='collapsible-14']/p[1]")
	public WebElement personalInformationText;

	@FindBy(xpath = "//div[@id='collapsible-15']/p[1]")
	public WebElement indemnityText;

	@FindBy(xpath = "//div[@id='collapsible-16']/p[1]")
	public WebElement applicableLawText;

	@FindBy(xpath = "//div[@id='collapsible-17']/p[1]")
	public WebElement generalInformationText;

	@FindBy(xpath = "//div[@id='collapsible-18']/p[1]")
	public WebElement copyrightInfringementPolicyText;

	@FindBy(xpath = "//parent::li[@class='footer_legal_item']/a[@class='footer_legal_itemlink' and text()='Privacy Policy']")
	public WebElement privacyPolicyElement;

	@FindBy(xpath = "//parent::li[@class='footer_legal_item']/a[@class='footer_legal_itemlink' and text()='California Supply Chains Act']")
	public WebElement caleforniaSupplyChainActElement;

	@FindBy(xpath = "//a[@class='backtotop icon-topside' and text()='Top side']")
	public WebElement topSideElement;

	@FindBy(xpath = "//div[@class='footer_legal_fineprint']/ul/li[1]/a")
	public WebElement privacyHeader;

	@FindBy(xpath = "//parent::div[@class= 'pageheading']//h2[@class='pageheading_title pageheading_title-static pageheading_title-static-article']")
	public WebElement caleforniaSupplyChainActHeader;

	public TermsOfUse(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);
	}

	public WebElement prohabitedUse() {
		return new WebDriverWait(driver, 100).until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-12']")));
	}

	public WebElement siteContentsAndUses() {
		return new WebDriverWait(driver, 100).until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-10']")));
	}

	public WebElement intellectualProperty() {
		return new WebDriverWait(driver, 100).until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-11']")));
	}

	public WebElement disclaimersAndLimitationOfLiability() {
		return new WebDriverWait(driver, 100).until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-13']")));
	}

	public WebElement personalInformation() {
		return new WebDriverWait(driver, 100).until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-14']")));
	}

	public WebElement indemnity() {
		return new WebDriverWait(driver, 100).until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-15']")));
	}

	public WebElement applicableLaw() {
		return new WebDriverWait(driver, 100).until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-16']")));
	}

	public WebElement generalInformation() {
		return new WebDriverWait(driver, 100).until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-17']")));
	}

	public WebElement copyrightInfringementPolicy() {
		return new WebDriverWait(driver, 100).until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-18']")));
	}

	public WebElement getSubSections(String name) {
		return driver.findElement(By.xpath("//h3[@class='collapsible-header' and text()='" + name + "']"));
	}

	public WebElement getProhabitedUses() {
		return getSubSections("Prohibited Uses");
	}

	public WebElement getSiteContentsAndUses() {
		return getSubSections("Site Content & Use");
	}

	public WebElement getDisclaimersAndLimitationOfLiability() {
		return getSubSections("Disclaimers and Limitation of Liability");
	}

	public WebElement getIntellectualProperty() {
		return getSubSections("Intellectual Property");
	}

	public WebElement getPersonalInformation() {
		return getSubSections("Personal Information");
	}

	public WebElement getIndemnity() {
		return getSubSections("Indemnity");
	}

	public WebElement getApplicableLaw() {
		return getSubSections("Applicable Law");
	}

	public WebElement getGeneralInformation() {
		return getSubSections("General Information");
	}

	public WebElement getCopyrightInfringementPolicy() {
		return getSubSections("Copyright Infringement Policy (DMCA Notice)");
	}

	public void expandProhabitedUses() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-12']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-12']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-12']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void collapseProhabitedUses() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-12']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-12']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-12']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void expandSiteContentsAndUses() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-10']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-10']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-10']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void collapseSiteContentsAndUses() throws InterruptedException {
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-10']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-10']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-10']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void expandIntellectualProperty() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-11']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-11']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-11']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void collapseIntellectualProperty() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-11']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-11']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-11']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void expandDisclaimersAndLimitationOfLiability() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-13']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-13']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-13']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void collapseDisclaimersAndLimitationOfLiability() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-13']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-13']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-13']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void expandPersonalInformation() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-14']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-14']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-14']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void collapsePersonalInformation() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-14']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-14']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-14']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void expandIndemnity() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-15']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-15']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-15']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void collapseIndemnity() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-15']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-15']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-15']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void expandApplicableLaw() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-16']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-16']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-16']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void collapseApplicableLaw() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-16']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-16']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-16']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void expandGeneralInformation() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-17']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-17']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-17']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void collapseGeneralInformation() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-17']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-17']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-17']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void expandCopyrightInfringementPolicy() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-18']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-18']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-18']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void collapseCopyrightInfringementPolicy() throws InterruptedException {
		UtilityMethods.wait1Seconds();
		if (UtilityMethods.isElementPresent(By.xpath("//h3[@aria-controls='collapsible-18']"))) {
			new WebDriverWait(driver, 100).until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@aria-controls='collapsible-18']")));
		}
		driver.findElement(By.xpath("//h3[@aria-controls='collapsible-18']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	public void TermsOfUsePageDisplayed() throws Exception {

		UtilityMethods.waitForPageLoadAndPageReady();

		UtilityMethods.scrollToElement(pageHeader);

		UtilityMethods.validateAssertEqual("Terms of Use", pageHeader.getText());

		UtilityMethods.waitForPageLoadAndPageReady();

	}

}