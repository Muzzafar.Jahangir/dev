package TeamViewer.TVAutomation.Pages;


import java.io.File;
import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;

public class ActivationPageLogin extends DriverFactory {

	@FindBy (xpath = "//*[@id='UserName']")
	public WebElement TVActivationLoginField;

	@FindBy (xpath = "//*[@id='Password']")
	public WebElement TVActivationPassWordField;


	@FindBy(xpath = "//*[@id='btn-sign-in']")
	public WebElement TVActivationGetStartedButton;

	@FindBy (xpath= "//a[contains(text(),'Create an account now')]")
	public WebElement createNewAccountButton;

	@FindBy (xpath= "//*[contains(text(),'Not now')]")
	public WebElement notNowButtonOnTvStartPage;


	@FindBy (xpath= "//*[@class='tv-button  ']")
	public WebElement closePopUpDevTrunk;


	@FindBy(xpath = "//div[@id='headerRightMenu']")
	public WebElement devTrunkProfileIcon;

	@FindBy (xpath= "//*[contains(text(),'Edit profile')]")
	public WebElement editProfileLink;

	@FindBy (xpath= "//div[@class='col-md-12']/span[1]")
	public WebElement LicenseName;

	@FindBy (xpath= "(//span[text()='Business'])[1]")
	public WebElement businessLicenseName;

	@FindBy (xpath= "(//span[text()='Premium'])[1]")
	public WebElement premiumLicenseName;

	@FindBy (xpath= "(//span[text()='Corporate'])[1]")
	public WebElement corporateLicenseName;

	@FindBy (xpath= "//*[contains(text(),'Delete account')]")
	public WebElement deleteButton;

	@FindBy (xpath= "(//button[@class='tv-button  '])[2]")
	public WebElement yesButton;


	public void clickCreateAccountButton()  throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();
		createNewAccountButton.click();

	}


	public void validateLicense()  throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		try {
			WebDriverWait wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.elementToBeClickable(notNowButtonOnTvStartPage));
			if(notNowButtonOnTvStartPage.isDisplayed()){


				Reporter.addStepLog("USER HAS SIGNED UP TO TEAMVIEWER MANAGEMENT CONSOLE SUCCESSFULLY");
				Reporter.addStepLog("TEAMVIEWER MANAGEMENT CONSOLE ACCESSED SUCCESSFULLY");
				notNowButtonOnTvStartPage.click();
			}
			
		} catch (Exception e) {

			System.out.println("NOT NOW BUTTON IS NOT VISIBLE");

		}



		try {
			WebDriverWait wait = new WebDriverWait(driver,80);
			wait.until(ExpectedConditions.elementToBeClickable(closePopUpDevTrunk));
			if(closePopUpDevTrunk.isDisplayed()){
				closePopUpDevTrunk.click();
			}
			
		} catch (Exception e) {

			System.out.println("CLOSE BUTTON IS NOT VISIBLE");

		}

		UtilityMethods.wait30Seconds();
		devTrunkProfileIcon.click();
		UtilityMethods.wait3Seconds();
		editProfileLink.click();
		UtilityMethods.wait5Seconds();


		//for Production

		try{

			String lName = LicenseName.getText();

			if(lName.contains("Remote") || lName.contains("Business") || lName.contains("Premium") || lName.contains("Corporate")){

				UtilityMethods.validateAssertEqual("License Added", "License Added");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog(lName+" LICENSE IS ADDED TO THE USER");
				System.out.println(lName+" LICENSE IS ADDED TO THE USER");
			}


		}
		catch(Exception e){

			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("LICENSE IS NOT ADDED FOR THE USER");
			System.out.println("ACCESS LICENSE IS NOT ADDED FOR THE USER");
			UtilityMethods.validateAssertEqual("License Added", "License Not Added");

		}




	}

	public void deleteAccount() throws Throwable{

		WebDriverWait wait = new WebDriverWait(driver,120);
		wait.until(ExpectedConditions.elementToBeClickable(deleteButton));
		UtilityMethods.wait3Seconds();
		Actions action = new Actions(driver);
		action.moveToElement(deleteButton).click().perform();
		UtilityMethods.wait10Seconds();
		yesButton.click();
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS DELETED ITS ACCOUNT SUCCESSFULLY");
		System.out.println("USER HAS DELETED ITS ACCOUNT SUCCESSFULLY");
	}


	public ActivationPageLogin(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);

	}

}
