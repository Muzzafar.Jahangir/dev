package TeamViewer.TVAutomation.Pages;

import static TeamViewer.TVAutomation.Utils.UtilityMethods.clickEmailPromptIfAppears;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import org.openqa.selenium.support.ui.WebDriverWait;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;

public class MainPage extends DriverFactory {

	protected WebElement pageHeading() {
		return new WebDriverWait(driver, 15)
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("img[class='mast_logo_img']")));
	}
	public void isLoaded() throws Error, InterruptedException {
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();UtilityMethods.wait3Seconds();
		clickEmailPromptIfAppears();
		UtilityMethods.waitForPageLoadAndPageReady();
	}


public MainPage(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);
	}
	}