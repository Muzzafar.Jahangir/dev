package TeamViewer.TVAutomation.Pages;
import java.io.File;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;

public class SystemEmailLogin extends DriverFactory {


	public static String finalOrderID;
	@FindBy (xpath = "(//*[@class='h-c-header__nav-li-link '])[4]")
	public WebElement EmailsignIn;

	@FindBy (xpath = "//*[@name='identifier']")
	public WebElement emailFieldLogin;

	@FindBy (xpath = "//*[@name='password']")
	public WebElement passwordFieldLogin;

	@FindBy (xpath = "//span[contains(text(),'Next')]")
	public WebElement nextButton;


	@FindBy (xpath = "//a[@id = 'lo']")
	public WebElement signOutLink;


	@FindBy (xpath = "(//*[contains(text(),'Your TeamViewer order ') or contains(text(),'Su confirmación ') or contains(text(),'Sua confirmação d') or contains(text(),'Confirmation de votre co') or contains(text(),'Uw TeamViewer-beste')])[1]")
	public WebElement clickOrderConfirmationEmailItem;
	
	@FindBy (xpath = "(//*[contains(text(),'Invoice Reminder Email')])[1]")
	public WebElement clickInvoiceReminderEmailItem;

	@FindBy (xpath = "(//*[@title='Inbox'])[1]")
	public WebElement openInbox;

	@FindBy (xpath = "(//*[contains(text(),'Device authorization needed')])[1]")
	public WebElement deviceAuthorizationItem;

	@FindBy (xpath = "//a[@title='Options']")
	public WebElement optionButtonOnDifferentOutLookVersion;

	@FindBy (xpath = "(//input[@type='checkbox'])[1]")
	public WebElement selectAllEmails;


	@FindBy (id = "selfld")
	public WebElement chooseInboxToMove;

	@FindBy (xpath = "//option[contains(text(),'Inbox')]")
	public WebElement dropDownValueInbox;

	@FindBy (xpath = "//option[contains(text(),'Archive')]")
	public WebElement dropDownValueArchive;

	@FindBy (id = "lnkHdrmove")
	public WebElement moveToInboxButton;


	@FindBy (xpath = "//*[contains(text(),'Add to trusted devices')]")
	public WebElement addToTrustedDevicesLink;

	@FindBy (xpath = "//button[@type='submit']")
	public WebElement trustButtonToAddDevice;




	@FindBy (xpath = "(//*[contains(text(),'Activate TeamViewer Remote Acce')])[1]")
	public WebElement clickEmailItemActivation;

	@FindBy (xpath = "//*[contains(text(),'Activate now.')]")
	public WebElement activateLink;



	@FindBy (xpath = "(//span[contains(text(),'Archive')])[3]")
	public WebElement addToArchive;


	@FindBy (xpath = "//input[@id='username']")
	public WebElement userNameFieldEmail ;

	@FindBy (xpath = "//input[@id='password']")
	public WebElement passwordFieldEmail;

	@FindBy (xpath = "(//span[@class='signinTxt'])[1]")
	public WebElement signInButtonEmail ;

	@FindBy (xpath = "//*[@title='Junk Email']")
	public WebElement junkFolderEmail ;

	@FindBy (xpath = "(//*[contains(text(),'Device successfully added')])[2]")
	public WebElement deviceAdded ;


	@FindBy (id = "btnmv")
	public WebElement moveButton  ;

	@FindBy (xpath = "//tr[@class='x_email-intro']/td/*[2]")
	public WebElement orderNumberLine;

	@FindBy (xpath = "(//*[@class='hdtxnr'])[1]")
	public WebElement timeStampOfActivateLicenseEmail;



	public void loginToEmail() throws Throwable{

		UtilityMethods.waitForPageLoadAndPageReady();
		userNameFieldEmail.sendKeys("teamviewer.automation@systemsltd.com");
		UtilityMethods.wait3Seconds();
		passwordFieldEmail.sendKeys("abcd@1234");
		UtilityMethods.wait3Seconds();
		signInButtonEmail.click();
		UtilityMethods.wait10Seconds();

		if(signOutLink.isDisplayed()){

			UtilityMethods.validateAssertEqual("WEBMAIL ACCESSED", "WEBMAIL ACCESSED");
			Reporter.loadXMLConfig(new File("E:\\TVAutomation\\src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS SUCCESSFULLY LOGGED IN TO WEBMAIL");
			System.out.println("USER HAS SUCCESSFULLY LOGGED IN TO WEBMAIL");

		}

		else{
			Reporter.loadXMLConfig(new File("E:\\TVAutomation\\src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER IS NOT ABLE TO LOGGED IN TO WEBMAIL");
			System.out.println("USER IS NOT ABLE TO LOGGED IN TO WEBMAIL");
			UtilityMethods.validateAssertEqual("WEBMAIL ACCESSED", "WEBMAIL NOT ACCESSED");

		}

	}


	public void clickEmailItem() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		junkFolderEmail.click();

		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS WAITING FOR ACTIVATE LICENSE EMAIL");
		System.out.println("USER IS WAITING FOR 10 MINS FOR ACTIVATE LICENSE EMAIL");

		UtilityMethods.wait600Seconds();
		
	//	UtilityMethods.wait120Seconds();
		
		//Update To 1200s FOR PRODUCTION
		//UtilityMethods.wait300Seconds(); //Update To 2s FOR STAGING
		//UtilityMethods.wait3Seconds();
		junkFolderEmail.click();

		try{

			if(clickOrderConfirmationEmailItem.isDisplayed()){
				
				System.out.println("ORDER CONFIRMATION EMAIL IS RECEIVED SUCCESSFULLY");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("ORDER CONFIRMATION EMAIL IS RECEIVED SUCCESSFULLY");
				
			} 
			
		}
		catch(Exception e){
			
			System.out.println("ORDER CONFIRMATION EMAIL IS NOT RECEIVED");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("ORDER CONFIRMATION EMAIL IS NOT RECEIVED");
			UtilityMethods.validateAssertEqual("ORDER CONFIRMATION EMAIL RECEIVED", "ORDER CONFIRMATION EMAIL NOT RECEIVED");
			
		}
		
		
		
		try{
			
			if(clickEmailItemActivation.isDisplayed()){
				
				System.out.println("ACTIVATE LICENSE EMAIL IS RECEIVED SUCCESSFULLY IN 10 MINUTES");
		}

		}

		catch(Exception e){
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog(" ACTIVATION EMAIL NOT RECEIVED WITHIN 10 MINUTES");
			System.out.println("ACTIVATION EMAIL NOT RECEIVED WITHIN 10 MINUTES");
			UtilityMethods.validateAssertEqual("ACTIVATE LICENSE EMAIL RECEIVED", "ACTIVATE LICENSE EMAIL NOT RECEIVED");

		}
		
		if(clickOrderConfirmationEmailItem.isDisplayed() && clickEmailItemActivation.isDisplayed()){
		
			
			selectAllEmails.click();
		
		}
		UtilityMethods.wait3Seconds();
		moveToInboxButton.click();
		UtilityMethods.wait1Seconds();
		chooseInboxToMove.click();
		UtilityMethods.wait1Seconds();
		dropDownValueInbox.click();
		UtilityMethods.wait3Seconds();
		moveButton.click();

		openInbox.click();
		UtilityMethods.wait3Seconds();
		clickOrderConfirmationEmailItem.click();
		UtilityMethods.wait5Seconds();

		//********************Getting the Order Number from Order Confirmation Email******************
		String readlingEmailLine = orderNumberLine.getText();

		int startOf = readlingEmailLine.indexOf('#');

		String truncatingTheOrderID =  readlingEmailLine.substring(startOf+1, 80);

		int endOf =  truncatingTheOrderID.indexOf(" ");

		finalOrderID = truncatingTheOrderID.substring(0,endOf);
		System.out.println("\n");
		System.out.println("\n");
		System.out.println("THE ID FOR THIS ORDER IS: "+finalOrderID);
		System.out.println("\n");
		System.out.println("\n");

		//********************************************************************************************
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS SUCCESSFULLY READ ORDER CONFIRMATION EMAIL THE ID FOR THIS ORDER IS: "+finalOrderID);
		System.out.println("USER HAS SUCCESSFULLY READ ORDER CONFIRMATION EMAIL");
		UtilityMethods.wait5Seconds();

	}




	public void clickEmailItemActivation() throws Throwable{


		openInbox.click();
		UtilityMethods.wait3Seconds();
		clickEmailItemActivation.click();
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS SUCCESSFULLY READ ACTIVATE LICENSE EMAIL");
		System.out.println("USER HAS SUCCESSFULLY READ ACTIVATE LICENSE EMAIL");
		UtilityMethods.wait5Seconds();

		//Getting Date and Time of Activate License Email
		String dateandTime = timeStampOfActivateLicenseEmail.getText();
		System.out.println(dateandTime);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("ACTIVATION LICENSE EMAIL RECEIVED AT: "+"<b>"+dateandTime+"<b>");
		activateLink.click();
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CLICKED ON 'ACTIVATE NOW' LINK");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(2));
		UtilityMethods.wait30Seconds();
		String currentURL = driver.getCurrentUrl();
		//
		String finalURL = currentURL.replace("login", "dev-rc"); // for production
		//String finalURL = currentURL.replace("login", "dev-trunk"); //for staging

		driver.navigate().to(finalURL);

		String urlOfDevC = driver.getCurrentUrl();

				//for production
		if(urlOfDevC.contains("dev-rc.teamviewer.com")){ 

		//		//for staging
		//if(urlOfDevC.contains("dev-trunk.teamviewer.com")){

			UtilityMethods.validateAssertEqual("Teamviewer URL opened", "Teamviewer URL opened");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("TEAMVIEWER MANAGEMENT CONSOLE IS ACCESSED");
			System.out.println("TEAMVIEWER MANAGEMENT CONSOLE IS ACCESSED");
		}

		else{

			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("TEAMVIEWER MANAGEMENT CONSOLE IS NOT ACCESSED");
			UtilityMethods.validateAssertEqual("Teamviewer URL opened", "Teamviewer URL not opened");

		}

		UtilityMethods.wait10Seconds();

	}

	public void deleteEmailItemsInInbox() throws Throwable{

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		UtilityMethods.wait3Seconds();


		//Removing Items From Inbox
		openInbox.click();
		UtilityMethods.wait3Seconds();
		selectAllEmails.click();
		UtilityMethods.wait3Seconds();
		moveToInboxButton.click();
		UtilityMethods.wait1Seconds();
		chooseInboxToMove.click();
		UtilityMethods.wait1Seconds();
		dropDownValueArchive.click();
		UtilityMethods.wait3Seconds();
		moveButton.click();

	}
	

	public void clickInvoiceReminderEmailItem() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		junkFolderEmail.click();

		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS WAITING FOR ACTIVATE LICENSE EMAIL");
		System.out.println("USER IS WAITING FOR 10 MINS FOR ACTIVATE LICENSE EMAIL");

		UtilityMethods.wait120Seconds();
		
	
		
		//Update To 1200s FOR PRODUCTION
		//UtilityMethods.wait300Seconds(); //Update To 2s FOR STAGING
		//UtilityMethods.wait3Seconds();
		junkFolderEmail.click();

		try{

			if(clickInvoiceReminderEmailItem.isDisplayed()){
				
				System.out.println("INVOICE REMINDER EMAIL IS RECEIVED SUCCESSFULLY");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("INVOICE REMINDER EMAIL IS RECEIVED SUCCESSFULLY");
				
			} 
			
		}
		catch(Exception e){
			
			System.out.println("INVOICE REMINDER EMAIL IS NOT RECEIVED");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("ORDER CONFIRMATION EMAIL IS NOT RECEIVED");
			UtilityMethods.validateAssertEqual("INVOICE REMINDER  EMAIL RECEIVED", "INVOICE REMINDER  EMAIL NOT RECEIVED");
			
		}

	}


	public SystemEmailLogin(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);

	}



}
