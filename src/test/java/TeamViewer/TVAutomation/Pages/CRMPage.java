package TeamViewer.TVAutomation.Pages;

import static TeamViewer.TVAutomation.Utils.DataPool.readExcelData;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;

import TeamViewer.TVAutomation.DataProvider.TestData;
import TeamViewer.TVAutomation.Stepdefinitions.CRMPageSteps;
import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;


public class CRMPage  extends DriverFactory  {

//	private AX365Page axPage =  new AX365Page(driver);
	
	
	public String parentWinHandle;
	public static String firstNameInCRM = "Lesbrown";
	public static String emailForLead;
	public static String accountName;
	public static String accountNumber;
	public static String salesTaxGroup;
	public static int flag = 0;
	public static String emailOnAccountCRM;
	
	public static String customerGroup;
	public static String countryLab;
	public static int flagForSalesOrderFromCRM = 0;
	public static int flagForAccountToOppertunity = 0;
	public boolean report = true;
	boolean abc = true;
	public static String accountNoLabel;
	boolean order = true;


	//CRM Login Creation 
	@FindBy (xpath= "//input[@type='email']")
	public WebElement emailField;

	@FindBy (xpath= "//input[@type='submit']")
	public WebElement nextButton;

	@FindBy (id= "passwordInput")
	public WebElement passwordField;

	@FindBy (xpath= "//span[@id='submitButton']")
	public WebElement SignInButton;

	@FindBy (xpath= "//input[@type='submit']")
	public WebElement yesButtonForStaySignedIn;

	@FindBy (name= "TabHome")
	public static WebElement SaleDropDown;
	
	@FindBy(xpath = "//span[contains(text(),'Sales') and @class = 'navActionButtonLabel']")
	public static WebElement sales;

	@FindBy (xpath= "//span[@class='nav-rowLabel' and contains(text(),'Leads')]")
	public WebElement Leads;

	@FindBy (xpath= "//span[@class='nav-rowLabel' and contains(text(),'Accounts')]")
	public static WebElement Accounts;
	
	@FindBy (xpath= "//span[contains(text(),'Account Name')]")
	public WebElement AccountName;
	
	@FindBy (xpath= "(//span[contains(text(),'Email')])[1]")
	public WebElement Email;
	
	@FindBy (xpath= "//input[@id='emailaddress1_i']")
	public WebElement Email01;
	
	@FindBy (xpath= "(//span[contains(text(),'Street 1')])[1]")
	public WebElement Street;
	
	@FindBy (xpath= "//input[@id = 'address1_line1_i']")
	public WebElement Street_1;
	
	@FindBy (xpath= "(//span[contains(text(),'City')])[1]")
	public WebElement City;
	
	@FindBy (xpath= "//input[@id = 'address1_city_i']")
	public WebElement City_1;
	
	@FindBy (xpath= "(//span[contains(text(),'Country')])[2]")
	public WebElement Country;
	
	
	@FindBy (xpath= "//input[@id = 'tv_country_ledit']")
	public WebElement Country_1;
	
	@FindBy (xpath= "(//span[contains(text(),'ZIP/Postal Code')])[1]")
	public WebElement zipCode;

	@FindBy (xpath= "//input[@id = 'address1_postalcode_i']")
	public WebElement zipCode1;

	@FindBy (xpath= "//input[@id='name_i']")
	public WebElement AccountName1;
	
	
	@FindBy (xpath= "//span[contains(text(),'New') and @class = 'ms-crm-CommandBar-Menu']")
	public WebElement NewLead;


	@FindBy (xpath = "(//div[contains(@id,'header_tv_lead_source')])[1]") 
	public WebElement SourceDropdownOnHeaderBar;


	@FindBy (xpath= "//select[@id='header_tv_lead_source_i']/*[5]") 
	public WebElement chooseResselerOption;

	@FindBy (xpath= "(//span[contains(text(),'Salutation')])[1]") 
	public WebElement Salutation1;

	@FindBy (xpath= "//*[@id='header_process_salutation_i']")
	public WebElement Salutation2;

	@FindBy (xpath= "(//span[contains(text(),'First Name')])[1]") 
	public WebElement FirstName1;

	@FindBy (xpath= "//*[@id='header_process_firstname_i']")
	public WebElement FirstName2;

	@FindBy (xpath= "(//span[contains(text(),'Last Name')])[1]") 
	public WebElement LastName1;

	@FindBy (xpath= "//*[@id='header_process_lastname_i']")
	public WebElement LastName2;

	@FindBy (xpath= "(//span[contains(text(),'Company Name')])[1]") 
	public WebElement Company1;

	@FindBy (xpath= "//*[@id='header_process_companyname_i']") 
	public WebElement Company2;


	@FindBy (xpath= "(//span[contains(text(),'(N) Product Line')])[1]") 
	public WebElement ProductLine1;

	@FindBy (xpath= "//*[@id='header_process_tv_latestinterestinproductline_i']/option[3]") 
	public WebElement ProductLine2;

	@FindBy (xpath= "(//span[contains(text(),'Street')])[1]") 
	public WebElement Street1;

	@FindBy (xpath= "//*[@id='header_process_address1_line1_i']") 
	public WebElement Street2;


	@FindBy (xpath= "(//span[contains(text(),'ZIP/Postal Code')])[1]") 
	public WebElement Zip1;

	@FindBy (xpath= "	//*[@id='header_process_address1_postalcode_i']") 
	public WebElement Zip2;

	@FindBy (xpath= "(//span[contains(text(),'City')])[1]") 
	public WebElement City1;

	@FindBy (xpath= "//*[@id='header_process_address1_city_i']") 
	public WebElement City2;

	@FindBy (xpath= "(//span[contains(text(),'Country')])[1]") 
	public WebElement Country1;

	@FindBy (xpath= "	//*[@id='header_process_tv_country_ledit']") 
	public WebElement Country2;

	@FindBy (xpath= "(//span[contains(text(),'Email')])[1]") 
	public WebElement Email1;

	@FindBy (xpath= "//*[@id='header_process_emailaddress1_i']") 
	public WebElement Email2;

	@FindBy (xpath= "(//span[contains(text(),'Tax Number Provided')])[1]") 
	public WebElement TaxNumber1;
	
	@FindBy (xpath= "(//option[contains(text(),'Yes')])[1]") 
	public WebElement TaxNumber4;

	@FindBy (xpath= "(//option[contains(text(),'Not Applicable')])[1]") 
	public WebElement TaxNumber2;
	
	@FindBy (xpath= "(//option[contains(text(),'No')])[1]") 
	public WebElement TaxNumber3;
	
	@FindBy(id = "Tax Number Provided_label")
	public WebElement taxNumberSelectBox;

	
	@FindBy(id = "Tax Number_label")
	public WebElement taxNumberField;
	
	@FindBy(id = "header_process_tv_taxnumber_i")
	public WebElement taxNumberInput;

	@FindBy (xpath= "(//span[contains(text(),'Topic')])[1]") 
	public WebElement Topic1;

	@FindBy (xpath= "//*[@id='subject_i']") 
	public WebElement Topic2;

	@FindBy (xpath= "(//span[contains(text(),'Save')])[1]") 
	public WebElement SaveButton;
	
	@FindBy (xpath= "//button[@id = 'butBegin']") 
	public WebElement SaveButtonIfRecordDuplicate;
	


	//**************************************************Qualify and create new qoute****************************************
	@FindBy (xpath= "//span[@class='ms-crm-CommandBar-Menu' and contains(text(),'Qualify')]") 
	public WebElement qualifyButtonOnHeaderBar;

	@FindBy (xpath= "//span[contains(text(),'New Quote')]") 
	public WebElement newQouteButtonOnHeaderBar;
	
	@FindBy (xpath= "//*[contains(text(),'OK')]")
	public WebElement okButtonOnCreateQoutation;
	
	@FindBy (xpath= "(//input[contains(@title,'Click')])[4]")
	public WebElement getNameOfTheLead;
	
	@FindBy (xpath= "//input[contains(@id,'CustName_input')]")
	public WebElement getNameOfTheCustomer;
	
	
	@FindBy (xpath= "//button[@id='ok_id']")
	public WebElement continueButtonOnPopup;
	
	@FindBy (name= "SalesQuotationTable_CustAccount")
	public WebElement cusAccntNrInERPForLeadScenario;
	
	@FindBy (xpath= "//input[contains(@id,'CustAccount_input')]")
	public WebElement cusAccntNrInERPForDirectOrderScenario;
	
	@FindBy (xpath= "//input[contains(@id,'HeaderInfoStatus_input')]")
	public WebElement qoutationStatusOnERPIfCreated;
	
	@FindBy (xpath= "//li[@id='moreCommands']")
	public WebElement threeDotsMoreCommand;
	
	@FindBy (xpath= "//span[contains(text(), ' New Order ')] ")
	public WebElement newOrderOptionInDropDown;
	
	@FindBy(id = "Account Number_label")
	public WebElement accountNo;
	
	@FindBy(id = "PrimaryContact_PrimaryContact_contact_parentcustomerid_lookupValue")
	public WebElement companyNameOnAccount;
	
	@FindBy(id = "Email_label")
	public WebElement emailOnAccount;
	
	@FindBy(id = "tv_billtocountry_lookupValue")
	public WebElement countryNameOnAccount;
	
	@FindBy(id = "transactioncurrencyid_lookupValue")
	public WebElement currency;
	
	@FindBy(name = "SalesQuotationTable_TaxGroup")
	public WebElement STGQuotationCRMtoERP;
	
	@FindBy(name = "customerName")
	public WebElement customerName;
	
	@FindBy(name = "SalesQuotationTable_CurrencyCode")
	public WebElement currencyQuotationCRMtoERP;
	
	@FindBy(name = "groupAdministration_LanguageId")
	public WebElement languageQuotationCRMtoERP;

	@FindBy(id = "parentaccountid_lookupValue")
	public WebElement parentAccountName;
	
	@FindBy(xpath = "(//label[@id='Total Amount_label'])[1]")
	public WebElement totalAmountCRM;
	
	@FindBy(id = "Payment Status_label")
	public WebElement paymentStatus;
	
	@FindBy(id = "Payment Type_label")
	public WebElement paymentMethod;
	
	@FindBy(id = "salesorderid_label")
	public WebElement orderOnInvoiceCRM;
	
	@FindBy (id= "tv_customergroup_d")
	public WebElement CustomerGroupLabel;

	@FindBy (id= "crmGrid_findCriteria")
	public static WebElement AccountSearch;
	
	@FindBy (id = "crmGrid_findCriteriaButton")
	public static WebElement SearchButton;
	
	@FindBy (xpath= "//*[contains(text(),'Search Results')]")
	public static WebElement VerifySearchResultsDisplay;
	
	@FindBy (xpath= "//table[@class='ms-crm-List-Data']//tr[@class='ms-crm-List-Row'][1]")
	public static WebElement AccountSearchFirstResult;
	
	@FindBy (xpath= "//a[contains(@id,'gridBodyTable_primaryField_')]")
	public static WebElement AccountSearchFirstRowName;
	
	@FindBy (id= "tv_country_lookupValue")
	public WebElement CountryLabel;
	
	@FindBy (id= "accountopportunitiesgrid_addImageButton")
	public WebElement AddOpportunityButton;
	
	@FindBy (xpath= "//input[@id='name']")
	public WebElement AddOpportunityTopicLabel;
	
	@FindBy (xpath= "//input[@id='name_i']")
	public WebElement AddOpportunityTopic;
	
	@FindBy (xpath= "//input[@id='parentcontactid_ledit']")
	public WebElement AddOpportunityContactName;
	
	@FindBy (xpath= "//select[@id='tv_source_opportunity_i']")
	public WebElement AddOpportunitySource;
	
	@FindBy (xpath= "//div[@id='tv_productline']")
	public WebElement AddOpportunityProductLine;
	
	@FindBy (xpath= "//select[@id='tv_productline_i']")
	public WebElement AddOpportunityProductLineList;
	
	@FindBy (xpath= "//span[@id='parentaccountid_lookupValue' and @class='ms-crm-Lookup-Item']")
	public WebElement AddOpportunityAccountName;
	
	@FindBy (xpath= "//div[@id='parentcontactid']")
	public WebElement AddOpportunityContactNameLabel;
	
	@FindBy (xpath= "//div[@id='Source_label']")
	public WebElement AddOpportunitySourceName;
	
	@FindBy (xpath= "//span[@id='transactioncurrencyid_lookupValue' and @class='ms-crm-Lookup-Item']")
	public WebElement AddOpportunityCurrencyName;
	
	@FindBy (xpath= "//span[@id='ownerid_lookupValue' and @class='ms-crm-Lookup-Item'] ")
	public WebElement AddOpportunityOwner;
	
	@FindBy (xpath= "//button[@id='globalquickcreate_save_button_NavBarGloablQuickCreate'] ")
	public WebElement AddOpportunitySaveButton;
	
	@FindBy (xpath= "//table[@ologicalname='opportunity']")
	public WebElement FirstTopicOpportunity;
	
	@FindBy (xpath= "//table[@ologicalname='opportunity']/tbody/tr[1]/td[3]/nobr/a")
	public WebElement FirstTopicOpportunityTable;
	
	@FindBy (xpath= "//div[@class='stageActionTextForIcon' and contains(text(), 'Next Stage')]")
	public WebElement OpportunityDetailsNextStageButton;
	
	@FindBy (xpath= "//div[@id='stage_1']/div[2]/div/div/div/span/span[contains(text(),'Active')]")
	public WebElement OpportunityDevelopStageActive;
	
	@FindBy (xpath= "//div[@id='tv_taxnumberprovided']")
	public WebElement taxNumberProvidedLabel;
	
	@FindBy (xpath= "//select[@id='tv_taxnumberprovided_i']")
	public WebElement taxNumberProvidedList;

	@FindBy(id = "tv_taxnumber")
	public WebElement taxNumberField_Account;
	
	@FindBy(id = "tv_taxnumber_i")
	public WebElement taxNumberInput_Account;

	@FindBy(id="Business Relationship_label")
	public WebElement businessRelationshipLabel;
	
	@FindBy (xpath= "//span[contains(text(),'New Order') and @class = 'ms-crm-CommandBar-Menu']")
	public WebElement NewOrder;
	
	@FindBy(xpath= "//div[@title='Save']")
	public WebElement saveButtonFooter;
	
	@FindBy(id = "accountcategorycode")
	public WebElement accountCategory;
	
	@FindBy(id = "accountcategorycode_i")
	public WebElement categorySelectBox;


	//Login into CRM
	public void doLoginInCRM() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		emailField.sendKeys("Yasir.memon@visionetsystems.com");
		UtilityMethods.wait3Seconds();
		nextButton.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();

		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(passwordField));
		passwordField.sendKeys("qwer@1234");
		UtilityMethods.wait1Seconds();

		SignInButton.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		try{

			if(yesButtonForStaySignedIn.isDisplayed()){

				yesButtonForStaySignedIn.click();
			}

		}
		catch(Exception e){

			System.out.println("Couldn't find YES button");
		}

		System.out.println(("USER HAS SUCESSFULLY LOGGED IN TO CRM"));
		Reporter.addStepLog("USER HAS SUCESSFULLY LOGGED IN TO CRM");


		UtilityMethods.waitForPageLoadAndPageReady();

	}

	
	public void createNewCustomeronCRM() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		SaleDropDown.click();
		UtilityMethods.wait5Seconds();
		
		Accounts.click();
		UtilityMethods.wait10Seconds();
		NewLead.click();
		
		UtilityMethods.wait15Seconds();
		
		WebDriverWait wait1 = new WebDriverWait(driver,30);
		wait1.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("contentIFrame1")));
		
		UtilityMethods.wait5Seconds();
		
		System.out.println("USER IS ENTERING THE DETAILS OF CUSTOMER");
		//Creating a customer account
		AccountName.click();
		AccountName1.sendKeys(firstNameInCRM);
		UtilityMethods.wait2Seconds();
		Email.click();
		UtilityMethods.wait2Seconds();
		Email01.sendKeys("naltesting@tester.com");
		UtilityMethods.wait2Seconds();
		Street.click();
		UtilityMethods.wait2Seconds();
		Street_1.sendKeys("13J");
		UtilityMethods.wait2Seconds();
		City.click();
		UtilityMethods.wait2Seconds();
		City_1.sendKeys("Paris");
		UtilityMethods.wait2Seconds();
		zipCode.click();
		UtilityMethods.wait2Seconds();
		zipCode1.sendKeys("23345");
		UtilityMethods.wait2Seconds();
		Country.click();
		UtilityMethods.wait2Seconds();
		Country_1.sendKeys("France");
		Country_1.sendKeys(Keys.TAB);
		
		UtilityMethods.wait3Seconds();
		
		
		
		driver.switchTo().defaultContent();
		UtilityMethods.wait3Seconds();
		
		SaveButton.click();
		UtilityMethods.wait15Seconds();
		
		try{
			
			WebDriverWait wait2 = new WebDriverWait(driver,15);
			wait2.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("InlineDialog_Iframe")));
			if(SaveButtonIfRecordDuplicate.isDisplayed()){
				
				SaveButtonIfRecordDuplicate.click();
				System.out.println("DUPLICATES DETECTED : CLICK SAVE BUTTON");
				Reporter.addStepLog("DUPLICATES DETECTED : CLICK SAVE BUTTON");
				driver.switchTo().defaultContent();
				UtilityMethods.wait15Seconds();
			}
			
			
		}
		
		catch(Exception e){
			
			System.out.println("RECORD IS NOT A DUPLICATE ONE");
		}
	}


	public void createNewLead(String salutation,String street,String zipCode,String city,
			String taxNumber,String taxNumberValue,String source,String productLine,
			String businessPhone,String nameFirst,String nameLast,String topic,
			String email, String company, String country) throws Throwable{

		//Select LEAD from main page
		UtilityMethods.waitForPageLoadAndPageReady();
		SaleDropDown.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		sales.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		Leads.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		
		boolean staleElement = true; 
		while(staleElement){
		  try{
				WebDriverWait wait = new WebDriverWait(driver,30);
				wait.until(ExpectedConditions.visibilityOf(NewLead));
				UtilityMethods.wait2Seconds();
			  new Actions(driver).moveToElement(NewLead).click().build().perform();
			  System.out.println("USER HAS CLICKED ON NEW BUTTON TO CREATE NEW LEAD");
			Reporter.addStepLog("USER HAS CLICKED ON NEW BUTTON TO CREATE NEW LEAD");
			abc = false;
		     if(abc == false){
		    	 
		    	 staleElement = false;
		    	 
		     }

		  } catch(StaleElementReferenceException e){
		    staleElement = true;
		    UtilityMethods.wait1Seconds();
		  }
		}
	
		//Select NEW Button
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchTOIframeCRM();
		UtilityMethods.wait5Seconds();
		new WebDriverWait(driver,50).until(ExpectedConditions.visibilityOf(SourceDropdownOnHeaderBar));
		SourceDropdownOnHeaderBar.click();
		System.out.println("USER IS FILLING MANDATORY FIELDS TO CREATE A LEAD");
		Reporter.addStepLog("USER IS FILLING MANDATORY FIELDS TO CREATE A LEAD");
		UtilityMethods.wait3Seconds();
		chooseResselerOption.click();


		UtilityMethods.wait3Seconds();
		
	//	System.out.println(salutation+street+zipCode+city+taxNumber);
		Salutation1.click();
		Salutation2.sendKeys(salutation);
		UtilityMethods.wait1Seconds();

		FirstName1.click();
		FirstName2.sendKeys(nameFirst);
		UtilityMethods.wait1Seconds();

		LastName1.click();
		LastName2.sendKeys(nameLast);
		UtilityMethods.wait1Seconds();

		Company1.click();
		Company2.sendKeys(company);
		UtilityMethods.wait1Seconds();

		ProductLine1.click();
		ProductLine2.click();
		UtilityMethods.wait1Seconds();

		Street1.click();
		Street2.sendKeys(street);
		UtilityMethods.wait1Seconds();

		Zip1.click();
		Zip2.sendKeys(zipCode);
		UtilityMethods.wait1Seconds();



		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", City1);
		
		City2.sendKeys(city);
		UtilityMethods.wait1Seconds();


		Country1.click();
		Country2.sendKeys(country);
		UtilityMethods.wait2Seconds();


		Email1.click();
		//Email2.sendKeys("teamviewer.automation402@systemsltd.com");
		emailForLead = email;
		Email2.sendKeys(email);
		UtilityMethods.wait1Seconds();

		TaxNumber1.click();
		TaxNumber2.click();
		UtilityMethods.wait1Seconds();
		
		Topic1.click();
		Topic2.sendKeys(topic);
		UtilityMethods.wait1Seconds();

		System.out.println("USER IS FILLED THE COMPLETE DATA");
		Reporter.addStepLog("USER IS FILLED THE COMPLETE DATA");
		driver.switchTo().defaultContent();
		UtilityMethods.wait5Seconds();
		
		
		UtilityMethods.waitForPageLoadAndPageReady();
		SaveButton.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		System.out.println("USER IS SAVING THE LEAD AFTER FILLING DATA");
		Reporter.addStepLog("USER IS SAVING THE LEAD AFTER FILLING DATA");
	
		try{
		WebDriverWait wait2 = new WebDriverWait(driver,15);
		wait2.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("InlineDialog_Iframe"))));
		driver.switchTo().frame(driver.findElement(By.id("InlineDialog_Iframe")));
		UtilityMethods.wait7Seconds();
	
				
				UtilityMethods.clickElemetJavaSciptExecutor(SaveButtonIfRecordDuplicate);
				System.out.println("DUPLICATES DETECTED : CLICK SAVE BUTTON");
				Reporter.addStepLog("DUPLICATES DETECTED : CLICK SAVE BUTTON");
				driver.switchTo().defaultContent();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait5Seconds();
		
		}
		catch(Exception e){
	
			System.out.println("RECORD IS NOT A DUPLICATE ONE");
			Reporter.addStepLog("RECORD IS NOT A DUPLICATE ONE");
		}
		

	}

	//Quality And Creating new qoutation
	public void qualifyAndCreateNewQoute() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchToParentWindow(UtilityMethods.switchToSubWindowAndReturnParentWindowHandler());
	//	UtilityMethods.switchToSubWindowAndReturnParentWindowHandler();
		
		UtilityMethods.wait5Seconds();
		boolean staleElement = true; 
		while(staleElement){
		  try{
			  new Actions(driver).moveToElement(qualifyButtonOnHeaderBar).click().build().perform();
		//	  UtilityMethods.clickElemetJavaSciptExecutor(qualifyButtonOnHeaderBar);
			  UtilityMethods.waitForPageLoadAndPageReady();
			  UtilityMethods.switchTOIframeCRM();
			  UtilityMethods.wait5Seconds();
		     if(driver.findElements(By.id("header_process_customerneed_cl_span")).size()!=0){
		    	 
		    	 staleElement = false;
		    	 
		     }

		  } catch(StaleElementReferenceException e){
			    staleElement = true;
			    UtilityMethods.wait1Seconds();
		  }
		}
		System.out.println("USER HAS CLICKED ON QUALIFY BUTTON");
		Reporter.addStepLog("USER HAS CLICKED ON QUALIFY BUTTON");
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchTOIframeCRM();
		UtilityMethods.wait5Seconds();
		
		try{
			
			WebDriverWait wait1 = new WebDriverWait(driver,30);
			wait1.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("InlineDialog_Iframe"))));
			new WebDriverWait(driver,30).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("InlineDialog_Iframe")));
			continueButtonOnPopup.click();
			
			System.out.println("DUPLICATE WARNING : CLICK CONTINUE BUTTON");
			Reporter.addStepLog("DUPLICATE WARNING : CLICK CONTINUE BUTTON");
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait10Seconds();
			UtilityMethods.switchToSubWindowAndReturnParentWindowHandler();
		}
		
		catch(Exception e){
			
			System.out.println("RECORD IS NOT A DUPLICATE ONE");
		}
		
		UtilityMethods.switchToFirstTab();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchTOIframeCRM();
		
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(Topic1));
		UtilityMethods.wait10Seconds();
		UtilityMethods.moveToElementWithJSExecutor(parentAccountName);
		String contactNo = driver.findElement(By.id("parentcontactid_lookupValue")).getText();
		accountName = parentAccountName.getText();
		System.out.println("Contact Name is:"+contactNo+"\nAccount Name is: "+accountName);
		Reporter.addStepLog("Contact Name is:"+contactNo+"\nAccount Name is: "+accountName);
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.switchTo().defaultContent();
		

		
	}
	

	
	public void clickNewOrderAndSwitchToERPFromCRM() throws Throwable{
		
		System.out.println("USER HAS CLICKED ON NEW ORDER BUTTON IN CRM");
		UtilityMethods.wait3Seconds();
		newOrderOptionInDropDown.click();
		
		UtilityMethods.wait2Seconds();
		//Switch Driver from CRM to ERP
				parentWinHandle = driver.getWindowHandle();
				Set<String> winHandles = driver.getWindowHandles();
				
				// Loop through all handles
				for(String handle: winHandles){
					if(!handle.equals(parentWinHandle)){
						driver.switchTo().window(handle);
						System.out.println("USER IS SWITHED FROM CRM TO ERP PAGE");
					}
				}
				
				WebDriverWait wait = new WebDriverWait(driver,30);
				wait.until(ExpectedConditions.visibilityOf(cusAccntNrInERPForDirectOrderScenario));
				
				String customerAccountNumber = cusAccntNrInERPForDirectOrderScenario.getAttribute("title").substring(0,15);
				System.out.println("CUSTOMER ACCOUNT NUMBER IS :" +customerAccountNumber);
				Reporter.addStepLog("CUSTOMER ACCOUNT NUMBER IS :" +customerAccountNumber);
				String CompletenameInERP = getNameOfTheCustomer.getAttribute("title");
				
				if(CompletenameInERP.contains(firstNameInCRM)){
					UtilityMethods.validateAssertEqual("Name Matched", "Name Matched");
					System.out.println(CompletenameInERP.toUpperCase().substring(0,8)+" IS THE NAME OF LEAD IN ERP AND CRM");
					Reporter.addStepLog(CompletenameInERP.toUpperCase().substring(0,8)+" IS THE NAME OF LEAD IN ERP AND CRM");
				}
				
				else{
					UtilityMethods.validateAssertEqual("Name Matched", "Name not Matched");
					System.out.println("LEAD NAME IN CRM AND ERP ARE NOT MATCHED");
					Reporter.addStepLog("LEAD NAME IN CRM AND ERP ARE NOT MATCHED");
				}
				
				okButtonOnCreateQoutation.click();
				System.out.println("USER HAS CLICKED ON OK BUTTON TO CREATE SALES ORDER");
				UtilityMethods.wait15Seconds();
				
	}
	
	public void validateAccount() throws Throwable{
		
		//Can be used for account validations

				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.switchTOIframeCRM();
				UtilityMethods.wait5Seconds();
				new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(Topic1));
				UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.id("parentaccountid_lookupValue")));
				driver.findElement(By.id("parentaccountid_lookupValue")).click();
				//TODO: Validation on account
				UtilityMethods.waitForPageLoadAndPageReady();
				new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(accountNo));
				UtilityMethods.wait3Seconds();
				accountNumber = accountNo.getText();
				
				System.out.println(accountNumber);	
				UtilityMethods.wait1Seconds();
				System.out.println(emailOnAccount.getText());
				emailForLead = emailOnAccount.getText();
				accountCategory.click();
				UtilityMethods.wait2Seconds();
				new Select(categorySelectBox).selectByVisibleText("Standard");
				UtilityMethods.wait2Seconds();
				System.out.println(companyNameOnAccount.getText());
				System.out.println(countryNameOnAccount.getText());
				System.out.println(currency.getText());
				
				while(report){
				Reporter.addStepLog("ACCOUNT NUMBER ON ACCOUNT IS: "+accountNumber);
				Reporter.addStepLog("COMPANY NAME ON ACCOUNT IS: "+companyNameOnAccount.getText());
				Reporter.addStepLog("COUNTRY NAME ON ACCOUNT IS: "+countryNameOnAccount.getText());
				Reporter.addStepLog("CURRENCY VALUE ON ACCOUNT IS: "+currency.getText());
				report = false;
				}
				
			//	driver.switchTo().defaultContent();
				UtilityMethods.wait3Seconds();
				
			//	SaveButton.click();
				driver.findElement(By.id("savefooter_statuscontrol"));
				UtilityMethods.wait15Seconds();
				
				try{
					
					WebDriverWait wait2 = new WebDriverWait(driver,15);
					wait2.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("InlineDialog_Iframe")));
					if(SaveButtonIfRecordDuplicate.isDisplayed()){
						
						SaveButtonIfRecordDuplicate.click();
						System.out.println("DUPLICATES DETECTED : CLICK SAVE BUTTON");
						Reporter.addStepLog("DUPLICATES DETECTED : CLICK SAVE BUTTON");
						driver.switchTo().defaultContent();
						UtilityMethods.wait15Seconds();
					}
					
					
				}
				
				catch(Exception e){
					
					System.out.println("RECORD IS NOT A DUPLICATE ONE");
				}
				
				try{
					UtilityMethods.waitForPageLoadAndPageReady();
					UtilityMethods.wait3Seconds();
					if(driver.findElements(By.id("ignoreCrmDuplicateDetectionDialogButtonText")).size()!=0){
						driver.findElement(By.id("ignoreCrmDuplicateDetectionDialogButtonText")).click();
					}
				}
				catch(Exception e){}
				
	}
	
	public void validateAccountValuesAfterOrderConfirmation() throws Throwable{
		
		//Can be used for account validations
		if(flagForSalesOrderFromCRM==0){
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchTOIframeCRM();
		UtilityMethods.wait5Seconds();
		new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(Topic1));
		UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.id("parentaccountid_lookupValue")));
		driver.findElement(By.id("parentaccountid_lookupValue")).click();
		}
		
		if(flagForSalesOrderFromCRM==1){
			
			driver.navigate().refresh();
		}
		try{
			UtilityMethods.waitForPageLoadAndPageReady();
		}
		catch(Exception e){
			((JavascriptExecutor)driver).executeScript("return window.stop");
		}

		UtilityMethods.switchToFirstTab();
		UtilityMethods.switchTOIframeCRM();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(accountNo));
		UtilityMethods.wait5Seconds();
		accountNumber = accountNo.getText();
		System.out.println(accountNumber);	
		//div[contains(text(),'000291802')]
		WebElement order;
		try{
		//	UtilityMethods.scrollToWebElement(driver.findElement(By.xpath("//div[contains(text(),'"+AX365Page.salesOrderNumberCRMtoERP.trim()+"')]")));
			driver.findElement(By.xpath("//h3[contains(text(),'TRIAL')]")).click();
			order = driver.findElement(By.xpath("//div[contains(text(),'"+AX365Page.salesOrderNumberCRMtoERP.trim()+"')]"));
			UtilityMethods.scrollToElement(order);
			System.out.println("ORDER ID IN CRM = "+order.getText()+" SALES ORDER NUMBER FROM SALES ORDER: "+AX365Page.salesOrderNumberCRMtoERP);
		}
		catch(Exception e){
			
			driver.findElement(By.xpath("(//a[@title='Sort by Order ID'])[1]")).click();
		//	UtilityMethods.pageDown();
		//	UtilityMethods.scrollToWebElement(driver.findElement(By.xpath("//div[contains(text(),'"+AX365Page.salesOrderNumberCRMtoERP.trim()+"')]")));
			driver.findElement(By.xpath("//h3[contains(text(),'TRIAL')]")).click();
			UtilityMethods.wait5Seconds();
			order = driver.findElement(By.xpath("//div[contains(text(),'"+AX365Page.salesOrderNumberCRMtoERP.trim()+"')]"));
			UtilityMethods.scrollToElement(order);
			System.out.println("ORDER ID IN CRM = "+order.getText()+" SALES ORDER NUMBER FROM SALES ORDER: "+AX365Page.salesOrderNumberCRMtoERP);
			
		}
	
		new Actions(driver).contextClick(order).perform();
		UtilityMethods.wait3Seconds();
		driver.findElement(By.xpath("//span[@title='Open']")).click();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchToFirstTab();
		UtilityMethods.wait7Seconds();
		UtilityMethods.switchTOIframeCRM();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(totalAmountCRM));
		System.out.println("TOTAL AMOUNT IN CRM IS: "+totalAmountCRM.getText());
		
		if(flagForSalesOrderFromCRM==0)
		{
			if(totalAmountCRM.getText().contains(AX365Page.totalAmountINErp))
				{
				System.out.println("TOTAL AMOUNT IN CRM MATCHED WITH ERP");
				Reporter.addStepLog("TOTAL AMOUNT IN CRM: "+totalAmountCRM.getText()+" MATCHED WITH ERP: "+AX365Page.totalAmountINErp);
				}
		
			if(totalAmountCRM.getText().contains("€") && AX365Page.currencyINErp.contains("EUR"))
				{
				System.out.println("CURRENCY IN ERP MATCHED WITH CURRENCY IN CRM");
				Reporter.addStepLog("CURRENCY IN CRM: € MATCHED WITH ERP: EUR");
				
				}
		
		}
		UtilityMethods.switchToFirstTab();
		UtilityMethods.wait5Seconds();
		driver.findElement(By.xpath("//img[@title='Close' and @class = 'closeButton']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();

		if(flagForSalesOrderFromCRM==0)
		{
			UtilityMethods.clickElemetJavaSciptExecutor(driver.findElement(By.xpath("//img[@title='Close' and @class = 'closeButton']")));
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
		
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.switchTOIframeCRM();
			UtilityMethods.wait5Seconds();
			new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(Topic1));
			UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.id("parentcontactid_lookupValue")));
		
		}
	

	
	}

	public void validateContact() throws Throwable{
		
		System.out.println(flagForAccountToOppertunity);
		if(flagForAccountToOppertunity != 1){
		UtilityMethods.switchToFirstTab();
		UtilityMethods.wait5Seconds();
		driver.findElement(By.xpath("//img[@title='Close' and @class = 'closeButton']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchTOIframeCRM();
		UtilityMethods.wait5Seconds();
		new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(Topic1));
		UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.id("parentcontactid_lookupValue")));
		}

		UtilityMethods.switchToSubWindowAndReturnParentWindowHandler();
		UtilityMethods.wait5Seconds();
		try{
		new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(newQouteButtonOnHeaderBar));
		
		newQouteButtonOnHeaderBar.click();
		System.out.println("USER HAS CLICKED ON NEW QOUTE BUTTON");
		Reporter.addStepLog("USER HAS CLICKED ON NEW QOUTE BUTTON");
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		}
		catch(Exception e){
			
			UtilityMethods.switchToFirstTab();
			UtilityMethods.wait5Seconds();
			driver.findElement(By.xpath("//img[@title='Close' and @class = 'closeButton']")).click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.switchTOIframeCRM();
			UtilityMethods.wait5Seconds();
			new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(Topic1));
			UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.id("parentcontactid_lookupValue")));
			
			UtilityMethods.switchToSubWindowAndReturnParentWindowHandler();
			UtilityMethods.wait5Seconds();
			
			new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(newQouteButtonOnHeaderBar));
			
			newQouteButtonOnHeaderBar.click();
			System.out.println("USER HAS CLICKED ON NEW QOUTE BUTTON");
			Reporter.addStepLog("USER HAS CLICKED ON NEW QOUTE BUTTON");
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			
		}
		
		//Switch Driver from CRM to ERP
				parentWinHandle = driver.getWindowHandle();
				Set<String> winHandles = driver.getWindowHandles();
				
				// Loop through all handles
				for(String handle: winHandles){
					if(!handle.equals(parentWinHandle)){
						driver.switchTo().window(handle);
						System.out.println("USER IS SWITHED FROM CRM TO ERP PAGE");
						Reporter.addStepLog("USER IS SWITHED FROM CRM TO ERP PAGE");
					}
				}
				
				
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait20Seconds();
		
		WebElement accountNumberFieldInERP = driver.findElement(By.xpath("(//input[contains(@title,'"+accountNumber+"')])[1]"));
		
		UtilityMethods.wait2Seconds();
		String customerAccountNumber = accountNumberFieldInERP.getAttribute("title").substring(0,8);
		System.out.println("CUSTOMER ACCOUNT NUMBER IS :" +customerAccountNumber);
		Reporter.addStepLog("CUSTOMER ACCOUNT NUMBER IS :" +customerAccountNumber+" VERIFED");
		driver.findElement(By.xpath("//h2[contains(text(),'General')]")).click();
		UtilityMethods.wait5Seconds();
		try{
			try{
				int index = STGQuotationCRMtoERP.getAttribute("title").indexOf("Click");
				System.out.println("SALES TAX GROUP IS: "+STGQuotationCRMtoERP.getAttribute("title").substring(0, index));
				salesTaxGroup = STGQuotationCRMtoERP.getAttribute("title").substring(0, index);
				Reporter.addStepLog("SALES TAX GROUP IN ERP IS : "+salesTaxGroup);
			}
			catch(Exception a){
				System.out.println("SALES TAX GROUP IS: "+STGQuotationCRMtoERP.getAttribute("title"));
				salesTaxGroup = STGQuotationCRMtoERP.getAttribute("title");
				Reporter.addStepLog("SALES TAX GROUP IN ERP IS : "+salesTaxGroup);
			}
		}
		catch(Exception e){
			driver.findElement(By.xpath("//h2[contains(text(),'General')]")).click();
			UtilityMethods.wait3Seconds();
			try{
				int index = STGQuotationCRMtoERP.getAttribute("title").indexOf("Click");
				System.out.println("SALES TAX GROUP IS: "+STGQuotationCRMtoERP.getAttribute("title").substring(0, index));
				salesTaxGroup = STGQuotationCRMtoERP.getAttribute("title").substring(0, index);
				Reporter.addStepLog("SALES TAX GROUP IN ERP IS : "+salesTaxGroup);
			}
			catch(Exception a){
				System.out.println("SALES TAX GROUP IS: "+STGQuotationCRMtoERP.getAttribute("title"));
				salesTaxGroup = STGQuotationCRMtoERP.getAttribute("title");
				Reporter.addStepLog("SALES TAX GROUP IN ERP IS : "+salesTaxGroup);
			}
		}
		try{		
		int index2 = currencyQuotationCRMtoERP.getAttribute("title").indexOf("Click");
		System.out.println("CURRENCY IS: "+currencyQuotationCRMtoERP.getAttribute("title").substring(0, index2));
		Reporter.addStepLog("CURRENCY IN ERP IS "+currencyQuotationCRMtoERP.getAttribute("title").substring(0, index2));
		}
		catch(Exception e){
			System.out.println("CURRENCY IS: "+currencyQuotationCRMtoERP.getAttribute("title"));
			Reporter.addStepLog("CURRENCY IN ERP IS "+currencyQuotationCRMtoERP.getAttribute("title"));
		}
		try{
	//	int index3 = languageQuotationCRMtoERP.getAttribute("title").indexOf("Click");
		System.out.println("LANGUAGE IS: "+languageQuotationCRMtoERP.getAttribute("title"));//.substring(0, index3));
		Reporter.addStepLog("LANGUAGE IS: "+languageQuotationCRMtoERP.getAttribute("title"));
		}
		catch(Exception e){
		//	int index3 = languageQuotationCRMtoERP.getAttribute("old-title").indexOf("Click");
			System.out.println("LANGUAGE IS: "+languageQuotationCRMtoERP.getAttribute("old-title"));//.substring(0, index3));
			Reporter.addStepLog("LANGUAGE IS: "+languageQuotationCRMtoERP.getAttribute("old-title"));
		}
		okButtonOnCreateQoutation.click();
		UtilityMethods.wait15Seconds();
		System.out.println("USER HAS CLICKED ON OK BUTTON TO CREATE QOUTATION");
		Reporter.addStepLog("USER HAS CLICKED ON OK BUTTON TO CREATE QOUTATION");

	}
	
	public void validateAccountValuesAfterOrderInvoicing() throws Throwable{
		
		//Can be used for account validations

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchTOIframeCRM();
		UtilityMethods.wait5Seconds();
		new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(Topic1));
		UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.id("parentaccountid_lookupValue")));
		driver.findElement(By.id("parentaccountid_lookupValue")).click();
		//TODO: Validation on account
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchToFirstTab();
		UtilityMethods.switchTOIframeCRM();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(accountNo));
		UtilityMethods.wait5Seconds();
		accountNumber = accountNo.getText();
		System.out.println(accountNumber);	
		//div[contains(text(),'000291802')]
		WebElement invoice = driver.findElement(By.xpath("//div[contains(text(),'"+AX365Page.invoiceNumber.trim()+"')]"));
		UtilityMethods.scrollToElement(invoice);
		System.out.println("ORDER ID IN CRM = "+invoice.getText()+" SALES ORDER NUMBER FROM SALES ORDER: "+AX365Page.salesOrderNumberCRMtoERP);
		
		new Actions(driver).contextClick(invoice).perform();
		UtilityMethods.wait3Seconds();
		driver.findElement(By.xpath("//span[@title='Open']")).click();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchToFirstTab();
		UtilityMethods.wait5Seconds();
		UtilityMethods.switchTOIframeCRM();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(totalAmountCRM));
		System.out.println("TOTAL AMOUNT IN CRM IS: "+totalAmountCRM.getText());
		if(totalAmountCRM.getText().contains(AX365Page.totalAmountINErp)){
			
			System.out.println("TOTAL AMOUNT IN CRM MATCHED WITH ERP");
			
			
		}
		
		if(totalAmountCRM.getText().contains("€") && AX365Page.currencyINErp.contains("EUR")){
			
			System.out.println("CURRENCY IN ERP MATCHED WITH CURRENCY IN CRM");
		}
		
		if(paymentStatus.getText().contains("Posted")){
			
			System.out.println("PAYMENT STATUS IS POSTED IN CRM");
		}
		
		if(paymentMethod.getText().contains("100")){
			
			System.out.println("PAYMENT METHOD IS INVOICE IN CRM");
		}
		
		if(orderOnInvoiceCRM.getText().contains(AX365Page.salesOrderNumberCRMtoERP)){
			
			System.out.println("ORDER IS SAME ON INVOICE ERP AND CRM");
		}
		
		
		UtilityMethods.switchToFirstTab();
		UtilityMethods.wait5Seconds();
		driver.findElement(By.xpath("//img[@title='Close' and @class = 'closeButton']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
	//	UtilityMethods.switchTOIframeCRM();
		
		driver.findElement(By.xpath("//img[@title='Close' and @class = 'closeButton']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchTOIframeCRM();
		UtilityMethods.wait5Seconds();
		new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(Topic1));
		UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.id("parentcontactid_lookupValue")));
	}
	
	public void createNewLeadPolandVAT(String salutation,String street,String zipCode,String city,
			String taxNumber,String taxNumberValue,String source,String productLine,
			String businessPhone,String nameFirst,String nameLast,String topic,
			String email, String company, String country) throws Throwable{

		//Select LEAD from main page
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(SaleDropDown));
		
		SaleDropDown.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(sales));
		
		sales.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		Leads.click();
		UtilityMethods.waitForPageLoadAndPageReady();
	
		boolean staleElement = true; 
		while(staleElement){
		  try{
				WebDriverWait wait = new WebDriverWait(driver,30);
				wait.until(ExpectedConditions.visibilityOf(NewLead));
				UtilityMethods.wait2Seconds();
			  new Actions(driver).moveToElement(NewLead).click().build().perform();
			  System.out.println("USER HAS CLICKED ON NEW BUTTON TO CREATE NEW LEAD");
			Reporter.addStepLog("USER HAS CLICKED ON NEW BUTTON TO CREATE NEW LEAD");
			abc = false;
		     if(abc == false){
		    	 
		    	 staleElement = false;
		    	 
		     }

		  } catch(StaleElementReferenceException e){
		    staleElement = true;
		    UtilityMethods.wait1Seconds();
		  }
		}
		UtilityMethods.wait3Seconds();


		//Select NEW Button

		System.out.println("USER HAS CLICKED ON NEW BUTTON TO CREATE NEW LEAD");
		Reporter.addStepLog("USER HAS CLICKED ON NEW BUTTON TO CREATE NEW LEAD");
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		UtilityMethods.switchTOIframeCRM();
		UtilityMethods.wait3Seconds();
		new WebDriverWait(driver,50).until(ExpectedConditions.visibilityOf(SourceDropdownOnHeaderBar));
		
		new Actions(driver).moveToElement(SourceDropdownOnHeaderBar).click(SourceDropdownOnHeaderBar).perform();
		System.out.println("USER IS FILLING MANDATORY FIELDS TO CREATE A LEAD");
		Reporter.addStepLog("USER IS FILLING MANDATORY FIELDS TO CREATE A LEAD");
		UtilityMethods.wait3Seconds();
		chooseResselerOption.click();

		UtilityMethods.wait3Seconds();
		
		System.out.println(salutation+street+zipCode+city+taxNumber);
		Salutation1.click();
		Salutation2.sendKeys(salutation);
		UtilityMethods.wait1Seconds();

		FirstName1.click();
		FirstName2.sendKeys(nameFirst);
		UtilityMethods.wait1Seconds();

		LastName1.click();
		LastName2.sendKeys(nameLast);
		UtilityMethods.wait1Seconds();

		Company1.click();
		Company2.sendKeys(company);
		UtilityMethods.wait1Seconds();

		ProductLine1.click();
		ProductLine2.click();
		UtilityMethods.wait1Seconds();

		Street1.click();
		Street2.sendKeys(street);
		UtilityMethods.wait1Seconds();

		Zip1.click();
		Zip2.sendKeys(zipCode);
		UtilityMethods.wait1Seconds();



		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", City1);
		
		City2.sendKeys(city);
		UtilityMethods.wait1Seconds();


		Country1.click();
		Country2.sendKeys(country);
		UtilityMethods.wait2Seconds();


		Email1.click();
		flag = 1;
		emailForLead = email;
		Email2.sendKeys(email);
		UtilityMethods.wait1Seconds();

		TaxNumber1.click();
		TaxNumber3.click();
		UtilityMethods.wait1Seconds();
		
		Topic1.click();
		Topic2.sendKeys(topic);
		UtilityMethods.wait1Seconds();

		System.out.println("USER IS FILLED THE COMPLETE DATA");
		Reporter.addStepLog("USER IS FILLED THE COMPLETE DATA");
		driver.switchTo().defaultContent();
		UtilityMethods.wait5Seconds();
		
		
		UtilityMethods.waitForPageLoadAndPageReady();
		SaveButton.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		System.out.println("USER IS SAVING THE LEAD AFTER FILLING DATA");
		Reporter.addStepLog("USER IS SAVING THE LEAD AFTER FILLING DATA");
		
		try{
			WebDriverWait wait2 = new WebDriverWait(driver,15);
			wait2.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("InlineDialog_Iframe")));
			if(SaveButtonIfRecordDuplicate.isDisplayed()){
				
				SaveButtonIfRecordDuplicate.click();
				System.out.println("DUPLICATES DETECTED : CLICK SAVE BUTTON");
				Reporter.addStepLog("DUPLICATES DETECTED : CLICK SAVE BUTTON");
				driver.switchTo().defaultContent();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait5Seconds();
			}
			
			
		}
		
		catch(Exception e){
			
			System.out.println("RECORD IS NOT A DUPLICATE ONE");
		}
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchTOIframeCRM();
		UtilityMethods.wait3Seconds();
		TaxNumber1.click();
		TaxNumber4.click();
		
		UtilityMethods.wait3Seconds();
		taxNumberField.click();
		new Actions(driver).moveToElement(taxNumberInput).sendKeys(taxNumberValue).perform();
		taxNumberInput.sendKeys(Keys.ENTER);
		UtilityMethods.waitForPageLoadAndPageReady();
		
	}
	
	
	//TODOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO//////////////
	

	public void createNewCustomeronCRM_NLD_STA(String firstName, String street, String city, String postalCode, String country) throws Throwable{
			
			
			UtilityMethods.waitForPageLoadAndPageReady();
			SaleDropDown.click();
			UtilityMethods.wait5Seconds();
			try{
			sales.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			Accounts.click();
			}
			catch(Exception e){
				UtilityMethods.waitForPageLoadAndPageReady();
				SaleDropDown.click();
				UtilityMethods.wait5Seconds();
				sales.click();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait5Seconds();
				Accounts.click();
			}
			UtilityMethods.waitForElementDisplayed(NewLead, 10);
			NewLead.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			
//			WebDriverWait wait1 = new WebDriverWait(driver,30);
//			wait1.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("contentIFrame1")));
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.switchTOIframeCRM();
			UtilityMethods.wait5Seconds();
			System.out.println("USER IS ENTERING THE DETAILS OF CUSTOMER");
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(AccountName));
		//	UtilityMethods.wait3Seconds();
			AccountName.click();
			AccountName1.sendKeys(firstName);
			UtilityMethods.waitForClickable(accountCategory);
			UtilityMethods.wait1Seconds();
			accountCategory.click();
			UtilityMethods.wait2Seconds();
			new Select(categorySelectBox).selectByVisibleText("Standard");
			UtilityMethods.wait2Seconds();
			UtilityMethods.waitForClickable(Email);
			Email.click();
			UtilityMethods.waitForClickable(Email01);;
			Email01.sendKeys("naltesting@tester.com");
			UtilityMethods.waitForClickable(Street);
			Street.click();
			UtilityMethods.waitForClickable(Street_1);
			Street_1.sendKeys(street);
			UtilityMethods.waitForClickable(City);
			City.click();
			UtilityMethods.waitForClickable(City_1);
			City_1.sendKeys(city);
			UtilityMethods.waitForClickable(zipCode);
			zipCode.click();
			UtilityMethods.waitForClickable(zipCode1);
			zipCode1.sendKeys(postalCode);
			UtilityMethods.waitForClickable(Country);
			Country.click();
			UtilityMethods.wait2Seconds();
//			HashMap<String, String> row = readExcelData(TestData.ADD_CUSTOMER_DATA_SHEET_CRM, TestData.CREATE_NL_DATA);
//			Country_1.sendKeys(row.get(TestData.COUNTRY_NAME).toString().trim());
			
			Country_1.sendKeys(country);
			Country_1.sendKeys(Keys.TAB);
			
			UtilityMethods.wait10Seconds();      //SHOULD BE CONVERTED TO 3 SECONDS
					
			driver.switchTo().defaultContent();
			UtilityMethods.wait3Seconds();
			
			SaveButton.click();
			UtilityMethods.wait15Seconds();
			
			try{
				
				WebDriverWait wait2 = new WebDriverWait(driver,15);
				wait2.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("InlineDialog_Iframe"))));
				driver.switchTo().frame(driver.findElement(By.id("InlineDialog_Iframe")));
				if(SaveButtonIfRecordDuplicate.isDisplayed()){
					
					SaveButtonIfRecordDuplicate.click();
					System.out.println("DUPLICATES DETECTED : CLICK SAVE BUTTON");
					Reporter.addStepLog("DUPLICATES DETECTED : CLICK SAVE BUTTON");
					UtilityMethods.waitForPageLoadAndPageReady();
					UtilityMethods.wait10Seconds();
					driver.switchTo().defaultContent();
					UtilityMethods.wait15Seconds();
				}
				
				
			}
			
			catch(Exception e){
				
				System.out.println("RECORD IS NOT A DUPLICATE ONE");
			}
			
			driver.navigate().refresh();
			try{
				UtilityMethods.waitForPageLoadAndPageReady();
			}
			catch(Exception e){
				((JavascriptExecutor)driver).executeScript("return window.stop");
			}
			UtilityMethods.wait10Seconds();
			UtilityMethods.switchTOIframeCRM();
			
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(CustomerGroupLabel));
			UtilityMethods.wait3Seconds();
			customerGroup = CustomerGroupLabel.getAttribute("title");
			if(customerGroup.contains("STA"))
				{
					System.out.println("Customer Tax Group is STA");
				}
				else 
				{
					System.out.println("Customer Tax Group is not STA");
				}
			
			accountNoLabel = accountNo.getText();
		}

	 public void createNewOrderERP() throws Throwable{
	 
		 	UtilityMethods.switchToFirstTab();
			UtilityMethods.wait3Seconds();
			boolean staleElement = true; 
			while(staleElement){
			  try{
					WebDriverWait wait = new WebDriverWait(driver,30);
					wait.until(ExpectedConditions.visibilityOf(NewOrder));
					UtilityMethods.wait2Seconds();
					new Actions(driver).moveToElement(NewOrder).click().build().perform();
					System.out.println("USER HAS CLICKED ON NEW ORDER BUTTON");
					Reporter.addStepLog("USER HAS CLICKED ON NEW ORDER BUTTON");
					abc = false;
			     if(abc == false){
			    	 
			    	 staleElement = false;
			    	 
			     }

			  } catch(StaleElementReferenceException e){
			    staleElement = true;
			    UtilityMethods.wait1Seconds();
			  }
			}
		
		//	NewOrder.click();
			flagForSalesOrderFromCRM = 1;
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.switchToNewTab();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait10Seconds();
			
			try{
				new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(okButtonOnCreateQoutation));
				okButtonOnCreateQoutation.click();
			}
			catch(Exception e){
				System.out.println("Problem is: "+e.getMessage());
				UtilityMethods.switchToSecondTab();
				okButtonOnCreateQoutation.click();
			}
			
			
			
	 	}


	public static void openAndSearchAccID() throws Throwable{

		UtilityMethods.waitForPageLoadAndPageReady();
		SaleDropDown.click();
		UtilityMethods.wait5Seconds();
		sales.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		Accounts.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.switchTOIframeCRM();
		UtilityMethods.wait3Seconds();
	//	searchACCId(AccountSearchId);
		
		
		
		}


//	public void searchACCIdArgentina(String AccountSearchId) throws InterruptedException {
//		WebDriverWait wait2 = new WebDriverWait(driver,15);
//		wait2.until(ExpectedConditions.visibilityOf(AccountSearch));
//		AccountSearch.click();
//		AccountSearch.sendKeys(AccountSearchId);
//		SearchButton.click();
//		UtilityMethods.wait7Seconds();
//		UtilityMethods.waitForTextPresentInElement("Search Results", VerifySearchResultsDisplay, 20);
//		UtilityMethods.waitForElementVisible(AccountSearchFirstResult);
//		UtilityMethods.wait3Seconds();
//		System.out.println("USER HAS SUCCESSFULLY SEARCHED ACCOUNT-ID");
//		new Actions(driver).contextClick(AccountSearchFirstRowName).perform();
//		UtilityMethods.wait3Seconds();
//		driver.findElement(By.xpath("//span[@title='Open']")).click();
//	}
		
	public static void searchACCId(String AccountSearchId) throws InterruptedException {
		WebDriverWait wait2 = new WebDriverWait(driver,15);
		wait2.until(ExpectedConditions.visibilityOf(AccountSearch));
		AccountSearch.click();
		AccountSearch.sendKeys(AccountSearchId);
		SearchButton.click();
		UtilityMethods.wait7Seconds();
		UtilityMethods.waitForTextPresentInElement("Search Results", VerifySearchResultsDisplay, 20);
		UtilityMethods.waitForElementVisible(AccountSearchFirstResult);
		UtilityMethods.wait3Seconds();
		System.out.println("USER HAS SUCCESSFULLY SEARCHED ACCOUNT-ID");
		new Actions(driver).contextClick(AccountSearchFirstRowName).perform();
		UtilityMethods.wait3Seconds();
		driver.findElement(By.xpath("//span[@title='Open']")).click();
	}
	
//	public void searchACCIdPoland(String AccountSearchId) throws InterruptedException {
//		WebDriverWait wait2 = new WebDriverWait(driver,15);
//		wait2.until(ExpectedConditions.visibilityOf(AccountSearch));
//		AccountSearch.click();
//		AccountSearch.sendKeys(AccountSearchId);
//		SearchButton.click();
//		UtilityMethods.wait7Seconds();
//		UtilityMethods.waitForTextPresentInElement("Search Results", VerifySearchResultsDisplay, 20);
//		UtilityMethods.waitForElementVisible(AccountSearchFirstResult);
//		UtilityMethods.wait3Seconds();
//		System.out.println("USER HAS SUCCESSFULLY SEARCHED ACCOUNT-ID");
//		new Actions(driver).contextClick(AccountSearchFirstRowName).perform();
//		UtilityMethods.wait3Seconds();
//		driver.findElement(By.xpath("//span[@title='Open']")).click();
//	}
	

	public void openCustomerAndCreateOpportunity() throws Throwable{
	//s	driver.switchTo().defaultContent();
		UtilityMethods.switchToParentWindow(UtilityMethods.switchToSubWindowAndReturnParentWindowHandler());
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait40Seconds();
		UtilityMethods.switchTOIframeCRM();
		new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOf(CustomerGroupLabel));
		UtilityMethods.wait3Seconds();
		emailOnAccountCRM = emailOnAccount.getText();
		System.out.println(emailOnAccountCRM);	
		flagForAccountToOppertunity = 1;
		flag=1;
		customerGroup = CustomerGroupLabel.getAttribute("title");
		if(customerGroup.contains("STA"))
			{
				System.out.println("Customer Tax Group is STA");
			}
			else 
			{
				System.out.println("Customer Tax Group is not STA");
			}
		
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(CountryLabel));
		UtilityMethods.wait3Seconds();
		countryLab = CountryLabel.getAttribute("title");
		if(countryLab.contains("Netherlands"))
			{
				System.out.println("Customer Country = Netherlands");
			}
			else 
			{
				System.out.println("Customer Country is not Netherlands");
			}
				
		AddOpportunityButton.click();
		UtilityMethods.wait10Seconds();	
		UtilityMethods.switchToParentWindow(UtilityMethods.switchToSubWindowAndReturnParentWindowHandler());
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("NavBarGloablQuickCreate"))));
		driver.switchTo().frame(driver.findElement(By.id("NavBarGloablQuickCreate")));
		
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(AddOpportunityAccountName));
		
		UtilityMethods.wait5Seconds();	
		
		//AddOpportunityTopicLabel.click();
		AddOpportunityTopic.sendKeys("Test Topic");
		AddOpportunityContactNameLabel.click();
//		driver.findElement(By.id("parentaccountid_lookupDiv")).sendKeys(Keys.TAB);
		
		UtilityMethods.wait3Seconds();
		driver.findElement(By.id("parentcontactid_ledit")).click();
		
		if(CRMPageSteps.argentinaFlag==true){
			driver.findElement(By.id("parentcontactid_ledit")).sendKeys("john doe");
		}
		else if(CRMPageSteps.netherlandFlag==true){
			driver.findElement(By.id("parentcontactid_ledit")).sendKeys("Lesbrown contact");
		}
		else if(CRMPageSteps.polandFlag==true){
			driver.findElement(By.id("parentcontactid_ledit")).sendKeys("Isabella Mark");
		}
		else if(CRMPageSteps.ukFlag==true){
			driver.findElement(By.id("parentcontactid_ledit")).sendKeys("Alex GB contact");
		}
		
		driver.findElement(By.id("parentcontactid_ledit")).sendKeys(Keys.TAB);
		UtilityMethods.wait2Seconds();
////		try{
	
	//	UtilityMethods.clickElemetJavaSciptExecutor(driver.findElement(By.id("parentcontactid_lookupSearch")));
	//	UtilityMethods.wait7Seconds();
//		int size =  driver.findElements(By.xpath("/html/body/iframe[2]")).size();
//		System.out.println("Size is: "+size);
//		driver.switchTo().frame(driver.findElement(By.id("customScriptsFrame")));
//		System.out.println("User switch to main iframe");
	//	UtilityMethods.wait5Seconds();
		//driver.switchTo().frame(driver.findElement(By.id("customScriptsFrame")));
		UtilityMethods.waitForPageLoadAndPageReady();
	//	UtilityMethods.wait7Seconds();
		//driver.switchTo().frame(driver.findElement(By.xpath("/html/body/iframe[2]")));
		int size2 =  driver.findElements(By.xpath("/html/body/iframe[2]")).size();
		System.out.println("Size is: "+size2);
		System.out.println("User switch to child iframe");
		//UtilityMethods.waitForPageLoadAndPageReady();
	//	driver.switchTo().frame(driver.findElement(By.xpath("/html/body/iframe[2]")));
	
	//	driver.switchTo().defaultContent();
		UtilityMethods.wait7Seconds();
//		try{
//			new Actions(driver).moveToElement(driver.findElement(By.xpath("//nobr[@class='ms-crm-IL-MenuItem-Title ms-crm-IL-MenuItem-Title-Rest']"))).click().build().perform();
//		}
//		catch(Exception e){
//			
//			JavascriptExecutor executor = (JavascriptExecutor) driver;
//			executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//nobr[@class='ms-crm-IL-MenuItem-Title ms-crm-IL-MenuItem-Title-Rest']")));
//
//		}

	 //   UtilityMethods.wait2Seconds();
		Select select = new Select(AddOpportunitySource);
		select.selectByIndex(0);
		
		AddOpportunityProductLine.click();
		Select select2 = new Select(AddOpportunityProductLineList);
		select2.selectByIndex(0);

		
		if(AddOpportunityOwner.getText().contains("Yasir Memon"))
		{
			
			System.out.println("Add Opportunity-Source Name = " + AddOpportunityOwner.getText());
			
		}
		else
		{
			System.out.println("Incorrect Source Name = " + AddOpportunityOwner.getText());
		}
		
		if(AddOpportunityCurrencyName.getText().contains("Euro"))
		{
			
			System.out.println("Add Opportunity-Currency Name = " + AddOpportunityCurrencyName.getText());
			
		}
		else
		{
			System.out.println("Incorrect Currency Name = " + AddOpportunityCurrencyName.getText());
		}
		
		driver.switchTo().defaultContent();
		AddOpportunitySaveButton.click();
		
		System.out.println("USER HAS SUCCESSFULLY CREATED OPPORTUNITY");
		
		}


	public void generateQuoteFromOpportunity() throws Throwable{
			
		UtilityMethods.wait3Seconds();
		driver.switchTo().defaultContent();
		UtilityMethods.switchTOIframeCRM();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(FirstTopicOpportunity));
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(FirstTopicOpportunityTable));
		FirstTopicOpportunityTable.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.switchTo().defaultContent();
		UtilityMethods.switchTOIframeCRM();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(OpportunityDetailsNextStageButton));
		OpportunityDetailsNextStageButton.click();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(OpportunityDevelopStageActive));
		OpportunityDetailsNextStageButton.click();
		driver.switchTo().defaultContent();
			
		}
		

	 public void createNewCustomeronCRM_NLD_RC(String firstName, String street, String city, String postalCode, String countryName) throws Throwable{
		 

			UtilityMethods.waitForPageLoadAndPageReady();
			SaleDropDown.click();
			UtilityMethods.wait5Seconds();
			sales.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			Accounts.click();
			UtilityMethods.waitForElementDisplayed(NewLead, 10);
			NewLead.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			
//			WebDriverWait wait1 = new WebDriverWait(driver,30);
//			wait1.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("contentIFrame1")));
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.switchTOIframeCRM();
				
			System.out.println("USER IS ENTERING THE DETAILS OF CUSTOMER");
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(AccountName));
			UtilityMethods.wait3Seconds();
			AccountName.click();
			AccountName1.sendKeys(firstName);
			UtilityMethods.waitForClickable(Email);
			Email.click();
			UtilityMethods.waitForClickable(Email01);;
			Email01.sendKeys("johntesting1@tester.com");
			UtilityMethods.waitForClickable(Street);
			Street.click();
			UtilityMethods.waitForClickable(Street_1);
			Street_1.sendKeys(street);
			UtilityMethods.waitForClickable(City);
			City.click();
			UtilityMethods.waitForClickable(City_1);
			City_1.sendKeys(city);
			UtilityMethods.waitForClickable(zipCode);
			zipCode.click();
			UtilityMethods.waitForClickable(zipCode1);
			zipCode1.sendKeys(postalCode);
			UtilityMethods.waitForClickable(Country);
			Country.click();
			UtilityMethods.wait2Seconds();
			Country_1.sendKeys(countryName);
			Country_1.sendKeys(Keys.TAB);
			
			UtilityMethods.wait3Seconds();
			
			
			
			driver.switchTo().defaultContent();
			UtilityMethods.wait3Seconds();
			
			SaveButton.click();
			UtilityMethods.wait15Seconds();
			
			try{
				
				WebDriverWait wait2 = new WebDriverWait(driver,15);
				wait2.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("InlineDialog_Iframe")));
				if(SaveButtonIfRecordDuplicate.isDisplayed()){
					
					SaveButtonIfRecordDuplicate.click();
					System.out.println("DUPLICATES DETECTED : CLICK SAVE BUTTON");
					Reporter.addStepLog("DUPLICATES DETECTED : CLICK SAVE BUTTON");
					driver.switchTo().defaultContent();
					UtilityMethods.wait15Seconds();
				}
				
				
			}
			
			catch(Exception e){
				
				System.out.println("RECORD IS NOT A DUPLICATE ONE");
			}
		 
			//driver.navigate().refresh();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.switchTOIframeCRM();
			
			taxNumberProvidedLabel.click();
			Select select = new Select(taxNumberProvidedList);
			select.selectByIndex(1);
			UtilityMethods.wait3Seconds();
			taxNumberField_Account.click();
			taxNumberInput_Account.sendKeys("NL850025047B01");
			taxNumberInput_Account.sendKeys(Keys.TAB);
			UtilityMethods.wait5Seconds();
			saveButtonFooter.click();
			UtilityMethods.wait20Seconds();
			
			if(businessRelationshipLabel.getText().contains("RC"))
			{
				
				System.out.println("Sales Tax Group = " + businessRelationshipLabel.getText());
				
			}
			else
			{
				System.out.println("Incorrect Sales Tax Group = " + businessRelationshipLabel.getText());
			}
			
		 
	 }
	 
	 public void openAndSearchSTAAccID(String accountSearchID) throws Throwable{
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.switchToParentWindow(UtilityMethods.switchToSubWindowAndReturnParentWindowHandler());
			UtilityMethods.wait5Seconds();
			UtilityMethods.clickElemetJavaSciptExecutor(SaleDropDown);
	//		SaleDropDown.click();
			UtilityMethods.wait5Seconds();
			sales.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			Accounts.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.switchTOIframeCRM();
			UtilityMethods.wait3Seconds();
			WebDriverWait wait2 = new WebDriverWait(driver,15);
			wait2.until(ExpectedConditions.visibilityOf(AccountSearch));
			AccountSearch.click();
			AccountSearch.sendKeys(accountSearchID);
			SearchButton.click();
			UtilityMethods.waitForTextPresentInElement("Search Results", VerifySearchResultsDisplay, 20);
			UtilityMethods.waitForElementVisible(AccountSearchFirstResult);
			UtilityMethods.wait3Seconds();

			AccountSearchFirstRowName.click();
			System.out.println("USER HAS SUCCESSFULLY SEARCHED ACCOUNT-ID");
			
			
		}
	 
	public void validateAccountValuesAfterOrderInvoiced() throws Throwable{
			
			//Can be used for account validations
			if(flagForSalesOrderFromCRM==0){
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.switchTOIframeCRM();
			UtilityMethods.wait5Seconds();
			new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(Topic1));
			UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.id("parentaccountid_lookupValue")));
			driver.findElement(By.id("parentaccountid_lookupValue")).click();
			}
			//TODO: Validation on account]
			if(flagForSalesOrderFromCRM==1){
				driver.navigate().refresh();
			}
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			UtilityMethods.switchToFirstTab();
			UtilityMethods.switchTOIframeCRM();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(accountNo));
			UtilityMethods.wait5Seconds();
			accountNumber = accountNo.getText();
			System.out.println(accountNumber);	
			//div[contains(text(),'000291802')]
			WebElement invoice;
			try{
			//	UtilityMethods.scrollToWebElement(driver.findElement(By.xpath("//div[contains(text(),'"+AX365Page.salesOrderNumberCRMtoERP.trim()+"')]")));
				driver.findElement(By.xpath("//h3[contains(text(),'TRIAL')]")).click();
				invoice = driver.findElement(By.xpath("//div[contains(text(),'"+AX365Page.invoiceNumber.trim()+"')]"));
				UtilityMethods.scrollToElement(invoice);
				System.out.println("INVOICE NUMBER IN CRM = "+invoice.getText()+"INVOICE NUMBER FROM SALES ORDER: "+AX365Page.invoiceNumber);
			}
			catch(Exception e){
				
				driver.findElement(By.xpath("(//a[@title='Sort by Order ID'])[1]")).click();
			//	UtilityMethods.pageDown();
			//	UtilityMethods.scrollToWebElement(driver.findElement(By.xpath("//div[contains(text(),'"+AX365Page.salesOrderNumberCRMtoERP.trim()+"')]")));
				driver.findElement(By.xpath("//div[contains(text(),'TRIAL')]")).click();
				UtilityMethods.wait5Seconds();
				invoice = driver.findElement(By.xpath("//div[contains(text(),'"+AX365Page.invoiceNumber.trim()+"')]"));
				UtilityMethods.scrollToElement(invoice);
				System.out.println("INVOICE NUMBER IN CRM = "+invoice.getText()+" INVOICE NUMBER FROM SALES ORDER: "+AX365Page.invoiceNumber);
				
			}
		
			new Actions(driver).contextClick(invoice).perform();
			UtilityMethods.wait3Seconds();
			driver.findElement(By.xpath("//span[@title='Open']")).click();
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.switchToFirstTab();
			UtilityMethods.wait7Seconds();
			UtilityMethods.switchTOIframeCRM();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(totalAmountCRM));
			System.out.println("TOTAL AMOUNT IN CRM IS: "+totalAmountCRM.getText());
			
			if(flagForSalesOrderFromCRM==0)
			{
				if(totalAmountCRM.getText().contains(AX365Page.totalAmountINErp))
					{
					System.out.println("TOTAL AMOUNT IN CRM MATCHED WITH ERP");
					Reporter.addStepLog("TOTAL AMOUNT IN CRM: "+totalAmountCRM.getText()+" MATCHED WITH ERP: "+AX365Page.totalAmountINErp);
					}
			
				if(totalAmountCRM.getText().contains("€") && AX365Page.currencyINErp.contains("EUR"))
					{
					System.out.println("CURRENCY IN ERP MATCHED WITH CURRENCY IN CRM");
					Reporter.addStepLog("CURRENCY IN CRM: € MATCHED WITH ERP: EUR");
					
					}
			
			}
			UtilityMethods.switchToFirstTab();
			UtilityMethods.wait5Seconds();
			driver.findElement(By.xpath("//img[@title='Close' and @class = 'closeButton']")).click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();

			if(flagForSalesOrderFromCRM==0)
			{
				driver.findElement(By.xpath("//img[@title='Close' and @class = 'closeButton']")).click();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait5Seconds();
			
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.switchTOIframeCRM();
				UtilityMethods.wait5Seconds();
				new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(Topic1));
				UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.id("parentcontactid_lookupValue")));
			
			}
		

		
		}
	
	public CRMPage(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);

	}

}
