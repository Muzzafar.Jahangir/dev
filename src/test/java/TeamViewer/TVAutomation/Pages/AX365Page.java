package TeamViewer.TVAutomation.Pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDActionURI;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationLink;
import org.apache.pdfbox.util.PDFTextStripper;
import org.junit.ComparisonFailure;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;

import TeamViewer.TVAutomation.Stepdefinitions.CRMPageSteps;
import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.PropertyReader;
import TeamViewer.TVAutomation.Utils.UtilityMethods;
import cucumber.api.java.Before;

public class AX365Page  extends DriverFactory  {
	
	private HomePage homePage= new HomePage(driver);
	private CRMPage cmpPage =  new CRMPage(driver);
//	private ConfirmationPage confirmPage = new ConfirmationPage(driver);


	public static String inVoiceNo;
	public static String customerName;
	public static String taxAmountInD365;
	public static String subTotalAmountInD365;
	public static String invoiceTotal;
	public static String subTotalInD365AfterContractCorrect;
	public static String TaxAfterContractCorrectioninString;
	public static String finalTotalSuminTotalInStringContract;
	public static String salesOrderNumberOnOrder;
	public static String lastLineDiscount;
	public static String lastLineAmount;
	public static String settlementNumberOfInvoice;
	public static String visaCard = "Visa";
	public static String americanCard = "AMEX";
	public static String masterCard = "MasterCard";
	public static ArrayList<WebElement> journal = new ArrayList<WebElement>();
	public static ArrayList<String> journalValues = new ArrayList<String>();
	public static boolean modification = false;
	public static boolean consolidation = false;
	String customerDeliveryAddress= "";
	String currentLanguageOfInvoice = "";
	String languageOfInvoiceAfterChange = "";
	String date = "";
	String subToTalOfALicenseBeforeSwitch="";
	String subToTalOfALicenseAfterSwitch = "";
	String currencyOfTheCountryy ="";
	String cusAccontNur="";
	String customerSalesTaxGroup;
	String languageOfStore;
	String salesTaxCode;
	String mainAccountNumber;
	String salesTaxPayAblNumber;
	String revenueRecognitionSaleOrderDateValue;
	public static String revenueRecognitionItemValue;
	String revenueRecognitionQuantityValue;
	public static String revenueRecognitionCurrencyValue;
	String revenueRecognitionAmountValue;
	String revenueRecognitionAmountInAccoutingCurrencyValue;
	String revenueRecognitionDeferralAmountValue;
	String revenueRecognitionRealisedRevenueValue;
	String revenueRecognitionCalculatedFromDateValue;
	String revenueRecognitionValidDateValue;
	String revenueRecognitionEndDateValue;
	String revenueRecognitionPACLicenseValue;
	String productNameForInvoice;
	public static String quoteCheckOutPageUrl;
	String salesOrderID;
	String quoteID;
	public static String product;
	public static String invoiceVoucher;
	public static String salesQuotationNumber;
	public static String currencyINErp;
	public static String totalAmountINErp;
	public static String totalAmountCRM;
	public static String pacLicenseERP;
	public static String salesOrderNumberCRMtoERP;
	public static String totalTaxInJournalConfirmation = "";
	public static String taxCodeInJournalConfirmation;
	public static String amountWithTaxInJournalConfirmation = "";
	public static String baseInJournalConfirmation = "";
	public static String invoiceNumber;
	public static String productNameinText;
	public static boolean transferFlow;

	@FindBy (xpath= "//input[@type='email']")
	public WebElement emailField;

	@FindBy (xpath= "//input[@type='submit']")
	public WebElement nextButton;


	@FindBy (xpath= "//input[@id='passwordInput']")
	public WebElement passwordField;


	@FindBy (xpath= "//span[@id='submitButton']")
	public WebElement SignInButton;

	@FindBy (xpath= "//input[@type='submit']")
	public WebElement yesButtonForStaySignedIn;

	@FindBy (xpath= "//button[@id = 'CompanyButton']")
	public WebElement chooseCompanyButton;

	@FindBy (xpath= "(//*[@title='Open'])[15]")
	public WebElement chooseCompanyDropdown;


	@FindBy (xpath= "//input[@title = 'TVDE']")
	public WebElement chooseTVDEOption;

	@FindBy (xpath= "(//*[contains(text(),'Channel reference ID')])[2]")
	public WebElement channelIdFilterIcon;

	@FindBy (xpath= "//input[contains(@id,'_FilterField')]")
	public WebElement channelIdFilterField;

	@FindBy (xpath= "(//input[contains(@id,'_FilterField')])[2]")
	public WebElement InvoiceVoucherFilterField;
	
	@FindBy (xpath= "(//input[contains(@id,'_FilterField')])[1]")
	public WebElement InvoiceVoucherFilterField2;
	
	@FindBy (xpath= "(//input[contains(@id,'_FilterField')])[2]")
	public WebElement billingFilterField;

	@FindBy (xpath= "//*[@aria-label='Apply']")
	public WebElement applyButtonOnChannelFilter;
	
	@FindBy (xpath= "//input[contains(@id,'SalesOrder_AccountNum_input')]")
	public WebElement mainAccountElement;
	
	@FindBy (xpath= "//input[@name = 'QuickFilterControl_Input']")
	public WebElement inputFieldToEnterSalesTaxCode;
	
	@FindBy (xpath= "//input[contains(@id,'TaxAccountGroup_TaxAccountOutgoing_input')]")
	public WebElement salesTaxPayAbleElement;
	
	@FindBy(id = "ShellMail_link")
	public WebElement outlook;
	
	@FindBy (id= "__CustTrans_Voucher_ApplyFilters_0_label")
	public WebElement applyButtonOnInvoiceFilter;

	@FindBy (xpath= "//*[contains(text(),'View settlements')]")
	public WebElement viewSettlementsOnHeaderBar;

	@FindBy (xpath= "(//input[@name='SettlementView_TransactionType'])[1]")
	public WebElement transactionTypeIsSaleOrder;

	@FindBy (xpath= "(//input[@name='SettlementView_TransactionType'])[2]")
	public WebElement transactionTypeIsPayment;

	@FindBy (xpath= "(//input[@name = 'SettlementView_Voucher'])[2]")
	public WebElement settlementID;


	@FindBy (xpath= "//span[contains(@id,'mibTransactions_label')]")
	public WebElement TransactionsButton;

	@FindBy (xpath= "(//*[contains(text(),'Customer')])[1]")
	public WebElement customerOnHeaderBar;


	@FindBy (xpath= "//input[@name='CustTable_Name']")
	public WebElement getCustomerName;


	@FindBy (name= "DetailsHeader_SalesId")
	public WebElement salesOrderValue;


	@FindBy (xpath= "//span[contains(text(),'View details')]")
	public WebElement openSalesOrderDetails;

	@FindBy (xpath= "//span[contains(text(),'Totals')]")
	public WebElement openTotalsPage;

	@FindBy (xpath= "(//span[contains(text(),'Totals')])[2]")
	public WebElement openTotalsPageForSwitchedOrder;

	@FindBy (xpath= "//input[contains(@id, '_SumLines')]")
	public WebElement subTotalsOnD365;


	@FindBy (xpath= "//input[contains(@id, '_SumTax_input')]")
	public WebElement taxAmountsOnD365;


	@FindBy (xpath= "//input[contains(@id, '_InvoiceAmount_input')]")
	public WebElement totalInvoiceAmountsOnD365;

	@FindBy (xpath= "//span[contains(text(),'Confirm sales order')]")
	public WebElement confirmSalesOrderButton;

	@FindBy (xpath= "(//span[contains(text(),'Confirm sales order')])[2]")
	public WebElement confirmSwitchedSalesOrderButton;

	@FindBy (xpath= "(//span[contains(text(),'OK')])[1]")
	public WebElement clickOkForConfirmSalesOrder;
	
	@FindBy (xpath= "(//span[contains(text(),'OK')])[2]")
	public WebElement clickOkForConfirmSalesOrder2;

	@FindBy (xpath= "(//span[contains(text(),'OK')])[3]")
	public WebElement clickOkToContinuePopUp;
	
	@FindBy(name = "Yes")
	public WebElement yes;

	@FindBy (xpath= "(//span[contains(@id,'tmvButtonUpdateInvoice_label')])[1]")
	public WebElement inVoiceButton;
	
	@FindBy (xpath= "(//span[contains(@id,'tmvButtonUpdateInvoice_label')])[2]")
	public WebElement inVoiceButton2;

	@FindBy (xpath= "//span[contains(@id,'tmvButtonJournalInvoice_label')]")
	public WebElement inVoiceButtonUnderJournalForLasernetPrint;

	@FindBy (xpath= "(//span[contains(@id,'tmvButtonJournalInvoice_label')])[2]")
	public WebElement inVoiceButtonUnderJournalForLasernetPrintOfSwitchedOrder;


	@FindBy (xpath= "(//span[contains(@id,'tmvButtonJournalInvoice_label')])[2]")
	public WebElement inVoiceButtonUnderJournalForCorporateDiscount;

	@FindBy (xpath= "//input[contains(@id , 'CustInvoiceJour_InvoiceNum_Grid_input')]")
	public WebElement inVoiceNumber;

	@FindBy (xpath= "//span[contains(@id , 'TMVCustInvoiceCorrection_label')]")
	public WebElement invoiceCorrectionButtonOnMainHeader;

	@FindBy (xpath= "//span[contains(@id , 'TMVCustInvoiceCorrectionCreate_label')]")
	public WebElement correctInvoiceButton;

	@FindBy (xpath= "(//span[contains(@id,'tmvButtonJournalInvoice_label')])[2]")
	public WebElement inVoiceButtonForLasernetPrintAfterMigrationAndSwitching;

	@FindBy (xpath= "//span[contains(@id,'TransactVoucher_label')]")
	public WebElement voucherButtonOnInvoiceInJournal;
	

	@FindBy (xpath= "//span[contains(@id,'LACMenuButton_label')]")
	public WebElement lasernetPrintdropdown;

	@FindBy (xpath= "//span[contains(@id,'LACForceRegeneration_toggle')]")
	public WebElement forceReRun;


	@FindBy (xpath= "//span[contains(text(),'Resend')]")
	public WebElement reSendButton;


	@FindBy (xpath= "//span[contains(text(),'Lasernet Original preview')]")
	public WebElement lasernetOriginalPreviewButton;


	@FindBy (xpath= "(//span[contains(@id,'tmvButtonUpdateInvoice_label')])[2]")
	public WebElement inVoiceButtonForSwitchedOrder;


	@FindBy (xpath= "//button[contains(@id, '_No')]")
	public WebElement noButtonOnInvoiceSchedule;

	@FindBy (xpath= "(//span[contains(@id,'OK_label')])")
	public WebElement okButtonForPostingInvoice;

	@FindBy (xpath= "(//button[contains(@id,'SysBoxForm')])[1]")
	public WebElement okButtonForPostingDocument;

	@FindBy (xpath= "(//*[contains(text(),'Cancel contract')])")
	public WebElement cancelContractButtonForSimpleOrder;

	@FindBy (xpath= "(//span[contains(@id,'TMVSalesContractCancelTable')])[2]")
	public WebElement cancelContractButtonForSwitchedOrder;



	@FindBy (xpath= "(//*[contains(text(),'Cancel contract')])[1]")
	public WebElement cancelContractButtonAfterPepetual;

	@FindBy (xpath= "(//span[contains(@id ,'TMVSalesContractCancelTable_label')])[2]")
	public WebElement cancelContractButtonAfterMigration;

	@FindBy (xpath= "//*[contains(text(),'OK')]")
	public WebElement cancelContractPopUpOkButton;


	@FindBy (xpath= "//button[contains(@id,'SysBoxForm')]")
	public WebElement closeButtonIfOrderIsCancelledAlready;

	//********************************************Perpetual Orders Switch*****************************************
	@FindBy (xpath= "//div[@aria-label = 'Account']")
	public WebElement customerAccountFilterIcon ;

	@FindBy (xpath= "(//div[contains(@id,'CustTrans_Voucher')])[1]")
	public WebElement voucherColumnToSearchiNVOICEnUMBER ;



	@FindBy (xpath= "//input[contains(@id , 'CustTable_AccountNum_input')]")
	public WebElement customerOrderNumberValue ;

	@FindBy (xpath= "(//span[contains(text(),'Sell')])[2]")
	public WebElement clickSELLbuttonOnCustomerDetailPage;

	@FindBy (xpath= "//span[starts-with(@id, 'CustTable') and contains(text(),'Sales order')]")
	public WebElement salesOrderButtonOnCustomerDetailPage;
	
	@FindBy (xpath= "(//span[contains(text(),'Sales order')])[1]")
	public WebElement salesOrderButtonOnCustomerDetailPage2;

	@FindBy (xpath= "(//*[contains(text(),'Processing operation - Order invoice')])[2]")
	public WebElement processingOperationOrderInvoice;


	@FindBy (xpath= "(//span[contains(@id,'HeaderTitle')])[2]")
	public WebElement salesOrderNumber;

	@FindBy (xpath= "(//span[contains(@id,'HeaderTitle')])[1]")
	public WebElement salesOrderNumber2;

	@FindBy (xpath= "((//span[contains(@id,'HeaderTitle')]))[3]")
	public WebElement salesOrderNumberAfterSwitchingMigration;
	
	@FindBy (xpath= "((//span[contains(@id,'HeaderTitle')]))[2]")
	public WebElement salesOrderNumberAfterSwitchingMigration2;

	@FindBy (xpath= "//span[contains(@id,'HeaderTitle')]")
	public WebElement salesOrderNumberAfterQoutation;

	@FindBy (xpath= "//input[contains(@id,'ItemId_input')]")
	public WebElement itemNumberInputField;

	@FindBy (xpath= "(//input[@title='TVB0001'])[1]")
	public WebElement businessLicenseInputValue;

	@FindBy (xpath= "(//input[@title='TVB0001'])[2]")
	public WebElement businessLicenseInputValue1;


	@FindBy (xpath= "(//input[@title='TV14B0001'])[1]")
	public WebElement perpetualBusinessInputValue;
	
	@FindBy (xpath= "(//input[@title='TV14B0001'])[2]")
	public WebElement perpetualBusinessInputValue1;

	@FindBy (xpath= "(//div[contains(@id,'InventItemDimTmpFilter_ItemId')])[3]")
	public WebElement perpetualBusinessInputValueVersion13;

	@FindBy (xpath= "(//input[@title='TV14P0001'])[1]")
	public WebElement perpetualPremiumInputValue;

	@FindBy (xpath= "//input[@title='TV14C0001']")
	public WebElement perpetualCorporateInputValue;

	@FindBy (xpath= "//input[@title='TVP0001']")
	public WebElement premiumLicenseInputValue;

	@FindBy (xpath= "//input[@title='TVC0001']")
	public WebElement corporateLicenseInputValue;

	@FindBy (xpath= "//input[contains(@id,'itemName_input')]")
	public WebElement productNameOfFirstOrder;
	
	@FindBy (xpath= "//input[contains(@id,'ItemName_input')]")
	public WebElement productNameOfFirstOrder2;

	@FindBy (xpath= "//input[@name='itemName']")
	public WebElement productNameOfSalesQoutationOrder;
	
	@FindBy (xpath= "//input[@name='ItemName']")
	public WebElement productNameOfSalesQoutationOrder2;

	@FindBy(xpath = "(//input[@name = 'Voucher'])[1]")
	public WebElement invoiceVoucherValue;
	
	@FindBy(xpath = "//span[contains(@id, 'CustInvoiceJournal_') and contains(text(),'Voucher')]")
	public WebElement invoiceVoucherButton;

	@FindBy (xpath= "(//*[@title = 'TeamViewer Premium / TeamViewer Premium Yearly Subscription'])[1]")
	public WebElement productNameOfSwitchedPremiumOrder;

	@FindBy (xpath= "(//*[@title = 'TeamViewer Corporate / TeamViewer Corporate Yearly Subscription'])[1]")
	public WebElement productNameOfSwitchedCorporateOrder;

	@FindBy (xpath= "(//span[contains(@id,'_Complete_label')])[1]")
	public WebElement completeButtonForSimpletOrder;
	
	@FindBy (xpath= "(//span[contains(@id,'_Complete_label')])[2]")
	public WebElement completeButtonForSimpletOrder2;

	@FindBy (xpath= "(//span[contains(text(),'Price information')])[2]")
	public WebElement priceInformation;

	@FindBy (xpath= "//input[contains(@id,'TMVLastSalesLineDisc_input')]")
	public WebElement discountAmount;

	@FindBy (xpath= "//input[contains(@id,'TMVLastLineAmount_input')]")
	public WebElement finalAmount;

	@FindBy (xpath= "//input[@title='Periodic discount']")
	public WebElement periodicDis;

	@FindBy (xpath= "//input[@title='Manual discount']")
	public WebElement manualDis;

	@FindBy(id = "otherTileText")
	public WebElement userOtherAccount;

	@FindBy (xpath= "(//span[contains(@id,'_Complete_label')])[2]")
	public WebElement completeButtonForSwitchedOrder;

	@FindBy (xpath= "(//div[@class='section-page-arrow'])[35]")
	public WebElement PaymentExpander;
	
	@FindBy(xpath = "//span[contains(text(),'Cancel') and contains(@id,'_CancelButton_label')]")
	public WebElement cancelButton;

	@FindBy (xpath= "(//span[contains(text(),'Add')])[4]")
	public WebElement addPaymentMethodButton;

	@FindBy (xpath= "//span[contains(@id,'AddBtn_label')]")
	public WebElement addPaymentMethodButtonForSwitchedOrder;


	@FindBy (xpath= "(//input[contains(@id,'enderTypeId_input')])[3]")
	public WebElement enterPaymentMethodCode;

	@FindBy (xpath= "//input[contains(@id,'MCRCustPaymTable_CardTypeId_input')]")
	public WebElement enterCreditCardName;

	@FindBy (xpath= "//input[contains(@id,'TMVCreditCard_M_editCreditCard_input')]")
	public WebElement enterNameOfCardHolder;

	@FindBy (xpath= "(//input[contains(@id,'displayProcessorsName_input')])[1]")
	public WebElement visaWireCardOption;

	@FindBy (xpath= "//span[contains(@id,'OKButton_label')]")
	public WebElement okButtonOnCustomerInformation;
	
	@FindBy(xpath = "//h2[contains(text(),'Payments')]")
	public WebElement payments;

	@FindBy (xpath= "//input[@title='Credit Card']")
	public WebElement creditCardAccepted;


	@FindBy (xpath= "//span[contains(@id,'SubmitButton')]")
	public WebElement submitButtonOnSaleOrderSummary;


	//**********************************************************Switching***************************************
	@FindBy (xpath= "//span[contains(@id,'TMVSwitchTransfer_label')]")
	public WebElement switchButton;


	@FindBy (xpath= "//input[contains(@title , 'TVP0001')]")
	public WebElement switchtoPremiumButton;

	@FindBy (xpath= "//input[contains(@title , 'TVC0001')]")
	public WebElement switchtoCorporateButton;

	@FindBy (xpath= "//span[contains(@id,'OKButton_label')]")
	public WebElement okButtonOnSwitchProducts;

	//**********************************************************Upgrading***************************************

	@FindBy (xpath= "//span[contains(@id,'TMVContractLineMenuBtn_label')]")
	public WebElement contractDropDownForTV14Products;

	@FindBy (xpath= "//span[contains(@id,'TMVTmpMCRUpSellOrderEntryUpgrade_label')]")
	public WebElement upgradeButtonOnDropDownForTV14Products;

	@FindBy (xpath= "//span[contains(@id,'TMVTmpMCRUpSellOrderEntryUpdate_label')]")
	public WebElement updateButtonOnDropDownForTV14Products;



	@FindBy (xpath= "//span[contains(@id,'TMVTmpMCRUpSellOrderEntryMigration_label')]")
	public WebElement migrateButtonOnDropDownForTV14Products;

	@FindBy(xpath = "//*[starts-with(@id,'SalesTable') and @name='SystemDefinedRefreshButton']")
	public WebElement refreshRecord;


	@FindBy (xpath= "//input[contains(@title , 'TV14C0001')]")
	public WebElement TV14C0001Product;


	@FindBy (xpath= "(//input[@name = 'MCRUpSellTableOE_ItemId'])[1]")
	public WebElement TVB0001ProductMigration;


	@FindBy (xpath= "(//input[@name = 'MCRUpSellTableOE_ItemId'])[2]")
	public WebElement TVP0001ProductMigration;


	@FindBy (xpath= "(//input[@name = 'MCRUpSellTableOE_ItemId'])[3]")
	public WebElement TVC0001ProductMigration;

	@FindBy (xpath= "//input[contains(@title , 'TV14P0001')]")
	public WebElement TV14P0001Product;

	@FindBy (xpath= "//span[contains(text(),'Master')]")
	public WebElement masterVsActivate;

	@FindBy (xpath= "//input[contains(@id,'ItemId_input')]")
	public WebElement itemId;

	@FindBy (xpath= "(//input[contains(@id,'ItemId_input')])[1]")
	public WebElement itemIdAfterUpdate;


	//*********************************************************Terminate Contract************************************************

	@FindBy (xpath= "//span[contains(@id,'TMVSalesContractTerminateTable_label')]")
	public WebElement terminateContractButton ;

	@FindBy (xpath= "//span[contains(@id, 'Ok_label')]")
	public WebElement okButtonForConfirmTerminating ;

	@FindBy (xpath= "(//*[contains(@title,'Terminated')])[2]")
	public WebElement terminatedTextVerifyIt ;



	//***************************************************Correct Contract *************************************************

	@FindBy (xpath= "//button[contains(@id,'_Invoice_button')]")
	public WebElement inVoiceButtonOnMainHeaderBar;


	@FindBy (xpath= "//span[contains(@id,'TMVContractCorrection_label')]")
	public WebElement contractCorrectionButton;


	@FindBy (xpath= "//span[contains(@id,'Yes_label')]")
	public WebElement yesButton;

	@FindBy (xpath= "//input[contains(@id,'Due_DueDate_input')]")
	public WebElement newDueDate;



	@FindBy (name = "ExtensionTableCorrection_DueDate")
	public WebElement calenderIcon;

	@FindBy (xpath= "//a[contains(text(),'30')]")
	public WebElement dateIs31st;

	@FindBy (xpath= "//input[contains(@id,'ExtensionTableOrig_LanguageId_input')]")
	public WebElement currentLanguage;

	@FindBy (xpath= "//input[contains(@id,'ExtensionTableCorrection_LanguageId_input')]")
	public WebElement languageInputFieldForInvoiceCorrection;


	@FindBy (xpath= "//input[@title='en-US']")
	public WebElement USAEnglishLanguage;

	@FindBy (xpath= "//input[@title = 'German']")
	public WebElement Germen_DE_Language;


	@FindBy (xpath= "//textarea[contains(@id , 'ExtensionTableCorrection_IndirectCustomerStreet_textArea')]")
	public WebElement customerAddressFieldForCorrectInvoice;

	@FindBy (xpath= "//textarea[contains(@id , 'ExtensionTableCorrection_Street_textArea')]")
	public WebElement invoiceAddressFieldForCorrectInvoice;

	@FindBy (xpath= "//input[contains(@id,'ExtensionTableCorrection_DueDate_input')]")
	public WebElement dueDateField;



	@FindBy (xpath= "//span[contains(@id, 'OK_label')]")
	public WebElement okButtonForCorrectInvoice;





	@FindBy (xpath= "//span[contains(@id,'DiscountLineNew_label')]")
	public WebElement addLineButton;


	@FindBy (xpath= "//input[@name='TMVContractCorrection_VatNum']")
	public WebElement addVATNumber;

	@FindBy (xpath= "//span[contains(text(),'Invalid tax exempt number')]")
	public WebElement messageOnInvalidVatNumber;



	@FindBy (xpath= "//input[contains(@id,'PriceOverrideReasonCode_input')]")
	public WebElement enterReasonCodeForDiscount;

	@FindBy (xpath= "//input[contains(@id,'DiscountPercentage_input')]")
	public WebElement enterDiscountPercentageValue;

	@FindBy (xpath= "//span[contains(@id ,'Correct_label')]")
	public WebElement correctButton;

	@FindBy (xpath= "//span[contains(text(),'Price information')]")
	public WebElement PriceInformation;


	@FindBy (xpath= "//input[contains(@id,'TMVLastPrices_TMVLastLineAmount_input')]")
	public WebElement subTotalOrLastLineAmountAfterContractCorrection;


	@FindBy (xpath= "//span[contains(@id,'SystemDefinedSaveButton_label')]")
	public WebElement saveButton;

	@FindBy (xpath= "//input[contains(@id,'HeaderInfoStatus_input')]")
	public WebElement qoutationStatusOnERPIfCreated;

	@FindBy (xpath= "(//span[contains(text(), 'Sales quotation') and @class = 'appBarTab-headerLabel allowFlyoutClickPropagation'])[1]")
	public WebElement qoutationButtonOnHeaderBar;
	
	@FindBy (xpath= "(//span[contains(text(), 'Sales quotation') and @class = 'appBarTab-headerLabel allowFlyoutClickPropagation'])[2]")
	public WebElement qoutationButtonOnHeaderBar2;

	@FindBy (xpath= "//span[contains(@id,'ButtonUpdateQuotation_label')]")
	public WebElement sendQoutationButton;

	@FindBy (xpath= "//span[contains(text(),'OK')]")
	public WebElement oKButtonTosendQoutation;

	@FindBy (xpath= "//button[contains(@id,'FollowUp_button')]")
	public WebElement followUpButton;

	@FindBy (xpath= "(//span[contains(@id,'ButtonUpdateConfirmation_label')])[1]")
	public WebElement confirmButtonForQoutation;
	
	@FindBy (xpath= "(//span[contains(@id,'ButtonUpdateConfirmation_label')])[2]")
	public WebElement confirmButtonForQoutation2;

	@FindBy (xpath= "(//button[contains(@id,'General_button')])[2]")
	public WebElement generalButtonOnHeaderBar;

	@FindBy (xpath= "//span[contains(@id,'AttachedSalesOrders_label')]")
	public WebElement salesOrderButtonOnGeneralPage;
	
	@FindBy(xpath = "(//span[contains(text(),'General') and @class='pivot-label'])[1]")
	public WebElement generalTab;
	
	@FindBy(xpath = "(//span[contains(text(),'General') and @class='pivot-label'])[2]")
	public WebElement generalTab1;
	
	@FindBy(xpath = "(//span[contains(text(),'General') and @class='pivot-label'])[3]")
	public WebElement generalTab2;
	
	@FindBy(name = "TaxGroups_TaxGroup")
	public WebElement salesTaxGroupOnPostedSalesTax;
	
	@FindBy(name = "Description_TaxCode")
	public WebElement taxCodePostedSalesTax;

	//**************************************Customer Service Portal********************************************

	@FindBy (xpath= "//input[contains(@id,'SearchText_input')]")
	public WebElement searchInputFieldForCustomer;

	@FindBy (xpath= "//span[contains(@id,'CustSearch_label')]")
	public WebElement searchButton;

	@FindBy (xpath= "//div[@aria-label = 'Sales order']")
	public WebElement salesOrderFilterIcon;

	@FindBy (xpath= "//span[contains(@id,'ModifyOrder_label')]")
	public WebElement detailButtonOfSalesOrder;

	@FindBy (xpath= "//input[contains(@id,'_TMVStatus_input')]")
	public WebElement statusOfSalesOrder;
	
	@FindBy (xpath= "(//input[@name='TMVStatus'])[2]")
	public WebElement statusOfSalesOrder2;

	@FindBy (xpath= "(//input[(@title = 'Paid')])[1]")
	public WebElement statusOfPayment;

	//********************HEADER BAR DETAILS*************************
	@FindBy (xpath= "(//*[contains(text(),'Header')])[2]")
	public WebElement headerBarForCustomerPersonalInformation;
	
	@FindBy (xpath= "(//*[contains(text(),'Header')])[1]")
	public WebElement headerBarForCustomerPersonalInformation2;

	@FindBy (xpath= "//input[contains(@id,'SalesTable_SalesId_input')]")
	public WebElement EcomSaleOrderNumber;

	@FindBy (xpath= "(//input[contains(@id,'InvoiceAccount_input')])[1]")
	public WebElement customerAccountNumber;
	
	@FindBy (xpath= "(//input[contains(@id,'InvoiceAccount_input')])[2]")
	public WebElement customerAccountNumber2;

	@FindBy (xpath= "//input[@name='ContactInfo_Email']")
	public WebElement customerEmailID;

	@FindBy (xpath= "//h2[contains(text(),'Address')]")
	public WebElement addressDetailsExpander;

	@FindBy (xpath= "//textarea[contains(@id,'TMVCustInvAddress')]")
	public WebElement customerAddress;

	@FindBy (xpath= "//span[contains(@id,'MCRCustPaymTableButton_label')]")
	public WebElement paymentLink;

	@FindBy (xpath= "//input[@name = 'PaymGrid_TenderTypeId']")
	public WebElement payMethodCodeOnPaymentStatus;

	@FindBy (xpath= "//input[@name = 'PaymGrid_Amount']")
	public WebElement totalPaymentAmountInPaymentStatus;


	@FindBy (xpath= "//span[contains(@id,'LedgerJournalMultiPostChoose_label')]")
	public WebElement selectButtonForJob;
	
	@FindBy(xpath = "(//div[contains(text(),'Criteria')])[1]")
	public WebElement criteria;
	
	@FindBy(name = "FilterField_RangeValue_RangeValue_Input_0")
	public WebElement criteriaTextBox;

	@FindBy (name= "RangeValue")
	public WebElement clickCUSTPAYMCJob;

	@FindBy (xpath= "(//span[contains(@id,'OkButton_label')])[1]")
	public WebElement okButton;
	
	@FindBy (xpath= "(//span[contains(@id,'OkButton_label')])[2]")
	public WebElement okButton2;

	@FindBy (xpath= "//span[contains(@id,'OK_label')]")
	public WebElement okButtonAgain;

	@FindBy (xpath= "//button[contains(@id,'aptabCustomer_button')]")
	public WebElement customerMainTab;

	@FindBy (xpath= "//span[contains(@id,'mibTransactions_label')]")
	public WebElement transactionsButton;

	@FindBy (xpath= "//button[contains(@id,'OriginalDocumentButton')]")
	public WebElement originalDocumentButton;

	@FindBy (xpath= "//span[contains(text(),'View details')]")
	public WebElement viewDetails;
	
	@FindBy (xpath= "//span[contains(text() , 'View details')]")
	public WebElement viewDetailsForSalesTaxGroup;
	
	@FindBy (xpath= "//input[contains(@id,'TaxGroupData_TaxCode_input')]")
	public WebElement salesTaxCodeElement;

	@FindBy (xpath= "//span[contains(@id,'JournalLines_label')]")
	public WebElement lines;

	@FindBy (xpath= "//input[contains(@id,'LedgerJournalTrans_OffsetAccount_input')]")
	public WebElement offsetAccount;

	@FindBy (xpath= "(//*[contains(text(),'Setup')])[2]")
	public WebElement setUpExpander;

	@FindBy (xpath= "//input[contains(@id,'TaxGroup_input')]")
	public WebElement salesTaxGroupInSetUpTab;

	@FindBy (name = "Administration_LanguageId")
	public WebElement languageOfStoreInSetUpTab;

	@FindBy (xpath= "(//*[contains(text(),'Price and discount')])[2]")
	public WebElement priceAndDiscountExpander;

	@FindBy (xpath= "//input[contains(@id,'Currency_CurrencyCode_input')]")
	public WebElement currencyOfTheCountry;

	@FindBy (xpath= "(//*[contains(text(),'Financial dimensions')])[3]")
	public WebElement financialDimensionsExpander;

	@FindBy (xpath= "//input[contains(@id,'DimensionEntryControlTable_DECDesc_RetailChannel_input')]")
	public WebElement retailChannelName;

	@FindBy (xpath= "//input[contains(@id,'DimensionEntryControlTable_DECDesc_Worker_input')]")
	public WebElement retailChannelWorker;

	@FindBy (xpath= "//input[contains(@id,'SelectionUpdate_input')]")
	public WebElement selectionDropDown;

	@FindBy (xpath= "//li[contains(text(),'All')]")
	public WebElement AllOptionInDropdown;
	
	@FindBy (xpath= "//span[contains(text(),'Revenue')]")
	public WebElement revenueRadioButton;
	
	@FindBy (xpath= "//div[contains(text(),'Sales tax group')]")
	public WebElement salesTaxGroupDropdown;
	
	@FindBy(css = "div.modulesPane-opener")
	public WebElement sideNavigationPanelButton;
	
	@FindBy(xpath = "//a[contains(text(),'General ledger')]")
	public WebElement generalLegder;
	
	@FindBy(xpath = "//a[contains(text(),'TeamViewer')]")
	public WebElement teamViewer;
	
	@FindBy(xpath = "//a[contains(text(),'Data Revenue Recognition')]")
	public WebElement dataRevenueRecognition;

	@FindBy(xpath = "//div[contains(text(),'Sales order')]")
	public WebElement salesOrderColoumn;
	
	@FindBy(xpath = "//input[contains(@id,'_FilterField_CustInvoiceTrans_SalesId_SalesId_Input')]")
	public WebElement salesOrderFilter;
	
	@FindBy(xpath = "//a[contains(text(),'Periodic tasks')]")
	public WebElement periodicTasks;
	
	@FindBy(xpath = "//a[starts-with(text(),'Revenue Recognition')]")
	public WebElement revenueRecognition;
	
	@FindBy(xpath = "//span[contains(text(),'OK')]")
	public WebElement revenueRecognitionOK;
	
	@FindBy(id = "NavBar_buttonNotifications")
	public WebElement notification;
	
	@FindBy(xpath = "//a[contains(text(),'Journal entries')]")
	public WebElement journalEntries;
	
	@FindBy(xpath = "//a[contains(text(),'General journals')]")
	public WebElement generalJournals;
	
	@FindBy(xpath = "//div[contains(text(),'Journal batch number')]")
	public WebElement journalBatchNumberColoumn;
	
	@FindBy(xpath = "//span[contains(text(),'Lines')]")
	public WebElement linesHeaderButton;
	
	@FindBy(xpath = "//div[contains(text(),'Voucher')]")
	public WebElement voucherColoumn;
	
	@FindBy(xpath = "(//span[contains(text(),'Post') and starts-with(@id,'LedgerJournalTransDaily')])[1]")
	public WebElement postButton;
	
	@FindBy(xpath = "//span[contains(text(),'Validate') and starts-with(@id,'LedgerJournalTransDaily')]")
	public WebElement validateButton;
	
	@FindBy(xpath = "(//span[contains(text(),'Validate') and starts-with(@id,'LedgerJournalTransDaily')])[2]")
	public WebElement validateButton2;
	
	@FindBy(xpath = "//input[contains(@id,'_FilterField_LedgerJournalTrans_Voucher_Voucher_Input')]")
	public WebElement voucherFilter;
	
	@FindBy(name = "CustInvoiceTrans_InvoiceDate")
	public WebElement revenueRecognitionSaleOrderDate;
	
	@FindBy(xpath = "(//input[@name='CustInvoiceTrans_ItemId'])[1]")
	public WebElement revenueRecognitionItem;
	
	@FindBy(xpath = "(//input[@name='CustInvoiceTrans_Qty'])[1]")
	public WebElement revenueRecognitionQuantity;
	
	@FindBy(xpath = "(//input[@name='CustInvoiceTrans_CurrencyCode'])[1]")
	public WebElement revenueRecognitionCurrency;
	
	@FindBy(xpath = "(//input[@name='CustInvoiceTrans_LineAmount'])[1]")
	public WebElement revenueRecognitionAmount;
	
	@FindBy(xpath = "(//input[@name='CustInvoiceTrans_LineAmountMst'])[1]")
	public WebElement revenueRecognitionAmountInAccoutingCurrency;
	
	
	@FindBy(xpath = "(//input[@name='CustInvoiceTrans_TMVDeferralAmount'])[1]")
	public WebElement revenueRecognitionDeferralAmount;
	
	@FindBy(xpath = "(//input[@name='CustInvoiceTrans_TMVRealisedAsRevenue'])[1]")
	public WebElement revenueRecognitionRealisedRevenue;
	
	@FindBy(xpath = "(//input[@name='CustInvoiceTrans_TMVContractCalculateFrom'])[1]")
	public WebElement revenueRecognitionCalculatedFromDate;
	
	@FindBy(xpath = "(//input[@name='CustInvoiceTrans_TMVContractValidTo'])[1]")
	public WebElement revenueRecognitionValidDate;
	
	@FindBy(xpath = "(//input[@name='CustInvoiceTrans_TMVEndDateRevRec'])[1]")
	public WebElement revenueRecognitionEndDate;
	
	
	@FindBy(xpath = "(//input[@name='CustInvoiceTrans_PACLicense_LicenseId'])[1]")
	public WebElement revenueRecognitionPACLicense;
	
	@FindBy(name = "Fld2_1")
	public WebElement fromDate;
	
	@FindBy(name = "Fld3_1")
	public WebElement toDate;
	
	@FindBy(xpath = "//a[contains(text(),'Tax')]")
	public WebElement tax;
	
	@FindBy(xpath = "//a[contains(text(),'Indirect taxes')]")
	public WebElement indirectTaxes;
	
	@FindBy(xpath = "(//a[contains(text(),'Sales tax')])[1]")
	public WebElement salesTax;
	
	@FindBy(xpath = "//a[contains(text(),'Sales tax groups')]")
	public WebElement salesTaxGroup;
	
	@FindBy(id = "paymentlink-forgot")
	public WebElement forgotPasswordLink;
	
	@FindBy(id = "forgot-invoice")
	public WebElement forgotInvoicePopUpField;
	
	@FindBy (xpath = "//*[contains(text(),'Next Step')]")
	public WebElement nextStepButtonForECOMPayment;
	
	@FindBy(id = "modal-title-14")
	public WebElement emailPopUp;
	
	@FindBy(xpath = "//span[contains(text(),'Ok')]")
	public WebElement okButtonEmailPopUp;
	
	@FindBy(xpath = "(//div[contains(text(),'Billing interval')])[2]")
	public WebElement billingInterval;
	
	@FindBy(xpath = "(//div[contains(text(),'Item number')])[2]")
	public WebElement itemNumber;
	
	@FindBy(name = "SalesLine_PACLicense_LicenseId")
	public WebElement pacLicenseNumber;
	
	@FindBy(name = "PACLicense_PACActivationLink")
	public WebElement licenseActivation;
	
	@FindBy(xpath = "//a[starts-with(@id,'ui-id')]")
	public WebElement getOrderIdMCOPortal;
	
	@FindBy(xpath = "//span[starts-with(@id,'main-product-name')]")
	public WebElement productName;
	
	@FindBy(xpath = "//a[contains(text(),'Invoices')]")
	public WebElement invoiceTab;
	
	@FindBy(xpath = "//a[contains(text(),'Download')]")
	public WebElement downloadPDF;
	
	@FindBy(xpath = "(//span[contains(text(),'Lasernet-Test')])[1]")
	public WebElement clickEmail;
	
	@FindBy(xpath = "(//span[contains(text(),'Quotations')])[1]")
	public WebElement quotationsFolder;
	
	@FindBy(xpath = "(//span[contains(text(),'Inbox')])[2]")
	public WebElement inboxFolder;
	
	@FindBy(xpath = "(//div[contains(@title,'QUO')])[1]")
	public WebElement quotationID;
	
	@FindBy(xpath = "//span[contains(text(),'Download')]")
	public WebElement downloadPDFOfQuotation;
	
	@FindBy(xpath = "//a[contains(text(),'Sales and marketing')]")
	public WebElement salesAndMarketing;
	
	@FindBy(xpath = "//a[contains(text(),'Sales quotations')]")
	public WebElement salesQuoations;
	
	@FindBy(xpath = "//a[contains(text(),'All quotations')]")
	public WebElement allQuotations;
	
	@FindBy(xpath = "(//div[contains(text(),'Quotation')])[1]")
	public WebElement searchQuotation;
	
	@FindBy(xpath = "//input[starts-with(@id,'__FilterField_QuotationId_QuotationId_Input')]")
	public WebElement searchQuotationInput;
	
	@FindBy(xpath = "//div[contains(text(),'Quotation status')]")
	public WebElement quotationStatus;
	
	@FindBy(xpath = "//input[contains(@name,'QuotationStatus')]")
	public WebElement quotationStatusValue;
	
	@FindBy(id ="username")
	public WebElement userName;
	
	@FindBy(id ="login")
	public WebElement password;
	
	@FindBy(xpath = "//span[contains(text(),'Sign in')]")
	public WebElement signIn;
	
	@FindBy(id = "menu-magento-sales-sales")
	public WebElement sales;
	
	@FindBy(xpath = "(//span[contains(text(),'Orders')])[1]")
	public WebElement orders;
	
	@FindBy(id = "fulltext")
	public WebElement searchReferenceID;
	
	@FindBy(xpath = "//a[contains(text(),'View') and @class='action-menu-item']")
	public WebElement viewOrder;
	
	@FindBy(xpath = "//div[contains(text(),'CL Response Quotation:') and @class='note-list-comment']")
	public WebElement getSalesOrderId;
	
	@FindBy(xpath = "//a[contains(text(),'Sales orders')]")
	public WebElement salesOrders;
	
	@FindBy(xpath = "//a[contains(text(),'All sales orders')]")
	public WebElement allSalesOrders;
	
	@FindBy(xpath = "//div[contains(text(),'Sales order')]")
	public WebElement salesOrder;
	
	@FindBy(xpath = "//input[starts-with(@id,'__FilterField_SalesTable_SalesIdAdvanced_SalesId_Input')]")
	public WebElement inputSearcSalesOrder;
	
	@FindBy(xpath = "(//input[contains(@name,'SalesTable_SalesIdAdvanced')])[2]")
	public WebElement viewSalesOrder;
	
	@FindBy(xpath = "//button[@title = 'Close']")
	public WebElement closePopUp;
	
	@FindBy(id = "meInitialsButton")
	public WebElement signOutButton;
	
	@FindBy(id = "meControlSignoutLink")
	public WebElement signOut;
	
	@FindBy(name = "LedgerJournalTrans_TransDate")
	public WebElement verifyDataAgainstJournal;
	
	@FindBy(name = "LedgerJournalTrans_Txt")
	public WebElement journalProductValue;
	
	@FindBy(xpath = "(//input[@name = 'LedgerJournalTrans_CurrencyCode'])[1]")
	public WebElement journalCurrencyValue;
	
	@FindBy(xpath = "//input[contains(@id,'_LedgerJournalTrans_AccountNum_input')]")
	public WebElement financialDimensionOnJournalPage;

	@FindBy(name = "SystemDefinedRefreshButton")
	public WebElement refreshButton;
	
	@FindBy(xpath = "//span[contains(text(),'Totals') and contains(@id,'CustInvoiceJournal_')]")
	public WebElement total;
	
	@FindBy(name = "CustInvoiceJour_SalesBalance")
	public WebElement invoiceSubTotalInErp;
	
	@FindBy(name = "CustInvoiceJour_SumTax")
	public WebElement invoiceSalesTaxInErp;
	
	@FindBy(name = "CustInvoiceJour_InvoiceAmount")
	public WebElement invoiceAmountInErp;
	
	@FindBy(xpath = "//button[@name='CloseButton']")
	public WebElement closeButton;
	
	@FindBy(xpath = "//button[@name='CloseButton']")
	public WebElement closeButton1;
	
	@FindBy(name = "TaxAmountCurTotal")
	public WebElement totalSalesTaxOnPostedSalesTaxTab;
	
	@FindBy(xpath = "//span[contains(text(),'Posted sales tax')]")
	public WebElement postedSalesTaxButton;
	
	@FindBy(name = "Administration_SalesOriginId")
	public WebElement salesWebOrigin;
	
	@FindBy(name = "SalesTable_RetailChannel")
	public WebElement storeName;
	
	@FindBy(name = "VATNum")
	public WebElement taxExempt;
	
	@FindBy(xpath = "//h2[contains(text(),'Setup')]")
	public WebElement setup;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_TMVPriceOverrideReasonCode'])[2]")
	public WebElement reasonCodeTVB;
	
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_TMVPriceOverrideReasonCode'])[1]")
	public WebElement reasonCodeTVB4;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_TMVPriceOverrideReasonCode'])[1]")
	public WebElement reasonCodeTVB3;
	
	@FindBy(xpath = "(//span[contains(text(),'Add line')])[3]")
	public WebElement addline1;
	
	@FindBy(xpath = "(//span[contains(text(),'Add line')])[2]")
	public WebElement addline2;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesDiscountLine_TMVPriceOverrideReasonCode'])[1]")
	public WebElement reasonCodeTVB1;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesDiscountLine_TMVPriceOverrideReasonCode'])[1]")
	public WebElement reasonCodeTVB2;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_TMVPriceOverrideReasonCode'])[3]")
	public WebElement reasonCodeMDS;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_TMVPriceOverrideReasonCode'])[2]")
	public WebElement reasonCodeMDS2;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_TMVDiscountMethod'])[2]")
	public WebElement discountMethodDropDownTVB;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_TMVDiscountMethod'])[1]")
	public WebElement discountMethodDropDownTVB4;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesDiscountLine_TMVDiscountMethod'])[1]")
	public WebElement discountMethodDropDownTVB1;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesDiscountLine_TMVDiscountMethod'])[1]")
	public WebElement discountMethodDropDownTVB2;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_TMVDiscountMethod'])[3]")
	public WebElement discountMethodDropDownMDS;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_TMVDiscountMethod'])[2]")
	public WebElement discountMethodDropDownMDS2;
	
	@FindBy(xpath = "(//input[@name='TMVRetailSalesQuotationDiscountLine_TMVDiscountPercentage'])[2]")
	public WebElement discountPercentage;
	
	@FindBy(xpath = "(//input[@name='TMVRetailSalesQuotationDiscountLine_TMVDiscountPercentage'])[1]")
	public WebElement discountPercentage3;
	
	@FindBy(xpath = "//span[contains(@id,'_TMVManualDiscounts_TMVManualDiscountApproved_toggle')]")
	public WebElement manualDiscount;
	
	@FindBy(xpath = "(//input[@name='TMVRetailSalesDiscountLine_TMVDiscountPercentage'])[1]")
	public WebElement discountPercentage2;
	
	@FindBy(xpath = "(//input[@name='TMVRetailSalesDiscountLine_TMVDiscountPercentage'])[1]")
	public WebElement discountPercentage1;
	
	@FindBy(xpath = "//li[contains(text(),'Discount percentage')]")
	public WebElement discountPercentageMethod;
	
	@FindBy(xpath = "//li[contains(text(),'Target amount')]")
	public WebElement targetAmount;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_TMVTargetAmount'])[3]")
	public WebElement targetAmountValueForMobileAddOn;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_TMVTargetAmount'])[2]")
	public WebElement targetAmountValueForMobileAddOn2;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_Amount'])[3]")
	public WebElement cashDiscountAmount;
	
	@FindBy(xpath = "(//input[@name = 'TMVRetailSalesQuotationDiscountLine_Amount'])[2]")
	public WebElement cashDiscountAmount2;
	
	@FindBy(xpath = "//h3[contains(text(),'QUOTES')]")
	public WebElement quoteSection;
	
	@FindBy(xpath = "//label[contains(text(),'Sent')]")
	public WebElement quoteStatusInCRM;
	
	@FindBy(xpath = "//span[contains(@id,'_printFormletter_toggle')]")
	public WebElement printQuotationToggle;

	@FindBy(xpath = "//span[contains(@id,'UsePrintManagement_toggle')]")
	public WebElement printQuotationDestination;
	
	@FindBy(xpath = "(//span[contains(text(),'Sales quotation') and @class = 'appBarTab-headerLabel allowFlyoutClickPropagation'])[2]")
	public WebElement salesQuotationsButton;
	
	@FindBy(xpath = "(//span[contains(text(),'Sales quotation') and @class = 'appBarTab-headerLabel allowFlyoutClickPropagation'])[1]")
	public WebElement salesQuotationsButton1;
	
	@FindBy(xpath = "//span[contains(text(),'Totals') and contains(@id,'SalesQuotationTable_')]")
	public WebElement totalsButton;
	
	@FindBy(name = "CurrencyCode")
	public WebElement currencyErp;
	
	@FindBy(name = "SumLines")
	public WebElement totalAmountErp;
	
	@FindBy(id = "Total Amount_label")
	public WebElement totalAmountINCRM;
	
	@FindBy (xpath= "(//span[contains(text(),'Topic')])[1]") 
	public WebElement Topic1;
	
	@FindBy(name = "TotalLineAmount")
	public WebElement totalAmountInERP;
	
	@FindBy(name = "SalesTable_DeliveryName1")
	public WebElement customerNameInERP;
	
	@FindBy(name = "Quotation_QuotationName")
	public WebElement customerNameInERP2;
	
	@FindBy(name = "CopyOfLogisticsPostalAddressDeliveryHeader_Address1")
	public WebElement customerDeliveryAddressInERP;
	
	@FindBy(name = "TMVCustDlvAddress")
	public WebElement customerDeliveryAddressInERP2;
	
	@FindBy(name = "ContactInfo_Email")
	public WebElement customerEmailInERP;
	
	@FindBy(name = "TMVLicense_PACLicense_LicenseId")
	public WebElement pacLicenseInERP;
	
	@FindBy(name = "TaxGroup")
	public WebElement salesTaxGroupForCRM;
	
	@FindBy(xpath = "//span[contains(text(),'Sales order confirmation')]")
	public WebElement salesOrderConfirmation;
	
	@FindBy(name = "CustConfirmJour_ConfirmAmount")
	public WebElement amountWithTaxInConfirmationJournal;
	
	@FindBy(name = "CustConfirmJour_TMVConfirmationJournalAmount")
	public WebElement baseInConfirmationJournal;
	
	@FindBy(xpath = "(//span[contains(text(),'Sales tax')])[1]")
	public WebElement salesTaxButton;
	
	@FindBy(xpath = "(//span[contains(text(),'Sales tax')])[2]")
	public WebElement salesTaxButton2;
	
	@FindBy(name = "TaxAmountCurTotal")
	public WebElement totalTaxInConfirmationJournal;
	
	@FindBy(xpath = "(//input[@name = 'TaxJournalTrans_TaxCode'])[1]")
	public WebElement taxCodeInConfirmationJournal;
	
	@FindBy(name = "SystemDefinedCloseButton")
	public WebElement close;
	
	@FindBy(name = "CustInvoiceJour_InvoiceAmount_Grid")
	public WebElement amountWithTaxInInvoiceJournal;
	
	@FindBy(xpath = "(//span[contains(text(),'Add line')])[2]")
	public WebElement priceInfromationAddLine1;
	
	@FindBy(xpath = "(//span[contains(text(),'Add line')])[3]")
	public WebElement priceInfromationAddLine;
	
	@FindBy(xpath = "//span[contains(text(),'Price information')]")
	public WebElement priceInformationTab;
	
	@FindBy(xpath = "(//span[contains(text(),'Sales order') and @class = 'appBarTab-headerLabel allowFlyoutClickPropagation'])[1]")
	public WebElement salesOrderHeader;
	
	@FindBy(xpath = "(//span[contains(text(),'Sales order') and @class = 'appBarTab-headerLabel allowFlyoutClickPropagation'])[2]")
	public WebElement salesOrderHeader2;
	
	@FindBy(xpath = "(//span[contains(text(),'Sales order') and @class = 'appBarTab-headerLabel allowFlyoutClickPropagation'])[3]")
	public WebElement salesOrderHeader3;
	
	@FindBy(xpath = "(//span[contains(@id,'TMVContractLineMenuBtn_label')])[1]")
	public WebElement contractButtonSalesOrder;
	
	@FindBy(xpath = "(//span[contains(@id,'TMVContractLineMenuBtn_label')])[2]")
	public WebElement contractButtonSalesOrder2;
	
	@FindBy(xpath = "(//span[contains(text(),'Contract line status')])[1]")
	public WebElement contractLineStatusButton;
	
	@FindBy(xpath = "//input[@title='TeamViewer Mobile Device Support']")
	public WebElement mobileAddOn;

	@FindBy(xpath = "//span[contains(@id,'TMVContractCorrectionBtn_label')]")
	public WebElement contractCorrectionButtonSalesOrder;


	@FindBy(xpath = "//input[@name='TMVContractCorrection_TaxGroup']")
	public WebElement contractCorrectionSalesTaxGroupInput;

	@FindBy(xpath = "//input[@name='TMVContractCorrection_VatNum']")
	public WebElement contractCorrectionTaxExemptNumberInput;

	@FindBy(xpath = "//button[@name='DiscountLineNew']")
	public WebElement contractCorrectionAddDiscountLineButton;

	@FindBy(xpath = "//input[@name='TMVContractCorrectionDiscountLine_PriceOverrideReasonCode']")
	public WebElement contractCorrectionDiscountLineReasonCode;

	@FindBy(xpath = "//input[@name='TMVContractCorrectionDiscountLine_DiscountMethod']")
	public WebElement contractCorrectionDiscountMethodDropDown;

	@FindBy(xpath = "//input[@name='TMVContractCorrectionDiscountLine_DiscountPercentage']")
	public WebElement contractCorrectionDiscountPercentage;

	@FindBy(xpath = "//textarea[@name='TMVContractCorrection_BuisnessStreet']")
	public WebElement contractCorrectionDeliveryAddressStreetInput;

	@FindBy(xpath = "//textarea[@name='TMVContractCorrection_Street']")
	public WebElement contractCorrectionInvoiceAddressStreetInput;

	@FindBy(xpath = "//h2[contains(text(),'Correction')]")
	public WebElement contractCorrection_CorrectionTab;

	@FindBy(xpath = "(//span[contains(text(),'Save') and contains(@id,'SystemDefinedSaveButton_label')])[1]")
	public WebElement contractCorrectionSaveButton;
	
	@FindBy(xpath = "(//span[contains(text(),'Save') and contains(@id,'SystemDefinedSaveButton_label')])[2]")
	public WebElement contractCorrectionSaveButton2;

	@FindBy(name = "Correct")
	public WebElement contractCorrectionCorrectButton;
	
	@FindBy(xpath = "(//input[@name='SalesLine_TMVContractLineAction'])[1]")
	public WebElement contractLine1;
	
	@FindBy(xpath = "(//input[@name='SalesLine_TMVContractLineAction'])[2]")
	public WebElement contractLine2;
	
	@FindBy(xpath = "(//input[@name='SalesLine_TMVContractLineAction'])[3]")
	public WebElement contractLine3;
	
	@FindBy(xpath = "(//input[@name='SalesLine_TMVContractLineAction'])[4]")
	public WebElement contractLine4;
	
	@FindBy(xpath = "//span[contains(text(),'Modify')]")
	public WebElement modifyButton;
	
	@FindBy(name = "TMVSalesContractCancelTable")
	public WebElement cancelContractButton;
	
	@FindBy(xpath = "//span[contains(text(),'Cancel contract line')]")
	public WebElement cancelContractLineButton;
	
	@FindBy(xpath = "//span[contains(text(),'Transfer')]")
	public WebElement tranferButton;
	
	@FindBy(name = "SalesTable_TMVResellerAccount")
	public WebElement reseller;
	
	@FindBy(name = "SalesTable_TMVIndirectCustomer")
	public WebElement indirect;
	
	@FindBy(xpath = "(//input[@name = 'tmvContactInfo_Email'])[2]")
	public WebElement invoiceActivationLink;
	
	@FindBy(xpath = "(//input[@name = 'SalesTable_TMVOCMEmail'])[2]")
	public WebElement activationLink;
	
	@FindBy(name = "TMVConsolidateContract")
	public WebElement consolidateButton;
	
	@FindBy(xpath = "//div[@class='markContainer']")
	public WebElement selectOrderForConsolidation;
	
	@FindBy(name= "PACLicense")
	public WebElement pacLicenseForConsolidation;
	
	@FindBy(name = "SalesTable_Email")
	public WebElement emailForConsolidation;
	
	@FindBy(xpath = "(//input[@name='SysGen_LicenseId'])[1]")
	public WebElement selectPacLicense;
	
	@FindBy(xpath = "(//input[@name='SysGen_Email'])[1]")
	public WebElement selectEmail;
	
	@FindBy(xpath = "//h2[contains(text(),'Sales orders')]")
	public WebElement salesOrderSection;
	
	@FindBy(xpath = "//h2[contains(text(),'Lines')]")
	public WebElement linesSection;
	
	@FindBy(xpath="(//*[@role='switch'])[29]")
	public WebElement toggleCreateOpportunity;

	
	//Login into ERP
	public void doLoginInERP() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		try{
			
			if(userOtherAccount.isDisplayed()){
				
				System.out.println("Use other account pop up appears");
				userOtherAccount.click();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait3Seconds();
				
			}
			
		}
		catch(NoSuchElementException e){
			
			System.out.println("Use other account pop up does not appear");
		}

		try{

		emailField.sendKeys("naveed.khan@visionetsystems.com");
		UtilityMethods.wait3Seconds();

		nextButton.click();

		WebDriverWait wait = new WebDriverWait(driver,120);
		wait.until(ExpectedConditions.visibilityOf(passwordField));


		passwordField.sendKeys("durrani@63");
		UtilityMethods.wait3Seconds();

		SignInButton.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();

		try{

			if(yesButtonForStaySignedIn.isDisplayed()){

				yesButtonForStaySignedIn.click();
			}

		}
		catch(NoSuchElementException e){

			System.out.println("Couldn't find YES button");
		}

		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS SUCESSFULLY LOGGED IN TO D365");


		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		}
		catch(Exception e)
		{
			e.getMessage();
			System.out.println("USER IS LOGGED IN ALREADY");
			
		}

	}
	
	//Login into Quotation Email
		public void doLoginInQuotationEmail() throws Throwable{
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			try{
				
				if(userOtherAccount.isDisplayed()){
					
					System.out.println("Use other account pop up appears");
					userOtherAccount.click();
					UtilityMethods.waitForPageLoadAndPageReady();
					UtilityMethods.wait3Seconds();
					
				}
				
			}
			catch(NoSuchElementException e){
				
				System.out.println("Use other account pop up does not appear");
			}


			emailField.sendKeys("uatteamviewer@visionetsystems.com");
			UtilityMethods.wait3Seconds();

			nextButton.click();

			WebDriverWait wait = new WebDriverWait(driver,120);
			wait.until(ExpectedConditions.visibilityOf(passwordField));



			passwordField.sendKeys("P@ssword61");
			UtilityMethods.wait3Seconds();

			SignInButton.click();
			wait.until(ExpectedConditions.visibilityOf(yesButtonForStaySignedIn));

			try{

				if(yesButtonForStaySignedIn.isDisplayed()){

					yesButtonForStaySignedIn.click();
				}

			}
			catch(Exception e){

				System.out.println("Couldn't find YES button");
			}

			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS SUCESSFULLY LOGGED IN TO D365");


			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			outlook.click();
			driver.close();
			UtilityMethods.switchToFirstTab();
			
			
			//Selecting Company On D365 to TVDE
			//changeCompany();
		}


	//Login into ERP
	public void doLoginInCRM() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();



		emailField.sendKeys("ali.isfahany@visionetsystems.com");
		UtilityMethods.wait3Seconds();

		nextButton.click();
		UtilityMethods.wait20Seconds();


		passwordField.sendKeys("M@hi!1986");
		UtilityMethods.wait3Seconds();

		SignInButton.click();
		UtilityMethods.wait10Seconds();

		try{

			if(yesButtonForStaySignedIn.isDisplayed()){

				yesButtonForStaySignedIn.click();
			}

		}
		catch(Exception e){

			System.out.println("Couldn't find YES button");
		}

		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS SUCESSFULLY LOGGED IN TO D365");


		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait20Seconds();

		//Selecting Company On D365 to TVDE
		//changeCompany();
	}



	public void changeCompany() throws Throwable{



		chooseCompanyButton.click();

		UtilityMethods.wait3Seconds();

		chooseCompanyDropdown.click();

		UtilityMethods.wait3Seconds();

		chooseTVDEOption.click();


		UtilityMethods.wait7Seconds();
		System.out.println("USER IS CHANGING THE COMPANY");
	}


	public void verifyContractIsInvoiced() throws Throwable{
		try{
		refreshRecord.click();
		}
		catch(Exception e){}
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		try{
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(statusOfSalesOrder));
		}
		catch(Exception e){
			new Actions(driver).sendKeys(Keys.ESCAPE).perform();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait10Seconds();
		}
		
		String salesOrderStatusAfterInvoicing;
		try{
			statusOfSalesOrder.click();
			UtilityMethods.wait5Seconds();
			salesOrderStatusAfterInvoicing = statusOfSalesOrder.getAttribute("title");
			System.out.println("Status of sales order is: "+salesOrderStatusAfterInvoicing);
			
		}
		catch(Exception e){
			System.out.println("Problem is: "+e.getMessage());
			statusOfSalesOrder2.click();
			UtilityMethods.wait3Seconds();
			salesOrderStatusAfterInvoicing = statusOfSalesOrder2.getAttribute("title");
			System.out.println("Status of sales order is 2: "+salesOrderStatusAfterInvoicing);
		}
		UtilityMethods.wait1Seconds();
		if(salesOrderStatusAfterInvoicing.contains("Invoiced")){

			System.out.println("ORDER HAS BEEN INVOICED SUCCESSFULLY");
			UtilityMethods.validateAssertEqual("Invoiced", "Invoiced");

		}
		else{
			
			System.out.println("ORDER HAS NOT INVOICED SUCCESSFULLY");
			UtilityMethods.validateAssertEqual("Invoiced", "Not Invoiced");

		}
		
	
	}


	public void switchToECOM() throws Throwable{

		UtilityMethods.wait10Seconds();
		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		String url = "https://uat:abcd@1234@uat.service.teamviewer.com/en-pl/payment/";
		if(CRMPage.flag == 1 && CRMPageSteps.ukFlag == true){
			
			System.out.println("Getting and opening paylink URL FOR QUOTAION SALES ORDER");
			System.out.println(url);
			ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(2));
			UtilityMethods.waitForPageLoadAndPageReady();
			driver.navigate().to("https://uat:abcd@1234@uat.service.teamviewer.com/en-gb/payment/");
			
		}
		else if(CRMPage.flag == 1 && CRMPageSteps.polandFlag == true){
			
			System.out.println("Getting and opening paylink URL FOR QUOTAION SALES ORDER");
			System.out.println(url);
			ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(2));
			UtilityMethods.waitForPageLoadAndPageReady();
			driver.navigate().to("https://uat:abcd@1234@uat.service.teamviewer.com/en-pl/payment/");
			
		}
		else if(CRMPage.flag == 1 && CRMPageSteps.netherlandFlag == true){
			
			System.out.println("Getting and opening paylink URL FOR QUOTAION SALES ORDER");
			System.out.println(url);
			ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(2));
			UtilityMethods.waitForPageLoadAndPageReady();
			driver.navigate().to("https://uat:abcd@1234@uat.service.teamviewer.com/en-nl/payment/");
			
		}
		else if(CRMPage.flag == 1 && CRMPageSteps.argentinaFlag == true){
			
			System.out.println("Getting and opening paylink URL FOR QUOTAION SALES ORDER");
			System.out.println(url);
			ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(2));
			UtilityMethods.waitForPageLoadAndPageReady();
			driver.navigate().to("https://uat:abcd@1234@uat.service.teamviewer.com/en-nl/payment/");
			
		}
		else{
			System.out.println("Getting and opening paylink URL for ECOM order");
			UtilityMethods.CloseCurrentTabAndOpenNewTab(url);
		}
	
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		
	}
	
	public void switchToDifferentCountryStoreAndAddPayment() throws Throwable{

		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		UtilityMethods.wait3Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();

		String url = "https://uat:abcd@1234@uat.teamviewer.com/en-pl/payment/";
		UtilityMethods.CloseCurrentTabAndOpenNewTab(url);
		//driver.navigate().to("https://stg:abcd@1234@staging.teamviewer.com/en-pl/payment/");

	}


	public void openCustomerTransactions() throws Throwable{

		customerOnHeaderBar.click();

		System.out.println("USER IS WAITING 20 SECONDS FOR TRANSACTION BUTTON TO BE CLICKABLE");
		UtilityMethods.wait20Seconds();

		try{
			
		TransactionsButton.click();
		}
		catch(Exception e){
			
			System.out.println("TRANSACTION BUTTON IS NOT VISIBLE");
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		voucherColumnToSearchiNVOICEnUMBER.click();
		UtilityMethods.wait5Seconds();
		try{

			if(InvoiceVoucherFilterField.isDisplayed()){

				//INVOICE NUMBER HARDCORE FOR 18555
				//InvoiceVoucherFilterField.sendKeys("R00066472");
				//FOR ONLINE REAL DEMO
				InvoiceVoucherFilterField.sendKeys(inVoiceNo);
				System.out.println("Invoice Number is "+inVoiceNo);
				InvoiceVoucherFilterField.sendKeys(Keys.ENTER);

				//InvoiceVoucherFilterField.sendKeys("R00066296"); 
				UtilityMethods.wait1Seconds();
			}

		}


		catch(Exception e){

			System.out.println("Account Filter Field is not Displayed at ALL");
		}

		
		UtilityMethods.wait5Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();
		new Actions(driver).moveToElement(viewSettlementsOnHeaderBar).doubleClick(viewSettlementsOnHeaderBar).build().perform();
	//	viewSettlementsOnHeaderBar.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();

		try{


			if(transactionTypeIsPayment.isDisplayed() && transactionTypeIsSaleOrder.isDisplayed()){


				UtilityMethods.validateAssertEqual("SETTLED", "SETTLED");
				System.out.println("INVOICE HAS BEEN SETTLED SUCCESSFULLY");

			}
		}
		catch(Exception e){

			System.out.println("INVOICE HAS NOT SETTLED");
			UtilityMethods.validateAssertEqual("SETTLED", "NOT SETTLED");

		}

		UtilityMethods.wait5Seconds();
		
		settlementNumberOfInvoice = settlementID.getAttribute("title");
	//	int settlementNo = settlementNumberOfInvoice.indexOf('C');
	//	settlementNumberOfInvoice = settlementNumberOfInvoice.substring(0, settlementNo);
		System.out.println(settlementNumberOfInvoice);
		
		
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SETTLEMENT ID :"+settlementNumberOfInvoice);
		System.out.println("Settlement ID :"+settlementNumberOfInvoice);

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		UtilityMethods.clickElemetJavaSciptExecutor(okButtonForConfirmTerminating);
	

	}

	public void openSettledInvoice() throws Throwable{

		UtilityMethods.wait5Seconds();
		UtilityMethods.clickElemetJavaSciptExecutor(voucherColumnToSearchiNVOICEnUMBER);
		UtilityMethods.wait5Seconds();
		try{

			if(InvoiceVoucherFilterField.isDisplayed()){

				//SETTLEMENT ID HARDCORE AGAINST VOUCHER

				Actions action = new Actions(driver);
				action.doubleClick(InvoiceVoucherFilterField).build().perform();
				//InvoiceVoucherFilterField.sendKeys("WCZ0002375");
				
				
				InvoiceVoucherFilterField.sendKeys(settlementNumberOfInvoice);
				InvoiceVoucherFilterField.sendKeys(Keys.ENTER);
				//System.out.println("SETTLEMENT NUMBER OF INVOICE IS :"+"WCZ0002375");
				System.out.println("SETTLEMENT NUMBER OF INVOICE IS :"+settlementNumberOfInvoice);
				UtilityMethods.wait5Seconds();
			}

		}


		catch(Exception e){

			System.out.println("Account Filter Field is not Displayed at ALL");
		}

		UtilityMethods.wait5Seconds();

		originalDocumentButton.click();

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();

		try{
		viewDetails.click();}
		catch(NoSuchElementException e){
			UtilityMethods.wait7Seconds();
			UtilityMethods.clickElemetJavaSciptExecutor(viewDetails);
		}
		catch(Exception e){
			UtilityMethods.wait7Seconds();
			UtilityMethods.clickElemetJavaSciptExecutor(viewDetails);
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		
		try{
			lines.click();}
			catch(NoSuchElementException e){
				UtilityMethods.wait7Seconds();
				UtilityMethods.clickElemetJavaSciptExecutor(lines);
			}
			catch(Exception e){
				UtilityMethods.wait7Seconds();
				UtilityMethods.clickElemetJavaSciptExecutor(lines);
			}

	}
	
	public void verifyOffsetAccountNumberForPaypal() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		String offsetNumber = offsetAccount.getAttribute("title");
		if(!offsetNumber.contains("13042")){

			System.out.println("OFFSET ACCOUNT NUMBER FOR THIS TRANSACTION IS : "+ConfirmationPage.paymentCodeOfOrder);
			Reporter.addStepLog("OFFSET ACCOUNT NUMBER FOR THIS TRANSACTION IS : "+ConfirmationPage.paymentCodeOfOrder);
		}

		else{

			UtilityMethods.validateAssertEqual("OFFSET ACCOUNT MATCHED", "OFFSET ACCOUNT NOT MATCHED");
		}
		
		
	}
	
	public void verifyOffsetAccountNumberForCreditCard() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		String offsetNumber = offsetAccount.getAttribute("title");
		if(!offsetNumber.contains("13042")){

			System.out.println("OFFSET ACCOUNT NUMBER FOR THIS TRANSACTION IS : "+ConfirmationPage.paymentCodeOfOrder);
			Reporter.addStepLog("OFFSET ACCOUNT NUMBER FOR THIS TRANSACTION IS : "+ConfirmationPage.paymentCodeOfOrder);
		}

		else{

			UtilityMethods.validateAssertEqual("OFFSET ACCOUNT MATCHED", "OFFSET ACCOUNT NOT MATCHED");
		}
		
		if(CRMPage.flag==1){
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.close();
			driver.switchTo().window(tabs.get(1));
				
			}
		
		
	}


	public void switchToERP() throws Throwable{

		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		UtilityMethods.wait5Seconds();
		String url = "https://tv-d365fo-ben-01.sandbox.operations.dynamics.com/?cmp=TVDE&mi=CustTableListPage";
		UtilityMethods.waitForPageLoadAndPageReady();
	//	driver.navigate().to("https://tv-d365fo-ben-01.sandbox.operations.dynamics.com/?cmp=TVDE&mi=CustTableListPage");
		if(CRMPage.flag==1){
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.close();
			
			driver.switchTo().window(tabs.get(3));
			UtilityMethods.waitForPageLoadAndPageReady();
		}
		else{
		UtilityMethods.CloseCurrentTabAndOpenNewTab(url);
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(customerAccountFilterIcon));
		customerAccountFilterIcon.click();
		UtilityMethods.wait10Seconds();



		try{

			if(channelIdFilterField.isDisplayed()){

				
				if(CRMPage.flag==1){
					channelIdFilterField.sendKeys(CRMPage.accountNumber);
				}
				else{
					channelIdFilterField.sendKeys(cusAccontNur);
				}

				//Customer for 18555
				//channelIdFilterField.sendKeys("20209465");



				//Customer for Premium - 18913
				//channelIdFilterField.sendKeys("20209371");

				//Customer for Corporate - 18910
				//channelIdFilterField.sendKeys("20209416");

				//channelIdFilterField.sendKeys("10000010"); 

				// customer created through AX directly
				//channelIdFilterField.sendKeys(finalOrderID);
			}

		}


		catch(Exception e){

			System.out.println("Account Filter Field is not Displayed at ALL");
		}

		UtilityMethods.wait5Seconds();


		applyButtonOnChannelFilter.click();

		UtilityMethods.wait5Seconds();

		customerName = getCustomerName.getAttribute("title").substring(0,7);
		//The output will  be NAL TC09
		System.out.println(customerName);
		UtilityMethods.wait5Seconds();

		
	}



	public void runTheJob() throws Throwable{

		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		
		String navigationURL = "https://tv-d365fo-ben-01.sandbox.operations.dynamics.com/?cmp=TVDE&mi=Action%3ALedgerJournalMultiPost";
		//	driver.navigate().to("https://tv-d365fo-ben-01.sandbox.operations.dynamics.com/?cmp=TVDE&mi=Action%3ALedgerJournalMultiPost");
		if(CRMPage.flag==1){
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			
			driver.switchTo().window(tabs.get(2));
			driver.navigate().to(navigationURL);
			UtilityMethods.waitForPageLoadAndPageReady();
		}
		else{
			UtilityMethods.CloseCurrentTabAndOpenNewTab(navigationURL);
		}
		
		System.out.println("User is waiting 5 mins");

		UtilityMethods.wait300Seconds();
	//	UtilityMethods.wait300Seconds();
	//	UtilityMethods.wait300Seconds();


		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(selectButtonForJob));
		selectButtonForJob.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		UtilityMethods.clickElemetJavaSciptExecutor(criteria);
		UtilityMethods.wait3Seconds();
		new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(criteriaTextBox));
		criteriaTextBox.click();
		criteriaTextBox.clear();
		criteriaTextBox.sendKeys("P-Link");
		criteriaTextBox.sendKeys(Keys.ENTER);
		
		
		UtilityMethods.wait20Seconds();
		
		try{
			UtilityMethods.clickElemetJavaSciptExecutor(okButton);
	
		}
		catch(Exception e){
			UtilityMethods.clickElemetJavaSciptExecutor(okButton2);
			
		}
		
		UtilityMethods.waitForPageLoadAndPageReady();
		
		UtilityMethods.wait7Seconds();
		UtilityMethods.clickElemetJavaSciptExecutor(okButtonAgain);
	
		System.out.println("USER IS WAITING 5 MINUTES FOR POSTING THE PAYMENT");
		UtilityMethods.wait300Seconds();
	
		if(CRMPage.flag==1){
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			
			driver.switchTo().window(tabs.get(1));
			UtilityMethods.waitForPageLoadAndPageReady();
		}
	}
	
	public void clickForgotPasswordLink() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();
		WebDriverWait wait = new WebDriverWait(driver,60);

		wait.until(ExpectedConditions.elementToBeClickable(nextStepButtonForECOMPayment));
		forgotPasswordLink.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(forgotInvoicePopUpField));
		UtilityMethods.wait3Seconds();
		System.out.println("Invoice number is "+inVoiceNo);
		//forgotInvoicePopUpField.sendKeys(keysToSend);
		forgotInvoicePopUpField.sendKeys(inVoiceNo);
	//	new Actions(driver).moveToElement(forgotInvoicePopUpField).sendKeys("R00070799").build().perform();
		UtilityMethods.wait2Seconds();
		forgotInvoicePopUpField.sendKeys(Keys.ENTER);
		
		
	}
	
	public void verifyPopUpEmail() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.elementToBeClickable(emailPopUp));
		UtilityMethods.wait7Seconds();
		UtilityMethods.clickElemetJavaSciptExecutor(okButtonEmailPopUp);

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		
		
		
	}

	public void switchToERPForVerification() throws Throwable{

		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		UtilityMethods.wait3Seconds();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait2Seconds();
		String url = new PropertyReader().readProperty("D365URL");
		UtilityMethods.CloseCurrentTabAndOpenNewTab(url);
		UtilityMethods.waitForPageLoadAndPageReady();
		
		
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ACCESSING D365");
		System.out.println("USER IS ACCESSING D365");

	}
	
	public void switchToMagento() throws Throwable{

		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		UtilityMethods.wait3Seconds();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait2Seconds();
		String url = new PropertyReader().readProperty("MagentoUatURL");
		UtilityMethods.CloseCurrentTabAndOpenNewTab(url);
		UtilityMethods.waitForPageLoadAndPageReady();
		
		
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("User is accessing magento");
		System.out.println("USER IS ACCESSING MAGENTO");

	}
	
	public void switchToCheckOutPageOfQuotation() throws Throwable{
		
		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		UtilityMethods.wait3Seconds();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait2Seconds();
		System.out.println("SYSTEM IS NAVIGATING TO MAGENTO");
		System.out.println(quoteCheckOutPageUrl);
		UtilityMethods.CloseCurrentTabAndOpenNewTab(quoteCheckOutPageUrl);
		UtilityMethods.waitForPageLoadAndPageReady();
		
		
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ACCESSING CHECKOUT PAGE");
		System.out.println("USER IS ACCESSING CHECKOUT PAGE");
		
	}
	
	public void switchToEmailForQuotationIDAndActivationLink() throws Throwable{

		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		UtilityMethods.wait3Seconds();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait2Seconds();
	//	String url = new PropertyReader().readProperty("D365URL");
		String url = "https://login.microsoftonline.com";
		UtilityMethods.CloseCurrentTabAndOpenNewTab(url);
		UtilityMethods.waitForPageLoadAndPageReady();
		
		
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ACCESSING QUOTATION EMAIL ");
		System.out.println("USER IS ACCESSING QUOTATION EMAIL");

	}
	
	public void switchToERPAgain() throws Throwable{

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(0));
		System.out.println("USER IS WAITING FOR 5 MINUTES SO THAT PAID STATUS IS SHOWING");
		UtilityMethods.wait300Seconds();

	}

	//Confirm Sales Order and Invoice Functionality
	public void confirmSalesOrderandInvoice() throws Throwable{
		System.out.println("Waiting for Sales Order Confirm button ..");
		UtilityMethods.wait30Seconds();

		try{

			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", confirmSalesOrderButton);
		//	UtilityMethods.clickElemetJavaSciptExecutor(confirmSalesOrderButton);
			System.out.println("USER HAS CLICKED ON CONFIRM SALES ORDER 1");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CLICKED ON CONFIRM SALES ORDER");
			
		}
		catch(Exception e){

			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", confirmSwitchedSalesOrderButton);
	//		UtilityMethods.clickElemetJavaSciptExecutor(confirmSwitchedSalesOrderButton);
			System.out.println("USER HAS CLICKED ON CONFIRM SALES ORDER 2");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CLICKED ON CONFIRM SALES ORDER");
		}
		
		UtilityMethods.wait5Seconds();
		WebDriverWait wait = new WebDriverWait(driver,20);
		try{
			
			wait.until(ExpectedConditions.visibilityOf(clickOkForConfirmSalesOrder));
			clickOkForConfirmSalesOrder.click();
		}
		catch(Exception e){
			try{

				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", confirmSalesOrderButton);
			//	UtilityMethods.clickElemetJavaSciptExecutor(confirmSalesOrderButton);
				System.out.println("USER HAS CLICKED ON CONFIRM SALES ORDER 3");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER HAS CLICKED ON CONFIRM SALES ORDER");
				
				UtilityMethods.wait5Seconds();
				wait.until(ExpectedConditions.visibilityOf(clickOkForConfirmSalesOrder));
				clickOkForConfirmSalesOrder.click();
				
			}
			catch(Exception d){

				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", confirmSwitchedSalesOrderButton);
		//		UtilityMethods.clickElemetJavaSciptExecutor(confirmSwitchedSalesOrderButton);
				System.out.println("USER HAS CLICKED ON CONFIRM SALES ORDER 4");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER HAS CLICKED ON CONFIRM SALES ORDER");
				
				UtilityMethods.wait5Seconds();
				wait.until(ExpectedConditions.visibilityOf(clickOkForConfirmSalesOrder));
				clickOkForConfirmSalesOrder.click();
			}
		}

		wait.until(ExpectedConditions.visibilityOf(yes));
		yes.click();

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait20Seconds();
		if(driver.findElements(By.xpath("//span[contains(text(),'Open Lasernet setup')]")).size()!=0){
			UtilityMethods.waitForPageLoadAndPageReady();
			
			driver.navigate().back();
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();

		}

		try{

			inVoiceButton.click();
			System.out.println("USER HAS CLICKED ON INVOICE BUTTON");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CLICKED ON INVOICE BUTTON");
		}
		catch(Exception e){


			inVoiceButtonForSwitchedOrder.click();
			System.out.println("USER HAS CLICKED ON INVOICE BUTTON OF SWITCHED ORDER");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CLICKED ON INVOICE BUTTON");
		}



		try{
			
		UtilityMethods.wait10Seconds();
		noButtonOnInvoiceSchedule.click();
		
		}
		catch(Exception j){


			System.out.println("NO POPUP IS NOT SHOWING");
		}

		try{
			UtilityMethods.wait7Seconds();
			okButtonForPostingInvoice.click();
		}

		catch(Exception e){

			System.out.println("POP UP NOT VISIBLE");
		}

		UtilityMethods.wait7Seconds();
		
		try{
		wait.until(ExpectedConditions.visibilityOf(okButtonForPostingDocument));
		okButtonForPostingDocument.click();

		UtilityMethods.wait60Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();

		if(driver.findElements(By.xpath("//span[contains(text(),'Open Lasernet setup')]")).size()!=0){
			UtilityMethods.waitForPageLoadAndPageReady();
			
			driver.navigate().back();
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();

		}
		
		System.out.println("USER HAS CONFIRMED SIMPLE SALES ORDER AND INVOICED IT");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CONFIRMED SIMPLE SALES ORDER AND INVOICED IT");
		UtilityMethods.waitForPageLoadAndPageReady();
		}
		catch (Exception e) {
			System.out.println("ALREADY INVOICED");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CONFIRMED SIMPLE SALES ORDER AND INVOICED IT");
			UtilityMethods.waitForPageLoadAndPageReady();
		}
		
		
//		}
		
		
	}

	public void confirmSalesOrderandInvoiceForCCPayment() throws Throwable{

		try{

			confirmSalesOrderButton.click();
			System.out.println("USER HAS CLICKED ON CONFIRM SALES ORDER");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("User has clicked on confirm button");
		}
		catch(Exception e){

			confirmSwitchedSalesOrderButton.click();
			System.out.println("USER HAS CLICKED ON CONFIRM SALES ORDER");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("User has clicked on confirm button");
		}
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(clickOkForConfirmSalesOrder));
		clickOkForConfirmSalesOrder.click();

		wait.until(ExpectedConditions.visibilityOf( clickOkToContinuePopUp));
		clickOkToContinuePopUp.click();

		UtilityMethods.wait15Seconds();

		try{

			inVoiceButton.click();
			System.out.println("USER HAS CLICKED ON INVOICE BUTTON");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("User has clicked on invoice button");
		}
		catch(Exception e){


			inVoiceButtonForSwitchedOrder.click();
			System.out.println("USER HAS CLICKED ON INVOICE BUTTON OF SWITCHED ORDER");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("User has clicked on invoice button");
		}

		try{
		wait.until(ExpectedConditions.visibilityOf(noButtonOnInvoiceSchedule));
		noButtonOnInvoiceSchedule.click();
		
		}
		catch(Exception c){

			System.out.println("NO BUTTON IS NOT SHOWING");
		}

		try{
			UtilityMethods.wait5Seconds();
			okButtonForPostingInvoice.click();}

		catch(Exception e){

			System.out.println("POPUP NOT VISIBLE");
		}

		wait.until(ExpectedConditions.visibilityOf( okButtonForPostingDocument));
		okButtonForPostingDocument.click();

		//		//TOTAL 260 SECONDS
		UtilityMethods.wait240Seconds();
		UtilityMethods.wait60Seconds();

		System.out.println("USER HAS CONFIRMED SIMPLE SALES ORDER AND INVOICED IT");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("User has confirmed simple sales order and invoice it successfully");

	}


	public void confirmSalesOrderandInvoiceAgainForSwitchOrder() throws Throwable{

		UtilityMethods.wait30Seconds();
		try{

			confirmSalesOrderButton.click();
			System.out.println("USER HAS CLICKED ON CONFIRM SALES ORDER");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("User has clicked on confirm button");
		}
		catch(Exception e){

			confirmSwitchedSalesOrderButton.click();
			System.out.println("USER HAS CLICKED ON CONFIRM SALES ORDER");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("User has clicked on confirm button");
		}
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(clickOkForConfirmSalesOrder));
		clickOkForConfirmSalesOrder.click();

		wait.until(ExpectedConditions.visibilityOf( clickOkToContinuePopUp));
		clickOkToContinuePopUp.click();

		UtilityMethods.wait30Seconds();

		try{

			inVoiceButton.click();
			System.out.println("USER HAS CLICKED ON INVOICE BUTTON");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("User has clicked on invoice button");
		}
		catch(Exception e){


			inVoiceButtonForSwitchedOrder.click();
			System.out.println("USER HAS CLICKED ON INVOICE BUTTON OF SWITCHED ORDER");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("User has clicked on invoice button");
		}

		try{
		wait.until(ExpectedConditions.visibilityOf( noButtonOnInvoiceSchedule));
		noButtonOnInvoiceSchedule.click();
		
		}

		catch(Exception e){

			System.out.println("NO BUTTON IS NOT SHOWING");
		}

		try{
			UtilityMethods.wait7Seconds();
			okButtonForPostingInvoice.click();
		}
		catch(Exception e){

			System.out.println("POPUP NOT VISIBLE");
		}

		UtilityMethods.wait7Seconds();
		wait.until(ExpectedConditions.visibilityOf( okButtonForPostingDocument));
		okButtonForPostingDocument.click();

		UtilityMethods.wait30Seconds();

		System.out.println("USER HAS CONFIRMED SIMPLE SALES ORDER AND INVOICED IT");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("User has confirmed and invoiced sales order");

	}

	public void terminateSalesOrder() throws Throwable{
		UtilityMethods.wait5Seconds();

		terminateContractButton.click();

		UtilityMethods.wait10Seconds();
		driver.findElement(By.xpath("//textarea[@name='Fld2_1']")).sendKeys("PLEASE TERMINATE");
		
		UtilityMethods.wait2Seconds();
		
		oKButtonTosendQoutation.click();
		
		UtilityMethods.wait15Seconds();
	}

	public void verifyterminateofSalesOrder() throws Throwable{

		try{

			if(terminatedTextVerifyIt.isDisplayed()){

				UtilityMethods.validateAssertEqual("Terminated", "Terminated");
				System.out.println("CONTRACT HAS BEEN TERMINATED SUCCESSFULLY");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("CONTRACT HAS BEEN TERMINATED SUCCESSFULLY");

			}

		}

		catch(Exception e){

			System.out.println("CONTRACT Has not Terminated");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("CONTRACT HAS NOT BEEN TERMINATED");
			UtilityMethods.validateAssertEqual("Terminated", "NOT Terminated");

			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("CONTRACT HAS NOT BEEN TERMINATED");
		}


	}	


	//Validate Business To Premium Switch
	public void validateBusinessToPremiumSwitchedOrNot() throws Throwable{


		String productNameinText = productNameOfSwitchedPremiumOrder.getAttribute("title");

		if(productNameinText.contains("TeamViewer Premium / TeamViewer Premium Yearly Subscription")){


			UtilityMethods.validateAssertEqual("Premium License is Added", "Premium License is Added");
			System.out.println("USER HAS SWITCHED FROM BUSINESS -> PREMIUM");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS SWITCHED FROM BUSINESS -> PREMIUM");
		}
		else{
			System.out.println("USER HAS NOT SWITCHED FROM BUSINESS -> PREMIUM");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS NOT SWITCHED FROM BUSINESS -> PREMIUM");
			UtilityMethods.validateAssertEqual("Premium License is Added", "Premium License is NOT Added");
		}

	}


	//Validate Business To Corporate Switch
	public void validateBusinessToCorporateSwitchedOrNot() throws Throwable{


		String productNameinText = productNameOfSwitchedCorporateOrder.getAttribute("title");

		if(productNameinText.contains("TeamViewer Corporate / TeamViewer Corporate Yearly Subscription")){


			UtilityMethods.validateAssertEqual("Corporate License is Added", "Corporate License is Added");
			System.out.println("USER HAS SWITCHED TO CORPORATE LICENSE");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS SWITCHED TO CORPORATE LICENSE WITH PRODUCT" +productNameinText);
		}
		else{
			System.out.println("USER HAS NOT SWITCHED TO CORPORATE LICENSE");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS NOT SWITCHED TO CORPORATE LICENSE");
			UtilityMethods.validateAssertEqual("Corporate License is Added", "Corporate License is NOT Added");
		}

	}

	//Validate TV14B0001 To TV14P0001 upgrade
	public void validateTV14B0001ToTV14P0001OrNot() throws Throwable{


		String productNameinText = itemId.getAttribute("title");

		if(productNameinText.contains("TV14P0001")){


			UtilityMethods.validateAssertEqual("Perpetual Premium is Added", "Perpetual Premium is Added");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS UPGRADED TO PERPETUAL PREMIUM");
			System.out.println("USER HAS UPGRADED TO PERPETUAL PREMIUM");
		}
		else{
			System.out.println("USER HAS NOT UPGRADED TO PERPETUAL PREMIUM");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS NOT UPGRADED TO PERPETUAL PREMIUM "+productNameinText);
			UtilityMethods.validateAssertEqual("Perpetual Premium is Added", "Perpetual Premium NOT is Added");
		}

	}

	//Validate TV14B0001 To TV14C0001 upgrade
	public void validateTV14B0001ToTV14C0001OrNot() throws Throwable{


		String productNameinText = itemId.getAttribute("title");

		if(productNameinText.contains("TV14C0001")){


			UtilityMethods.validateAssertEqual("Perpetual CORPORATE is Added", "Perpetual CORPORATE is Added");
			System.out.println("USER HAS UPGRADED TO PERPETUAL CORPORATE");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS UPGRADED TO PERPETUAL CORPORATE "+productNameinText);
		}
		else{
			System.out.println("USER HAS NOT UPGRADED TO PERPETUAL CORPORATE");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS NOT UPGRADED TO PERPETUAL CORPORATE");
			UtilityMethods.validateAssertEqual("Perpetual CORPORATE is Added", "Perpetual CORPORATE NOT is Added");
		}

	}

	public void validateTV13B0001ToTV14B0001OrNot() throws Throwable{


		String productNameinText = itemIdAfterUpdate.getAttribute("title");

		if(productNameinText.contains("TV14B0001")){


			UtilityMethods.validateAssertEqual("Perpetual Business is Added", "Perpetual Business is Added");
			System.out.println("USER HAS UPDATED TO PERPETUAL Business 14");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS UPDATED TO PERPETUAL BUSINESS 14 "+productNameinText);
		}
		else{
			System.out.println("USER HAS NOT UPDATED TO PERPETUAL BUSINESS 14");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS NOT UPDATED TO PERPETUAL BUSINESS 14");
			UtilityMethods.validateAssertEqual("Perpetual BUSINESS is Added", "Perpetual BUSINESS NOT is Added");
		}

	}

	public void validateTV13B0001isActiveBack() throws Throwable{


		String productNameinText = itemIdAfterUpdate.getAttribute("title");

		if(productNameinText.contains("TV13B0001")){


			UtilityMethods.validateAssertEqual("Perpetual Business 13 is Added", "Perpetual Business 13 is Added");
			System.out.println("USER HAS DOWNGRADED TO PERPETUAL Business 13 Again");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS DOWNGRADED TO PERPETUAL BUSINESS 13 : "+productNameinText);
		}
		else{
			System.out.println("USER HAS NOT DOWNGRADED TO PERPETUAL BUSINESS 13");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS NOT DOWNGRADED TO PERPETUAL BUSINESS 13");
			UtilityMethods.validateAssertEqual("Perpetual BUSINESS 13 is Added", "Perpetual BUSINESS 13 NOT is Added");
		}

	}

	//Validate TV14B0001 To TVB0001 migrate
	public void validateTV14B0001ToTVB0001OrNot() throws Throwable{


		String productNameinText = itemId.getAttribute("title");

		if(productNameinText.contains("TVB0001")){


			UtilityMethods.validateAssertEqual("Business License is Added", "Business License is Added");
			System.out.println("USER HAS UPGRADED TO BUSINESS SUBSCRIPTION");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS UPGRADED TO BUSINESS SUBSCRIPTION "+productNameinText);
		}
		else{
			System.out.println("USER HAS NOT UPGRADED TO BUSINESS SUBSCRIPTION");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS NOT UPGRADED TO BUSINESS SUBSCRIPTION");
			UtilityMethods.validateAssertEqual("Business License is Added", "Business License is NOT Added");
		}

	}
	
	//Validate TV14B0001 To TVP0001 migrate
		public void validateTV14B0001ToTVP0001OrNot() throws Throwable{


			String productNameinText = itemId.getAttribute("title");

			if(productNameinText.contains("TVP0001")){


				UtilityMethods.validateAssertEqual("Business License is Added", "Business License is Added");
				System.out.println("USER HAS UPGRADED TO BUSINESS SUBSCRIPTION");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER HAS UPGRADED TO BUSINESS SUBSCRIPTION "+productNameinText);
			}
			else{
				System.out.println("USER HAS NOT UPGRADED TO BUSINESS SUBSCRIPTION");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER HAS NOT UPGRADED TO BUSINESS SUBSCRIPTION");
				UtilityMethods.validateAssertEqual("Business License is Added", "Business License is NOT Added");
			}

		}

	//Validate TV14P0001 To TVP0001 migrate
	public void validateTV14P0001ToTVC0001OrNot() throws Throwable{


		String productNameinText = itemId.getAttribute("title");

		if(productNameinText.contains("TVC0001")){


			UtilityMethods.validateAssertEqual("Premium License is Added", "Premium License is Added");
			System.out.println("USER HAS UPGRADED TO PREMIUM SUBSCRIPTION");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS UPGRADED TO PREMIUM SUBSCRIPTION "+productNameinText);
		}
		else{
			System.out.println("USER HAS NOT UPGRADED TO PREMIUM SUBSCRIPTION");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS NOT UPGRADED TO PREMIUM SUBSCRIPTION");
			UtilityMethods.validateAssertEqual("Premium License is Added", "Premium License is NOT Added");
		}

	}

	//Validate TV14C0001 To TVC0001 migrate
	public void validateTV14C0001ToTVC0001OrNot() throws Throwable{


		String productNameinText = itemId.getAttribute("title");

		if(productNameinText.contains("TVC0001")){


			UtilityMethods.validateAssertEqual("Corporate License is Added", "Corporate License is Added");
			System.out.println("USER HAS UPGRADED TO CORPORATE SUBSCRIPTION");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS UPGRADED TO CORPORATE SUBSCRIPTION "+productNameinText);
		}
		else{
			System.out.println("USER HAS NOT UPGRADED TO CORPORATE SUBSCRIPTION");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS NOT UPGRADED TO CORPORATE SUBSCRIPTION");
			UtilityMethods.validateAssertEqual("CORPORATE License is Added", "CORPORATE License is NOT Added");
		}

	}





	//Cancel Contract Functionality for Perpetual Order
	public void cancelContractFunctionalityForSwitchedOrder() throws Throwable{

		System.out.println("TRYING TO CANCEL THE CONTRACT");


		try{
			if(cancelContractButtonAfterPepetual.isDisplayed()){
				System.out.println("CANCEL CONTRACT BUTTON AFTER MIGRATION IS VISIBLE");
				cancelContractButtonAfterPepetual.click();
				System.out.println("Switch/Terminate Cancel Contract Button is visible ");
				UtilityMethods.wait10Seconds();
			}

		}
		catch(Exception d){


		}


		try{

			if(cancelContractPopUpOkButton.isDisplayed()){
				System.out.println("USER IS CANCELLING THE ORDER NOW");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER IS CANCELLING THE ORDER NOW");
				cancelContractPopUpOkButton.click();
				UtilityMethods.validateAssertEqual("Cancelled", "Cancelled");
			}
		}

		catch(Exception a){

			UtilityMethods.validateAssertEqual("Cancelled", "Not Cancelled");

		}

	}

	//Cancel Contract Functionality for Migrated Order
	public void cancelContractFunctionalityForMigrateOrder() throws Throwable{

		System.out.println("TRYING TO CANCEL THE CONTRACT");


		try{
			if(cancelContractButtonAfterPepetual.isDisplayed()){
				System.out.println("CANCEL CONTRACT BUTTON IS VISIBLE");
				cancelContractButtonAfterPepetual.click();
				System.out.println("Switch/Terminate Cancel Contract Button is visible ");
				UtilityMethods.wait10Seconds();
			}

		}
		catch(Exception d){


		}


		try{

			if(cancelContractPopUpOkButton.isDisplayed()){
				System.out.println("USER IS CANCELLING THE ORDER NOW");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER IS CANCELLING THE ORDER NOW");
				cancelContractPopUpOkButton.click();
				UtilityMethods.validateAssertEqual("Cancelled", "Cancelled");
			}
		}

		catch(Exception a){

			UtilityMethods.validateAssertEqual("Cancelled", "Not Cancelled");

		}

	}

	//Cancel Contract Functionality for Perpetual Order
	public void cancelContractFunctionalityForPerpetualOrder() throws Throwable{

		System.out.println("TRYING TO CANCEL THE CONTRACT");


		try{
			if(cancelContractButtonAfterPepetual.isDisplayed()){
				System.out.println("CANCEL CONTRACT BUTTON AFTER MIGRATION IS VISIBLE");
				cancelContractButtonAfterPepetual.click();
				System.out.println("Switch/Terminate Cancel Contract Button is visible ");
				UtilityMethods.wait10Seconds();
			}

		}
		catch(Exception d){


		}


		try{

			if(cancelContractPopUpOkButton.isDisplayed()){
				System.out.println("USER IS CANCELLING THE ORDER NOW");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER IS CANCELLING THE ORDER NOW");
				cancelContractPopUpOkButton.click();
				UtilityMethods.validateAssertEqual("Cancelled", "Cancelled");
			}
		}

		catch(Exception a){

			UtilityMethods.validateAssertEqual("Cancelled", "Not Cancelled");

		}

	}

	//Cancel Contract Functionality for Migrated Order
	public void cancelContractFunctionalityForMigratedOrder() throws Throwable{

		System.out.println("TRYING TO CANCEL THE CONTRACT");


		try{
			if(cancelContractButtonAfterMigration.isDisplayed()){
				System.out.println("CANCEL CONTRACT BUTTON IS VISIBLE");
				cancelContractButtonAfterMigration.click();
				System.out.println("Switch/Terminate Cancel Contract Button is visible ");
				UtilityMethods.wait10Seconds();
			}

		}
		catch(Exception d){


		}


		try{

			if(cancelContractPopUpOkButton.isDisplayed()){
				System.out.println("USER IS CANCELLING THE ORDER NOW");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER IS CANCELLING THE ORDER NOW");
				cancelContractPopUpOkButton.click();
				UtilityMethods.validateAssertEqual("Cancelled", "Cancelled");
			}
		}

		catch(Exception a){

			UtilityMethods.validateAssertEqual("Cancelled", "Not Cancelled");

		}

	}



	public void switchToCustomerServicePortal() throws Throwable {


		//Navigate to Customer Service Section
		driver.navigate().to("https://tv-d365fo-ben-01.sandbox.operations.dynamics.com/?cmp=TVDE&mi=MCRCustomerService");

		//Waiting so that sales order can be created
		UtilityMethods.wait30Seconds();
		searchInputFieldForCustomer.sendKeys("20209951");
		UtilityMethods.wait3Seconds();
		searchButton.click();

		UtilityMethods.wait10Seconds();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,2000)");

		UtilityMethods.wait5Seconds();

		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", salesOrderFilterIcon);
		//salesOrderFilterIcon.click();

		UtilityMethods.wait5Seconds();

		try{

			if(channelIdFilterField.isDisplayed()){

				System.out.println("USER IS ENTERING THE SALES ORDER NUMBER");
				//channelIdFilterField.sendKeys("20010752"); //customer created through CRM use for Case 3
				channelIdFilterField.sendKeys(salesOrderNumberOnOrder);  // customer created through AX directly

			}

		}


		catch(Exception e){

			System.out.println("Account Filter Field is not Displayed at ALL");
		}

		UtilityMethods.wait3Seconds();

		applyButtonOnChannelFilter.click();

		UtilityMethods.wait3Seconds();

		detailButtonOfSalesOrder.click();
		UtilityMethods.wait15Seconds();


	}


	//Cancel Contract Functionality for Simple
	public void cancelContractFunctionalityForSimpleOrder() throws Throwable{

		System.out.println("TRYING TO CANCEL THE CONTRACT");

		try{

			if(cancelContractButtonForSimpleOrder.isDisplayed()){
				cancelContractButtonForSimpleOrder.click();
				System.out.println("CANCEL CONTRACT BUTTON IS VISIBLE");
				UtilityMethods.wait10Seconds();
			}
		}



		catch(Exception d){
		}


		try{

			if(cancelContractPopUpOkButton.isDisplayed()){
				System.out.println("USER IS CANCELLING THE ORDER NOW");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER IS CANCELLING THE ORDER NOW");
				cancelContractPopUpOkButton.click();
				UtilityMethods.validateAssertEqual("Cancelled", "Cancelled");
			}
		}

		catch(Exception a){

			UtilityMethods.validateAssertEqual("Cancelled", "Not Cancelled");

		}

	}

	//Cancel Contract Functionality for Switched Order
	public void cancelContractFunctionalityForSwitchOrder() throws Throwable{

		System.out.println("TRYING TO CANCEL THE CONTRACT");

		try{

			if(cancelContractButtonForSwitchedOrder.isDisplayed()){
				cancelContractButtonForSwitchedOrder.click();
				System.out.println("CANCEL CONTRACT BUTTON IS VISIBLE");
				UtilityMethods.wait10Seconds();
			}
		}



		catch(Exception d){

			System.out.println("CANCEL CONTRACT BUTTON IS NOT AVAILABLE");
		}


		try{

			if(cancelContractPopUpOkButton.isDisplayed()){
				System.out.println("USER IS CANCELLING THE ORDER NOW");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER IS CANCELLING THE ORDER NOW");
				cancelContractPopUpOkButton.click();
				UtilityMethods.validateAssertEqual("Cancelled", "Cancelled");
			}
		}

		catch(Exception a){
			System.out.println("POPUP IS NOT OPENED FOR CANCEL CONTRACT");
			UtilityMethods.validateAssertEqual("Cancelled", "Not Cancelled");

		}

	}

	//Searching for the Customer
	public void searchForCustomer() throws Throwable{

		driver.navigate().to("https://tv-d365fo-ben-01.sandbox.operations.dynamics.com/?cmp=TVDE&mi=CustTableListPage");
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(customerAccountFilterIcon));
		customerAccountFilterIcon.click();
		UtilityMethods.wait5Seconds();



		try{

			if(channelIdFilterField.isDisplayed()){


				//channelIdFilterField.sendKeys("20010752"); //customer created through CRM use for Case 3
				//channelIdFilterField.sendKeys("20209951"); 
				channelIdFilterField.sendKeys("20217931");
				 
				//channelIdFilterField.sendKeys("10000010"); 

				// customer created through AX directly
				//channelIdFilterField.sendKeys(finalOrderID);
			}

		}


		catch(Exception e){

			System.out.println("Account Filter Field is not Displayed at ALL");
		}

		UtilityMethods.wait1Seconds();


		applyButtonOnChannelFilter.click();

		UtilityMethods.wait3Seconds();

		customerName = getCustomerName.getAttribute("title").substring(0,7);
		//The output will  be NAL TC09
		System.out.println(customerName);
		UtilityMethods.wait3Seconds();


	}


	public void searchForChannelReferenceID() throws Throwable{

		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(channelIdFilterIcon));
	//	channelIdFilterIcon.click();

		System.out.println("USER IS WAITING FOR 02 MINUTES SO THAT SALES ORDER CREATES IN ERP");
		UtilityMethods.wait420Seconds();
	
		driver.navigate().refresh();
		UtilityMethods.waitForPageLoadAndPageReady();

		wait.until(ExpectedConditions.elementToBeClickable(channelIdFilterIcon));
		channelIdFilterIcon.click();
		UtilityMethods.wait2Seconds();


		try{

			if(channelIdFilterField.isDisplayed()){
				
				channelIdFilterField.sendKeys(ConfirmationPage.channelReferenceID);
			//	channelIdFilterField.sendKeys("61UAT000003369");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("ORDER IS SEARCHED");

			}
		}

		catch(Exception e){

			System.out.println("Channel Filter ID is not Displayed at ALL");
		}

		//For Execution - On Demo


		UtilityMethods.wait5Seconds();
		System.out.println("Clicking on Apply button");
		try{
			new Actions(driver).moveToElement(applyButtonOnChannelFilter).click().perform();
		

		}
		catch(NoSuchElementException e){
			UtilityMethods.clickElemetJavaSciptExecutor(applyButtonOnChannelFilter);
		}
		UtilityMethods.wait3Seconds();
//				if(driver.findElement(By.xpath("//input[@name = 'DetailsHeader_ChannelReferenceId']")).getAttribute("title").contains(ConfirmationPage.channelReferenceID)){
//		
//					System.out.println("SALES ORDER CREATED FOR CHANNEL REFERENCE ID");
//		
//				}
//		
//				else {
//		
//					UtilityMethods.validateAssertEqual("SALE ORDER CREATED", "SALE ORDER NOT CREATED");
//		
				//}

	}




	//Sending the qoutation and Completing it
	public void sendQoutationAndVerifyTheOrder() throws Throwable{

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		try{
		salesQuotationsButton.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(totalsButton));
		UtilityMethods.wait2Seconds();
		Reporter.addStepLog("SEND QUOTATION BUTTON IS CLICKED");
		}
		catch(Exception e){
			salesQuotationsButton1.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(totalsButton));
			UtilityMethods.wait2Seconds();
			Reporter.addStepLog("SEND QUOTATION BUTTON IS CLICKED");
			
		}
		totalsButton.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(currencyErp));
		try{
			try{
				int index = currencyErp.getAttribute("title").indexOf("Click");
			
				currencyINErp = currencyErp.getAttribute("title").substring(0, index).trim();
			}
			catch(Exception d){
				currencyINErp = currencyErp.getAttribute("title");
			}
				System.out.println("CURRENCY IS: "+currencyINErp);
			
			if(currencyINErp==null || currencyINErp==""){
				int index1 = currencyErp.getAttribute("old-title").indexOf("Click");
				
				currencyINErp = currencyErp.getAttribute("old-title").substring(0, index1).trim();
				System.out.println("CURRENCY IS: "+currencyINErp);
				Reporter.addStepLog("CURRENCY IS: "+currencyINErp);

			}
		}
		catch(Exception e)
		{
			currencyINErp = "";
			System.out.println("NOT ABLE TO CATCH CURRENCY");
		}
		
		try{
			
		
			totalAmountINErp = totalAmountErp.getAttribute("title");
			System.out.println("Total amount is: "+totalAmountINErp);
			if(totalAmountINErp==null || totalAmountINErp==""){
				
				totalAmountINErp = totalAmountErp.getAttribute("old-title");
				System.out.println("Total amount is: "+totalAmountErp.getAttribute("old-title"));
				Reporter.addStepLog("TOTAL AMOUNT IS: "+totalAmountErp.getAttribute("old-title"));

			}
		}
		catch(Exception e)
		{
			System.out.println("NOT ABLE TO CATCH TOTAL AMOUNT");
		}
		

		UtilityMethods.wait3Seconds();
		clickOkForConfirmSalesOrder.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		UtilityMethods.clickElemetJavaSciptExecutor(qoutationButtonOnHeaderBar);
		UtilityMethods.wait5Seconds();
		try{
			if(sendQoutationButton.isDisplayed()!=true){
				UtilityMethods.clickElemetJavaSciptExecutor(qoutationButtonOnHeaderBar2);
			}
			UtilityMethods.wait5Seconds();
			sendQoutationButton.click();
		}
		catch(Exception e){
			UtilityMethods.clickElemetJavaSciptExecutor(qoutationButtonOnHeaderBar);
			UtilityMethods.wait5Seconds();
			sendQoutationButton.click();
		}
		UtilityMethods.wait10Seconds();
//		try{
//			new Actions(driver).moveToElement(printQuotationToggle).click().perform();
//		}
//		catch(Exception e){
			UtilityMethods.clickElemetJavaSciptExecutor(printQuotationToggle);
//		}
	
		
		UtilityMethods.wait3Seconds();
//		try{
//			new Actions(driver).moveToElement(printQuotationDestination).click().perform();
//		}
//		catch(Exception e){
			UtilityMethods.clickElemetJavaSciptExecutor(printQuotationDestination);
//		}
	
		UtilityMethods.wait5Seconds();
		oKButtonTosendQoutation.click();
		UtilityMethods.wait15Seconds();

		if(qoutationStatusOnERPIfCreated.getAttribute("title").contains("Sent")){

			System.out.println("QOUTATION HAS BEEN SENT SUCCESSFULLY");
			Reporter.addStepLog("QOUTATION HAS BEEN SENT SUCCESSFULLY");

		}

		UtilityMethods.switchToFirstTab();
		cmpPage.validateAccount();
		Reporter.addStepLog("VALUES IS ACCOUNT ARE VALIDATED");
		UtilityMethods.scrollToWebElement(quoteSection);
		UtilityMethods.wait1Seconds();
		int salesOrderOnAccountFlag = 0;
		try{
			WebElement quotaion = driver.findElement(By.xpath("//a[@title = "+salesQuotationNumber+"]"));
			salesOrderOnAccountFlag = 1;
		}
		catch(NoSuchElementException e){
			
			driver.findElement(By.xpath("//a[@title='Sort by Quote ID']")).click();
			UtilityMethods.wait3Seconds();
			WebElement quotaion2 = driver.findElement(By.xpath("//a[@title = "+salesQuotationNumber+"]"));
			salesOrderOnAccountFlag = 1;
		}
		if(salesOrderOnAccountFlag==1){
			
			System.out.println("Quotation with same Quote id exist in CRM");
			driver.findElement(By.xpath("//a[@title = "+salesQuotationNumber+"]")).click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.switchToFirstTab();
			UtilityMethods.wait7Seconds();
			UtilityMethods.switchTOIframeCRM();
			UtilityMethods.wait3Seconds();
			
			try{
			if(quoteStatusInCRM.isDisplayed()){
				System.out.println("Quotation with same quote id  exist in CRM quotes section with ERP status = Sent");
			}
			}
			catch(NoSuchElementException e){
				
				driver.navigate().refresh();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.switchToFirstTab();
				UtilityMethods.wait7Seconds();
				UtilityMethods.switchTOIframeCRM();
				UtilityMethods.wait3Seconds();
				if(quoteStatusInCRM.isDisplayed()){
					System.out.println("Quotation with same quote id  exist in CRM quotes section with ERP status = Sent");
				}
			}
			
			totalAmountCRM = totalAmountINCRM.getText();
			if(totalAmountINCRM.getText().contains(totalAmountINErp)){
				
				System.out.println("TOTAL AMOUNT IN ERP = TOTAL AMOUNT IN CRM");
			}
			
			if(totalAmountINCRM.getText().contains("€") && currencyINErp.contains("EUR")){
				
				System.out.println("CURRENCY IN ERP MATCHED WITH CURRENCY IN CRM");
			}
			
			
			
		}
		

		UtilityMethods.switchToFirstTab();
		UtilityMethods.wait5Seconds();
		driver.findElement(By.xpath("//img[@title='Close' and @class = 'closeButton']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
	
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//img[@title='Close' and @class = 'closeButton']"))));
		UtilityMethods.wait5Seconds();
		driver.findElement(By.xpath("//img[@title='Close' and @class = 'closeButton']")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		UtilityMethods.switchTOIframeCRM();
		new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(Topic1));
		UtilityMethods.switchToNewTab();
		UtilityMethods.waitForPageLoadAndPageReady();
		
	}


	public void createDirectBusinessLicenseOrder() throws Throwable{

		UtilityMethods.wait5Seconds();
		System.out.println("SALES ORDER NUMBER :"+"<b>"+salesOrderNumberAfterQoutation.getText().substring(0,10)+"<b>");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER :"+"<b>"+salesOrderNumberAfterQoutation.getText().substring(0,10)+"<b>");
		itemNumberInputField.click();

		UtilityMethods.wait3Seconds();

		itemNumberInputField.sendKeys("tvb");
		UtilityMethods.wait5Seconds();



		try{

			if(businessLicenseInputValue.isDisplayed()){

				businessLicenseInputValue.click();

			}
		}
		catch(Exception e){


		}

		UtilityMethods.wait3Seconds();
		String productNameinText = productNameOfFirstOrder.getAttribute("title");

		try{
			if(productNameinText.equals("TeamViewer Business / TeamViewer Business Yearly Subscription")){


				UtilityMethods.validateAssertEqual("Business License is Added", "Business License is Added");

			}
		}
		catch(Exception e){


		}

		System.out.println("USER HAS CREATED A QOUTATION ORDER: "+productNameinText);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);

	}

	//Checking The sales order
	public void checkSalesOrderAfterQoutationConfirmed() throws Throwable{

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		for(int i=1;i<=3;i++){
			
			if(driver.findElements(By.xpath("//input[contains(@id,'itemName_input')]")).size()>0){
				break;
			}
			else{
				UtilityMethods.waitForPageLoadAndPageReady();
				
				driver.navigate().back();
				
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait5Seconds();
			}
			
		}

		salesOrderButtonOnGeneralPage.click();
		System.out.println("USER HAS CLICKED ON GENERAL > SALES ORDER BUTTON");
		UtilityMethods.wait20Seconds();

		try{
		System.out.println("SALES ORDER NUMBER :"+salesOrderNumber.getText().substring(0,10));
		salesOrderNumberCRMtoERP = salesOrderNumber.getText().substring(0,10);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER :"+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
		}
		catch(NoSuchElementException e){
			System.out.println("SALES ORDER NUMBER :"+salesOrderNumber2.getText().substring(0,10));
			salesOrderNumberCRMtoERP = salesOrderNumber2.getText().substring(0,10);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("SALES ORDER NUMBER :"+"<b>"+salesOrderNumber2.getText().substring(0,10)+"<b>");
		}

		String productNameinText = productNameOfFirstOrder.getAttribute("title");

		if(productNameinText.equals("TeamViewer 14 Premium / TeamViewer 14 Premium")){

			System.out.println("SALESORDER OF QOUTATION IS CORRECTLY GENERATED");
			UtilityMethods.validateAssertEqual("Premium License is Added", "Premium License is Added");
			Reporter.addStepLog("PREMIUM LICENCSE IS ADDED");

		}
		else if(productNameinText.equals("TeamViewer Business / TeamViewer Business Yearly Subscription")){

			System.out.println("SALESORDER OF QOUTATION IS CORRECTLY GENERATED");
			UtilityMethods.validateAssertEqual("TeamViewer Business License is Added", "TeamViewer Business License is Added");
			Reporter.addStepLog("TEAMVIEWER BUSINESS LICENCSE IS ADDED");
			
		}
		else if(productNameinText.contains("TeamViewer Premium / TeamViewer Premium Yearly Subscription")){
			
			System.out.println("SALESORDER OF QOUTATION IS CORRECTLY GENERATED");
			UtilityMethods.validateAssertEqual("TeamViewer Premium Yearly Subscription","TeamViewer Premium Yearly Subscription");
			Reporter.addStepLog("PREMIUM YEARLY LICENCSE IS ADDED");
		}
		else{
			
			UtilityMethods.validateAssertEqual("Select product added", "Selected product not added");
			
		}
		
		// VERIFICATION REGARDING SALES ORDER
		UtilityMethods.wait2Seconds();	
		try{
		headerBarForCustomerPersonalInformation.click();
		}
		catch(NoSuchElementException e){
			e.getMessage();
			headerBarForCustomerPersonalInformation2.click();
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		String customerName;
		new Actions(driver).moveToElement(driver.findElement(By.xpath("//h2[contains(text(),'Address')]"))).click().perform();
	//	driver.findElement(By.xpath("//h2[contains(text(),'Address')]")).click();
		UtilityMethods.wait3Seconds();
		try{
			customerName = customerNameInERP.getAttribute("title");
			}
			catch(Exception d){
				try{
				driver.findElement(By.xpath("//h2[contains(text(),'Address')]")).click();
				UtilityMethods.wait3Seconds();
				customerName = customerNameInERP.getAttribute("title");
			}
				catch(Exception h){
					customerName = customerNameInERP2.getAttribute("title");

				}
			
			}
			
	
		
		
		
		try{
			if(customerName.contains(CRMPage.accountName)){
			
				System.out.println("CUSTOMERS IN ERP "+customerNameInERP.getAttribute("title")+" MATCHED "+CRMPage.accountName);
				Reporter.addStepLog("CUSTOMERS IN ERP: "+customerNameInERP.getAttribute("title")+" MATCHED WITH CRM: "+CRMPage.accountName);
			
			}
		} 
		catch(Exception e){
			
			try{
		if(customerName.contains(CRMPage.firstNameInCRM)){
				
				
				System.out.println("CUSTOMERS IN ERP "+customerNameInERP2.getAttribute("title")+" MATCHED "+CRMPage.firstNameInCRM);
				Reporter.addStepLog("CUSTOMERS IN ERP: "+customerNameInERP2.getAttribute("title")+" MATCHED WITH CRM: "+CRMPage.firstNameInCRM);
			}
			}
			catch(NoSuchElementException f){
				System.out.println("CUSTOMERS IN ERP "+customerNameInERP.getAttribute("title")+" MATCHED "+CRMPage.firstNameInCRM);
				Reporter.addStepLog("CUSTOMERS IN ERP: "+customerNameInERP.getAttribute("title")+" MATCHED WITH CRM: "+CRMPage.firstNameInCRM);
			}
			
		}
		
		
		UtilityMethods.wait2Seconds();
		int index3;
		try{
		index3 = customerEmailInERP.getAttribute("title").indexOf("Click");
		
		if(customerEmailInERP.getAttribute("title").contains(CRMPage.emailForLead)){
			
			System.out.println("CUSTOMERS EMAIL IN ERP "+customerEmailInERP.getAttribute("title").substring(0,index3)+" MATCHED EMAIL IN CRM "+CRMPage.emailForLead);
			Reporter.addStepLog("CUSTOMERS EMAIL IN ERP "+customerEmailInERP.getAttribute("title").substring(0,index3)+" MATCHED EMAIL IN CRM "+CRMPage.emailForLead);
		}
		}
		catch(NoSuchElementException | NullPointerException e){
			index3 = customerEmailInERP.getAttribute("title").indexOf("Click");
			if(customerEmailInERP.getAttribute("title").contains(CRMPage.emailOnAccountCRM)){
				
				System.out.println("CUSTOMERS EMAIL IN ERP "+customerEmailInERP.getAttribute("title").substring(0,index3)+" MATCHED EMAIL IN CRM "+CRMPage.emailOnAccountCRM);
				Reporter.addStepLog("CUSTOMERS EMAIL IN ERP "+customerEmailInERP.getAttribute("title").substring(0,index3)+" MATCHED EMAIL IN CRM "+CRMPage.emailOnAccountCRM);
			}
			System.out.println("EMAIL ADDRESS OF NOT FOUND IN ERP");
		}
		UtilityMethods.wait1Seconds();
		try{
			System.out.println("CUSTOMER DELIVERY ADDRESS IS: "+customerDeliveryAddressInERP.getAttribute("title"));
			Reporter.addStepLog("CUSTOMER DELIVERY ADDRESS IS: "+customerDeliveryAddressInERP.getAttribute("title"));
		}
		catch(NoSuchElementException e){
		System.out.println("CUSTOMER DELIVERY ADDRESS IS: "+customerDeliveryAddressInERP2.getAttribute("title"));
		Reporter.addStepLog("CUSTOMER DELIVERY ADDRESS IS: "+customerDeliveryAddressInERP2.getAttribute("title"));
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		UtilityMethods.scrollToElement(salesTaxGroupForCRM);
		int index = salesTaxGroupForCRM.getAttribute("title").indexOf("Click");
		System.out.println("Sales tax number is: "+salesTaxGroupForCRM.getAttribute("title"));
		if(salesTaxGroupForCRM.getAttribute("title").contains(CRMPage.salesTaxGroup))
		{
			System.out.println("Sales tax group is "+salesTaxGroupForCRM.getAttribute("title").substring(0, index));
			Reporter.addStepLog("SALES TAX GROUP IS: "+salesTaxGroupForCRM.getAttribute("title").substring(0, index));
		}
		
		if(languageOfStoreInSetUpTab.getAttribute("title").contains("nl"))
		{
			System.out.println("Language is "+languageOfStoreInSetUpTab.getAttribute("title"));
			Reporter.addStepLog("LANGUAGE IS: "+languageOfStoreInSetUpTab.getAttribute("title"));
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait2Seconds();
		driver.findElement(By.xpath("//h2[contains(text(),'Price and discount')]")).click();
		UtilityMethods.wait3Seconds();
		try{
		
		int index2 = currencyOfTheCountry.getAttribute("title").indexOf("Click");
		if(currencyOfTheCountry.getAttribute("title").contains("EUR")){
			
			System.out.println("Currency is: "+currencyOfTheCountry.getAttribute("title").substring(0,index2));
			Reporter.addStepLog("CURRENCY IS: "+currencyOfTheCountry.getAttribute("title").substring(0,index2));
			
		}
		}
		catch(Exception e){
			driver.findElement(By.xpath("//h2[contains(text(),'Price and discount')]")).click();
			UtilityMethods.wait3Seconds();
			
			int index2 = currencyOfTheCountry.getAttribute("title").indexOf("Click");
			if(currencyOfTheCountry.getAttribute("title").contains("EUR")){
				try{
				System.out.println("Currency is: "+currencyOfTheCountry.getAttribute("title").substring(0,index2));
				Reporter.addStepLog("CURRENCY IS: "+currencyOfTheCountry.getAttribute("title").substring(0,index2));
				}
				catch(Exception f)
				{
					System.out.println("Currency is: "+currencyOfTheCountry.getAttribute("title"));
					Reporter.addStepLog("CURRENCY IS: "+currencyOfTheCountry.getAttribute("title"));
				}				
			}
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		try{
			driver.findElement(By.xpath("(//span[contains(text(),'Lines')])[4]")).click();
		}
		catch(Exception e){
			driver.findElement(By.xpath("(//span[contains(text(),'Lines')])[2]")).click();
		}
	
		UtilityMethods.wait5Seconds();
		if(CRMPage.flagForAccountToOppertunity!=1){
			//h2[contains(text(),'Sales order header')]
			driver.findElement(By.xpath("//h2[contains(text(),'Sales order header')]")).click();
			UtilityMethods.wait3Seconds();
			try{
			UtilityMethods.scrollToElement(totalAmountInERP);
			System.out.println("Total Line amount"+totalAmountInERP.getAttribute("title"));
			if(totalAmountInERP.getAttribute("title").contains(totalAmountCRM)){
			
				System.out.println("Amount is ERP = Amount in CRM");
				Reporter.addStepLog("AMOUNT IN ERP = TOTAL AMOUNT IN CRM");
			
			}
			}
			catch(Exception e){
				driver.findElement(By.xpath("//h2[contains(text(),'Sales order header')]")).click();
				UtilityMethods.wait3Seconds();
				UtilityMethods.scrollToElement(totalAmountInERP);
				System.out.println("Total Line amount"+totalAmountInERP.getAttribute("title"));
				if(totalAmountInERP.getAttribute("title").contains(totalAmountCRM)){
				
					System.out.println("Amount is ERP = Amount in CRM");
					Reporter.addStepLog("AMOUNT IN ERP = TOTAL AMOUNT IN CRM");
				
				}
			}
		}
	}


	//Open customer details and add a sales qoutation for CRM Customer
	public void openingCustomerDetailsAndBusinessOrderforCRMFlow() throws Throwable{

	UtilityMethods.wait20Seconds();
		
		salesQuotationNumber = salesOrderNumberAfterQoutation.getText().trim();
		System.out.println(salesQuotationNumber);
		try{
			salesQuotationNumber = salesQuotationNumber.substring(0, 9);
		}
		catch(StringIndexOutOfBoundsException e){
			salesQuotationNumber = salesQuotationNumber.substring(0, 8);
		}
		System.out.println("SALES QUOTATION NUMBER :"+"<b>"+salesQuotationNumber+"<b>");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES QUOTATION NUMBER :"+"<b>"+salesQuotationNumber+"<b>");
		itemNumberInputField.click();

		UtilityMethods.wait3Seconds();

		itemNumberInputField.sendKeys("TVB0001");
		UtilityMethods.wait5Seconds();

		if(driver.findElements(By.xpath("(//input[@title='TVB0001'])[1]")).size()!=0){

			businessLicenseInputValue.click();

		}
		else{
			UtilityMethods.wait5Seconds();
			businessLicenseInputValue.click();
		}
		
		UtilityMethods.waitForPageLoadAndPageReady();
		String productNameinText;
		UtilityMethods.wait10Seconds();
		
		try{
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(productNameOfSalesQoutationOrder));
			productNameinText = productNameOfSalesQoutationOrder.getAttribute("title");
			new Actions(driver).moveToElement(productNameOfSalesQoutationOrder).click().perform();
		}
		catch(Exception e){
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(productNameOfSalesQoutationOrder2));
			productNameinText = productNameOfSalesQoutationOrder2.getAttribute("title");
			new Actions(driver).moveToElement(productNameOfSalesQoutationOrder2).click().perform();
		}
		

		try{
			if(productNameinText.equals("TeamViewer Business / TeamViewer Business Yearly Subscription")){

				System.out.println("Business License is Added");
				Reporter.addStepLog("BUSINESS LICENSE IS ADDED");
				UtilityMethods.validateAssertEqual("Business License is Added", "Business License is Added");

			}
		}
		catch(Exception e){

			System.out.println("Business License is not Added");
			UtilityMethods.validateAssertEqual("Business License is Added", "Business License is not Added");
		}

		System.out.println("USER HAS CREATED A QOUTATION ORDER: "+productNameinText);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);


		UtilityMethods.wait5Seconds();
		new Actions(driver).moveToElement(saveButton).click().build().perform();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		
		if(CRMPage.flagForSalesOrderFromCRM ==  0){

		String isQoutationCreated = qoutationStatusOnERPIfCreated.getAttribute("title");

		if(isQoutationCreated.contains("Create")){

			System.out.println("QOUTATION HAS BEEN SUCCESSFULLY CREATED ON ERP");
			Reporter.addStepLog("QOUTATION HAS BEEN SUCCESSFULLY CREATED ON ERP");
		}
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		}
		
		UtilityMethods.moveToElementWithJSExecutor(priceInformationTab);
		new Actions(driver).moveToElement(priceInformationTab).click().build().perform();
		// ENTER DISCOUNTS ETC HERE
		UtilityMethods.wait5Seconds();
		if(CRMPage.flagForSalesOrderFromCRM == 0 && driver.findElements(By.xpath("(//span[contains(text(),'Add line')])[3]")).size()!=0){
			
			try{
				priceInfromationAddLine.click();
				}
				catch(NoSuchElementException e){
					
					UtilityMethods.wait3Seconds();
					priceInfromationAddLine.click();
					
				}
		
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		try{
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(reasonCodeTVB));
			reasonCodeTVB.clear();
			reasonCodeTVB.sendKeys("17");
			reasonCodeTVB.sendKeys(Keys.TAB);
			Reporter.addStepLog("REASON CODE IS SELECTED: 17");
		}
		catch(Exception e){
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(reasonCodeTVB4));
			reasonCodeTVB4.clear();
			reasonCodeTVB4.sendKeys("17");
			reasonCodeTVB4.sendKeys(Keys.TAB);
			Reporter.addStepLog("REASON CODE IS SELECTED: 17");
		}
		UtilityMethods.wait1Seconds();
		try{
			if(!discountMethodDropDownTVB.getAttribute("title").contains("Discount percentage")){
				discountMethodDropDownTVB.click();
				UtilityMethods.waitForPageLoadAndPageReady();
			//	new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(discountPercentageMethod));
				UtilityMethods.wait5Seconds();
				discountPercentageMethod.click();
				discountMethodDropDownTVB.sendKeys(Keys.TAB);
				Reporter.addStepLog("DISCOUNT PERCENTAGE METHOD IS SELECTED");
			}
			else{
				
			//	System.out.println(discountMethodDropDownTVB.getAttribute("title"));
				discountMethodDropDownTVB.sendKeys(Keys.TAB);
			}
		}
		catch(Exception e){
			if(!discountMethodDropDownTVB4.getAttribute("title").contains("Discount percentage")){
				discountMethodDropDownTVB4.click();
				UtilityMethods.waitForPageLoadAndPageReady();
			//	new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(discountPercentageMethod));
				UtilityMethods.wait5Seconds();
				discountPercentageMethod.click();
				discountMethodDropDownTVB4.sendKeys(Keys.TAB);
				Reporter.addStepLog("DISCOUNT PERCENTAGE METHOD IS SELECTED");
			}
			else{
				
			//	System.out.println(discountMethodDropDownTVB.getAttribute("title"));
				discountMethodDropDownTVB4.sendKeys(Keys.TAB);
			}
			
		}
		try{
			discountPercentage.clear();
			discountPercentage.sendKeys("10");
			discountPercentage.sendKeys(Keys.ENTER);
		}
		catch(Exception e){
			discountPercentage3.clear();
			discountPercentage3.sendKeys("10");
			discountPercentage3.sendKeys(Keys.ENTER);
		}
		Reporter.addStepLog("DISCOUNT PERCENTAGE IS SELECTED");
		UtilityMethods.waitForPageLoadAndPageReady();
	
		}else if(CRMPage.flagForSalesOrderFromCRM == 1 && driver.findElements(By.xpath("(//span[contains(text(),'Add line')])[2]")).size()!=0){
			
			orderFlowForAddon();
			
		}
		
		System.out.println("Manual discount is: "+manualDiscount.getAttribute("aria-checked"));
		if(manualDiscount.getAttribute("aria-checked").equalsIgnoreCase("false")==true){
			
			try{
				System.out.println("Simple clicking manual discount");
				UtilityMethods.clickElemetJavaSciptExecutor(manualDiscount);
			}
			catch(Exception e){
				System.out.println("Clicking through action");
				new Actions(driver).moveToElement(manualDiscount).click().perform();
			}
			
		}
	
		
	}

	public void orderFlowForAddon() throws InterruptedException {
		try{
				priceInfromationAddLine1.click();
			}
			catch(NoSuchElementException e){
				
				UtilityMethods.wait3Seconds();
				priceInfromationAddLine1.click();
				
			}
			
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait2Seconds();
		try{
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(reasonCodeTVB2));
			reasonCodeTVB2.clear();
			reasonCodeTVB2.sendKeys("17");
			reasonCodeTVB2.sendKeys(Keys.TAB);
		}
		catch(Exception e){
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(reasonCodeTVB1));
			reasonCodeTVB1.clear();
			reasonCodeTVB1.sendKeys("17");
			reasonCodeTVB1.sendKeys(Keys.TAB);
		}
		
		Reporter.addStepLog("REASON CODE IS SELECTED: 17");
		UtilityMethods.wait2Seconds();
		try{
			if(!discountMethodDropDownTVB1.getAttribute("title").contains("Discount percentage")){
					discountMethodDropDownTVB1.click();
					UtilityMethods.waitForPageLoadAndPageReady();
					new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(discountPercentageMethod));
					UtilityMethods.wait2Seconds();
					discountPercentageMethod.click();
					discountMethodDropDownTVB1.sendKeys(Keys.TAB);
					Reporter.addStepLog("DISCOUNT PERCENTAGE METHODS IS SELECTED");
			}
			else{
					
			//	System.out.println(discountMethodDropDownTVB.getAttribute("title"));
					discountMethodDropDownTVB1.sendKeys(Keys.TAB);
			}
		}
		catch(Exception e){
			if(!discountMethodDropDownTVB2.getAttribute("title").contains("Discount percentage")){
				discountMethodDropDownTVB2.click();
				UtilityMethods.waitForPageLoadAndPageReady();
				new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(discountPercentageMethod));
				UtilityMethods.wait2Seconds();
				discountPercentageMethod.click();
				discountMethodDropDownTVB2.sendKeys(Keys.TAB);
				Reporter.addStepLog("DISCOUNT PERCENTAGE METHODS IS SELECTED");
		}
		else{
				
		//	System.out.println(discountMethodDropDownTVB.getAttribute("title"));
				discountMethodDropDownTVB2.sendKeys(Keys.TAB);
			}
		}
		try{
			discountPercentage1.clear();
			discountPercentage1.sendKeys("10");
			discountPercentage1.sendKeys(Keys.ENTER);
		}
		catch(Exception e){
			discountPercentage2.clear();
			discountPercentage2.sendKeys("10");
			discountPercentage2.sendKeys(Keys.ENTER);
		}
		Reporter.addStepLog("DISCOUNT PERCENTAGE IS SELECTED");
		UtilityMethods.waitForPageLoadAndPageReady();
	}

	//Open customer details and add a business product
	public void openingCustomerDetailsAndBusinessOrder() throws Throwable{

		WebDriverWait wait = new WebDriverWait(driver,30);

		Actions action = new Actions(driver);
		action.contextClick(customerOrderNumberValue).perform();
		UtilityMethods.wait1Seconds();
		openSalesOrderDetails.click();

		wait.until(ExpectedConditions.visibilityOf(clickSELLbuttonOnCustomerDetailPage));
		UtilityMethods.wait10Seconds();
		clickSELLbuttonOnCustomerDetailPage.click();
		wait.until(ExpectedConditions.visibilityOf(salesOrderButtonOnCustomerDetailPage));
		salesOrderButtonOnCustomerDetailPage.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		wait.until(ExpectedConditions.visibilityOf(salesOrderNumber));

		salesOrderNumberOnOrder = salesOrderNumber.getText().substring(0,9); 
		System.out.println("SALES ORDER NUMBER :"+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER :"+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
		itemNumberInputField.click();

		UtilityMethods.wait1Seconds();

		itemNumberInputField.sendKeys("tvb0001");
		UtilityMethods.wait7Seconds();
		businessLicenseInputValue.click();

		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait20Seconds();
		String productNameinText;
		try{
			productNameinText = productNameOfFirstOrder.getAttribute("title");
		}
		catch(Exception e){
			productNameinText = productNameOfFirstOrder2.getAttribute("title");
		}

		if(productNameinText.equals("TeamViewer Business / TeamViewer Business Yearly Subscription")){


			UtilityMethods.validateAssertEqual("Business License is Added", "Business License is Added");

		}
		else{

			UtilityMethods.validateAssertEqual("Business License is Added", "Business License is NOT Added");
		}

		System.out.println("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
	}

	//Open customer details and add a TV14B0001 Product
	public void openingCustomerDetailsAndTV14B001Order() throws Throwable{

		UtilityMethods.wait5Seconds();
		Actions action = new Actions(driver);
		action.contextClick(customerOrderNumberValue).perform();
		UtilityMethods.wait1Seconds();
		openSalesOrderDetails.click();

		UtilityMethods.wait7Seconds();
		clickSELLbuttonOnCustomerDetailPage.click();
		salesOrderButtonOnCustomerDetailPage.click();
		UtilityMethods.wait30Seconds();

		salesOrderNumberOnOrder = salesOrderNumber.getText().substring(0,9);
		System.out.println("SALES ORDER NUMBER :"+salesOrderNumber.getText().substring(0,9));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER :"+"<b>"+salesOrderNumber.getText().substring(0,9)+"<b>");
		itemNumberInputField.click();
		UtilityMethods.wait1Seconds();

		itemNumberInputField.sendKeys("TV14B0001");
		UtilityMethods.wait10Seconds();
		perpetualBusinessInputValue.click();

		UtilityMethods.wait10Seconds();


		String productNameinText = driver.findElement(By.xpath("//span[contains(@title,'TV14B0001')]")).getAttribute("title");
		System.out.println(productNameinText);
		if(productNameinText.contains("TV14B0001")){


			UtilityMethods.validateAssertEqual("Perpetual Business 14 is Added", "Perpetual Business 14 is Added");

		}
		else{

			UtilityMethods.validateAssertEqual("Perpetual Business 14 is Added", "Perpetual Business 14 is NOT Added");
		}

		System.out.println("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
	}

	//Open customer details and add a TV14B0001 Product
	public void openingCustomerDetailsAndTV13B001Order() throws Throwable{

		UtilityMethods.wait5Seconds();
		Actions action = new Actions(driver);
		action.contextClick(customerOrderNumberValue).perform();
		UtilityMethods.wait1Seconds();
		openSalesOrderDetails.click();

		UtilityMethods.wait7Seconds();
		clickSELLbuttonOnCustomerDetailPage.click();
		salesOrderButtonOnCustomerDetailPage.click();
		UtilityMethods.wait30Seconds();

		salesOrderNumberOnOrder = salesOrderNumber.getText().substring(0,9);
		System.out.println("SALES ORDER NUMBER :"+salesOrderNumber.getText().substring(0,9));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER :"+"<b>"+salesOrderNumber.getText().substring(0,9)+"<b>");
		itemNumberInputField.click();
		UtilityMethods.wait1Seconds();

		itemNumberInputField.sendKeys("tv13");
		UtilityMethods.wait10Seconds();
		perpetualBusinessInputValueVersion13.click();

		UtilityMethods.wait10Seconds();


		String productNameinText = driver.findElement(By.xpath("//span[contains(@title,'TV13B0001')]")).getAttribute("title");
		System.out.println(productNameinText);
		if(productNameinText.contains("TV13B0001")){


			UtilityMethods.validateAssertEqual("Perpetual Business 13 is Added", "Perpetual Business 13 is Added");

		}
		else{

			UtilityMethods.validateAssertEqual("Perpetual Business 13 is Added", "Perpetual Business 13 is NOT Added");
		}

		System.out.println("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
	}

	//Open customer details and add a TV14P0001 Product for CRM customer
	public void addPerpetualOrderForCRMCustomer() throws Throwable{


		System.out.println("SALES QOUTATION NUMBER :"+salesOrderNumberAfterQoutation.getText().substring(0,9));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES QOUTATION NUMBER :"+"<b>"+salesOrderNumberAfterQoutation.getText().substring(0,9)+"<b>");
		itemNumberInputField.click();
		UtilityMethods.wait1Seconds();

		itemNumberInputField.sendKeys("tv14p");
		UtilityMethods.wait10Seconds();
		perpetualPremiumInputValue.click();

		UtilityMethods.wait10Seconds();


		String productNameinText = driver.findElement(By.xpath("//input[contains(@title,'TV14P0001')]")).getAttribute("title");
		System.out.println(productNameinText);
		if(productNameinText.contains("TV14P0001")){


			UtilityMethods.validateAssertEqual("Perpetual Premium 14 is Added", "Perpetual Premium 14 is Added");

		}
		else{

			UtilityMethods.validateAssertEqual("Perpetual Premium 14 is Added", "Perpetual Premium 14 is NOT Added");
		}

		System.out.println("USER HAS CREATED A QOUTATION ORDER: "+productNameinText);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);


		UtilityMethods.wait3Seconds();
		saveButton.click();
		UtilityMethods.wait10Seconds();

		String isQoutationCreated = qoutationStatusOnERPIfCreated.getAttribute("title");

		if(isQoutationCreated.contains("Create")){

			System.out.println("QOUTATION HAS BEEN SUCCESSFULLY CREATED ON ERP");
		}
	}

	//Open customer details and add a TV14P0001 Product
	public void openingCustomerDetailsAndTV14P001Order() throws Throwable{

		UtilityMethods.wait5Seconds();
		Actions action = new Actions(driver);
		action.contextClick(customerOrderNumberValue).perform();
		UtilityMethods.wait1Seconds();
		openSalesOrderDetails.click();

		UtilityMethods.wait7Seconds();
		clickSELLbuttonOnCustomerDetailPage.click();
		salesOrderButtonOnCustomerDetailPage.click();
		UtilityMethods.wait30Seconds();
		System.out.println("SALES ORDER NUMBER"+salesOrderNumber.getText().substring(0,10));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER :"+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
		itemNumberInputField.click();
		UtilityMethods.wait1Seconds();

		itemNumberInputField.sendKeys("TV14P0001");
		UtilityMethods.wait5Seconds();
		perpetualPremiumInputValue.click();

		UtilityMethods.wait10Seconds();


		String productNameinText = driver.findElement(By.xpath("//span[contains(@title,'TV14P0001')]")).getAttribute("title");
		System.out.println(productNameinText);
		if(productNameinText.contains("TV14P0001")){


			UtilityMethods.validateAssertEqual("Perpetual Premium 14 is Added", "Perpetual Premium 14 is Added");

		}
		else{

			UtilityMethods.validateAssertEqual("Perpetual Premium 14 is Added", "Perpetual Premium 14 is NOT Added");
		}

		System.out.println("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
	}

	//Open customer details and add a TV14P0001 Product
	public void openingCustomerDetailsAndTV14C0001Order() throws Throwable{

		UtilityMethods.wait5Seconds();
		Actions action = new Actions(driver);
		action.contextClick(customerOrderNumberValue).perform();
		UtilityMethods.wait1Seconds();
		openSalesOrderDetails.click();

		UtilityMethods.wait7Seconds();
		clickSELLbuttonOnCustomerDetailPage.click();
		salesOrderButtonOnCustomerDetailPage.click();
		UtilityMethods.wait30Seconds();
		System.out.println("SALES ORDER NUMBER"+salesOrderNumber.getText().substring(0,10));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER :"+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
		itemNumberInputField.click();
		UtilityMethods.wait1Seconds();

		itemNumberInputField.sendKeys("TV14C0001");
		UtilityMethods.wait5Seconds();
		perpetualCorporateInputValue.click();

		UtilityMethods.wait10Seconds();


		String productNameinText = driver.findElement(By.xpath("//span[contains(@title,'TV14C0001')]")).getAttribute("title");
		System.out.println(productNameinText);
		if(productNameinText.contains("TV14C0001")){


			UtilityMethods.validateAssertEqual("Perpetual Corporate 14 is Added", "Perpetual Corporate 14 is Added");

		}
		else{

			UtilityMethods.validateAssertEqual("Perpetual Corporate 14 is Added", "Perpetual Corporate 14 is NOT Added");
		}

		System.out.println("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
	}

	//Opening customer details and add a premium product
	public void openingCustomerDetailsAndPremiumOrder() throws Throwable{

		WebDriverWait wait = new WebDriverWait(driver,30);

		Actions action = new Actions(driver);
		action.contextClick(customerOrderNumberValue).perform();
		UtilityMethods.wait1Seconds();
		openSalesOrderDetails.click();

		wait.until(ExpectedConditions.visibilityOf(clickSELLbuttonOnCustomerDetailPage));
		UtilityMethods.wait2Seconds();
		clickSELLbuttonOnCustomerDetailPage.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait2Seconds();
		try{
			
			salesOrderButtonOnCustomerDetailPage.click();
		}
		catch(Exception e){
			
			salesOrderButtonOnCustomerDetailPage2.click();
		}
		
		wait.until(ExpectedConditions.visibilityOf(salesOrderNumber));

		salesOrderNumberOnOrder = salesOrderNumber.getText().substring(0,9); 
		System.out.println("SALES ORDER NUMBER "+salesOrderNumber.getText().substring(0,10));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER :"+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");

		itemNumberInputField.click();
		UtilityMethods.wait1Seconds();

		itemNumberInputField.sendKeys("tvp0001");
		UtilityMethods.wait3Seconds();
		premiumLicenseInputValue.click();

		UtilityMethods.wait10Seconds();
		String productNameinText = productNameOfFirstOrder.getAttribute("title");

		if(productNameinText.equals("TeamViewer Premium / TeamViewer Premium Yearly Subscription")){


			UtilityMethods.validateAssertEqual("Premium License is Added", "Premium License is Added");

		}
		else{

			UtilityMethods.validateAssertEqual("Premium License is Added", "Premium License is NOT Added");
		}

		System.out.println("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
	}

	//Open Customer Details and Add TVC0001 Product
	public void openingCustomerDetailsAndCorporateOrder() throws Throwable{

		UtilityMethods.wait5Seconds();
		Actions action = new Actions(driver);
		action.contextClick(customerOrderNumberValue).perform();
		UtilityMethods.wait1Seconds();
		openSalesOrderDetails.click();

		UtilityMethods.wait7Seconds();
		clickSELLbuttonOnCustomerDetailPage.click();
		salesOrderButtonOnCustomerDetailPage.click();
		UtilityMethods.wait30Seconds();

		System.out.println("SALES ORDER NUMBER"+salesOrderNumber.getText().substring(0,10));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER :"+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");

		itemNumberInputField.click();
		UtilityMethods.wait1Seconds();

		itemNumberInputField.sendKeys("tvc0001");
		UtilityMethods.wait3Seconds();
		corporateLicenseInputValue.click();

		UtilityMethods.wait10Seconds();
		String productNameinText = productNameOfFirstOrder.getAttribute("title");

		if(productNameinText.equals("TeamViewer Corporate / TeamViewer Corporate Yearly Subscription")){


			UtilityMethods.validateAssertEqual("Corporate License is Added", "Corporate License is Added");

		}
		else{

			UtilityMethods.validateAssertEqual("Corporate License is Added", "Corporate License is NOT Added");
		}

		System.out.println("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
	}

	//Switch from Business To Premium
	public void switchFromBusinessToPremium() throws Throwable{

		
		if(driver.findElements(By.xpath("//span[contains(text(),'Open Lasernet setup')]")).size()!=0){
			UtilityMethods.waitForPageLoadAndPageReady();
			
			driver.navigate().back();
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
		}
		
		switchButton.click();
		
		UtilityMethods.wait7Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();
		switchtoPremiumButton.click();
		UtilityMethods.wait5Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();
		okButtonOnSwitchProducts.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		try{
			
			System.out.println("SALES ORDER NUMBER :"+salesOrderNumberAfterSwitchingMigration2.getText().trim().substring(0,9));
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumberAfterSwitchingMigration2.getText().trim().substring(0,9)+"<b>");
		}
		catch(Exception e){
			System.out.println("SALES ORDER NUMBER :"+salesOrderNumberAfterSwitchingMigration.getText().trim().substring(0,9));
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumberAfterSwitchingMigration.getText().trim().substring(0,9)+"<b>");
		}


	}

	//Switch from Business to Corporate
	public void switchFromBusinessToCorporate() throws Throwable{


		switchButton.click();
		UtilityMethods.wait7Seconds();
		switchtoCorporateButton.click();
		UtilityMethods.wait5Seconds();
		okButtonOnSwitchProducts.click();
		UtilityMethods.wait30Seconds();
		System.out.println("SALES ORDER NUMBER :"+salesOrderNumberAfterSwitchingMigration.getText().substring(0,10));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumberAfterSwitchingMigration.getText().substring(0,10)+"<b>");


	}

	//Switch from Premium to Corporate
	public void switchFromPremiumToCorporate() throws Throwable{


		switchButton.click();
		UtilityMethods.wait7Seconds();
		switchtoCorporateButton.click();
		UtilityMethods.wait5Seconds();
		okButtonOnSwitchProducts.click();
		UtilityMethods.wait20Seconds();
		try{
			
			System.out.println("SALES ORDER NUMBER :"+salesOrderNumberAfterSwitchingMigration2.getText().trim().substring(0,9));
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumberAfterSwitchingMigration2.getText().trim().substring(0,9)+"<b>");
		}
		catch(Exception e){
			System.out.println("SALES ORDER NUMBER :"+salesOrderNumberAfterSwitchingMigration.getText().trim().substring(0,9));
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumberAfterSwitchingMigration.getText().trim().substring(0,9)+"<b>");
		}

	}


	//Upgarde from TV14B0001 to TV14P0001
	public void upgradeFromTV14B0001ToTV14P0001() throws Throwable{


		contractDropDownForTV14Products.click();
		UtilityMethods.wait3Seconds();
		upgradeButtonOnDropDownForTV14Products.click();
		UtilityMethods.wait7Seconds();
		TV14C0001Product.click();
		UtilityMethods.wait5Seconds();
		TV14C0001Product.click();
		UtilityMethods.wait5Seconds();
		TV14P0001Product.click();
		UtilityMethods.wait5Seconds();
		okButtonOnSwitchProducts.click();
		UtilityMethods.wait30Seconds();
		System.out.println("SALES ORDER NUMBER :"+salesOrderNumber.getText().substring(0,10));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
	}

	//Upgarde from TV14B0001 to TV14C0001
	public void upgradeFromTV14B0001ToTV14C0001() throws Throwable{


		contractDropDownForTV14Products.click();
		UtilityMethods.wait3Seconds();
		upgradeButtonOnDropDownForTV14Products.click();
		UtilityMethods.wait7Seconds();
		TV14C0001Product.click();
		UtilityMethods.wait5Seconds();
		okButtonOnSwitchProducts.click();
		UtilityMethods.wait30Seconds();
		System.out.println("SALES ORDER NUMBER :"+salesOrderNumber.getText().substring(0,10));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
	}

	//Upgarde from TV14B0001 to TV14C0001
	public void updateFromTV13B0001ToTV14B0001() throws Throwable{


		contractDropDownForTV14Products.click();
		UtilityMethods.wait3Seconds();
		updateButtonOnDropDownForTV14Products.click();
		UtilityMethods.wait7Seconds();
		okButtonOnSwitchProducts.click();
		System.out.println("USER IS UPDATED TO TV14B0001");
		UtilityMethods.wait30Seconds();
		System.out.println("SALES ORDER NUMBER :"+salesOrderNumber.getText().substring(0,10));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
	}

	//Upgarde from TV14P0001 to TV14C0001
	public void upgradeFromTV14P0001ToTV14C0001() throws Throwable{


		contractDropDownForTV14Products.click();
		UtilityMethods.wait3Seconds();
		upgradeButtonOnDropDownForTV14Products.click();
		UtilityMethods.wait7Seconds();
		TV14C0001Product.click();
		UtilityMethods.wait5Seconds();
		TV14C0001Product.click();
		UtilityMethods.wait5Seconds();
		okButtonOnSwitchProducts.click();
		UtilityMethods.wait30Seconds();
		System.out.println("SALES ORDER NUMBER :"+salesOrderNumber.getText().substring(0,10));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
	}


	//Migrate from TV14B0001 to TVB0001
	public void migrateFromTV14B0001ToTVB0001() throws Throwable{


		contractDropDownForTV14Products.click();
		UtilityMethods.wait3Seconds();
		migrateButtonOnDropDownForTV14Products.click();
		UtilityMethods.wait20Seconds();
		TVB0001ProductMigration.click();
		UtilityMethods.wait5Seconds();
		okButtonOnSwitchProducts.click();
		UtilityMethods.wait30Seconds();
		System.out.println("SALES ORDER NUMBER :"+salesOrderNumberAfterSwitchingMigration.getText().substring(0,10));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumberAfterSwitchingMigration.getText().substring(0,10)+"<b>");
	}
	
	//Migrate from TV14B0001 to TVB0001
		public void migrateFromTV14B0001ToTVP0001() throws Throwable{


			contractDropDownForTV14Products.click();
			UtilityMethods.wait3Seconds();
			migrateButtonOnDropDownForTV14Products.click();
			UtilityMethods.wait20Seconds();
			
			UtilityMethods.waitForPageLoadAndPageReady();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(itemNumber));
			UtilityMethods.wait5Seconds();
			new Actions(driver).moveToElement(itemNumber).click(itemNumber).build().perform();
			UtilityMethods.wait5Seconds();
			try{
				new Actions(driver).moveToElement(InvoiceVoucherFilterField).sendKeys("TVP0001").sendKeys(Keys.ENTER).build().perform();
			}
			catch(Exception e){
				new Actions(driver).moveToElement(InvoiceVoucherFilterField2).sendKeys("TVP0001").sendKeys(Keys.ENTER).build().perform();
			}
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait10Seconds();
			new Actions(driver).moveToElement(billingInterval).click(billingInterval).build().perform();
			UtilityMethods.wait5Seconds();
			new Actions(driver).moveToElement(billingFilterField).sendKeys("Y").sendKeys(Keys.ENTER).build().perform();

		
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
		
			okButtonOnSwitchProducts.click();
			UtilityMethods.wait20Seconds();
			try{
				
				System.out.println("SALES ORDER NUMBER :"+salesOrderNumberAfterSwitchingMigration2.getText().trim().substring(0,9));
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumberAfterSwitchingMigration2.getText().trim().substring(0,9)+"<b>");
			}
			catch(Exception e){
				System.out.println("SALES ORDER NUMBER :"+salesOrderNumberAfterSwitchingMigration.getText().trim().substring(0,9));
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumberAfterSwitchingMigration.getText().trim().substring(0,9)+"<b>");
			}
		}




	//Migrate from TV14P0001 to TVP0001
	public void migrateFromTV14P0001ToTVP0001() throws Throwable{


		contractDropDownForTV14Products.click();
		UtilityMethods.wait3Seconds();
		migrateButtonOnDropDownForTV14Products.click();
		UtilityMethods.wait20Seconds();

//		driver.findElement(By.xpath("(//div[contains(text(),'Item number')])[2]").click();
//		UtilityMethods.wait3Seconds();
//		driver.findElement(By.name("FilterField_MCRUpSellTableOE_ItemId_ItemId_Input_0")).sendKeys("TVP");
//		driver.findElement(By.name("FilterField_MCRUpSellTableOE_ItemId_ItemId_Input_0")).sendKeys(Keys.ENTER);
		
//		TVP0001ProductMigration.click();
		
		
		UtilityMethods.waitForPageLoadAndPageReady();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(itemNumber));
		UtilityMethods.wait5Seconds();
		new Actions(driver).moveToElement(itemNumber).click(itemNumber).build().perform();
		UtilityMethods.wait5Seconds();
		new Actions(driver).moveToElement(InvoiceVoucherFilterField).sendKeys("TVP0001").sendKeys(Keys.ENTER).build().perform();

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		new Actions(driver).moveToElement(billingInterval).click(billingInterval).build().perform();
		UtilityMethods.wait5Seconds();
		new Actions(driver).moveToElement(billingFilterField).sendKeys("Y").sendKeys(Keys.ENTER).build().perform();

	
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
	
		okButtonOnSwitchProducts.click();
		UtilityMethods.wait30Seconds();
		System.out.println("SALES ORDER NUMBER :"+salesOrderNumberAfterSwitchingMigration.getText().substring(0,10));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumberAfterSwitchingMigration.getText().substring(0,10)+"<b>");
	}
	
	//Migrate from TV14P0001 to TVP0001
		public void migrateFromTV14P0001ToTVC0001() throws Throwable{


			contractDropDownForTV14Products.click();
			UtilityMethods.wait3Seconds();
			migrateButtonOnDropDownForTV14Products.click();
			UtilityMethods.wait20Seconds();

//			driver.findElement(By.xpath("(//div[contains(text(),'Item number')])[2]").click();
//			UtilityMethods.wait3Seconds();
//			driver.findElement(By.name("FilterField_MCRUpSellTableOE_ItemId_ItemId_Input_0")).sendKeys("TVP");
//			driver.findElement(By.name("FilterField_MCRUpSellTableOE_ItemId_ItemId_Input_0")).sendKeys(Keys.ENTER);
			
//			TVP0001ProductMigration.click();
			
			
			UtilityMethods.waitForPageLoadAndPageReady();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(itemNumber));
			UtilityMethods.wait5Seconds();
			new Actions(driver).moveToElement(itemNumber).click(itemNumber).build().perform();
			UtilityMethods.wait5Seconds();
			new Actions(driver).moveToElement(InvoiceVoucherFilterField).sendKeys("TVC0001").sendKeys(Keys.ENTER).build().perform();

			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			new Actions(driver).moveToElement(billingInterval).click(billingInterval).build().perform();
			UtilityMethods.wait5Seconds();
			new Actions(driver).moveToElement(billingFilterField).sendKeys("Y").sendKeys(Keys.ENTER).build().perform();

		
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
		
			okButtonOnSwitchProducts.click();
			UtilityMethods.wait30Seconds();
			System.out.println("SALES ORDER NUMBER :"+salesOrderNumberAfterSwitchingMigration.getText().substring(0,10));
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumberAfterSwitchingMigration.getText().substring(0,10)+"<b>");
		}

	//Migrate from TV14C0001 to TVC0001
	public void migrateFromTV14C0001ToTVC0001() throws Throwable{


		contractDropDownForTV14Products.click();
		UtilityMethods.wait3Seconds();
		migrateButtonOnDropDownForTV14Products.click();
		UtilityMethods.wait15Seconds();
//		TVC0001ProductMigration.click();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		new Actions(driver).moveToElement(itemNumber).click(itemNumber).build().perform();
		UtilityMethods.wait3Seconds();
		new Actions(driver).moveToElement(InvoiceVoucherFilterField).sendKeys("TVC0001").sendKeys(Keys.ENTER).build().perform();

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		new Actions(driver).moveToElement(billingInterval).click(billingInterval).build().perform();
		UtilityMethods.wait3Seconds();
		new Actions(driver).moveToElement(billingFilterField).sendKeys("Y").sendKeys(Keys.ENTER).build().perform();

	
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		okButtonOnSwitchProducts.click();
		UtilityMethods.wait30Seconds();
		System.out.println("SALES ORDER NUMBER :"+salesOrderNumberAfterSwitchingMigration.getText().substring(0,10));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER NUMBER: "+"<b>"+salesOrderNumberAfterSwitchingMigration.getText().substring(0,10)+"<b>");
	}




	public void verifyDiscountOnCorporate() throws Throwable{


		priceInformation.click();
		UtilityMethods.wait3Seconds();
		lastLineDiscount = discountAmount.getAttribute("title");
		System.out.println("Discount Amount on Corporate is"+lastLineDiscount);

		lastLineAmount = finalAmount.getAttribute("title");
		System.out.println("Discount Amount on Corporate is"+lastLineAmount);


		try{

			if(periodicDis.isDisplayed()){

				System.out.println("PERIODIC DISCOUNT IS APPLIED TO CORPORATE LICENSE");
			}

		}
		catch(Exception e){

			System.out.println("PERIODIC DISCOUNT IS NOT AVAILABLE FOR CORPORATE LICENSE");

		}

		try{

			if(manualDis.isDisplayed()){

				System.out.println("MANUAL DISCOUNT IS APPLIED TO CORPORATE LICENSE");
			}

		}
		catch(Exception e){

			System.out.println("MANUAL DISCOUNT IS NOT APPLIED ON SWITCHING");

		}





	}




	//Complete the Order
	public void completeTheOrder() throws Throwable{
		

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		String paymentMethods;
		try{

			UtilityMethods.clickElemetJavaSciptExecutor(completeButtonForSimpletOrder);
			System.out.println("USER HAS CLICKED ON COMPLETE BUTTON");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CLICKED ON COMPLETE BUTTON");
			UtilityMethods.waitForPageLoadAndPageReady();
		//	new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOf( driver.findElement(By.name("TMVActiveTenderTypeId"))));
			UtilityMethods.wait20Seconds();
			paymentMethods = driver.findElement(By.name("TMVActiveTenderTypeId")).getAttribute("title");

		}
		catch(Exception e){

			UtilityMethods.clickElemetJavaSciptExecutor(completeButtonForSimpletOrder2);
			System.out.println("USER HAS CLICKED ON COMPLETE BUTTON");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CLICKED ON COMPLETE BUTTON");
			UtilityMethods.waitForPageLoadAndPageReady();
		//	new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOf( driver.findElement(By.name("TMVActiveTenderTypeId"))));
			UtilityMethods.wait10Seconds();
			paymentMethods = driver.findElement(By.name("TMVActiveTenderTypeId")).getAttribute("title");
		}
	
		if(paymentMethods.contains("100")){
			System.out.println("USER HAS COMPLETED THE ORDER");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS COMPLETED THE ORDER IN ERP");
			new Actions(driver).moveToElement(submitButtonOnSaleOrderSummary).click().perform();
			
		}
		else if(driver.findElements(By.name("TMVActiveTenderTypeId")).size()!=0){

				try{
					Actions action = new Actions(driver);
					action.moveToElement(PaymentExpander).click();
					UtilityMethods.wait5Seconds();
					addPaymentMethodButton.click();
					UtilityMethods.wait5Seconds();
				}

				catch(Exception e){

					payments.click();
					System.out.println("PAYMENT EXPANDER IS NOT AVAILABLE");
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("PAYMENT EXPANDER IS NOT AVAILABLE");
					UtilityMethods.wait3Seconds();
					try{
						addPaymentMethodButtonForSwitchedOrder.click();
						UtilityMethods.wait5Seconds();
					}
					catch(Exception n){
						payments.click();
						UtilityMethods.wait3Seconds();
						addPaymentMethodButtonForSwitchedOrder.click();
						UtilityMethods.wait5Seconds();
					}
				
				}

				enterPaymentMethodCode.sendKeys("100");
				enterPaymentMethodCode.sendKeys(Keys.TAB);

				UtilityMethods.wait3Seconds();

				UtilityMethods.clickElemetJavaSciptExecutor(okButtonOnCustomerInformation);
				UtilityMethods.wait5Seconds();
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,2000)");

				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", submitButtonOnSaleOrderSummary);

				UtilityMethods.wait30Seconds();
				System.out.println("USER HAS COMPLETED THE ORDER");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER HAS COMPLETED THE ORDER IN ERP");
				
				

			}

	

			try{
			System.out.println("SALES ORDER NUMBER :"+salesOrderNumber2.getText());
			salesOrderNumberCRMtoERP = salesOrderNumber2.getText().trim();
			salesOrderNumberCRMtoERP = salesOrderNumberCRMtoERP.substring(0, 9);
			System.out.println("SALES ORDER NUMBER :"+salesOrderNumberCRMtoERP);
			Reporter.addStepLog("SALES ORDER NUMBER :"+salesOrderNumberCRMtoERP);
//			driver.findElement(By.xpath("//button[contains(@id,'SalesOrder_button')]")).click();
//			System.out.println("CLICKING ON SALES ORDER BUTTON");

		}

		catch(Exception e){
			
			System.out.println("SALES ORDER NUMBER :"+salesOrderNumber.getText());
			salesOrderNumberCRMtoERP = salesOrderNumber.getText().trim();
			salesOrderNumberCRMtoERP = salesOrderNumberCRMtoERP.substring(0, 9);
			System.out.println("SALES ORDER NUMBER :"+salesOrderNumberCRMtoERP);
			Reporter.addStepLog("SALES ORDER NUMBER :"+salesOrderNumberCRMtoERP);
//			driver.findElement(By.xpath("//button[contains(@id,'SalesOrder_button')]")).click();
//			System.out.println("CLICKING ON SALES ORDER BUTTON");


		}
		

	}

	public void completeTheOrderForCreditCardPayment() throws Throwable{

		UtilityMethods.wait5Seconds();
		try{

			completeButtonForSimpletOrder.click();
			System.out.println("USER HAS CLICKED ON COMPLETE BUTTON");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CLICKED ON COMPLETE BUTTON");

		}
		catch(Exception e){

			completeButtonForSwitchedOrder.click();
			System.out.println("USER HAS CLICKED ON COMPLETE BUTTON");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CLICKED ON COMPLETE BUTTON");
		}
		UtilityMethods.wait20Seconds();

		try{

			if(masterVsActivate.isDisplayed()){

				try{
					Actions action = new Actions(driver);
					action.moveToElement(PaymentExpander).click();
					UtilityMethods.wait5Seconds();
					addPaymentMethodButton.click();
					UtilityMethods.wait5Seconds();
				}

				catch(Exception e){

					System.out.println("PAYMENT EXPANDER IS NOT AVAILABLE");
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("PAYMENT EXPANDER IS NOT AVAILABLE");
					UtilityMethods.wait3Seconds();
					addPaymentMethodButtonForSwitchedOrder.click();
					UtilityMethods.wait5Seconds();
				}

				enterPaymentMethodCode.sendKeys("500");
				enterPaymentMethodCode.sendKeys(Keys.TAB);

				UtilityMethods.wait1Seconds();

				enterCreditCardName.sendKeys("visa");
				enterCreditCardName.sendKeys(Keys.TAB);
				UtilityMethods.wait1Seconds();

				enterNameOfCardHolder.sendKeys("Nal Test");
				UtilityMethods.wait5Seconds();
				visaWireCardOption.click();
				UtilityMethods.wait2Seconds();

				okButtonOnCustomerInformation.click();
				UtilityMethods.wait5Seconds();
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("window.scrollBy(0,2000)");

				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", submitButtonOnSaleOrderSummary);


				try{
					UtilityMethods.wait120Seconds();
					if(creditCardAccepted.isDisplayed()){

						UtilityMethods.validateAssertEqual("CARD ACCEPTED", "CARD ACCEPTED");
						System.out.println("ORDER HAS BEEN COMPLETED WITH CREDIT CARD PAYMENT");
					}
				}

				catch(Exception g){

					System.out.println("ORDER HAS NOT COMPLETED WITH CREDIT CARD PAYMENT");
					UtilityMethods.validateAssertEqual("CARD ACCEPTED", "CARD NOT ACCEPTED");

				}

			}

		}
		catch(Exception e){

			UtilityMethods.wait120Seconds();
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("window.scrollBy(0,2000)");
			executor.executeScript("arguments[0].click();", submitButtonOnSaleOrderSummary);

		}

		try{
			UtilityMethods.wait10Seconds();
			driver.findElement(By.xpath("//button[contains(@id,'SalesOrder_button')]")).click();
			System.out.println("CLICKING ON SALES ORDER BUTTON");

		}

		catch(Exception e){


		}

	}


	//Searching for the Order ID
	public void searchOrderInERPViaChannelFilter() throws Throwable{
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(channelIdFilterIcon));
		channelIdFilterIcon.click();
		UtilityMethods.wait5Seconds(); 
		String finalOrderID  = SystemEmailLogin.finalOrderID;
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENETERING THE ORDER ID");
		System.out.println("Order ID on D365 :"+finalOrderID);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("ID OF THIS PLACED ORDER on D365 :"+"<b>"+finalOrderID+"</b>");

		try{

			if(channelIdFilterField.isDisplayed()){

				channelIdFilterField.sendKeys("61STG000001934");
				channelIdFilterField.sendKeys(finalOrderID);
			}
		}

		catch(Exception e){

			System.out.println("Channel Filter ID is not Displayed at ALL");
		}

		UtilityMethods.wait3Seconds();

		applyButtonOnChannelFilter.click();
	}



	//GETTING ITEM NUMBER

	public void verifyPurchasesInERP() throws Throwable{

		UtilityMethods.wait5Seconds();
		Actions action = new Actions(driver);
		
		int index = salesOrderValue.getAttribute("title").indexOf("C");
		salesOrderNumberOnOrder = salesOrderValue.getAttribute("title").substring(0, index).trim();
		System.out.println(salesOrderNumberOnOrder);
		action.contextClick(salesOrderValue).perform();
		UtilityMethods.wait3Seconds();
		openSalesOrderDetails.click();

		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES ORDER IS OPENING");



		UtilityMethods.wait15Seconds();	

		System.out.println("***************************************************");
		System.out.println();
		System.out.println();
		List<WebElement> itemNumberList = driver.findElements(By.xpath("//*[@name = 'SalesLine_ItemId']"));

		for(WebElement singleAddon : itemNumberList){

			String productCodes = singleAddon.getAttribute("title");
			//System.out.println(productCodes);

			try{

				if(productCodes.contains(CheckOutPage.subscriptionProductCode)){

					UtilityMethods.validateAssertEqual("TVP0001 are added", "TVP0001 are added");
					System.out.println("LICENSE IS ADDED :"+CheckOutPage.subscriptionProductCode);
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("LICENSE IS ADDED :"+CheckOutPage.subscriptionProductCode);
				}

			}
			catch(Exception e){

				//System.out.println("SUBSCRIPTION PRODUCT IS NOT SELECTED");
			}

			try{

				if(productCodes.contains(AdOnPage.additionalConcurrentUserCode) ){

					//if(!addOnCode.equals("") && addOnCode.contains("TVAD0001") ){
					UtilityMethods.validateAssertEqual("TVAD001 are added", "TVAD001 are added");
					System.out.println("ADDITIONAL CONCURRENT USERS ARE ADDED :"+AdOnPage.additionalConcurrentUserCode);
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("ADDITIONAL CONCURRENT USERS ARE ADDED :"+AdOnPage.additionalConcurrentUserCode);
				}
			}
			catch(Exception e){

				//System.out.println("ADDON PRODUCT IS NOT SELECTED");
			}


			try{
				if(productCodes.contains(AdOnPage.mobileSupportCode) ){
					//	if(!addOnCode.equals("") && addOnCode.contains("TVAD003") ){
					UtilityMethods.validateAssertEqual("TVAD003 are added", "TVAD003 are added");
					System.out.println("MOBILE DEVICES ARE ADDED :"+AdOnPage.mobileSupportCode);
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("SUPPORT FOR MOBILE DEVICES ARE ADDED :"+AdOnPage.mobileSupportCode);
				}
			}
			catch(Exception e){

				//System.out.println("MOBILE SUPPORT DEVICE IS NOT ADDED");
			}




			try{
				if(productCodes.contains(AdOnPage.teamViewerMonitoringCode) ){

					UtilityMethods.validateAssertEqual("TeamViewer Monitoring Code are added", "TeamViewer Monitoring Code are added");
					System.out.println("TEAMVIEWER MONITORING ARE ADDED ON D365");
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("TEAMVIEWER MONITORING ARE ADDED ON D365");
				}
			}
			catch(Exception e){

				//	System.out.println("TEAMVIEWER MONITORING IS NOT ADDED");
			}


			try{
				if(productCodes.contains(AdOnPage.teamviewerEndPointCode) ){

					UtilityMethods.validateAssertEqual("TeamViewer Endpoint code are added", "TeamViewer Endpoint code are added");
					System.out.println("TEAMVIEWER ENDPOINTS ARE ADDED ON D365");
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("TEAMVIEWER ENDPOINTS ARE ADDED ON D365");
				}

			}
			catch(Exception e){

				//System.out.println("TEAMVIEWER ENDPOINTS IS NOT ADDED");
			}

			try{
				if(productCodes.contains(AdOnPage.teamviewerBackupCode) ){

					UtilityMethods.validateAssertEqual("Teamviewer Backup are added", "Teamviewer Backup are added");
					System.out.println("TEAMVIEWER BACKUP ARE ADDED ON D365");
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("TEAMVIEWER BACKUP ARE ADDED ON D365");
				}

			}
			catch(Exception e){

				//System.out.println("TEAMVIEWER BACKUP IS NOT ADDED");
			}

			try{

				if(productCodes.contains(AdOnPage.teamViewerPilotCode) ){

					UtilityMethods.validateAssertEqual("Teamviewer Pilot are added", "Teamviewer Pilot are added");
					System.out.println("TEAMVIEWER PILOT ARE ADDED ON D365");
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("TEAMVIEWER PILOT ARE ADDED ON D365");
				}

			}
			catch(Exception e){

				//System.out.println("TEAMVIEWER PILOT IS NOT ADDED");
			}
			
			

		}
		

	}

	public void verifyVisaCardInERP() throws Throwable{
		
		String visa = driver.findElement(By.name("TMVCreditCardId")).getAttribute("title");
		System.out.println(visa);
		if(visa.equalsIgnoreCase(visaCard)){
			
			System.out.println("Card type matched");
			
		}
		else{
			
			UtilityMethods.validateAssertEqual("VISA CARD FOUND", "VISA CARD NOT FOUND");
		}

		
	}
	
	public void verifyAmericanCardInERP() throws Throwable{
		
		String american = driver.findElement(By.name("TMVCreditCardId")).getAttribute("title");
		System.out.println(american);
		if(american.equalsIgnoreCase(americanCard)){
			
			System.out.println("Card type matched");
			
		}
		else{
			
			UtilityMethods.validateAssertEqual("AMERICA CARD FOUND", "AMERICA CARD NOT FOUND");
		}

		
	}
	
	public void verifyMasterCardInERP() throws Throwable{
		
		String master = driver.findElement(By.name("TMVCreditCardId")).getAttribute("title");
		System.out.println(master);
		if(master.equalsIgnoreCase(masterCard)){
			
			System.out.println("Card type matched");
			
		}
		else{
			
			UtilityMethods.validateAssertEqual("MASTER CARD FOUND", "MASTER CARD NOT FOUND");
		}

		
	}
	
	//GETTING QUANTITY
	public void verifyQuantityOfPurchasedProducts() throws Throwable{

		List<WebElement> itemQuantityList = driver.findElements(By.xpath("//*[@name='SalesLine_SalesQty']"));


		for(WebElement quantity : itemQuantityList){

			String getProductsQuantity = quantity.getAttribute("title");
			System.out.println(getProductsQuantity);


			try{
				if(getProductsQuantity.contains("1.00")){

					UtilityMethods.validateAssertEqual("PREMIUM COUNT IS 1", "PREMIUM COUNT IS 1");
					System.out.println("LICENSE COUNT IS : 1");
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("LICENSE COUNT IS : 1");
				}
			}
			catch(Exception e){}


			try{


				if(getProductsQuantity.contains(CheckOutPage.quantityOfAdditionalUsers) ){
					//if(!addOnCode.equals("") && addOnCode.contains("TVAD0001") ){
					UtilityMethods.validateAssertEqual("QUANTITY OF ADDITIONAL USER MATCHED", "QUANTITY OF ADDITIONAL USER MATCHED");
					System.out.println("ADDITIONAL CONCURRENT USERS COUNT IS :"+CheckOutPage.quantityOfAdditionalUsers);
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("ADDITIONAL CONCURRENT USERS COUNT IS :"+CheckOutPage.quantityOfAdditionalUsers);
				}
			}
			catch(Exception e){


			}

			try{

				if(getProductsQuantity.contains(CheckOutPage.quantityOfMobileSupporters) ){
					//	if(!addOnCode.equals("") && addOnCode.contains("TVAD003") ){
					UtilityMethods.validateAssertEqual("QUANTITY OF MOBILE SUPPORT DEVICES MATCHED", "QUANTITY OF MOBILE SUPPORT DEVICES MATCHED");
					System.out.println("MOBILE DEVICES COUNT IS :"+CheckOutPage.quantityOfMobileSupporters);
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("MOBILE DEVICES COUNT IS :"+CheckOutPage.quantityOfMobileSupporters);
				}
			}
			catch(Exception e){

				//System.out.println("MOBILE SUPPORT COUNT IS 0");
			}


			try{
				if(getProductsQuantity.contains(CheckOutPage.quantityOfTVMonitoring) ){

					UtilityMethods.validateAssertEqual("TeamViewer Monitoring Code are added", "TeamViewer Monitoring Code are added");
					System.out.println("TEAMVIEWER MONITORING COUNT IS "+CheckOutPage.quantityOfTVMonitoring);
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("TEAMVIEWER MONITORING COUNT IS "+CheckOutPage.quantityOfTVMonitoring);
				}
			}

			catch(Exception e){

				//System.out.println("TEAMVIEWER MONITORING COUNT IS 0");
			}



			try{
				if(getProductsQuantity.contains(CheckOutPage.quantityOfTVEndpoint) ){

					UtilityMethods.validateAssertEqual("TeamViewer Endpoint code are added", "TeamViewer Endpoint code are added");
					System.out.println("TEAMVIEWER ENDPOINTS COUNT IS "+CheckOutPage.quantityOfTVEndpoint);
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("TEAMVIEWER ENDPOINTS COUNT IS "+CheckOutPage.quantityOfTVEndpoint);
				}
			}
			catch(Exception e){


			}

			try{
				if(getProductsQuantity.contains(CheckOutPage.quantityOfTVBackup) ){

					UtilityMethods.validateAssertEqual("Teamviewer Backup are added", "Teamviewer Backup are added");
					System.out.println("TEAMVIEWER BACKUP COUNT IS "+CheckOutPage.quantityOfTVBackup);
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("TEAMVIEWER BACKUP COUNT IS "+CheckOutPage.quantityOfTVBackup);
				}
			}catch(Exception e){

				//System.out.println("COUNT OF TEAMVIEWER BACKUP IS 0");
			}

			try{

				if(getProductsQuantity.contains(CheckOutPage.quantityOfTVPilot) ){

					UtilityMethods.validateAssertEqual("Teamviewer Pilot are added", "Teamviewer Pilot are added");
					System.out.println("TEAMVIEWER PILOT COUNT IS "+CheckOutPage.quantityOfTVPilot);
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("TEAMVIEWER PILOT COUNT IS "+CheckOutPage.quantityOfTVPilot);
				}
			}
			catch(Exception e){

				//System.out.println("COUNT OF TEAMVIEWER BACKUP IS 0");

			}

		}

	}

	//GETTING PRICES
	public void verifyAmountOfPurchasedProducts() throws Throwable{

		List<WebElement> itemPriceInERP = driver.findElements(By.xpath("//*[@name = 'SalesLine_TMVTargetAmount']"));


		for(WebElement price : itemPriceInERP){

			//HERE I AM REMOVING DOTS AND COMMAS TO COMPARE THE VALUES OF ECOM AND ERP
			String getProductAmount =removeDotsandCommas(price.getAttribute("title"));
			System.out.println(getProductAmount);

			try
			{
				if(getProductAmount.contains(CheckOutPage.priceOfTheSelectedLicense)){

					UtilityMethods.validateAssertEqual("PREMIUM PRICE MATCHED", "PREMIUM PRICE MATCHED");
					System.out.println("LICENSE PRICE IS "+getProductAmount);
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("LICENSE PRICE IS "+getProductAmount);;
				}
			}
			catch(Exception e){

			}

			try{
				if(getProductAmount.contains(CheckOutPage.priceOfAdditionalUsers) ){
					//if(!addOnCode.equals("") && addOnCode.contains("TVAD0001") ){
					UtilityMethods.validateAssertEqual("PRICE OF ADDITIONAL USER MATCHED", "PRICE OF ADDITIONAL USER MATCHED");
					System.out.println("ADDITIONAL CONCURRENT USERS PRICE IS :"+getProductAmount);
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("ADDITIONAL CONCURRENT USERS PRICE IS :"+getProductAmount);

				}
			}
			catch(Exception e){}


			try{
				if(getProductAmount.contains(CheckOutPage.priceOfMobileSupport) ){
					//	if(!addOnCode.equals("") && addOnCode.contains("TVAD003") ){
					UtilityMethods.validateAssertEqual("PRICE OF MOBILE SUPPORT DEVICES MATCHED", "PRICE OF MOBILE SUPPORT DEVICES MATCHED");
					System.out.println("MOBILE DEVICES PRICE IS :"+getProductAmount);
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("MOBILE DEVICES PRICE IS :"+getProductAmount);
				}
			}
			catch(Exception e){}

			try{
				
			if(getProductAmount.contains(CheckOutPage.priceOfTVMonitoring) ){

				UtilityMethods.validateAssertEqual("TeamViewer Monitoring Code are added", "TeamViewer Monitoring Code are added");
				System.out.println("TEAMVIEWER MONITORING PRICE IS "+CheckOutPage.priceOfTVMonitoring);
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("TEAMVIEWER MONITORING PRICE IS "+CheckOutPage.priceOfTVMonitoring);
			}
			}
			catch(Exception e){
				
				
			}

			try{
			if(getProductAmount.contains(CheckOutPage.priceOfTVEndpoint) ){

				UtilityMethods.validateAssertEqual("TeamViewer Endpoint code are added", "TeamViewer Endpoint code are added");
				System.out.println("TEAMVIEWER ENDPOINTS PRICE IS "+CheckOutPage.priceOfTVEndpoint);
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("TEAMVIEWER ENDPOINTS PRICE IS "+CheckOutPage.priceOfTVEndpoint);
			}
			}
			catch(Exception e){
				
			}
			
			
			try{
			if(getProductAmount.contains(CheckOutPage.priceOfTVBackUp) ){

				UtilityMethods.validateAssertEqual("Teamviewer Backup are added", "Teamviewer Backup are added");
				System.out.println("TEAMVIEWER BACKUP PRICE IS "+CheckOutPage.priceOfTVBackUp);
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("TEAMVIEWER BACKUP PRICE IS "+CheckOutPage.priceOfTVBackUp);
			}
			}
			catch(Exception e){
				
			}
			
			try{
			if(getProductAmount.contains(CheckOutPage.priceOfTVPilot) ){

				UtilityMethods.validateAssertEqual("Teamviewer Pilot are added", "Teamviewer Pilot are added");
				System.out.println("TEAMVIEWER PILOT PRICE IS "+CheckOutPage.priceOfTVPilot);
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("TEAMVIEWER PILOT PRICE IS "+CheckOutPage.priceOfTVPilot);
			}
			}
			catch(Exception e){
				
				
			}

		}

	}

	public void checkOrderIsCompleted() throws Throwable{



	}

	public void paymentStatusOnHeaderBar() throws Throwable{


		paymentLink.click();
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.visibilityOf(payMethodCodeOnPaymentStatus));

		String paymentCodeOnERP = payMethodCodeOnPaymentStatus.getAttribute("title");
		if(paymentCodeOnERP.contains(ConfirmationPage.paymentCodeOfOrder)){

			System.out.println("PAYMENT CODE IS :"+paymentCodeOnERP);
		}

		else{

			UtilityMethods.validateAssertEqual("PAYMENT METHOD MATCHED", "PAYMENT METHOD NOT MATCHED");
		}



		String paymentamntOnERP = totalPaymentAmountInPaymentStatus.getAttribute("title");
		paymentamntOnERP = removeDotsandCommas(paymentamntOnERP);

		if(paymentamntOnERP.contains(CheckOutPage.orderTotalValueForERP)){
			System.out.println("PAYMENT AMOUNT IS "+paymentamntOnERP);

		}
		else{

			UtilityMethods.validateAssertEqual("PAYMENT METHOD MATCHED", "PAYMENT METHOD NOT MATCHED");
		}


	}
	public void verifyCustomerInformationInGeneralTab() throws Throwable{

		try{
		headerBarForCustomerPersonalInformation.click();
		}
		catch(NoSuchElementException e){
			
			headerBarForCustomerPersonalInformation2.click();
		}

		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.visibilityOf(EcomSaleOrderNumber));

		int index = EcomSaleOrderNumber.getAttribute("title").indexOf("C");
		String ecomSaleOrder = EcomSaleOrderNumber.getAttribute("title").substring(0, index).trim();

		System.out.println("SALE ORDER NUMBER IS "+ecomSaleOrder);
		Reporter.addStepLog("SALE ORDER NUMBER IS "+ecomSaleOrder);

		try{
		int custIndex = customerAccountNumber.getAttribute("title").indexOf("Click");
		cusAccontNur = customerAccountNumber.getAttribute("title").substring(0,custIndex).trim();
		}
		catch(Exception e){
			int custIndex = customerAccountNumber2.getAttribute("title").indexOf("Click");
			cusAccontNur = customerAccountNumber2.getAttribute("title").substring(0,custIndex).trim();
		}
		
		System.out.println("CUSTOMER ERP ACCOUNT IS "+cusAccontNur);
		Reporter.addStepLog("CUSTOMER ERP ACCOUNT IS "+cusAccontNur);
		int index1 = customerEmailID.getAttribute("title").indexOf("Click");
		String cusEmailID = customerEmailID.getAttribute("title").substring(0, index1).trim();
		
	if(cusEmailID.contains("teamviewer.automation")){
		
		System.out.println("Verify In General Tab Customer email = "+cusEmailID+"  Web order email: teamviewer.automation");
		Reporter.addStepLog("VERIFY IN GENERAL TAB CUSTOMER EMAIL = EXPECTED EMAIL"+cusEmailID+"  \nWEB ORDER EMAIL: teamviewer.automation@systemsltd.com");
	}
	
	else{
		System.out.println("ORDER IS NOT FOUND IN ERP ");
		Reporter.addStepLog("VERIFY IN GENERAL TAB CUSTOMER EMAIL: \nEXPECTED EMAIL "+cusEmailID+" IS NOT EQUAL TO \nWEB ORDER EMAIL: teamviewer.automation");
		UtilityMethods.validateAssertEqual("CORRECT EMAIL", "NOT CORRECT EMAIL");
	}
	
	

		System.out.println("CUSTOMER EMAIL ID IS "+cusEmailID);
		String cusAddress;


		try{
			if(customerAddress.isDisplayed()){

				cusAddress = customerAddress.getAttribute("title").trim();
				System.out.println("CUSTOMER ADDRESS IS "+cusAddress);
				Reporter.addStepLog("CUSTOMER ADDRESS IS: "+cusAddress);
			}
		}

		catch(Exception e){

			addressDetailsExpander.click();

			WebDriverWait wait1 = new WebDriverWait(driver,20);
			wait1.until(ExpectedConditions.visibilityOf(customerAddress));
			cusAddress = customerAddress.getAttribute("title");
			System.out.println("CUSTOMER ADDRESS IS "+cusAddress);
			Reporter.addStepLog("CUSTOMER ADDRESS IS "+cusAddress);
		}




	}

	public void verifyCustomerInformationInSetUp() throws Throwable{


	//	try{
		
			if(driver.findElements(By.xpath("//input[contains(@id,'TaxGroup_input')]")).size()==0){
				
				setup.click();
			}
			
				UtilityMethods.wait5Seconds();
				customerSalesTaxGroup = salesTaxGroupInSetUpTab.getAttribute("title");
				int indexOfClick = customerSalesTaxGroup.indexOf("Click");
				customerSalesTaxGroup = customerSalesTaxGroup.substring(0,indexOfClick).trim();
				if(customerSalesTaxGroup.contains("C-NL-STA")){
				System.out.println("SalesTaxGroup "+customerSalesTaxGroup+" matched");
				Reporter.addStepLog("SALESTAXGROUP "+customerSalesTaxGroup+" MATCHED");
				}
				
				String taxExemptValue = taxExempt.getAttribute("title");
				if(taxExemptValue!=null){
				
				System.out.println("TAX EXEMPT IS NULL"+taxExemptValue);
				Reporter.addStepLog("TAX EXEMPT IS NULL"+taxExemptValue);
				}
				else{
					
					System.out.println("TAX EXEMPT IS NULL"+taxExemptValue);
					Reporter.addStepLog("TAX EXEMPT IS NULL"+taxExemptValue);
				}
				
				String currentURL = "https://tv-d365fo-ben-01.sandbox.operations.dynamics.com/";
				((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
				ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs.get(1));
				driver.navigate().to(currentURL);
				
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait7Seconds();
				
				UtilityMethods.clickElemetJavaSciptExecutor(sideNavigationPanelButton);
		//		new Actions(driver).moveToElement(sideNavigationPanelButton).click(sideNavigationPanelButton).build().perform();
	//			sideNavigationPanelButton.click();
				
				UtilityMethods.waitForPageLoadAndPageReady();
				
				UtilityMethods.scrollToElement(tax);
				tax.click();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait5Seconds();
				new Actions(driver).moveToElement(indirectTaxes).click(indirectTaxes).build().perform();
		//		indirectTaxes.click();
				try{
					UtilityMethods.waitForPageLoadAndPageReady();
					UtilityMethods.wait3Seconds();
					salesTax.click();
				}
				catch(Exception e){
					indirectTaxes.click();
					UtilityMethods.wait2Seconds();
					UtilityMethods.waitForPageLoadAndPageReady();
					salesTax.click();
				}
				
				
				UtilityMethods.wait2Seconds();
				UtilityMethods.waitForPageLoadAndPageReady();
				try{
					
					salesTaxGroup.click();
				}
				catch(Exception e){
					salesTax.click();
					UtilityMethods.wait2Seconds();
					UtilityMethods.waitForPageLoadAndPageReady();
					
					salesTaxGroup.click();
				}
				
				
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait7Seconds();
				
				inputFieldToEnterSalesTaxCode.clear();
				inputFieldToEnterSalesTaxCode.sendKeys(customerSalesTaxGroup);
				inputFieldToEnterSalesTaxCode.sendKeys(Keys.ENTER);
				UtilityMethods.waitForPageLoadAndPageReady();
				
				new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(setup));
				if(driver.findElements(By.xpath("//input[contains(@id,'TaxGroupData_TaxCode_input')]")).size()==0){
					setup.click();
				}
				else if(salesTaxCodeElement.isDisplayed())
				salesTaxCode = salesTaxCodeElement.getAttribute("title");
				int indexOfClick1 = salesTaxCode.indexOf("C");
				salesTaxCode = salesTaxCode.substring(0,indexOfClick1);
				
//				Actions action2 = new Actions(driver);
//				action2.sendKeys(Keys.ESCAPE).build().perform();
//				
//				UtilityMethods.wait7Seconds();
//				
//				Actions action3 = new Actions(driver);
//				action3.sendKeys(Keys.ESCAPE).build().perform();
//				UtilityMethods.wait7Seconds();
//				//Now get the value of language
//				
//				driver.navigate().to(currentURL);
//				UtilityMethods.waitForPageLoadAndPageReady();
//				UtilityMethods.wait5Seconds();
//				
				driver.close();
				driver.switchTo().window(tabs.get(0));
			
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait5Seconds();
				
				try{
				
					UtilityMethods.scrollToElement(languageOfStoreInSetUpTab);
					new Actions(driver).moveToElement(languageOfStoreInSetUpTab).contextClick().build().perform();
					System.out.println("STORE LANGUAGE IS "+languageOfStoreInSetUpTab.getCssValue("value"));
				//	int indexOfClick3 = languageOfStoreInSetUpTab.getAttribute("oldtitle").indexOf("Click");
				//	System.out.println(indexOfClick3);
					languageOfStore = languageOfStoreInSetUpTab.getCssValue("value");
					
				//	System.out.println("STORE LANGUAGE IS "+languageOfStore);
					
		//			languageOfStore = languageOfStore.substring(0, indexOfClick3);
					System.out.println("STORE LANGUAGE IS "+languageOfStore);
					if(languageOfStoreInSetUpTab.getCssValue("value")!=null){
						Reporter.addStepLog("STORE LANGUAGE IS "+languageOfStore);
					}
				}
				catch(Exception e){
					
					setup.click();
					if(driver.findElements(By.name("Administration_LanguageId")).size()!=0){
					UtilityMethods.scrollToElement(languageOfStoreInSetUpTab);
					new Actions(driver).moveToElement(languageOfStoreInSetUpTab).contextClick().build().perform();
					System.out.println("STORE LANGUAGE IS "+languageOfStoreInSetUpTab.getCssValue("value"));
				//	int indexOfClick3 = languageOfStoreInSetUpTab.getAttribute("oldtitle").indexOf("Click");
				//	System.out.println(indexOfClick3);
					languageOfStore = languageOfStoreInSetUpTab.getCssValue("value");
					
				//	System.out.println("STORE LANGUAGE IS "+languageOfStore);
					
		//			languageOfStore = languageOfStore.substring(0, indexOfClick3);
					System.out.println("STORE LANGUAGE IS "+languageOfStore);
					if(languageOfStoreInSetUpTab.getCssValue("value")!=null){
						Reporter.addStepLog("STORE LANGUAGE IS "+languageOfStore);
					}
					}
					else{
						setup.click();
						
						UtilityMethods.scrollToElement(languageOfStoreInSetUpTab);
						new Actions(driver).moveToElement(languageOfStoreInSetUpTab).contextClick().build().perform();
						System.out.println("STORE LANGUAGE IS "+languageOfStoreInSetUpTab.getCssValue("value"));
					//	int indexOfClick3 = languageOfStoreInSetUpTab.getAttribute("oldtitle").indexOf("Click");
					//	System.out.println(indexOfClick3);
						languageOfStore = languageOfStoreInSetUpTab.getCssValue("value");
						
					//	System.out.println("STORE LANGUAGE IS "+languageOfStore);
						
			//			languageOfStore = languageOfStore.substring(0, indexOfClick3);
						System.out.println("STORE LANGUAGE IS "+languageOfStore);
						if(languageOfStoreInSetUpTab.getCssValue("value")!=null){
							Reporter.addStepLog("STORE LANGUAGE IS "+languageOfStore);
						}
					}
					
				}
				
				int indexOfClick4 = salesWebOrigin.getAttribute("title").indexOf("C");
				String salesWebOriginValue = salesWebOrigin.getAttribute("title").substring(0,indexOfClick4).trim();
				System.out.println("SALES WEB ORGIN IS: "+salesWebOriginValue);
				Reporter.addStepLog("SALES WEB ORGIN IS: "+salesWebOriginValue);
				
				UtilityMethods.moveToElementWithJSExecutor(storeName);
		//		UtilityMethods.scrollToElement(storeName);
				String storeNameValue = storeName.getAttribute("title");
				System.out.println("STORE NAME IS "+storeNameValue);
				Reporter.addStepLog("STORE NAME IS "+storeNameValue);
				
			
	//	}

	//	catch(Exception e){
			
	//		System.out.println("Problem in customer information SetUP tab");
			
			
//			UtilityMethods.wait3Seconds();
//			
////			JavascriptExecutor executor = (JavascriptExecutor)driver;
////			executor.executeScript("arguments[0].click();", setUpExpander);
//			setUpExpander.click();
//			
//			UtilityMethods.wait5Seconds();
//			customerSalesTaxGroup = salesTaxGroupInSetUpTab.getAttribute("title");
//			int indexOfClick = customerSalesTaxGroup.indexOf("Click");
//			customerSalesTaxGroup = customerSalesTaxGroup.substring(0,indexOfClick);
//			
//			
//			System.out.println("SalesTaxGroup "+customerSalesTaxGroup+ "FOUND FOR THIS CUSTOMER");
//			Reporter.addStepLog("SalesTaxGroup "+customerSalesTaxGroup+ "FOUND FOR THIS CUSTOMER");
//			
//			
//			Actions action = new Actions(driver);
//			action.contextClick(salesTaxGroupInSetUpTab).perform();
//			UtilityMethods.wait3Seconds();
//			viewDetailsForSalesTaxGroup.click();
//			
//			//Here Get The Value of Sales Tax Code against Sales Tax Group
//			
//			UtilityMethods.wait5Seconds();
//			salesTaxCode = salesTaxCodeElement.getAttribute("title");
//			int indexOfClick1 = salesTaxCode.indexOf("Click");
//			salesTaxCode = salesTaxCode.substring(0,indexOfClick1 );
//			
//			System.out.println("SalesTaxCode "+salesTaxCode + "FOUND FOR THIS CUSTOMER");
//			
//			Reporter.addStepLog("SalesTaxCode "+salesTaxCode + "FOUND FOR THIS CUSTOMER");
//			
//			Actions action2 = new Actions(driver);
//			action2.sendKeys(Keys.ESCAPE).build().perform();
//			UtilityMethods.wait5Seconds();
//			Actions action3 = new Actions(driver);
//			action3.sendKeys(Keys.ESCAPE).build().perform();
//			
//			UtilityMethods.wait5Seconds();
//			
//			languageOfStore = languageOfStoreInSetUpTab.getAttribute("title");
//			System.out.println("STORE LANGUAGE IS "+languageOfStore);
//			Reporter.addStepLog("STORE LANGUAGE IS "+languageOfStore);
//		}


	}

	public void verifyCustomerInformationInPriceAndDiscountTab() throws Throwable{




		try{
			if(currencyOfTheCountry.isDisplayed()){

				currencyOfTheCountryy = currencyOfTheCountry.getAttribute("title").substring(0, currencyOfTheCountry.getAttribute("title").indexOf("C"));
				System.out.println("CUSTOMER CURRENCY IS "+currencyOfTheCountryy);
				Reporter.addStepLog("CUSTOMER CURRENCY IS "+currencyOfTheCountryy);

			}
		}

		catch(Exception e){

			priceAndDiscountExpander.click();

			WebDriverWait wait1 = new WebDriverWait(driver,20);
			wait1.until(ExpectedConditions.visibilityOf(currencyOfTheCountry));
			currencyOfTheCountryy = currencyOfTheCountry.getAttribute("title");
			System.out.println("CUSTOMER CURRENCY IS "+currencyOfTheCountryy);
			Reporter.addStepLog("CUSTOMER CURRENCY IS "+currencyOfTheCountryy);

		}

	}

	public void getNumberOfMainAccountAndSalesTax() throws Throwable{
		
		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		UtilityMethods.wait3Seconds();
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		UtilityMethods.wait2Seconds();
		driver.navigate().to("https://tv-d365fo-ben-01.sandbox.operations.dynamics.com/?t=2019-07-31T11%3A59%3A21.9684554Z&cmp=TVDE&mi=InventPosting");
		UtilityMethods.wait7Seconds();
		
		WebDriverWait wait1 = new WebDriverWait(driver,20);
		wait1.until(ExpectedConditions.visibilityOf(revenueRadioButton));
		revenueRadioButton.click();
		
		UtilityMethods.wait3Seconds();
		salesTaxGroupDropdown.click();
		
		UtilityMethods.wait3Seconds();
		try{

			if(channelIdFilterField.isDisplayed()){

				channelIdFilterField.sendKeys(customerSalesTaxGroup); 
			}

		}


		catch(Exception e){

			System.out.println("Account Filter Field is not Displayed at ALL");
		}

		UtilityMethods.wait1Seconds();

		try{
		applyButtonOnChannelFilter.click();
		}
		
		catch(Exception e){
			
			
		}

		UtilityMethods.wait3Seconds();
		
		mainAccountNumber = mainAccountElement.getAttribute("value");
		System.out.println("Main Account Number :"+mainAccountNumber);
		Reporter.addStepLog("MAIN ACCOUNT NUMBER IS VISIBLE AGAINST SALES TAX GROUP IS "+customerSalesTaxGroup+" AND VALUE IS :"+mainAccountNumber);
		
		//Open WebPage Ledger Posting To Get the Sales Tax Payable Number
		driver.get("https://tv-d365fo-ben-01.sandbox.operations.dynamics.com/?t=2019-07-31T11%3A59%3A21.9684554Z&cmp=TVDE&mi=TaxAccountGroup");
		WebDriverWait wait = new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.visibilityOf(inputFieldToEnterSalesTaxCode));
		inputFieldToEnterSalesTaxCode.sendKeys(salesTaxCode);
		inputFieldToEnterSalesTaxCode.sendKeys(Keys.ENTER);
		UtilityMethods.wait3Seconds();
		salesTaxPayAblNumber = salesTaxPayAbleElement.getAttribute("value");
		Reporter.addStepLog("SALES TAX PAYABLE AGAINST LEDGER POSTING GROUP IS VISIBLE ON LEDGER POSTING GROUPS PAGE WITH VALUE: "+salesTaxPayAblNumber);
		System.out.println("SALES TAX PAYABLE IS "+salesTaxPayAblNumber);
		UtilityMethods.wait3Seconds();
		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		driver.switchTo().window(tabs1.get(0));
		UtilityMethods.wait2Seconds();
		
		
	}
	public void verifyCustomerInformationInFinancialDimensionsTab() throws Throwable{


		String channelName;
		String Worker;


		try{
			if(retailChannelName.isDisplayed()){

				channelName = retailChannelName.getAttribute("title");
				System.out.println("RETAIL CHANNEL NAME IS "+channelName);
				Reporter.addStepLog("RETAIL CHANNEL IS "+channelName);
				Worker = retailChannelWorker.getAttribute("title");
				System.out.println("CHANNEL WORKER IS "+Worker);
				Reporter.addStepLog("CHANNEL WORKER IS "+Worker);

			}
		}

		catch(Exception e){

			financialDimensionsExpander.click();

			WebDriverWait wait1 = new WebDriverWait(driver,20);
			wait1.until(ExpectedConditions.visibilityOf(retailChannelName));
			channelName = retailChannelName.getAttribute("title");
			System.out.println("RETAIL CHANNEL NAME IS "+channelName);
			Reporter.addStepLog("RETAIL CHANNEL NAME IS "+channelName);
			Worker = retailChannelWorker.getAttribute("title");
			System.out.println("CHANNEL WORKER IS "+Worker);
			Reporter.addStepLog("CHANNEL WORKER IS "+Worker);

		}

	}


	public String removeDotsandCommas(String text){
		String finalString1 = "";
		String finalString ="";
		UtilityMethods.waitForPageLoadAndPageReady();

		finalString1 = text.replace(",", "");
		finalString = finalString1.replace(".","");
		return finalString;


	}

	public String replaceDotsWithCommas(String text){
		String finalString = "";
		UtilityMethods.waitForPageLoadAndPageReady();
		finalString = text.replace(".", ",");
		return finalString;


	}

	public void getInvoiceNumber() throws Throwable{

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		if(driver.findElements(By.xpath("//span[contains(text(),'Open Lasernet setup')]")).size()!=0){
			UtilityMethods.waitForPageLoadAndPageReady();
			
			driver.navigate().back();
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		inVoiceButtonUnderJournalForLasernetPrint.click();
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.visibilityOf(inVoiceNumber));

		try{

			UtilityMethods.validateAssertEqual("INVOICE NUMBER FOUND", "INVOICE NUMBER FOUND");
			if(inVoiceNumber.isDisplayed()){

				inVoiceNo = inVoiceNumber.getAttribute("title").substring(0,10);
				System.out.println("Invoice Number is :"+inVoiceNo);

				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("Invoice Number is :"+inVoiceNo);
			}

		}
		catch(Exception e){

			System.out.println("SYSTEM CAN NOT GET INVOICE NUMBER");
			UtilityMethods.validateAssertEqual("INVOICE NUMBER FOUND", "INVOICE NUMBER NOT FOUND");
		}

	}

	public void checkDiscountOnInvoiceForCorporate() throws Throwable{

		UtilityMethods.waitForPageLoadAndPageReady();
		System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
		inVoiceButtonUnderJournalForCorporateDiscount.click();
		UtilityMethods.wait10Seconds();

		inVoiceNo = inVoiceNumber.getAttribute("title").substring(0,10);
		System.out.println("Invoice Number is :"+inVoiceNo);

		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("Invoice Number is :"+inVoiceNo);


		System.out.println("USER HAS SELECTED LASERNET ORIGINAL PREVIEW BUTTON");
		lasernetPrintdropdown.click();
		UtilityMethods.wait5Seconds();

		lasernetOriginalPreviewButton.click();
		System.out.println("LASERNET ORIGINAL PREVIEW DOWNLOADED SUCCESSFULLY");
		UtilityMethods.wait30Seconds();


	}


	public void lasernetPrintOpenAndResendTheInvoice() throws Throwable{

		try{
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();
			System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
			inVoiceButtonUnderJournalForLasernetPrint.click();
			UtilityMethods.wait10Seconds();
		}
		catch(Exception e){
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait20Seconds();
			driver.navigate().back();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();
			System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
			inVoiceButtonUnderJournalForLasernetPrint.click();
			UtilityMethods.wait10Seconds();
			
			
		}
		UtilityMethods.wait5Seconds();
		System.out.println("USER HAS CLICKED ON LASERNET DROPDOWN");
		lasernetPrintdropdown.click();
		UtilityMethods.wait5Seconds();


		reSendButton.click();
		System.out.println("USER HAS CLICKED ON RESEND BUTTON TO RESEND THE INVOICE");
		UtilityMethods.wait5Seconds();

		forceReRun.click();

		UtilityMethods.wait3Seconds();
		okButtonForConfirmTerminating.click();

		System.out.println("INVOICE PDF IS BEING DOWNLOADED");
		UtilityMethods.wait30Seconds();

	}
	//Go to Lasernet Printer and Download it
	public void lasernetPrintOpen() throws Throwable{

	//	System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");



		inVoiceNo = inVoiceNumber.getAttribute("title").substring(0,10);
		System.out.println("Invoice Number is :"+inVoiceNo);

		UtilityMethods.wait10Seconds();
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("INVOICE NUMBER IS :"+inVoiceNo);


		UtilityMethods.wait3Seconds();
		System.out.println("USER HAS SELECTED LASERNET ORIGINAL PREVIEW BUTTON");
		lasernetPrintdropdown.click();
		Reporter.addStepLog("USER HAS SELECTED LASERNET ORIGINAL PREVIEW BUTTON");
		UtilityMethods.wait7Seconds();
		
		new WebDriverWait(driver,30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'Lasernet Original preview')]")));
		lasernetOriginalPreviewButton.click();
		System.out.println("LASERNET ORIGINAL PREVIEW DOWNLOADED SUCCESSFULLY");
		Reporter.addStepLog("LASERNET ORIGINAL PREVIEW DOWNLOADED SUCCESSFULLY");
		UtilityMethods.wait30Seconds();
		
		new Actions(driver).moveToElement(invoiceVoucherButton).click(invoiceVoucherButton).build().perform();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		
	//	UtilityMethods.moveToElementWithJSExecutor(invoiceVoucherValue);
	//	invoiceVoucher = invoiceVoucherValue.getAttribute("title");
	//	System.out.println(invoiceVoucher);
		

	}

	public void correctTheInvoiceForDeliveryAddress() throws Throwable{

		System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
		inVoiceButtonUnderJournalForLasernetPrint.click();
		UtilityMethods.wait10Seconds();

		invoiceCorrectionButtonOnMainHeader.click();
		UtilityMethods.wait5Seconds();

		correctInvoiceButton.click();
		UtilityMethods.wait5Seconds();
		yesButton.click();

		UtilityMethods.wait5Seconds();


		//Changing Delivery Address
		Actions action = new Actions(driver);
		action.doubleClick(customerAddressFieldForCorrectInvoice).build().perform();


		Random r = new Random();
		String address = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		for(int i = 0; i < 20 ; i++){


			customerDeliveryAddress +=address.charAt(r.nextInt(address.length()));
		}

		customerAddressFieldForCorrectInvoice.sendKeys(customerDeliveryAddress);
		System.out.println("NEW CUSTOMER ADDRESS : "+customerDeliveryAddress);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("NEW CUSTOMER ADDRESS : "+customerDeliveryAddress);

		UtilityMethods.wait5Seconds();



		okButtonForCorrectInvoice.click();
		UtilityMethods.wait3Seconds();
		yesButton.click();
		UtilityMethods.wait5Seconds();

		Actions action2 = new Actions(driver);
		action2.sendKeys(Keys.ESCAPE).build().perform();

		UtilityMethods.wait3Seconds();

		Actions action3 = new Actions(driver);
		action3.sendKeys(Keys.ESCAPE).build().perform();

	}


	public void correctTheInvoiceForChangeLanguage() throws Throwable{

		System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
		
	

		try{
			invoiceCorrectionButtonOnMainHeader.click();
			UtilityMethods.wait10Seconds();
		}
		catch(Exception e){
			driver.navigate().back();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();	
			UtilityMethods.clickElemetJavaSciptExecutor(invoiceCorrectionButtonOnMainHeader);
			UtilityMethods.wait5Seconds();
			correctInvoiceButton.click();
			
		}
//		UtilityMethods.wait5Seconds();
//
//		correctInvoiceButton.click();
		UtilityMethods.wait5Seconds();
		yesButton.click();

		UtilityMethods.wait5Seconds();

		currentLanguageOfInvoice = currentLanguage.getAttribute("title");

		try{

			if(currentLanguageOfInvoice.contains("nl")){
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("CURRENT INVOICE LANGUAGE IS ENGLISH");
				System.out.println("CURRENT INVOICE LANGUAGE IS ENGLISH");
				Actions action = new Actions(driver);
				action.doubleClick(languageInputFieldForInvoiceCorrection).build().perform();

				//Enter English Language
				languageInputFieldForInvoiceCorrection.clear();
				languageInputFieldForInvoiceCorrection.sendKeys("de");

				UtilityMethods.wait2Seconds();
				
				
				languageInputFieldForInvoiceCorrection.sendKeys(Keys.TAB);


				UtilityMethods.wait5Seconds();

				System.out.println("USER HAS CHANGED INVOICE TO EN-US LANGUAGE");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER HAS SUCCESSFULLY CHANGED INVOICE TO EN-US LANGUAGE");
			}


		}
		catch(Exception e){
			try{

				if(currentLanguageOfInvoice.contains("en-US")){

					System.out.println("CURRENT INVOICE LANGUAGE IS US-ENGLISH");
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("CURRENT INVOICE LANGUAGE IS US-ENGLISH");
					Actions action = new Actions(driver);
					action.doubleClick(languageInputFieldForInvoiceCorrection).build().perform();

					//Enter English Language
					languageInputFieldForInvoiceCorrection.sendKeys("de");

					UtilityMethods.wait2Seconds();
					languageInputFieldForInvoiceCorrection.sendKeys(Keys.TAB);


					UtilityMethods.wait3Seconds();

					languageOfInvoiceAfterChange = languageInputFieldForInvoiceCorrection.getAttribute("title");



					System.out.println("USER HAS SUCCESSFULLY CHANGED INVOICE TO GERMAN LANGUAGE");
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("USER HAS SUCCESSFULLY CHANGED INVOICE TO GERMAN LANGUAGE");


				}

			}
			catch(Exception i){



			}

		}



		UtilityMethods.wait5Seconds();



		okButtonForCorrectInvoice.click();
		UtilityMethods.wait3Seconds();
		yesButton.click();
		UtilityMethods.wait5Seconds();

		Actions action2 = new Actions(driver);
		action2.sendKeys(Keys.ESCAPE).build().perform();

		UtilityMethods.wait3Seconds();

//		Actions action3 = new Actions(driver);
//		action3.sendKeys(Keys.ESCAPE).build().perform();

	}
	public void correctTheInvoiceForInvoiceAddress() throws Throwable{

		System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
		inVoiceButtonUnderJournalForLasernetPrint.click();
		UtilityMethods.wait10Seconds();

		invoiceCorrectionButtonOnMainHeader.click();
		UtilityMethods.wait5Seconds();

		correctInvoiceButton.click();
		UtilityMethods.wait5Seconds();
		yesButton.click();

		UtilityMethods.wait5Seconds();


		//Changing Delivery Address
		Actions action = new Actions(driver);
		action.doubleClick(invoiceAddressFieldForCorrectInvoice).build().perform();


		Random r = new Random();
		String address = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		for(int i = 0; i < 20 ; i++){


			customerDeliveryAddress +=address.charAt(r.nextInt(address.length()));
		}

		invoiceAddressFieldForCorrectInvoice.sendKeys(customerDeliveryAddress);
		System.out.println("NEW CUSTOMER ADDRESS : "+customerDeliveryAddress);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("NEW CUSTOMER ADDRESS : "+customerDeliveryAddress);

		UtilityMethods.wait5Seconds();

		okButtonForCorrectInvoice.click();
		UtilityMethods.wait3Seconds();
		yesButton.click();
		UtilityMethods.wait5Seconds();

		Actions action2 = new Actions(driver);
		action2.sendKeys(Keys.ESCAPE).build().perform();

		UtilityMethods.wait3Seconds();

		Actions action3 = new Actions(driver);
		action3.sendKeys(Keys.ESCAPE).build().perform();	

	}

	public void correctTheInvoiceFordueDate() throws Throwable{

//		System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
//		UtilityMethods.wait5Seconds();
//		new Actions(driver).moveToElement(inVoiceButtonUnderJournalForLasernetPrint).click().build().perform();
//	//	inVoiceButtonUnderJournalForLasernetPrint.click();
//		UtilityMethods.wait15Seconds();

		
		UtilityMethods.clickElemetJavaSciptExecutor(invoiceCorrectionButtonOnMainHeader);
		UtilityMethods.wait5Seconds();
		try{
		correctInvoiceButton.click();
		}
		catch(Exception e){
			driver.navigate().back();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();	
			UtilityMethods.clickElemetJavaSciptExecutor(invoiceCorrectionButtonOnMainHeader);
			UtilityMethods.wait5Seconds();
			correctInvoiceButton.click();
		}
		UtilityMethods.wait5Seconds();
		yesButton.click();

		UtilityMethods.wait10Seconds();


		//Changing Delivery Address



		//date = "30/10/2019";
		
		Date dt = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(dt); 
		c.add(Calendar.DATE, 18);
		dt = c.getTime();
		
		Format date = new SimpleDateFormat("MM/dd/yyyy");
		String currentDate = date.format(dt);                    //CANNOT BE IMPLEMENTED
		System.out.println("Updated Date = "+currentDate);
		
		calenderIcon.click();
		calenderIcon.clear();
		calenderIcon.sendKeys(currentDate);
		UtilityMethods.wait3Seconds();

//		dateIs31st.click();

		UtilityMethods.wait5Seconds();

		okButtonForCorrectInvoice.click();
		UtilityMethods.wait3Seconds();
		yesButton.click();
		UtilityMethods.wait5Seconds();

		Actions action2 = new Actions(driver);
		action2.sendKeys(Keys.ESCAPE).build().perform();

		UtilityMethods.wait3Seconds();

		System.out.println("NEW DUE DATE IS "+newDueDate.getAttribute("title"));
		Actions action3 = new Actions(driver);
		action3.sendKeys(Keys.ESCAPE).build().perform();





	}


	public void openVoucherAndCheckSubTotal() throws Throwable{

		UtilityMethods.wait30Seconds();
		System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
		inVoiceButtonUnderJournalForLasernetPrint.click();
		UtilityMethods.wait10Seconds();

		//Validate Voucher balance is correct 
		System.out.println("USER IS VALIDATING THE VOUCHER CALCULATIONS");

		String customerBalance = driver.findElement(By.xpath("//input[contains(@id,'CustInvoiceJour_InvoiceAmount_Grid_input')]")).getAttribute("title");
		//Customer Balance in ERP comes in Dots

		if(customerBalance.contains(subTotalAmountInD365)){

			System.out.println("VAT NUMBER HAS BEEN APPLIED SUCCESSFULLY");
			UtilityMethods.validateAssertEqual("Equal", "Equal");
			System.out.println(customerBalance +" = "+"-"+subTotalAmountInD365);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("Customer Balance :"+customerBalance +" = "+"SubTotal Amount :"+"-"+subTotalAmountInD365);

		}
		else{

			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("Customer Balance :"+customerBalance +" != "+"SubTotal Amount :"+subTotalAmountInD365);
			UtilityMethods.validateAssertEqual("Equal", "Not Equal");
		}



	}

	
	public void openVoucherAndVerifyTheSalesTaxAndSalesOrderRevenue() throws Throwable{
		
		
	//	System.out.println("USER IS OPENING THE VOUCHER FOR THAT ORDER");
	//	new Actions(driver).moveToElement(voucherButtonOnInvoiceInJournal).click(voucherButtonOnInvoiceInJournal).build().perform();
		UtilityMethods.wait10Seconds();
		
		try{
			
			if(driver.findElement(By.xpath("//input[contains(@id , 'LedgerTrans_AccountNum_input')]")).getAttribute("title").contains(salesTaxPayAblNumber)){
				
				
				System.out.println("SALES TAX PAYABLE NUMBER IS MATCHED WITH POSTING TYPE : SALES TAX ");
				
			}
		}
			catch(Exception e){
				
				System.out.println("SALES TAX PAYBALE NUMBER IS NOT MATCHED WITH POSTING TYPE : SALES TAX");
				
			}
		
		UtilityMethods.wait5Seconds();
		
		
	try{
			
			if(driver.findElement(By.xpath("(//input[@name = 'LedgerTrans_AccountNum'])[1]")).getAttribute("title").contains(mainAccountNumber)){
				
				
				System.out.println("MAIN ACCOUNT NUMBER IS MATCHED WITH POSTING TYPE : SALES ORDER REVENUE ");
				
			}
		}
			catch(Exception e){
				
				System.out.println("MAIN ACCOUNT NUMBER IS NOT MATCHED WITH POSTING TYPE : SALES ORDER REVENUE");
				
			}
		
		
	}
	
	//Go to Lasernet Printer and See voucher for -ve value of invoice total
	public void lasernetPrintOpenAndVerifyCancelContract() throws Throwable{

		UtilityMethods.wait30Seconds();
		System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
		inVoiceButtonUnderJournalForLasernetPrint.click();
		UtilityMethods.wait10Seconds();


		System.out.println("USER IS OPENING THE VOUCHER FOR THAT ORDER");
		voucherButtonOnInvoiceInJournal.click();
		UtilityMethods.wait5Seconds();

		//Validate Voucher balance is correct 
		System.out.println("USER IS VALIDATING THE VOUCHER CALCULATIONS");

		String customerBalance = driver.findElement(By.xpath("(//input[@name = 'LedgerTrans_AmountCur'])[2]")).getAttribute("title");
		//Customer Balance in ERP comes in Dots

		if(customerBalance.contains("-"+invoiceTotal)){

			System.out.println("CREDITNOTE FOR CANCELLATION IS SUCCESSFULL");
			UtilityMethods.validateAssertEqual("Equal", "Equal");
			System.out.println(customerBalance +" = "+"-"+invoiceTotal);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("Customer Balance :"+customerBalance +" = "+"Invoice Total :"+"-"+invoiceTotal);

		}
		else{

			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("Customer Balance :"+customerBalance +" != "+"Invoice Total :"+invoiceTotal);
			UtilityMethods.validateAssertEqual("Equal", "Not Equal");
		}
	}

	public void checkCustomerSettlementForCreditCard() throws Throwable{

		String invoiceNumber = inVoiceNumber.getAttribute("title").substring(0,9);
		System.out.println("Invoice Number is : "+invoiceNumber);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("Invoice ID :"+invoiceNumber);

		driver.navigate().to("https://tv-d365fo-ben-01.sandbox.operations.dynamics.com/?cmp=TVDE&mi=CustTableListPage");
		UtilityMethods.wait20Seconds();
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(customerAccountFilterIcon));
		customerAccountFilterIcon.click();
		UtilityMethods.wait5Seconds();



		try{

			if(channelIdFilterField.isDisplayed()){


			
				channelIdFilterField.sendKeys("20209951");  // customer created through AX directly
				
			}

		}


		catch(Exception e){

			System.out.println("Account Filter Field is not Displayed at ALL");
		}

		UtilityMethods.wait1Seconds();


		applyButtonOnChannelFilter.click();

		UtilityMethods.wait3Seconds();


		if(driver.findElement(By.xpath("//input[contains(@id,'CustTable_AccountNum_input')]")).getAttribute("title").contains("20209951")){


			UtilityMethods.validateAssertEqual("CUSTOMER FOUND", "CUSTOMER FOUND");
			System.out.println("VALID CUSTOMER HAS BEEN FOUND");
		}

		else{
			System.out.println("VALID CUSTOMER IS NOT FOUND");
			UtilityMethods.validateAssertEqual("CUSTOMER FOUND", "CUSTOMER NOT FOUND");

		}

		customerOnHeaderBar.click();
		UtilityMethods.wait3Seconds();


		TransactionsButton.click();
		UtilityMethods.wait10Seconds();

		voucherColumnToSearchiNVOICEnUMBER.click();
		UtilityMethods.wait3Seconds();
		try{

			if(InvoiceVoucherFilterField.isDisplayed()){
				InvoiceVoucherFilterField.sendKeys(invoiceNumber);
				UtilityMethods.wait1Seconds();
			}

		}


		catch(Exception e){

			System.out.println("Account Filter Field is not Displayed at ALL");
		}

		UtilityMethods.wait1Seconds();


		applyButtonOnInvoiceFilter.click();
		UtilityMethods.wait5Seconds();
		viewSettlementsOnHeaderBar.click();

		UtilityMethods.wait5Seconds();

		try{


			if(transactionTypeIsPayment.isDisplayed() && transactionTypeIsSaleOrder.isDisplayed()){


				UtilityMethods.validateAssertEqual("SETTLED", "SETTLED");
				System.out.println("INVOICE HAS BEEN SETTLED SUCCESSFULLY");
			}
		}
		catch(Exception e){

			System.out.println("INVOICE HAS NOT SETTLED");
			UtilityMethods.validateAssertEqual("SETTLED", "NOT SETTLED");

		}

		System.out.println(settlementID.getAttribute("title"));
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("Settlement ID :"+settlementID.getAttribute("title"));

	}

	public void openJournalToCheckPaidStatus() throws Throwable{


		System.out.println("USER HAS CLICKED ON INVOICE BUTTON UNDER JOURNAL");
		UtilityMethods.wait15Seconds();
		try{

			UtilityMethods.clickElemetJavaSciptExecutor(inVoiceButtonUnderJournalForLasernetPrint);
			UtilityMethods.wait7Seconds();

		}

		catch(Exception e){

			System.out.println("COULDN'T FIND THE INVOICE BUTTON");
		}
	//	inVoiceButtonUnderJournalForLasernetPrint.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();

		String paymentStatus = driver.findElement(By.xpath("//input[contains(@id,'CustInvoiceJour_TMVPaymentStatus_input')]")).getAttribute("title");
		//Customer Balance in ERP comes in Dots

		if(paymentStatus.contains("Paid")){

			System.out.println("CREDIT CARD PAYMENT IS SUCCESSFUL");
			UtilityMethods.validateAssertEqual("Equal", "Equal");
			System.out.println(paymentStatus +" = "+paymentStatus);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("PAYMENT STATUS :"+paymentStatus);

		}
		else{

			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("PAYMENT STATUS :"+paymentStatus);
			UtilityMethods.validateAssertEqual("Equal", "Not Equal");
		}


		String currencyName = driver.findElement(By.xpath("//input[contains(@id,'CustInvoiceJour_CurrencyCode_Grid_input')]")).getAttribute("title");

		if(currencyName.contains(currencyOfTheCountryy)){
			System.out.println(currencyName + " = "+currencyOfTheCountryy);
			UtilityMethods.validateAssertEqual("Equal", "Equal");

		}
		else{
			System.out.println(currencyName + " != "+currencyOfTheCountryy);
			UtilityMethods.validateAssertEqual("Currency Equal", "Currency Not Equal");

		}


		String invoiceAmount = driver.findElement(By.xpath("//input[@name='CustInvoiceJour_InvoiceAmount_Grid']")).getAttribute("title");
		if(invoiceAmount.contains(invoiceTotal)){
			System.out.println(invoiceAmount + " = "+invoiceTotal);
			Reporter.addStepLog("PAYMENT AMOUNT :"+invoiceAmount+ " Total amount on invoice: "+invoiceTotal);
			UtilityMethods.validateAssertEqual("Invoice Total Equal", "Invoice Total Equal");

		}


	}

	public void lasernetPrintOpenAndVerifyCancelContractForSwitchedOrder() throws Throwable{

		UtilityMethods.wait30Seconds();

		inVoiceButtonUnderJournalForLasernetPrintOfSwitchedOrder.click();
		System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
		UtilityMethods.wait10Seconds();


		System.out.println("USER IS OPENING THE VOUCHER FOR THAT ORDER");
		voucherButtonOnInvoiceInJournal.click();
		UtilityMethods.wait5Seconds();

		//Validate Voucher balance is correct 
		System.out.println("USER IS VALIDATING THE VOUCHER CALCULATIONS");

		String customerBalance = driver.findElement(By.xpath("(//input[contains(@name,'LedgerTrans_AmountCur')])[2]")).getAttribute("title");
		//Customer Balance in ERP comes in Dots
		System.out.println("INVOICE TOTAL AT THIS STAGE : "+invoiceTotal);
		if(customerBalance.contains("-"+invoiceTotal)){

			System.out.println("CREDITNOTE FOR CANCELLATION OF SWITCHED ORDER IS SUCCESSFULL");
			UtilityMethods.validateAssertEqual("Equal", "Equal");
			System.out.println(customerBalance +" = "+"-"+invoiceTotal);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("Customer Balance :"+customerBalance +" = "+"Invoice Total :"+"-"+invoiceTotal);

		}
		else{

			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("Customer Balance :"+customerBalance +" != "+"Invoice Total :"+invoiceTotal);
			UtilityMethods.validateAssertEqual("Equal", "Not Equal");
		}



	}

	//Go to Lasernet Printer and Download it
	public void lasernetPrintOpenForContractCorrection() throws Throwable{

		System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
		inVoiceButtonUnderJournalForLasernetPrint.click();
		UtilityMethods.wait10Seconds();

		//Select the row for which invoice you want to generate

		driver.findElement(By.xpath("(//input[contains(@name,'InvoiceAmount_Grid')])[3]")).click();
		UtilityMethods.wait3Seconds();

		System.out.println("USER HAS SELECTED LASERNET ORIGINAL PREVIEW BUTTON");
		lasernetPrintdropdown.click();
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS SELECTED LASERNET ORIGINAL PREVIEW BUTTON");
		UtilityMethods.wait5Seconds();

//		lasernetOriginalPreviewButton.click();
		System.out.println("USER HAS SELECTED LASERNET ORIGINAL PREVIEW BUTTON");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS SELECTED LASERNET ORIGINAL PREVIEW BUTTON");
		UtilityMethods.wait30Seconds();

	}




	//Verify the values of lasernet printer on contract correction
	public void verifyTaxAndTotalOnlasernetPrinterForContractCorrection() throws Throwable{

		Optional<Path> lastFilePath = null;

		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;
		
//		PDPage pdfpage = new PDPage();
		
	//	List annotations = null;
	//	String urls = "";
    	 


		try {

			Path dir = Paths.get("C:\\Users\\naveed.khan\\Downloads");  // specify your directory
			
			lastFilePath = Files.list(dir)    // here we get the stream with full directory listing
					.filter(f -> !Files.isDirectory(f))  // exclude subdirectories from listing
					.max(Comparator.comparingLong(f -> f.toFile().lastModified()));  // finally get the last file using simple comparator by lastModified field

			if ( lastFilePath.isPresent() ) // your folder may be empty
			{
				System.out.println("This is requiered file"+lastFilePath.toString());

			}

		}
		catch(Exception e){


		}

		try{

			File file = new File(lastFilePath.toString().replace("\\", "\\\\").substring(9,92));
			String fileName = file.getName();
			System.out.println("This is also required: "+fileName);

			String filepath = "C:\\Users\\naveed.khan\\Downloads"+fileName+".PDF";

			//delete this line
			//String filepath = "C:\\Users\\naveed.khan\\Downloads\\SalesInvoice_TV_Replacement_1359_20190614_140607.PDF";
			//**************************************************************************************
			PDFParser parser = new PDFParser(new FileInputStream(new File(filepath)));


			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(1);

			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
		}

		catch (MalformedURLException e2) {
			System.err.println("URL string could not be parsed "+e2.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open PDF Parser. " + e.getMessage());
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}

		System.out.println("+++++++++++++++++");
		System.out.println(parsedText);
		System.out.println("+++++++++++++++++");


		if(parsedText.contains(",")){

			parsedText = parsedText.replace(",",".");
		}
		UtilityMethods.wait5Seconds();


		try{

			if(parsedText.contains(subTotalInD365AfterContractCorrect) && parsedText.contains(TaxAfterContractCorrectioninString) &&  parsedText.contains(finalTotalSuminTotalInStringContract)) {
				UtilityMethods.validateAssertEqual("Matched", "Matched");
				System.out.println( "TAX AMOUNT AFTER CONTRACT CORRECTION IS: " +TaxAfterContractCorrectioninString+ "& " + "SUBTOTAL AMOUNT AFTER CONTRACT CORRECTION IS: "+subTotalInD365AfterContractCorrect +"INVOICE AMOUNT AFTER CONTRACT CORRECTION IS : "+finalTotalSuminTotalInStringContract);
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("TAX AMOUNT AFTER CONTRACT CORRECTION IS: " +TaxAfterContractCorrectioninString+ "& " + "SUBTOTAL AMOUNT AFTER CONTRACT CORRECTION IS: "+subTotalInD365AfterContractCorrect +"INVOICE AMOUNT AFTER CONTRACT CORRECTION IS : "+finalTotalSuminTotalInStringContract);
			}
		}

		catch(Exception f){

			System.out.println("TAX, TOTAL AND SUBTOTAL ARE NOT EQUAL IN LASERNET PRINTER INVOICE FOR CONTRACT CORRECTION");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("TAX, TOTAL AND SUBTOTAL ARE NOT EQUAL IN LASERNET PRINTER INVOICE FOR CONTRACT CORRECTION");
			UtilityMethods.validateAssertEqual("Matched", "Not Matched");
		}

		//cosDoc.close();
		

	//	PDDocument document = PDDocument.load(filepath);
	//	document.getClass();
//		pdDoc.addPage(pdfpage);
		
		
//		List<PDAnnotationLink> annotations = pdfpage.getAnnotations();
//            	for (int j = -1; j < annotations.size(); j++) {
//            		PDAnnotationLink annot = annotations.get(j);
//            		if (annot instanceof PDAnnotationLink) {
//            			PDAnnotationLink link = (PDAnnotationLink) annot;
//            			PDAction action = link.getAction();
//            			if (action instanceof PDActionURI) {
//            				PDActionURI uri = (PDActionURI) action;
//            				urls += uri.getURI();
//            				System.out.println(uri.getURI());
//            			}
//            		}
//            	}
			}

	




	public void verifyDeliveryAndInvoiceAddressOnResending() throws Throwable{

		Optional<Path> lastFilePath = null;

		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;
		



		try {

			Path dir = Paths.get("C:\\Users\\naveed.khan\\Downloads");  // specify your directory
			lastFilePath = Files.list(dir)    // here we get the stream with full directory listing
					.filter(f -> !Files.isDirectory(f))  // exclude subdirectories from listing
					.max(Comparator.comparingLong(f -> f.toFile().lastModified()));  // finally get the last file using simple comparator by lastModified field

			if ( lastFilePath.isPresent() ) // your folder may be empty
			{
				System.out.println("This is requiered file"+lastFilePath.toString());

			}

		}
		catch(Exception e){


		}

		try{

			File file = new File(lastFilePath.toString().replace("\\", "\\\\").substring(9,92));
			String fileName = file.getName();
			System.out.println("This is also required: "+fileName);

			String filepath = "C:\\Users\\naveed.khan\\Downloads\\"+fileName+".PDF";

			//delete this line
			//String filepath = "C:\\Users\\naveed.khan\\Downloads\\SalesInvoice_TV_Replacement_1359_20190614_140607.PDF";
			//**************************************************************************************
			PDFParser parser = new PDFParser(new FileInputStream(new File(filepath)));


			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(1);

			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
		}

		catch (MalformedURLException e2) {
			System.err.println("URL string could not be parsed "+e2.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open PDF Parser. " + e.getMessage());
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}

		System.out.println("+++++++++++++++++");
		System.out.println(parsedText.replace(",","."));
		System.out.println("+++++++++++++++++");

		if(parsedText.contains(",")){

			parsedText = parsedText.replace(",",".");
		}
		UtilityMethods.wait5Seconds();


		try{

			if(parsedText.contains(customerDeliveryAddress)) {
				UtilityMethods.validateAssertEqual("Matched", "Matched");
				System.out.println( "UPDATED INVOICE ADDRESS IS : "+customerDeliveryAddress);
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("UPDATED INVOICE ADDRESS IS : "+customerDeliveryAddress);
			}
		}

		catch(Exception f){

			System.out.println("INVOICE ADDRESS IS NOT UPDATED");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("INVOICE ADDRESS IS NOT UPDATED");
			UtilityMethods.validateAssertEqual("Matched", "Not Matched");
		}
	}

	public void verifyDueDateOnResending() throws Throwable{

		Optional<Path> lastFilePath = null;

		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;



		try {

			Path dir = Paths.get("C:\\Users\\naveed.khan\\Downloads");  // specify your directory
			lastFilePath = Files.list(dir)    // here we get the stream with full directory listing
					.filter(f -> !Files.isDirectory(f))  // exclude subdirectories from listing
					.max(Comparator.comparingLong(f -> f.toFile().lastModified()));  // finally get the last file using simple comparator by lastModified field

			if ( lastFilePath.isPresent() ) // your folder may be empty
			{
				System.out.println("This is requiered file"+lastFilePath.toString());

			}

		}
		catch(Exception e){


		}

		try{

			File file = new File(lastFilePath.toString().replace("\\", "\\\\").substring(9,92));
			String fileName = file.getName();
			System.out.println("This is also required: "+fileName);

			String filepath = "C:\\Users\\naveed.khan\\Downloads\\"+fileName+".PDF";

			//delete this line
			//String filepath = "C:\\Users\\naveed.khan\\Downloads\\SalesInvoice_TV_Replacement_1359_20190614_140607.PDF";
			//**************************************************************************************
			PDFParser parser = new PDFParser(new FileInputStream(new File(filepath)));


			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(1);

			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
		}

		catch (MalformedURLException e2) {
			System.err.println("URL string could not be parsed "+e2.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open PDF Parser. " + e.getMessage());
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}

		System.out.println("+++++++++++++++++");
		System.out.println(parsedText.replace(",","."));
		System.out.println("+++++++++++++++++");

		if(parsedText.contains(",")){

			parsedText = parsedText.replace(",",".");
		}
		UtilityMethods.wait5Seconds();




		if(parsedText.contains(date)) {
			UtilityMethods.validateAssertEqual("Matched", "Matched");
			System.out.println( "UPDATED DUE DATE ON INVOICE RESENDING: "+date);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("UPDATED DUE DATE ON INVOICE RESENDING: "+date);
		}


		else{

			System.out.println("DUE DATE IS NOT UPDATED");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("DUE DATE IS NOT UPDATED");
			UtilityMethods.validateAssertEqual("Matched", "Not Matched");
		}
	}

	public void verifyLanguageChangeInInvoice() throws Throwable{

		Optional<Path> lastFilePath = null;

		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;



		try {

			Path dir = Paths.get("C:\\Users\\naveed.khan\\Downloads");  // specify your directory
			lastFilePath = Files.list(dir)    // here we get the stream with full directory listing
					.filter(f -> !Files.isDirectory(f))  // exclude subdirectories from listing
					.max(Comparator.comparingLong(f -> f.toFile().lastModified()));  // finally get the last file using simple comparator by lastModified field

			if ( lastFilePath.isPresent() ) // your folder may be empty
			{
				System.out.println("This is requiered file"+lastFilePath.toString());

			}

		}
		catch(Exception e){


		}

		try{

			File file = new File(lastFilePath.toString().replace("\\", "\\\\").substring(9,92));
			String fileName = file.getName();
			System.out.println("This is also required: "+fileName);

			String filepath = "C:\\Users\\naveed.khan\\Downloads\\"+fileName+".PDF";

			//delete this line
			//String filepath = "C:\\Users\\naveed.khan\\Downloads\\SalesInvoice_TV_Replacement_1359_20190614_140607.PDF";
			//**************************************************************************************
			PDFParser parser = new PDFParser(new FileInputStream(new File(filepath)));


			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(1);

			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
		}

		catch (MalformedURLException e2) {
			System.err.println("URL string could not be parsed "+e2.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open PDF Parser. " + e.getMessage());
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}

		System.out.println("+++++++++++++++++");
		System.out.println(parsedText.replace(",","."));
		System.out.println("+++++++++++++++++");

		if(parsedText.contains(",")){

			parsedText = parsedText.replace(",",".");
		}
		UtilityMethods.wait5Seconds();




		if(languageOfInvoiceAfterChange.contains("de")){

			if(parsedText.contains("Rechnung")) {
				System.out.println("Rechnung WORD IS AVAILABLE IN INVOICE");
				UtilityMethods.validateAssertEqual("Matched", "Matched");
				System.out.println( "UPDATED INVOICE LANGUAGE IS GERMAN");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("UPDATED INVOICE LANGUAGE IS GERMAN");
			}

			else{

				System.out.println("INVOICE LANGUAGE IS NOT UPDATED");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("INVOICE LANGUAGE IS NOT UPDATED");
				UtilityMethods.validateAssertEqual("Matched", "Not Matched");
			}
		}

		if(languageOfInvoiceAfterChange.contains("en-US")){

			if(parsedText.contains("Invoice")) {
				System.out.println("Invoice WORD IS PRESENT IN INVOICE");
				UtilityMethods.validateAssertEqual("Matched", "Matched");
				System.out.println( "UPDATED INVOICE LANGUAGE IS ENGLISH");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("Invoice WORD IS PRESENT IN INVOICE HENCE UPDATED INVOICE LANGUAGE IS ENGLISH");
			}

			else{

				System.out.println("INVOICE LANGUAGE IS NOT UPDATED");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("INVOICE LANGUAGE IS NOT UPDATED");
				UtilityMethods.validateAssertEqual("Matched", "Not Matched");
			}
		}

	}



	//Verify Values of Tax and Total on LaserNet Printer  
	public void verifyTaxAndTotalOnlasernetPrinter() throws Throwable{

		Optional<Path> lastFilePath = null;

		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;



		try {

			Path dir = Paths.get("C:\\Users\\naveed.khan\\Downloads");  // specify your directory
			lastFilePath = Files.list(dir)    // here we get the stream with full directory listing
					.filter(f -> !Files.isDirectory(f))  // exclude subdirectories from listing
					.max(Comparator.comparingLong(f -> f.toFile().lastModified()));  // finally get the last file using simple comparator by lastModified field

			if ( lastFilePath.isPresent() ) // your folder may be empty
			{
				System.out.println("This is requiered file"+lastFilePath.toString());

			}

		}
		catch(Exception e){


		}




		try{

			File file = new File(lastFilePath.toString().replace("\\", "\\\\").substring(9,92));
			String fileName = file.getName();
			System.out.println("This is also required: "+fileName);

			String filepath = "C:\\Users\\naveed.khan\\Downloads\\"+fileName+".PDF";

			//delete this line
			//String filepath = "C:\\Users\\naveed.khan\\Downloads\\SalesInvoice_TV_Replacement_1359_20190614_140607.PDF";
			//**************************************************************************************
			PDFParser parser = new PDFParser(new FileInputStream(new File(filepath)));


			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(1);

			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
		}

		catch (MalformedURLException e2) {
			System.err.println("URL string could not be parsed "+e2.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open PDF Parser. " + e.getMessage());
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}

		System.out.println("+++++++++++++++++");
		System.out.println(parsedText.replace(",","."));
		System.out.println("+++++++++++++++++");

		if(parsedText.contains(",")){

			parsedText = parsedText.replace(",",".");
		}
		UtilityMethods.wait5Seconds();


		try{

			if(parsedText.contains(taxAmountInD365) && parsedText.contains(customerName) && parsedText.contains(subTotalAmountInD365) &&  parsedText.contains(invoiceTotal)) {
				UtilityMethods.validateAssertEqual("Matched", "Matched");
				System.out.println( "Tax Amount is: " +taxAmountInD365+ "& " + "Subtotal Amount is: "+subTotalAmountInD365 +"Invoice Amount is: "+invoiceTotal);
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("Customer Name is : "+customerName+" & Tax Amount is: " +taxAmountInD365+ "& " + "Subtotal Amount is: "+subTotalAmountInD365 +"Invoice Amount is: "+invoiceTotal);
			}
		}

		catch(Exception f){

			System.out.println("TAX, TOTAL AND SUBTOTAL ARE NOT EQUAL IN LASERNET PRINTER INVOICE");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("TAX, TOTAL AND SUBTOTAL ARE NOT EQUAL IN LASERNET PRINTER INVOICE");
			UtilityMethods.validateAssertEqual("Matched", "Not Matched");
		}
	}


	public void verifyDiscountedPriceOnPrinter() throws Throwable{


		Optional<Path> lastFilePath = null;

		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;



		try {

			Path dir = Paths.get("C:\\Users\\naveed.khan\\Downloads");  // specify your directory
			lastFilePath = Files.list(dir)    // here we get the stream with full directory listing
					.filter(f -> !Files.isDirectory(f))  // exclude subdirectories from listing
					.max(Comparator.comparingLong(f -> f.toFile().lastModified()));  // finally get the last file using simple comparator by lastModified field

			if ( lastFilePath.isPresent() ) // your folder may be empty
			{
				System.out.println("This is requiered file"+lastFilePath.toString());

			}

		}
		catch(Exception e){


		}




		try{

			File file = new File(lastFilePath.toString().replace("\\", "\\\\").substring(9,92));
			String fileName = file.getName();
			System.out.println("This is also required: "+fileName);

			String filepath = "C:\\Users\\naveed.khan\\Downloads\\"+fileName+".PDF";

			//delete this line
			//String filepath = "C:\\Users\\naveed.khan\\Downloads\\SalesInvoice_TV_Replacement_1359_20190614_140607.PDF";
			//**************************************************************************************
			PDFParser parser = new PDFParser(new FileInputStream(new File(filepath)));


			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(1);

			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
		}

		catch (MalformedURLException e2) {
			System.err.println("URL string could not be parsed "+e2.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open PDF Parser. " + e.getMessage());
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}

		System.out.println("+++++++++++++++++");
		System.out.println(parsedText.replace(",","."));
		System.out.println("+++++++++++++++++");

		if(parsedText.contains(",")){

			parsedText = parsedText.replace(",",".");
		}
		UtilityMethods.wait5Seconds();


		try{


			if(parsedText.contains(lastLineDiscount) && parsedText.contains(lastLineAmount)) {
				UtilityMethods.validateAssertEqual("Matched", "Matched");
				System.out.println( "LAST LINE DISCOUNT IS : " +lastLineDiscount+ " & " + "LAST LINE AMOUNT IS : "+lastLineAmount);
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("LAST LINE DISCOUNT IS : " +lastLineDiscount+ " & " + "LAST LINE AMOUNT IS : "+lastLineAmount);
			}
		}

		catch(Exception f){

			System.out.println("LAST LINE AMOUNT AND LAST LINE DISCOUNT ARE NOT EQUAL IN LASERNET PRINTER INVOICE");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("LAST LINE AMOUNT AND LAST LINE DISCOUNTL ARE NOT EQUAL IN LASERNET PRINTER INVOICE");
			UtilityMethods.validateAssertEqual("Matched", "Not Matched");
		}
	}


	//Calculating the Order Totals for ECOM to ERP
	public void checkOrderTotalsInERP() throws Throwable{

		driver.findElement(By.xpath("//button[contains(@id,'SalesOrder_button')]")).click();
		//opening the totals page
		WebDriverWait wait = new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.elementToBeClickable(openTotalsPage));
		openTotalsPage.click();

		//Getting the values of Tax Subtotal and Total on D365
		wait.until(ExpectedConditions.visibilityOf(taxAmountsOnD365));
		taxAmountInD365 = taxAmountsOnD365.getAttribute("title");
		subTotalAmountInD365 = subTotalsOnD365.getAttribute("title");
		invoiceTotal = totalInvoiceAmountsOnD365.getAttribute("title");

		////Removing Dots and Commas from values of Tax Subtotal and Total on D365
		taxAmountInD365 = removeDotsandCommas(taxAmountInD365);
		subTotalAmountInD365 = removeDotsandCommas(subTotalAmountInD365);
		invoiceTotal = removeDotsandCommas(invoiceTotal);
		System.out.println("\n");
		System.out.println("\n");
		System.out.println("TAX | SUBTOTAL | TOTAL IN D365 : "+taxAmountInD365 +" "+subTotalAmountInD365+" "+invoiceTotal);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("TAX | SUBTOTAL | TOTAL IN D365 :    "+"<b>"+taxAmountInD365+"<b>" +"  "+"<b>"+subTotalAmountInD365+"<b>"+" "+"<b>"+invoiceTotal+"<b>");
		System.out.println("\n");


		String v1Tax = CheckOutPage.finalXpathTaxValueInString;
		String v2SubTotal = CheckOutPage.subTotalValue1;
		String v3OrderTotal = CheckOutPage.orderTotalValue;

		System.out.println("TAX | SUBTOTAL | TOTAL IN ECOM :"+v1Tax +"  "+ v2SubTotal+"  "+v3OrderTotal);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("TAX | SUBTOTAL | TOTAL IN ECOM :    "+"<b>"+v1Tax+"<b>"+"  "+ "<b>"+v2SubTotal+"<b>"+"  "+"<b>"+v3OrderTotal+"<b>");
		System.out.println("\n");
		System.out.println("\n");

		//For Production
		if(v1Tax.contains(taxAmountInD365)){

			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("TAX VALUE IN ECOM AND D365 ARE SAME");
			System.out.println("TAX VALUE IN ECOM AND D365 ARE SAME");

		}

		else{

			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("TAX VALUE IN ECOM AND D365 ARE NOT SAME");
			System.out.println("TAX VALUE IN ECOM AND D365 ARE NOT SAME");

		}


		if(v2SubTotal.contains(subTotalAmountInD365)){

			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("SUBTOTAL VALUE IN ECOM AND D365 ARE SAME");
			System.out.println("SUBTOTAL VALUE IN ECOM AND D365 ARE SAME");

		}

		else{
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("SUBTOTAL VALUE IN ECOM AND D365 ARE NOT SAME");
			System.out.println("SUBTOTAL VALUE IN ECOM AND D365 ARE NOT SAME");

		}

		if(v3OrderTotal.contains(invoiceTotal)){
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("ORDER TOTAL VALUE IN ECOM AND D365 ARE SAME");
			System.out.println("ORDER TOTAL VALUE IN ECOM AND D365 ARE SAME");

		}

		else{
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("ORDER TOTAL VALUE IN ECOM AND D365 ARE NOT SAME");
			System.out.println("ORDER TOTAL VALUE IN ECOM AND D365 ARE NOT SAME");

		}

		okButtonOnSwitchProducts.click();
		UtilityMethods.wait5Seconds();

	}


	//Calculating the Order Totals for LASERNET PRINT
	public void getOrderValuesForLasernetInvoiceInERP() throws Throwable{
		try{
		driver.findElement(By.xpath("//button[contains(@id,'SalesOrder_button')]")).click();
		
		}
		
		catch(ElementClickInterceptedException e){
				
		}
		//opening the totals page
		WebDriverWait wait = new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.elementToBeClickable(openTotalsPage));
		openTotalsPage.click();

		//Getting the values of Tax Subtotal and Total on D365
		wait.until(ExpectedConditions.visibilityOf(taxAmountsOnD365));
		taxAmountInD365 = taxAmountsOnD365.getAttribute("title");
		subTotalAmountInD365 = subTotalsOnD365.getAttribute("title");
		invoiceTotal = totalInvoiceAmountsOnD365.getAttribute("title");

		subToTalOfALicenseBeforeSwitch = subTotalAmountInD365;

		System.out.println("TAX AMOUNT IS :"+taxAmountInD365 );
		System.out.println("SUBTOTAL AMOUNT IS :"+subTotalAmountInD365);
		System.out.println("INVOICE TOTAL IS :"+invoiceTotal );


		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog(taxAmountInD365 +" + "+ subTotalAmountInD365 + " = "+invoiceTotal );

		//Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//Reporter.addStepLog(taxAmountInD365 +" + "+ subTotalAmountInD365 + " = "+invoiceTotal );

		okButtonOnSwitchProducts.click();
		UtilityMethods.wait5Seconds();


	}

	public void getOrderValuesFromTotalsPageForEcomToErpFLOW() throws Throwable{



		try{
		driver.findElement(By.xpath("(//button[contains(@id,'SalesOrder_button')])[1]")).click();
		}
		catch(Exception e){
			driver.findElement(By.xpath("(//button[contains(@id,'SalesOrder_button')])[2]")).click();
		}
		//opening the totals page
		WebDriverWait wait = new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.elementToBeClickable(openTotalsPage));
		openTotalsPage.click();
		wait.until(ExpectedConditions.visibilityOf(selectionDropDown));

		selectionDropDown.click();
		UtilityMethods.wait1Seconds();
		AllOptionInDropdown.click();

		UtilityMethods.wait3Seconds();
		//Getting the values of Tax Subtotal and Total on D365

		taxAmountInD365 = taxAmountsOnD365.getAttribute("title");
		subTotalAmountInD365 = subTotalsOnD365.getAttribute("title");
		invoiceTotal = totalInvoiceAmountsOnD365.getAttribute("title");

		subToTalOfALicenseBeforeSwitch = subTotalAmountInD365;

		System.out.println("TAX AMOUNT IS :"+taxAmountInD365 );
		System.out.println("SUBTOTAL AMOUNT IS :"+subTotalAmountInD365);
		System.out.println("INVOICE TOTAL IS :"+invoiceTotal );


		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog(taxAmountInD365 +" + "+ subTotalAmountInD365 + " = "+invoiceTotal );

		//Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//Reporter.addStepLog(taxAmountInD365 +" + "+ subTotalAmountInD365 + " = "+invoiceTotal );

		okButtonOnSwitchProducts.click();
		UtilityMethods.wait5Seconds();


	}


	public void againget_OrderValuesForLasernetInvoiceInERP() throws Throwable{

		driver.findElement(By.xpath("(//button[contains(@id,'SalesOrder_button')])[2]")).click();
		//opening the totals page
		WebDriverWait wait = new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.elementToBeClickable(openTotalsPageForSwitchedOrder));
		openTotalsPageForSwitchedOrder.click();

		//Getting the values of Tax Subtotal and Total on D365
		wait.until(ExpectedConditions.visibilityOf(taxAmountsOnD365));
		taxAmountInD365 = taxAmountsOnD365.getAttribute("title");
		subTotalAmountInD365 = subTotalsOnD365.getAttribute("title");
		invoiceTotal = totalInvoiceAmountsOnD365.getAttribute("title");
		System.out.println("Getting invoice total from Total Page "+invoiceTotal);

		//Change the value of Premium License Accordingly for Future Changes
		double originalPriceOfPremiumLicense = 694.80;
		double originalPriceOfBusinessLicense = Double.parseDouble(subToTalOfALicenseBeforeSwitch);

		double totalOfSwitchFromBusinessToPremium = originalPriceOfPremiumLicense - originalPriceOfBusinessLicense;
		totalOfSwitchFromBusinessToPremium = Math.round((totalOfSwitchFromBusinessToPremium +(totalOfSwitchFromBusinessToPremium*20)/100));
		System.out.println("My Calculation of invoice total :"+totalOfSwitchFromBusinessToPremium);	

		//In switch - business license subtotal value is completely minus from premium license subtotal value and after doing minus - we 
		//calculate the 20% of remaining value and add in it. that will become our invoice total value

		if(invoiceTotal.contains(""+totalOfSwitchFromBusinessToPremium)){
			UtilityMethods.validateAssertEqual("Invoice Total Matched", "Invoice Total Matched");
			System.out.println("TAX AMOUNT AFTER SWITCHING IS :"+taxAmountInD365 );
			System.out.println("SUBTOTAL AMOUNT AFTER SWITCHING IS :"+subTotalAmountInD365);
			System.out.println("INVOICE TOTAL AFTER SWITCHING IS :"+invoiceTotal );
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog(taxAmountInD365 +" + "+ subTotalAmountInD365 + " = "+invoiceTotal );


		}
		else{

			System.out.println("INVOICE TOTAL IS NOT MATCHED AFTER SWITCHING -");
			UtilityMethods.validateAssertEqual("Invoice Total Matched", "Invoice Total Matched");

		}

		okButtonOnSwitchProducts.click();
		UtilityMethods.wait5Seconds();


	}



	//Calculating the Order Totals for LASERNET PRINT
	public void getOrderValuesForContractCancellation() throws Throwable{

		driver.findElement(By.xpath("//button[contains(@id,'SalesOrder_button')]")).click();
		//opening the totals page
		WebDriverWait wait = new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.elementToBeClickable(openTotalsPage));
		openTotalsPage.click();

		//Getting the values of Tax Subtotal and Total on D365
		wait.until(ExpectedConditions.visibilityOf(taxAmountsOnD365));
		taxAmountInD365 = taxAmountsOnD365.getAttribute("title");
		subTotalAmountInD365 = subTotalsOnD365.getAttribute("title");
		invoiceTotal = totalInvoiceAmountsOnD365.getAttribute("title");

		System.out.println(taxAmountInD365 +" + "+ subTotalAmountInD365 + " = "+invoiceTotal );
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog(taxAmountInD365 +" + "+ subTotalAmountInD365 + " = "+invoiceTotal);

		okButtonOnSwitchProducts.click();
		UtilityMethods.wait5Seconds();


	}


	//Correcting the contract
	public void addingDiscountInContractCorrection() throws Throwable{

		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(inVoiceButtonOnMainHeaderBar));

		inVoiceButtonOnMainHeaderBar.click();

		System.out.println("USER HAS CLICKED ON INVOICE BUTTON ON HEADER BAR");

		UtilityMethods.wait3Seconds();

		contractCorrectionButton.click();
		System.out.println("USER HAS CLICKED ON CONTRACT CORRECT BUTTON");

		UtilityMethods.wait3Seconds();

		yesButton.click();

		wait.until(ExpectedConditions.visibilityOf(addLineButton));

		addLineButton.click();

		UtilityMethods.wait3Seconds();

		enterReasonCodeForDiscount.sendKeys("01");
		enterReasonCodeForDiscount.sendKeys(Keys.TAB);
		System.out.println("USER HAS ADDED DISCOUNT REASON 01 IN LINE");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS ADDED DISCOUNT REASON 01 IN LINE");

		UtilityMethods.wait3Seconds();

		Actions action = new Actions(driver);
		action.doubleClick(enterDiscountPercentageValue).build().perform();


		UtilityMethods.wait3Seconds();
		enterDiscountPercentageValue.sendKeys("15");
		enterDiscountPercentageValue.sendKeys(Keys.TAB);
		System.out.println("USER HAS ADDED 15% DISCOUNT IN LINE");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS ADDED 15% DISCOUNT IN LINE");

		UtilityMethods.wait5Seconds();	

		correctButton.click();

		UtilityMethods.wait5Seconds();
		yesButton.click();

		UtilityMethods.wait20Seconds();

		driver.findElement(By.xpath("//button[contains(@id,'SalesOrder_button')]")).click();

	}

	public void addingTaxExemptNumberInContractCorrection() throws Throwable{

		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(inVoiceButtonOnMainHeaderBar));

		inVoiceButtonOnMainHeaderBar.click();

		System.out.println("USER HAS CLICKED ON INVOICE BUTTON ON HEADER BAR");

		UtilityMethods.wait3Seconds();

		contractCorrectionButton.click();
		System.out.println("USER HAS CLICKED ON CONTRACT CORRECT BUTTON");

		UtilityMethods.wait3Seconds();

		yesButton.click();

		UtilityMethods.wait5Seconds();

		addVATNumber.sendKeys("BE0207258514");
		addVATNumber.sendKeys(Keys.TAB);
		UtilityMethods.wait3Seconds();

		try{

			if(messageOnInvalidVatNumber.isDisplayed()){

				System.out.println("YOUR VAT NUMBER IS INVALID  - CHANGE IT");
				UtilityMethods.validateAssertEqual("Valid Vat Number", "Invalid Vat Number");
			}
		}

		catch(Exception e){

			UtilityMethods.validateAssertEqual("Valid Vat Number", "Valid Vat Number");
			System.out.println("YOUR VAT NUMBER IS VALID - GOOD TO GO");

		}	


		UtilityMethods.wait3Seconds();	

		correctButton.click();

		UtilityMethods.wait5Seconds();
		yesButton.click();

		UtilityMethods.wait20Seconds();

		driver.findElement(By.xpath("//button[contains(@id,'SalesOrder_button')]")).click();

	}

	//Validating the contract correction
	public void validateValuesAfterCorrectContractForDiscount() throws Throwable{

		UtilityMethods.wait60Seconds();
		PriceInformation.click();

		UtilityMethods.wait5Seconds();

		//GETTING LAST LINE AMOUNT WHICH IS TOTALS > SUBTOTAL - its(15%)
		subTotalInD365AfterContractCorrect  = subTotalOrLastLineAmountAfterContractCorrection.getAttribute("title");
		System.out.println("SUBTOTAL AFTER 15% CONTRACT CORRECTION : "+subTotalInD365AfterContractCorrect);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SUBTOTAL AFTER 15% CONTRACT CORRECTION : "+subTotalInD365AfterContractCorrect);


		//CALCULATE 15% DISCOUNT BY MY CODING
		Double subTtlAmntInD365 = Double.parseDouble(subTotalAmountInD365);
		subTtlAmntInD365 = subTtlAmntInD365 - (subTtlAmntInD365*15)/100;

		//CONVERTING MY DOUBLE VALUE TO A STRING
		String subTtlAmntInD365InString = ""+subTtlAmntInD365;

		int dotPosition = subTtlAmntInD365InString.indexOf(".");
		subTtlAmntInD365InString = subTtlAmntInD365InString.substring(0,dotPosition);

		//VERIFYING THE SUBTOTALS

		if(subTotalInD365AfterContractCorrect.contains(subTtlAmntInD365InString)) {

			System.out.println("SUBTOTAL AFTER AUTOMATED CALCULATIONS "+subTotalInD365AfterContractCorrect);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("GETTING SUBTOTAL AMOUNT AFTER AUTOMATED CALCULATIONS "+subTotalInD365AfterContractCorrect);
			UtilityMethods.validateAssertEqual("SUBTOTAL IS SAME AFTER CONTRACT CORRECTION", "SUBTOTAL IS SAME AFTER CONTRACT CORRECTION");
			System.out.println("LAST LINE DISCOUNT : " +subTotalInD365AfterContractCorrect +" AND CALCULATED DISCOUNT : "+subTotalInD365AfterContractCorrect+" ARE SAME");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("LAST LINE DISCOUNT : " +subTotalInD365AfterContractCorrect +" AND CALCULATED DISCOUNT : "+subTotalInD365AfterContractCorrect+" ARE SAME");
		}
		else{


			System.out.println("LAST LINE DISCOUNT : " +subTotalInD365AfterContractCorrect +" AND CALCULATED DISCOUNT : "+subTtlAmntInD365InString+" ARE NOT SAME");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("LAST LINE DISCOUNT : " +subTotalInD365AfterContractCorrect +" AND CALCULATED DISCOUNT : "+subTtlAmntInD365InString+" ARE NOT SAME");
			UtilityMethods.validateAssertEqual("SUBTOTAL IS SAME AFTER CONTRACT CORRECTION", "SUBTOTAL IS NOT SAME AFTER CONTRACT CORRECTION");

		}


		//GETTING THE TAX VALUE

		Double subTtlAfterContractCorectionInDouble = Double.parseDouble(subTotalInD365AfterContractCorrect);
		Double taxAfterContractCorrectioninDouble = (subTtlAfterContractCorectionInDouble*20)/100;
		//convert calculated tax to string
		TaxAfterContractCorrectioninString = ""+taxAfterContractCorrectioninDouble;
		int dotPosition1 = TaxAfterContractCorrectioninString.indexOf(".");
		TaxAfterContractCorrectioninString = TaxAfterContractCorrectioninString.substring(0,dotPosition1);

		System.out.println("APPROXIMATE TAX AMOUNT IS :"+TaxAfterContractCorrectioninString);

		//CALCULATING THE TOTAL (SUBTOTAL + TAX)
		Double finalTotalSuminTotal = subTtlAmntInD365 + taxAfterContractCorrectioninDouble;
		finalTotalSuminTotalInStringContract = ""+finalTotalSuminTotal;
		int dotPosition2 = finalTotalSuminTotalInStringContract.indexOf(".");
		finalTotalSuminTotalInStringContract = finalTotalSuminTotalInStringContract.substring(0,dotPosition2);
		System.out.println("APPROXIMATE INVOICE TOTAL IS :"+finalTotalSuminTotalInStringContract);


	}
	
	
	public void openRecognitionForm() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(sideNavigationPanelButton));
		UtilityMethods.clickElemetJavaSciptExecutor(sideNavigationPanelButton);
//		new Actions(driver).moveToElement(sideNavigationPanelButton).click().build().perform();
		UtilityMethods.wait5Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();
		if (driver.findElements(By.xpath("//a[contains(text(),'General ledger')]")).size()!=0)
		{
		generalLegder.click();
		}
		else {
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(sideNavigationPanelButton));
			UtilityMethods.clickElemetJavaSciptExecutor(sideNavigationPanelButton);
//			new Actions(driver).moveToElement(sideNavigationPanelButton).click().build().perform();
			UtilityMethods.wait3Seconds();
			UtilityMethods.waitForPageLoadAndPageReady();
			generalLegder.click();
		}
		
		UtilityMethods.wait2Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();
		
		teamViewer.click();
		UtilityMethods.wait1Seconds();
		try{
			
			UtilityMethods.waitForPageLoadAndPageReady();
			dataRevenueRecognition.click();
			
		}
		catch(Exception e){
			
			teamViewer.click();
			UtilityMethods.wait1Seconds();
			UtilityMethods.waitForPageLoadAndPageReady();
			dataRevenueRecognition.click();
			
		}
		
		UtilityMethods.waitForPageLoadAndPageReady();
		new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOf(salesOrderColoumn));
		try{
		new Actions(driver).moveToElement(salesOrderColoumn).click(salesOrderColoumn).build().perform();
		
		UtilityMethods.wait3Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();
		System.out.println(salesOrderNumberOnOrder);
		
		salesOrderFilter.clear();
		}
		catch(Exception e){
			
			UtilityMethods.clickElemetJavaSciptExecutor(salesOrderColoumn);
			UtilityMethods.wait3Seconds();
			UtilityMethods.waitForPageLoadAndPageReady();
			System.out.println(salesOrderNumberOnOrder);
			
			salesOrderFilter.clear();
		}
		salesOrderFilter.sendKeys(salesOrderNumberOnOrder);				//TODO: GET VALUES TO MAKE ASSERTIONS HERE
		salesOrderFilter.sendKeys(Keys.ENTER);
		
		
		UtilityMethods.wait3Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(revenueRecognitionSaleOrderDate));
		UtilityMethods.wait3Seconds();
		
		revenueRecognitionSaleOrderDateValue = revenueRecognitionSaleOrderDate.getAttribute("title");
		int revSalesDate = revenueRecognitionSaleOrderDateValue.indexOf('C');
		revenueRecognitionSaleOrderDateValue = revenueRecognitionSaleOrderDateValue.substring(0, revSalesDate).trim();
		System.out.println(revenueRecognitionSaleOrderDateValue);
		
		revenueRecognitionItemValue = revenueRecognitionItem.getAttribute("title");
		int revItem = revenueRecognitionItemValue.indexOf('C');
		revenueRecognitionItemValue = revenueRecognitionItemValue.substring(0, revItem).trim();
		if(revenueRecognitionItemValue.contains("TVB0001")){
			
			product = "TeamViewer Business";
			
		}
		else if(revenueRecognitionItemValue.contains("TVPOOO1")){
			
			product = "TeamViewer Premium";
			
		}
		else if(revenueRecognitionItemValue.contains("TVCOOO1")){
			
			product = "TeamViewer Corporate";
			
		}
		
		revenueRecognitionQuantityValue = revenueRecognitionQuantity.getAttribute("title");
		
		revenueRecognitionCurrencyValue = revenueRecognitionCurrency.getAttribute("title");
		int revCurrency = revenueRecognitionCurrencyValue.indexOf('C');
		revenueRecognitionCurrencyValue = revenueRecognitionCurrencyValue.substring(0, revCurrency).trim();
		System.out.println(revenueRecognitionCurrencyValue);
		
		revenueRecognitionAmountValue = revenueRecognitionAmount.getAttribute("title");
		revenueRecognitionAmountInAccoutingCurrencyValue = revenueRecognitionAmountInAccoutingCurrency.getAttribute("title");
		
		revenueRecognitionDeferralAmountValue = revenueRecognitionDeferralAmount.getAttribute("title");
		revenueRecognitionRealisedRevenueValue = revenueRecognitionRealisedRevenue.getAttribute("title");
		
		revenueRecognitionCalculatedFromDateValue = revenueRecognitionCalculatedFromDate.getAttribute("title");
		revenueRecognitionValidDateValue = revenueRecognitionValidDate.getAttribute("title");
		
		
		revenueRecognitionEndDateValue = revenueRecognitionEndDate.getAttribute("title");
	
		revenueRecognitionPACLicenseValue = revenueRecognitionPACLicense.getAttribute("title");
		int revPAC = revenueRecognitionPACLicenseValue.indexOf("Click");
		System.out.println(revPAC);
		revenueRecognitionPACLicenseValue = revenueRecognitionPACLicenseValue.substring(0, revPAC-13).trim();
		System.out.println(revenueRecognitionPACLicenseValue);
		
		System.out.println(revenueRecognitionSaleOrderDateValue + revenueRecognitionItemValue + revenueRecognitionQuantityValue +
				revenueRecognitionCurrencyValue + revenueRecognitionAmountValue + revenueRecognitionAmountInAccoutingCurrencyValue +
				revenueRecognitionDeferralAmountValue + revenueRecognitionRealisedRevenueValue + revenueRecognitionCalculatedFromDateValue +
				revenueRecognitionValidDateValue + revenueRecognitionEndDateValue + revenueRecognitionPACLicenseValue
				);
		
		String parsedText = pdfReader();
		
		
	//	Format date = new SimpleDateFormat("dd/MM/yyyy");
		//String currentDate = date.format(new Date());                    //CANNOT BE IMPLEMENTED
		//System.out.println("Current Date = "+currentDate);
	//	revenueRecognitionSaleOrderDateValue.format("dd/MM/yyyy", revenueRecognitionSaleOrderDateValue);
	//	System.out.println(revenueRecognitionSaleOrderDateValue);           
	//	if(parsedText.contains(revenueRecognitionSaleOrderDateValue)){System.out.println("DATE VALUE");};
		
	//	&& parsedText.contains(revenueRecognitionAmountValue) && parsedText.contains(product) && parsedText.contains(revenueRecognitionCurrencyValue)
//		if(parsedText.contains(revenueRecognitionQuantityValue) && parsedText.contains(revenueRecognitionAmountInAccoutingCurrencyValue)
//			&&	parsedText.contains(revenueRecognitionDeferralAmountValue) && parsedText.contains(revenueRecognitionPACLicenseValue)
//			{
//			
//				System.out.println("ALL VALUES ARE VERIFIED AT CUSTOMER INVOICE");
//			}
	
		if(parsedText.contains(revenueRecognitionQuantityValue))
		{
			System.out.println("Quantity is Verified "+revenueRecognitionQuantityValue);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("VERIFY INFORMATION ON INVOICE AND DATE REVENUE RECOGNITION FORM:\n QUANTITY EXPECTED VALUE = "+revenueRecognitionQuantityValue+" QUANTITY ACTUAL VALUE = "+revenueRecognitionQuantityValue);
		}
		else{
			System.out.println("Quantity is not Verified ");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("EXPECTED QUANTITY VALUE IS NOT EQUAL TO ACTUAL QUANTITY VALUE");
		}
		if(parsedText.contains(revenueRecognitionAmountInAccoutingCurrencyValue))
		{
			System.out.println("Accounting Currency Value is Verified "+revenueRecognitionAmountInAccoutingCurrencyValue);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("AMOUNT EXPECTED VALUE = "+revenueRecognitionAmountInAccoutingCurrencyValue+" AMOUNT IN CURRENCY ACTUAL VALUE = "+revenueRecognitionAmountInAccoutingCurrencyValue);
		}
		else{
			System.out.println("Accounting Currency Value is not Verified ");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("AMOUNT EXPECTED VALUE IS NOT EQUAL TO AMOUNT IN CURRENCY VALUE");
		}
		if(parsedText.contains(revenueRecognitionDeferralAmountValue))
		{
			System.out.println("Recognition Deferral Amount Value is Verified "+revenueRecognitionDeferralAmountValue);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("DEFERAL EXPECTED VALUE = "+revenueRecognitionDeferralAmountValue+" DEFERAL ACTUAL VALUE = "+revenueRecognitionDeferralAmountValue);
		}
		else{
			System.out.println("Recognition Deferral Amount Value is not Verified ");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("DEFERAL EXPECTED VALUE IS NOT EQUAL TO DEFERAL ACTUAL VALUE");
		}
		if(parsedText.contains(revenueRecognitionPACLicenseValue))
		{
			System.out.println("Revenue Recognition PACLicense Value is Verified "+revenueRecognitionPACLicenseValue);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("PAC LICENSE EXPECTED VALUE = "+revenueRecognitionPACLicenseValue+" PAC LICENSE ACTUAL VALUE = "+revenueRecognitionPACLicenseValue);
		}
		else{
			System.out.println("Revenue Recognition PACLicense Value is not Verified ");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("PAC LICENSE EXPECTED VALUE IS NOT EQUAL TO PAC LICENSE ACTUAL VALUE");
		}
		if(parsedText.contains(revenueRecognitionAmountValue))
		{
			System.out.println("Revenue Recognition Amount Value is Verified "+revenueRecognitionAmountValue);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("AMOUNT EXPECTED VALUE = "+revenueRecognitionAmountValue+" AMOUNT ACTUAL VALUE = "+revenueRecognitionAmountValue);
		}
		else{
			System.out.println("Revenue Recognition Amount Value is not Verified ");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("AMOUNT EXPECTED VALUE IS NOT EQUAL TO AMOUNT ACTUAL VALUE");
		}
//		if(parsedText.contains(product))
//		{
//			System.out.println("Product is Verified"+product);
//		}
//		else{
//			System.out.println("Product is not Verified");
//		}
		if(parsedText.contains(revenueRecognitionCurrencyValue))
		{
			System.out.println("Revenue Recognition Currency Value is Verified "+revenueRecognitionCurrencyValue);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("CURRENCY EXPECTED VALUE = "+revenueRecognitionCurrencyValue+" CURRENCY ACTUAL VALUE = "+revenueRecognitionCurrencyValue);
		}
		else{
			System.out.println("Revenue Recognition Currency Value is not Verified ");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("CURRENCY EXPECTED VALUE IS NOT EQUAL TO CURRENCY ACTUAL VALUE");
		}

		
	}
	
	public void runJournalJob() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		
		sideNavigationPanelButton.click();
		Thread.sleep(2000);
		UtilityMethods.waitForPageLoadAndPageReady();
		
		if(periodicTasks.isDisplayed())
		{
			periodicTasks.click();
			Thread.sleep(1000);
			try{
				
				UtilityMethods.waitForPageLoadAndPageReady();
				revenueRecognition.click();
			}
			catch(Exception e){
				
				periodicTasks.click();
				Thread.sleep(1000);
				UtilityMethods.waitForPageLoadAndPageReady();
				revenueRecognition.click();
				
			}
			
			UtilityMethods.wait5Seconds();
			UtilityMethods.waitForPageLoadAndPageReady();
			
		}
		
		else{
		
				generalLegder.click();
				Thread.sleep(2000);
				UtilityMethods.waitForPageLoadAndPageReady();
				periodicTasks.click();
				Thread.sleep(1000);
				try{
			
						UtilityMethods.waitForPageLoadAndPageReady();
						revenueRecognition.click();
				
			
					}
				catch(Exception e){
						
						periodicTasks.click();
						Thread.sleep(1000);
						UtilityMethods.waitForPageLoadAndPageReady();
						revenueRecognition.click();
			
					}
		
				UtilityMethods.wait5Seconds();
				UtilityMethods.waitForPageLoadAndPageReady();
		
		}
		

		Format date = new SimpleDateFormat("MM/dd/yyyy");
		Date dt = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(dt); 
		c.add(Calendar.DATE, -1);
		dt = c.getTime();
		String fromDateField = date.format(dt);
		System.out.println(fromDateField);
		
		String currentDate = date.format(new Date());
		System.out.println("Current Date = "+currentDate);

		new WebDriverWait(driver,30).until(ExpectedConditions.elementToBeClickable(fromDate));
		fromDate.clear();
		fromDate.sendKeys(fromDateField);
		UtilityMethods.wait3Seconds();
		toDate.clear();
		toDate.sendKeys(currentDate);
		
		new WebDriverWait(driver,30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'OK')]")));
		revenueRecognitionOK.click();
		UtilityMethods.wait120Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();
		new WebDriverWait(driver,300).until(ExpectedConditions.visibilityOf(notification));
		UtilityMethods.wait60Seconds();
		UtilityMethods.clickElemetJavaSciptExecutor(notification);
		
		UtilityMethods.wait7Seconds();
		populateJournal();
		
	}

	private void populateJournal() {
		
		try{
		for(int i=1,j=0;i<=2;i++,j++){
			
			
			journal.add(driver.findElement(By.xpath("(//span[contains(text(),'Journal')])["+i+"]")));
			journalValues.add(journal.get(j).getText().substring(journal.get(j).getText().indexOf("J0"), journal.get(j).getText().indexOf("w")-1));
		
			System.out.println("Journal number/s is: "+journal.get(j).getText().substring(journal.get(j).getText().indexOf("J0"), journal.get(j).getText().indexOf("w")-1));
		}
		}
		catch(NoSuchElementException e){
			
			System.out.println(e.getMessage());
			
		}
		
		
	}
	
	public void openJournal() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		new Actions(driver).moveToElement(sideNavigationPanelButton).doubleClick().build().perform();
		Thread.sleep(2000);
		UtilityMethods.waitForPageLoadAndPageReady();
		
		if(driver.findElements(By.xpath("//a[contains(text(),'Journal entries')]")).size()!=0)
		{
			journalEntries.click();
			Thread.sleep(1000);
			try{
				
				UtilityMethods.waitForPageLoadAndPageReady();
				generalJournals.click();
				
			
			}
			catch(Exception e){
				
				journalEntries.click();
				Thread.sleep(1000);
				UtilityMethods.waitForPageLoadAndPageReady();
				generalJournals.click();
				
			}
			
			UtilityMethods.wait5Seconds();
			UtilityMethods.waitForPageLoadAndPageReady();
			
		}
			
	
	
			for(int i=0;i<journal.size();i++){
				
				UtilityMethods.waitForPageLoad();
				UtilityMethods.wait3Seconds();
				
				new WebDriverWait(driver,20).until(ExpectedConditions.elementToBeClickable(journalBatchNumberColoumn));
				journalBatchNumberColoumn.click();
				UtilityMethods.wait5Seconds();
				new WebDriverWait(driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[contains(@id,'_FilterField')]")));
				UtilityMethods.waitForPageLoad();
				UtilityMethods.wait3Seconds();
				new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOf(channelIdFilterField));
				UtilityMethods.wait5Seconds();
				System.out.println(journal.size());
				
				System.out.println(journalValues.get(i));
				channelIdFilterField.clear();
				channelIdFilterField.sendKeys(journalValues.get(i));		
				channelIdFilterField.sendKeys(Keys.ENTER);
				
				UtilityMethods.wait3Seconds();
				UtilityMethods.waitForPageLoadAndPageReady();
				linesHeaderButton.click();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait2Seconds();
				
				new WebDriverWait(driver,20).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(text(),'Voucher')]")));
				UtilityMethods.wait2Seconds();
				new Actions(driver).moveToElement(voucherColoumn).click(voucherColoumn).build().perform();
				UtilityMethods.wait2Seconds();
				
				new WebDriverWait(driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[contains(@id,'_FilterField_LedgerJournalTrans_Voucher_Voucher_Input')]")));
				new Actions(driver).moveToElement(voucherFilter).click(voucherFilter).build().perform();
				voucherFilter.clear();
				voucherFilter.sendKeys(inVoiceNo);
				UtilityMethods.wait1Seconds();
			//	voucherFilter.sendKeys(Keys.ENTER);

			//	UtilityMethods.wait3Seconds();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait5Seconds();
				
				if(driver.findElements(By.name("LedgerJournalTrans_TransDate")).size()!=0){
					
					System.out.println("DATA HAS BEEN FOUND AGAINST JOURNAL");
					break;
				}
				else{
					
					driver.navigate().back();
					
				}
				
			}		
		
		
	}
	
	public void validateAndPostJournal() throws Throwable{
		

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		
//		System.out.println(product);
	//	System.out.println(journalProductValue.getAttribute("title"));
	//	UtilityMethods.moveToElementWithJSExecutor(journalProductValue);
	//	UtilityMethods.moveToElementWithJSExecutor(journalCurrencyValue);
		
//		if(journalProductValue.getAttribute("title").contains(product) ){
//			System.out.println("product matched");
//		}
//		else{
//			System.out.println("Product is not matched");
//		}
		if(journalCurrencyValue.getAttribute("title").contains(revenueRecognitionCurrencyValue)){
			System.out.println("Revenue Recognition Currency Value"+revenueRecognitionCurrencyValue);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			//Reporter.addStepLog("CURRENCY EXPECTED VALUE = "+revenueRecognitionCurrencyValue+" CURRENCY ACTUAL VALUE = "+revenueRecognitionCurrencyValue);
		}
		else{
			System.out.println("Revenue Recognition Currency Value not matched");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			//Reporter.addStepLog("CURRENCY EXPECTED VALUE IS NOT EQUAL TO CURRENCY ACTUAL VALUE");
		}
		//CANNOT APPLY FINANCIAL DIMENSION VALIDATIONS AS ITS IMPOSSIBLE TO GET ITS VALUE FROM IT CURRENTS WEB ATTRIBUTES
		
	//	System.out.println(financialDimensionOnJournalPage.getText());
//		System.out.println("testing");
//		System.out.println(invoiceVoucher);
//		
//		if(financialDimensionOnJournalPage.getText().contains(invoiceVoucher)){
//			
//			System.out.println("Financial Dimension matched");
//			
//		}
		
		
		new WebDriverWait(driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'Validate') and starts-with(@id,'LedgerJournalTransDaily')]")));
		new Actions(driver).moveToElement(validateButton).click(validateButton).build().perform();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		new Actions(driver).moveToElement(validateButton2).click(validateButton2).build().perform();
	
		UtilityMethods.wait10Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();
	
		UtilityMethods.clickElemetJavaSciptExecutor(postButton);
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		
		
	}
	
	public void clickPACLicense() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.moveToElement(pacLicenseNumber);
		pacLicenseNumber.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		
		
	}
	
	public void openActivationLink() throws Throwable{
		
		UtilityMethods.wait5Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();
		String url = licenseActivation.getAttribute("title");
		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		UtilityMethods.CloseCurrentTabAndOpenNewTab(url);
		
		
		
	}
	
	public void switchMCOPortalToVerifyProduct() throws Throwable{
		
	
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
//		System.out.println(salesOrderNumberOnOrder);
//		System.out.println(getOrderIdMCOPortal.getText());
		
		productNameForInvoice = productName.getText();
		System.out.println(productName.getText());
		
		if(productName.getText().contains("TeamViewer Business")){
			
			System.out.println("CONGRATULATIONS WE ARE DONE");
			
		}
	}
		
		public void switchMCOPortalToVerifyPremiumProduct() throws Throwable{
			
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
//			System.out.println(salesOrderNumberOnOrder);
//			System.out.println(getOrderIdMCOPortal.getText());
			
			productNameForInvoice = productName.getText();
			System.out.println(productName.getText());
			
			if(productName.getText().contains("TeamViewer Premium")){
				
				System.out.println("CONGRATULATIONS WE ARE DONE");
				
			}
		
	}
	
	
	public void downloadPDFandValidateValues() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		invoiceTab.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.waitForVisibility(downloadPDF);
		downloadPDF.click();
		
		
		
	}
	
	public void verifyInvoiceandProductOnMCODownloadPDF() throws Throwable{

		Optional<Path> lastFilePath = null;

		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;



		try {

			Path dir = Paths.get("C:\\Users\\naveed.khan\\Downloads");  // specify your directory
			lastFilePath = Files.list(dir)    // here we get the stream with full directory listing
					.filter(f -> !Files.isDirectory(f))  // exclude subdirectories from listing
					.max(Comparator.comparingLong(f -> f.toFile().lastModified()));  // finally get the last file using simple comparator by lastModified field

			if ( lastFilePath.isPresent() ) // your folder may be empty
			{
				System.out.println("This is requiered file"+lastFilePath.toString());

			}

		}
		catch(Exception e){


		}

		try{

			File file = new File(lastFilePath.toString().replace("\\", "\\\\").substring(9,92));
			String fileName = file.getName();
			System.out.println("This is also required: "+fileName);

			String filepath = "C:\\Users\\naveed.khan\\Downloads\\"+fileName+".PDF";

			//delete this line
			//String filepath = "C:\\Users\\naveed.khan\\Downloads\\SalesInvoice_TV_Replacement_1359_20190614_140607.PDF";
			//**************************************************************************************
			PDFParser parser = new PDFParser(new FileInputStream(new File(filepath)));


			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(1);

			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
		}

		catch (MalformedURLException e2) {
			System.err.println("URL string could not be parsed "+e2.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open PDF Parser. " + e.getMessage());
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}

		System.out.println("+++++++++++++++++");
		System.out.println(parsedText);
		System.out.println("+++++++++++++++++");


		if(parsedText.contains(",")){

			parsedText = parsedText.replace(",",".");
		}
		UtilityMethods.wait5Seconds();
		System.out.println(productNameForInvoice);
		System.out.println(inVoiceNo);


		try{

			if(parsedText.contains(productNameForInvoice) && parsedText.contains(inVoiceNo)) {
				UtilityMethods.validateAssertEqual("Matched", "Matched");
//				System.out.println( "TAX AMOUNT AFTER CONTRACT CORRECTION IS: " +TaxAfterContractCorrectioninString+ "& " + "SUBTOTAL AMOUNT AFTER CONTRACT CORRECTION IS: "+subTotalInD365AfterContractCorrect +"INVOICE AMOUNT AFTER CONTRACT CORRECTION IS : "+finalTotalSuminTotalInStringContract);
//				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
//				Reporter.addStepLog("TAX AMOUNT AFTER CONTRACT CORRECTION IS: " +TaxAfterContractCorrectioninString+ "& " + "SUBTOTAL AMOUNT AFTER CONTRACT CORRECTION IS: "+subTotalInD365AfterContractCorrect +"INVOICE AMOUNT AFTER CONTRACT CORRECTION IS : "+finalTotalSuminTotalInStringContract);
			}
		}

		catch(Exception f){

//			System.out.println("TAX, TOTAL AND SUBTOTAL ARE NOT EQUAL IN LASERNET PRINTER INVOICE FOR CONTRACT CORRECTION");
//			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
//			Reporter.addStepLog("TAX, TOTAL AND SUBTOTAL ARE NOT EQUAL IN LASERNET PRINTER INVOICE FOR CONTRACT CORRECTION");
			UtilityMethods.validateAssertEqual("Matched", "Not Matched");
		}

		//cosDoc.close();

	}
	
	public void clickQuotationEmail() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait240Seconds();

		UtilityMethods.scrollToWebElement(quotationsFolder);
		new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOf(quotationsFolder));
		quotationsFolder.click();
		
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait30Seconds();
		new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOf(clickEmail));
		
		new Actions(driver).moveToElement(clickEmail).doubleClick().build().perform();
		//UtilityMethods.clickElemetJavaSciptExecutor(clickEmail);
		
		UtilityMethods.switchToNewPopUpWindow();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait30Seconds();
	
	
		
		quoteID = quotationID.getAttribute("title").substring(4, 13);
		System.out.println(quoteID);
		
		quotationID.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(downloadPDFOfQuotation));
		UtilityMethods.wait5Seconds();
		downloadPDFOfQuotation.click();
		System.out.println("File downloaded successfully");
		UtilityMethods.waitForPageLoad();
		UtilityMethods.wait10Seconds();
		
		
		
	}
	
	public void clickActivationLinkInPDF() throws Throwable{
		
		String parsedText = pdfReader();

		int indexofhttps = parsedText.indexOf("https");  
		quoteCheckOutPageUrl = parsedText.substring(indexofhttps, parsedText.length()-1);
		System.out.println(quoteCheckOutPageUrl);
	
		
	}

	private String pdfReader() throws InterruptedException {
		Optional<Path> lastFilePath = null;

		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		String parsedText = null;
    	 


		try {

			Path dir = Paths.get("C:\\Users\\naveed.khan\\Downloads");  // specify your directory
			
			lastFilePath = Files.list(dir)    // here we get the stream with full directory listing
					.filter(f -> !Files.isDirectory(f))  // exclude subdirectories from listing
					.max(Comparator.comparingLong(f -> f.toFile().lastModified()));  // finally get the last file using simple comparator by lastModified field

			if ( lastFilePath.isPresent() ) // your folder may be empty
			{
				System.out.println("This is requiered file"+lastFilePath.toString());

			}

		}
		catch(Exception e){


		}

		try{

			File file = new File(lastFilePath.toString());
			String fileName = file.getName().replace("]", "");
			System.out.println("This is also required: "+fileName);
		

			String filepath = "C:\\Users\\naveed.khan\\Downloads\\"+fileName+"";

			//delete this line
			//String filepath = "C:\\Users\\naveed.khan\\Downloads\\SalesInvoice_TV_Replacement_1359_20190614_140607.PDF";
			//**************************************************************************************
			PDFParser parser = new PDFParser(new FileInputStream(new File(filepath)));


			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(2);

			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
		}

		catch (MalformedURLException e2) {
			System.err.println("URL string could not be parsed "+e2.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open PDF Parser. " + e.getMessage());
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}

		System.out.println("+++++++++++++++++");
		System.out.println(parsedText);
		System.out.println("+++++++++++++++++");


		UtilityMethods.wait5Seconds();
		return parsedText;
	}
	
	public void signOutFromEmail() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		closePopUp.click();
		driver.close();
		UtilityMethods.switchToParentWindow(UtilityMethods.parentWindowHandler);
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(signOutButton));
		UtilityMethods.wait2Seconds();
		try{
		new Actions(driver).moveToElement(signOutButton).click(signOutButton).build().perform();
		}
		catch(Exception e){
			
			UtilityMethods.clickElemetJavaSciptExecutor(signOutButton);
		}
		
		UtilityMethods.waitForPageLoadAndPageReady();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(signOut));
		try{
			new Actions(driver).moveToElement(signOut).click(signOut).build().perform();
			}
			catch(Exception e){
				
				UtilityMethods.clickElemetJavaSciptExecutor(signOut);
			}
		
		
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
	}

	public void verifyQuotationStatus() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(sideNavigationPanelButton));
		new Actions(driver).moveToElement(sideNavigationPanelButton).click().build().perform();
		UtilityMethods.wait2Seconds();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait2Seconds();
		UtilityMethods.scrollToWebElement(salesAndMarketing);
		salesAndMarketing.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(salesQuoations));
		
		if(driver.findElements(By.xpath("//a[contains(text(),'All quotations')]")).size()!=0){
			allQuotations.click();
		}
		else{
		
		salesQuoations.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(allQuotations));
		UtilityMethods.wait2Seconds();
		allQuotations.click();
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(searchQuotation));
		UtilityMethods.wait3Seconds();
		searchQuotation.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(searchQuotationInput));
		searchQuotationInput.clear();
		searchQuotationInput.sendKeys(quoteID);
		searchQuotationInput.sendKeys(Keys.ENTER);
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		UtilityMethods.scrollToWebElement(quotationStatus);
		UtilityMethods.wait2Seconds();
		try{
			if(quotationStatusValue.getAttribute("title").contains("Sent"))
			{
				
				System.out.println("QUOTATION STATUS IS SENT");
			}
		}
		catch(Exception e){
			
			System.out.println("QUOTATION STATUS IS NOT SENT");
			//	UtilityMethods.validateAssertEqual("QUOTAION STATUS IS SENT", "QUOTATIOIN STATUS IS NOT EQUAL TO SENT");
				
			
		}
		
		
	}
	
	//Login into MAGENTO
		public void doLoginInMagento() throws Throwable{
			UtilityMethods.waitForPageLoadAndPageReady();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(userName));
			
			userName.sendKeys("Aamir");
			UtilityMethods.wait3Seconds();
			password.sendKeys("aamir@1234");
			UtilityMethods.wait2Seconds();
			signIn.click();
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS SUCESSFULLY LOGGED IN TO MAGENTO");


			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();

		}
		
		public void copySalesOrderID() throws Throwable{
			
			UtilityMethods.waitForPageLoadAndPageReady();
			System.out.println("User is waiting for sales order to be processed");
			UtilityMethods.wait300Seconds();

			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(sales));
			UtilityMethods.wait3Seconds();
			sales.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(orders));
			orders.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait10Seconds();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(searchReferenceID));
			System.out.println(ConfirmationPage.channelReferenceID);
			searchReferenceID.clear();
			searchReferenceID.sendKeys(ConfirmationPage.channelReferenceID);
			searchReferenceID.sendKeys(Keys.ENTER);
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait10Seconds();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(viewOrder));
			new Actions(driver).moveToElement(viewOrder).click(viewOrder).build().perform();

			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			try{
			UtilityMethods.scrollToWebElement(getSalesOrderId);
			}
			catch(NoSuchElementException e){
				
				UtilityMethods.wait10Seconds();
				new Actions(driver).moveToElement(viewOrder).click(viewOrder).build().perform();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait3Seconds();
				UtilityMethods.scrollToWebElement(getSalesOrderId);
			}
			int order = getSalesOrderId.getText().indexOf("order:");
			salesOrderID = getSalesOrderId.getText().substring(order+7, getSalesOrderId.getText().length()-1);
			System.out.println("SALES ORDER ID: "+salesOrderID);
			
			
		}
		
		public void openSalesOrder() throws Throwable{

			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(sideNavigationPanelButton));
			new Actions(driver).moveToElement(sideNavigationPanelButton).click().build().perform();
			Thread.sleep(2000);
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait2Seconds();
			UtilityMethods.scrollToWebElement(salesAndMarketing);
			salesAndMarketing.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(salesOrders));
			
			if(driver.findElements(By.xpath("//a[contains(text(),'All sales orders')]")).size()!= 0)
			{
				allSalesOrders.click();
				
			}
			else{
				salesOrders.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(allSalesOrders));
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
	//		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(salesOrder));
			allSalesOrders.click();
			
			}
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			salesOrder.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(inputSearcSalesOrder));
			inputSearcSalesOrder.clear();
			System.out.println(salesOrderID);
			inputSearcSalesOrder.sendKeys(salesOrderID);
			inputSearcSalesOrder.sendKeys(Keys.ENTER);
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(viewSalesOrder));
			
			Actions action = new Actions(driver);
			action.contextClick(viewSalesOrder).perform();
			UtilityMethods.wait3Seconds();
			openSalesOrderDetails.click();
			
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();

			System.out.println("***************************************************");
			System.out.println();
			System.out.println();
			List<WebElement> itemNumberList = driver.findElements(By.xpath("//*[@name = 'SalesLine_ItemId']"));

			for(WebElement singleAddon : itemNumberList){

				String productCodes = singleAddon.getAttribute("title");
				//System.out.println(productCodes);

				try{

					if(productCodes.contains(CheckOutPage.subscriptionProductCode)){

						UtilityMethods.validateAssertEqual("TVP0001 are added", "TVP0001 are added");
						System.out.println("LICENSE IS ADDED :"+CheckOutPage.subscriptionProductCode);
						Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
						Reporter.addStepLog("LICENSE IS ADDED :"+CheckOutPage.subscriptionProductCode);
					}

				}
				catch(Exception e){

					//System.out.println("SUBSCRIPTION PRODUCT IS NOT SELECTED");
				}

				try{

					if(productCodes.contains(AdOnPage.additionalConcurrentUserCode) ){

						//if(!addOnCode.equals("") && addOnCode.contains("TVAD0001") ){
						UtilityMethods.validateAssertEqual("TVAD001 are added", "TVAD001 are added");
						System.out.println("ADDITIONAL CONCURRENT USERS ARE ADDED :"+AdOnPage.additionalConcurrentUserCode);
						Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
						Reporter.addStepLog("ADDITIONAL CONCURRENT USERS ARE ADDED :"+AdOnPage.additionalConcurrentUserCode);
					}
				}
				catch(Exception e){

					//System.out.println("ADDON PRODUCT IS NOT SELECTED");
				}


				try{
					if(productCodes.contains(AdOnPage.mobileSupportCode) ){
						//	if(!addOnCode.equals("") && addOnCode.contains("TVAD003") ){
						UtilityMethods.validateAssertEqual("TVAD003 are added", "TVAD003 are added");
						System.out.println("MOBILE DEVICES ARE ADDED :"+AdOnPage.mobileSupportCode);
						Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
						Reporter.addStepLog("MOBILE DEVICES ARE ADDED :"+AdOnPage.mobileSupportCode);
					}
				}
				catch(Exception e){

					//System.out.println("MOBILE SUPPORT DEVICE IS NOT ADDED");
				}




				try{
					if(productCodes.contains(AdOnPage.teamViewerMonitoringCode) ){

						UtilityMethods.validateAssertEqual("TeamViewer Monitoring Code are added", "TeamViewer Monitoring Code are added");
						System.out.println("TEAMVIEWER MONITORING ARE ADDED ON D365");
						Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
						Reporter.addStepLog("TEAMVIEWER MONITORING ARE ADDED ON D365");
					}
				}
				catch(Exception e){

					//	System.out.println("TEAMVIEWER MONITORING IS NOT ADDED");
				}


				try{
					if(productCodes.contains(AdOnPage.teamviewerEndPointCode) ){

						UtilityMethods.validateAssertEqual("TeamViewer Endpoint code are added", "TeamViewer Endpoint code are added");
						System.out.println("TEAMVIEWER ENDPOINTS ARE ADDED ON D365");
						Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
						Reporter.addStepLog("TEAMVIEWER ENDPOINTS ARE ADDED ON D365");
					}

				}
				catch(Exception e){

					//System.out.println("TEAMVIEWER ENDPOINTS IS NOT ADDED");
				}

				try{
					if(productCodes.contains(AdOnPage.teamviewerBackupCode) ){

						UtilityMethods.validateAssertEqual("Teamviewer Backup are added", "Teamviewer Backup are added");
						System.out.println("TEAMVIEWER BACKUP ARE ADDED ON D365");
						Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
						Reporter.addStepLog("TEAMVIEWER BACKUP ARE ADDED ON D365");
					}

				}
				catch(Exception e){

					//System.out.println("TEAMVIEWER BACKUP IS NOT ADDED");
				}

				try{

					if(productCodes.contains(AdOnPage.teamViewerPilotCode) ){

						UtilityMethods.validateAssertEqual("Teamviewer Pilot are added", "Teamviewer Pilot are added");
						System.out.println("TEAMVIEWER PILOT ARE ADDED ON D365");
						Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
						Reporter.addStepLog("TEAMVIEWER PILOT ARE ADDED ON D365");
					}

				}
				catch(Exception e){

					//System.out.println("TEAMVIEWER PILOT IS NOT ADDED");
				}
				
				

			}
			

			
			
		}
		
		public void validationOverInvoice() throws Throwable{
				
			try{
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait7Seconds();
				System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
				inVoiceButtonUnderJournalForLasernetPrint.click();
				UtilityMethods.wait10Seconds();
			}
			catch(Exception e){
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait20Seconds();
				driver.navigate().back();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait7Seconds();
				System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
				inVoiceButtonUnderJournalForLasernetPrint.click();
				UtilityMethods.wait10Seconds();
				
				
			}
			
//			System.out.println("USER HAS CLICKED ON LASERPRINT BUTTON");
//			inVoiceButtonUnderJournalForLasernetPrint.click();
//			UtilityMethods.waitForPageLoadAndPageReady();						//should be removed 
//			UtilityMethods.wait5Seconds();
			
		//	new Actions(driver).moveToElement(total).click().build().perform();
			UtilityMethods.clickElemetJavaSciptExecutor(total);
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			
			System.out.println("INVOICE SUB TOTAL IS: "+invoiceSubTotalInErp.getAttribute("title"));
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("INVOICE SUB TOTAL IS: "+invoiceSubTotalInErp.getAttribute("title"));
			
			System.out.println("INVOICE TOTAL SALES TAX IS: "+invoiceSalesTaxInErp.getAttribute("title"));
			String totalSalesTaxOnInvoiceTotalTab = invoiceSalesTaxInErp.getAttribute("title");
			Reporter.addStepLog("INVOICE TOTAL SALES TAX IS: "+totalSalesTaxOnInvoiceTotalTab);
			
			System.out.println("INVOICE TOTAL AMOUNT IS: "+invoiceAmountInErp.getAttribute("title"));
			Reporter.addStepLog("INVOICE TOTAL AMOUNT IS: "+invoiceAmountInErp.getAttribute("title"));
			
			try{
				new Actions(driver).moveToElement(closeButton).click().build().perform();
			}
			catch(Exception e){
				UtilityMethods.clickElemetJavaSciptExecutor(closeButton);
			}
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			new Actions(driver).moveToElement(postedSalesTaxButton).click().build().perform();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait10Seconds();
			new WebDriverWait(driver,40).until(ExpectedConditions.visibilityOf(totalSalesTaxOnPostedSalesTaxTab));
			if(totalSalesTaxOnInvoiceTotalTab.contains(totalSalesTaxOnPostedSalesTaxTab.getAttribute("title")))
			{
			System.out.println("TOTAL SALES TAX ON POSTED AND TAX PAGE IS: "+totalSalesTaxOnPostedSalesTaxTab.getAttribute("title")+"EQUALS TO TAX ON TOTAL INVOICE TAB "+totalSalesTaxOnInvoiceTotalTab);
			Reporter.addStepLog("TOTAL SALES TAX ON POSTED AND TAX PAGE IS: "+totalSalesTaxOnPostedSalesTaxTab.getAttribute("title")+" EQUALS TO TAX ON TOTAL INVOICE TAB "+totalSalesTaxOnInvoiceTotalTab);
			}
			
			
			try{
				generalTab.click();
			}
			catch(Exception e){
				
				generalTab1.click();
				
			}
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();
			
		
			int index = salesTaxGroupOnPostedSalesTax.getAttribute("title").indexOf("C");
			System.out.println("SALES TAX GROUP ON INVOICE IS: "+salesTaxGroupOnPostedSalesTax.getAttribute("title").substring(0, index));
			if(salesTaxGroupOnPostedSalesTax.getAttribute("title").substring(0, index)!=null){
				
			Reporter.addStepLog("SALES TAX GROUP ON INVOICE IS: "+salesTaxGroupOnPostedSalesTax.getAttribute("title").substring(0, index));
		
			}
			int index1 = taxCodePostedSalesTax.getAttribute("title").indexOf("C");
			System.out.println("SALES TAX CODE ON INVOICE IS: "+taxCodePostedSalesTax.getAttribute("title").substring(0, index1));
			Reporter.addStepLog("SALES TAX CODE ON INVOICE IS: "+taxCodePostedSalesTax.getAttribute("title").substring(0, index1));
			
			
			driver.navigate().back();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait10Seconds();		
			
		}
		
	public void addBLPA001AndITBB0001Product() throws Throwable{
			
		
	//		driver.findElement(By.xpath("(//span[contains(text(),'Add line')])[3]")).click();
			// ENTER DISCOUNTS ETC HERE
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.xpath("(//span[contains(text(),'Contract')])[1]")));
	//		driver.findElement(By.xpath("//div[@class='columnHeader-label alignmentLeft' and contains(text(),'Item')]")).click();
			UtilityMethods.clickElemetJavaSciptExecutor(driver.findElement(By.xpath("(//span[contains(text(),'Add line')])[1]")));
	//		new Actions(driver).moveToElement(driver.findElement(By.xpath("(//span[contains(text(),'Add line')])[1]"))).click().build().perform();;
			UtilityMethods.wait3Seconds();

			itemNumberInputField.sendKeys("BLPA001");
			UtilityMethods.wait5Seconds();
//			try{

				if(driver.findElement(By.xpath("//input[@title='BLPA001 :  :  : Y : SUB-Y']")).isDisplayed()){

					driver.findElement(By.xpath("//input[@title='BLPA001 :  :  : Y : SUB-Y']")).click();
					Reporter.addStepLog("BLPA001 PRODUCT IS SELECTED");

				}
//			}
//			catch(Exception e){
//
//				System.out.println("BLPAOO1 PRODUCT NOT SELECTED");
//			}
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			driver.findElement(By.xpath("(//span[contains(text(),'Add line')])[1]")).click();
			UtilityMethods.wait2Seconds();
			try{
				itemNumberInputField.sendKeys("ITBB0001");
			}
			catch(ElementNotInteractableException e)
			{
				UtilityMethods.wait3Seconds();
				itemNumberInputField.sendKeys("ITBB0001");
			}
			UtilityMethods.wait5Seconds();
//			try{

				if(driver.findElement(By.xpath("//input[@title='ITBB0001 :  :  : Y : SUB-Y']")).isDisplayed()){

					driver.findElement(By.xpath("//input[@title='ITBB0001 :  :  : Y : SUB-Y']")).click();
					Reporter.addStepLog("ITBB0001 PRODUCT IS SELECTED");

				}
//			}
//			catch(Exception e){
//
//				System.out.println("ITBBOO1 PRODUCT NOT SELECTED");
//			}
			
			System.out.println("USER HAS CREATED A QOUTATION ORDER: ");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CREATED A SALES ORDER WITH AD ONS: ");


			UtilityMethods.wait5Seconds();
			
			new Actions(driver).moveToElement(saveButton).click().build().perform();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();
			
			
		}
	
	public void confirmSalesOrderAndVerify() throws Throwable{

		UtilityMethods.wait15Seconds();

			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			try{
				salesOrderHeader.click();
				System.out.println("SALES ORDER BUTTON CLICKED");
			}
			catch(Exception e){
				System.out.println("SALES ORDER BUTTON NOT FOUND AND FINDING SECOND ONE");
			}
		

		try{
		
					UtilityMethods.waitForPageLoadAndPageReady();
					UtilityMethods.wait7Seconds();
		
					confirmSalesOrderButton.click();
					
					System.out.println("USER HAS CLICKED ON CONFIRM SALES ORDER");
					Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
					Reporter.addStepLog("USER HAS CLICKED ON CONFIRM SALES ORDER");
			
		}
		catch(NoSuchElementException | ElementNotInteractableException e){
				System.out.println("problem is "+e.getMessage());
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait3Seconds();
				
					try{
						salesOrderHeader2.click();
						System.out.println("SALES ORDER BUTTON CLICKED");
					}
					catch(ElementNotInteractableException f){
						
						salesOrderHeader3.click();
						System.out.println("SALES ORDER BUTTON CLICKED");
						
					}
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait3Seconds();
				
					try{
						new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(confirmSalesOrderButton));
						confirmSalesOrderButton.click();
						System.out.println("USER HAS CLICKED ON CONFIRM SALES ORDER");
						Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
						Reporter.addStepLog("USER HAS CLICKED ON CONFIRM SALES ORDER");
					}
					catch(Exception y){}
					
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait7Seconds();
				
					if(driver.findElements(By.xpath("//div[contains(text(),'Confirm sales order')]")).size()==0){
						
						confirmSwitchedSalesOrderButton.click();
						System.out.println("USER HAS CLICKED ON CONFIRM SALES ORDER");
						Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
						Reporter.addStepLog("USER HAS CLICKED ON CONFIRM SALES ORDER");
						UtilityMethods.waitForPageLoadAndPageReady();

						UtilityMethods.wait7Seconds();
						
					}
			
					
			}
			
//			WebDriverWait wait = new WebDriverWait(driver,10);
//			wait.until(ExpectedConditions.elementToBeClickable(clickOkForConfirmSalesOrder));
			UtilityMethods.clickElemetJavaSciptExecutor(clickOkForConfirmSalesOrder);
			UtilityMethods.wait5Seconds();
			if(driver.findElements(By.name("Yes")).size()!=0){
			
				yes.click();
				
			}
			else{
			//	wait.until(ExpectedConditions.visibilityOf(clickOkForConfirmSalesOrder2));
				UtilityMethods.clickElemetJavaSciptExecutor(clickOkForConfirmSalesOrder2);
				UtilityMethods.wait5Seconds();
				if(yes.isDisplayed()){
					
					yes.click();
					}
			}

		System.out.println("USER HAS CONFIRMED SALES ORDER");
		Reporter.addStepLog("USER HAS CONFIRMED SALES ORDER");
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait15Seconds();
		new Actions(driver).sendKeys(Keys.ESCAPE).build().perform();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		//ONLY FOR CRM FLOW
		try{
		new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOf(pacLicenseInERP));
		UtilityMethods.scrollToElement(pacLicenseInERP);
	//	new Actions(driver).moveToElement(pacLicenseInERP).contextClick().perform();
		UtilityMethods.wait5Seconds();
		System.out.println("PAC LICENSE IS: "+ pacLicenseInERP.getAttribute("title"));
		
		int index4 = pacLicenseInERP.getAttribute("title").indexOf("Click");
		pacLicenseERP = pacLicenseInERP.getAttribute("title").substring(0, index4).trim();
		System.out.println("PAC LICENSE IS: "+pacLicenseERP);
		Reporter.addStepLog("PAC LICENSE IS: "+pacLicenseERP);
		UtilityMethods.wait3Seconds();
		
		
		
		}
		catch(Exception e){
			
			System.out.println("PAC LICENSE VALUE NOT GETTABLE");
			
		}
		
		
		UtilityMethods.switchToFirstTab();
		cmpPage.validateAccountValuesAfterOrderConfirmation();
		int sixe = driver.getWindowHandles().size();
		System.out.println("Number of tabs open is: "+sixe);
		if(sixe == 3 && consolidation == false){
			
			UtilityMethods.switchToSecondTab();
			
		}
		else{
			
			UtilityMethods.switchToNewTab();
		
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		
		
	}
	
	public void invoiceSalesOrderAndVerify() throws Throwable{
		//ONLY FOR CRM FLOW	
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		try{
		salesOrderHeader.click();
		System.out.println("SALES ORDER BUTTON CLICKED");
		}
		catch(Exception e){
			System.out.println("SALES ORDER BUTTON NOT FOUND AND FINDING SECOND ONE");
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		String salesOrderStatus;
		try{
			statusOfSalesOrder.click();
			salesOrderStatus = statusOfSalesOrder.getAttribute("title");
			System.out.println("STATUS OF SALES ORDER IS: "+salesOrderStatus);
			
		}
		catch(Exception e){
			statusOfSalesOrder2.click();
			salesOrderStatus = statusOfSalesOrder2.getAttribute("title");
			System.out.println("STATUS OF SALES ORDER IS: "+salesOrderStatus);
		}
		
		
		if(salesOrderStatus.contains("Invoiced")){
			System.out.println("USER HAS CONFIRMED SIMPLE SALES ORDER AND INVOICED IT");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CONFIRMED SIMPLE SALES ORDER AND INVOICED IT");
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
		}
		else{
		try{
			
			UtilityMethods.wait5Seconds();
			inVoiceButton.click();
			System.out.println("USER HAS CLICKED ON INVOICE BUTTON");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CLICKED ON INVOICE BUTTON");
		}catch(NoSuchElementException | ElementNotInteractableException e){
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		try{
			salesOrderHeader2.click();
			System.out.println("SALES ORDER BUTTON CLICKED");
			UtilityMethods.waitForPageLoadAndPageReady();
//			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(inVoiceButton));
		}
		catch(Exception n){
//			System.out.println("Clicking on escape button");
//			new Actions(driver).sendKeys(Keys.ESCAPE).build().perform();
	 }
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		try{
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(inVoiceButton));
			inVoiceButton.click();
		}
		catch(Exception d){
			
			inVoiceButton2.click();
		}
		System.out.println("USER HAS CLICKED ON INVOICE BUTTON OF SWITCHED ORDER");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CLICKED ON INVOICE BUTTON");
		}
	
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		
	try{
		if(driver.findElements(By.xpath("//button[contains(@id, '_No')]")).size()==0){
			
			try{
				inVoiceButtonForSwitchedOrder.click();
				System.out.println("USER HAS CLICKED ON INVOICE SALES ORDER");
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("USER HAS CLICKED ON INVOICE BUTTON");
				UtilityMethods.wait10Seconds();
			}
			catch(Exception e){
				System.out.println("Exception is: "+e.getMessage());
			}
			
			if(driver.findElements(By.xpath("//button[contains(@id, '_No')]")).size()!=0){
				try{
					
					UtilityMethods.wait10Seconds();
					noButtonOnInvoiceSchedule.click();
					
					}
					catch(Exception j){

						System.out.println("NO POPUP IS NOT SHOWING");
					}

					try{
						UtilityMethods.wait7Seconds();
						UtilityMethods.clickElemetJavaSciptExecutor(okButtonForPostingInvoice);
					}

					catch(Exception d){

						System.out.println("POP UP NOT VISIBLE");
					}

					UtilityMethods.wait7Seconds();
					try{
						new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(okButtonForPostingDocument));
						okButtonForPostingDocument.click();
					}
					catch(Exception e){
						okButtonForPostingInvoice.click();
					}
					UtilityMethods.waitForPageLoadAndPageReady();
					UtilityMethods.wait20Seconds();

				
				
			}
			
		}
		

	}
	catch(NoSuchElementException e){
		System.out.println("USER HAS CONFIRMED SIMPLE SALES ORDER AND INVOICED IT");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CONFIRMED SIMPLE SALES ORDER AND INVOICED IT");
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
	}
		
	if(driver.findElements(By.xpath("//button[contains(@id, '_No')]")).size()!=0){
		try{
			
			UtilityMethods.wait10Seconds();
			noButtonOnInvoiceSchedule.click();
			
			}
			catch(Exception j){

				System.out.println("NO POPUP IS NOT SHOWING");
			}

			try{
				UtilityMethods.wait7Seconds();
				UtilityMethods.clickElemetJavaSciptExecutor(okButtonForPostingInvoice);
			}

			catch(Exception d){

				System.out.println("POP UP NOT VISIBLE");
			}

			UtilityMethods.wait7Seconds();
			try{
				new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(okButtonForPostingDocument));
				okButtonForPostingDocument.click();
			}
			catch(Exception e){
				okButtonForPostingInvoice.click();
			}
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait20Seconds();

		
		
		}
	
		System.out.println("USER HAS CONFIRMED SIMPLE SALES ORDER AND INVOICED IT");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CONFIRMED SIMPLE SALES ORDER AND INVOICED IT");
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		}
		
			
		
	}
	
	public void terminatContract() throws Throwable{
		
		
		if(driver.findElements(By.xpath("//span[contains(text(),'Open Lasernet setup')]")).size()!=0){
			UtilityMethods.waitForPageLoadAndPageReady();
			
			driver.navigate().back();
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
		}
		try{
		UtilityMethods.clickElemetJavaSciptExecutor(salesOrderHeader);
		System.out.println("SALES ORDER BUTTON CLICKED");
		}
		catch(Exception e){
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			salesOrderHeader2.click();
			System.out.println("SALES ORDER BUTTON CLICKED");
		}
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(text(),'Terminate contract')]"))));
		
		UtilityMethods.clickElemetJavaSciptExecutor(driver.findElement(By.xpath("//span[contains(text(),'Terminate contract')]")));
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		new WebDriverWait(driver,15).until(ExpectedConditions.visibilityOf(driver.findElement(By.name("Fld2_1"))));
		driver.findElement(By.name("Fld2_1")).clear();
		driver.findElement(By.name("Fld2_1")).sendKeys("Terminating contract");
		UtilityMethods.wait3Seconds();
		driver.findElement(By.name("Fld3_1")).clear();
		driver.findElement(By.name("Fld3_1")).sendKeys("Terminating contract");
		
		if (toggleCreateOpportunity.getAttribute("aria-checked").contains("true")) {

			toggleCreateOpportunity.click();

		} else {

			System.out.println("Create Opportunity is already false");
			
		}
	
		clickOkForConfirmSalesOrder.click();
		Reporter.addStepLog("Contract has been terminated");
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		
		driver.findElement(By.xpath("//span[contains(text(),'Reactivate contract')]")).click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		new Actions(driver).moveToElement(driver.findElement(By.xpath("//button[@name='Yes']"))).click().perform();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		Reporter.addStepLog("Contract has been Reactivated successfully");

	}
	
	public void confirmationAndVerificationOfQuotation() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
	//	followUpButton.click();
		UtilityMethods.wait3Seconds();
	//	System.out.println("USER HAS CLICKED ON FOLLOWUP BUTTON TO CONFIRM QOUTATION");
		UtilityMethods.clickElemetJavaSciptExecutor(confirmButtonForQoutation);
		UtilityMethods.wait7Seconds();
		if(driver.findElements(By.xpath("//span[contains(text(),'OK')]")).size()==0){
			
			UtilityMethods.clickElemetJavaSciptExecutor(confirmButtonForQoutation2);
			UtilityMethods.wait7Seconds();
		}
		UtilityMethods.wait3Seconds();
		UtilityMethods.clickElemetJavaSciptExecutor(printQuotationToggle);

	
		
		UtilityMethods.wait3Seconds();

		UtilityMethods.clickElemetJavaSciptExecutor(printQuotationDestination);
		UtilityMethods.wait2Seconds();
		UtilityMethods.clickElemetJavaSciptExecutor(oKButtonTosendQoutation);
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		if(qoutationStatusOnERPIfCreated.getAttribute("title").contains("Confirmed")){

			System.out.println("USER HAS CONFIRMED THE QOUTATION");
			Reporter.addStepLog("USER HAS CONFIRMED THE QOUTATION");

		}
		else{

			System.out.println("QOUTATION IS NOT CONFIRMED");
			Reporter.addStepLog("QOUTATION IS NOT CONFIRMED");
		}
		
		
		
		
	}

	public void validatingValuesInConfirmationAndJournalInvoice() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		if(driver.findElements(By.xpath("//span[contains(text(),'Open Lasernet setup')]")).size()!=0){
			UtilityMethods.waitForPageLoadAndPageReady();
			
			driver.navigate().back();
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
		}
		try{
		UtilityMethods.clickElemetJavaSciptExecutor(salesOrderConfirmation);
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		}
		catch(Exception e){
			driver.navigate().back();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			UtilityMethods.clickElemetJavaSciptExecutor(salesOrderConfirmation);
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();
		}
		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(amountWithTaxInConfirmationJournal));
		System.out.println("Amount With Tax"+amountWithTaxInConfirmationJournal.getAttribute("title")+"\nBase Amount"+baseInConfirmationJournal.getAttribute("title"));
		amountWithTaxInJournalConfirmation = amountWithTaxInConfirmationJournal.getAttribute("title");
		baseInJournalConfirmation = baseInConfirmationJournal.getAttribute("title");
		System.out.println("AMOUNT WITH TAX ON CONFIRMATION JOURNAL"+amountWithTaxInJournalConfirmation+"\nBASE AMOUNT ON CONRIMATION JOURNAL"+baseInJournalConfirmation);
		
		try{

		for(int i=1;i<=4;i++){
			UtilityMethods.clickElemetJavaSciptExecutor(driver.findElement(By.xpath("(//span[contains(text(),'Sales tax')])["+i+"]")));
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();
		//	new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(totalTaxInConfirmationJournal));
			
			if(driver.findElements(By.name("TaxAmountCurTotal")).size()!=0){
				System.out.println("SALES TAX AMOUNT: "+totalTaxInConfirmationJournal.getAttribute("title"));
				break;
			}
		}
	
		if(totalTaxInConfirmationJournal.getAttribute("title") == null){
		totalTaxInJournalConfirmation = totalTaxInConfirmationJournal.getAttribute("old-title");
		}
		else{
			
			totalTaxInJournalConfirmation = totalTaxInConfirmationJournal.getAttribute("title");
		}
		System.out.println(taxCodeInConfirmationJournal.getAttribute("title"));
		taxCodeInJournalConfirmation = taxCodeInConfirmationJournal.getAttribute("title");
		}
		catch(Exception e){}
		UtilityMethods.waitForPageLoadAndPageReady();
		driver.navigate().back();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		driver.navigate().back();
		
	
	}

	public void openJournalToCheckPostedStatus() throws Throwable{


		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait15Seconds();	
		try{
			UtilityMethods.clickElemetJavaSciptExecutor(refreshRecord);
		}
		catch(Exception e){
			System.out.println("NOT ABLE TO REFRESH THE RECORD");
		}
		
		
		try{
				new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(salesOrderHeader));
				UtilityMethods.clickElemetJavaSciptExecutor(salesOrderHeader);
				System.out.println("SALES ORDER BUTTON CLICKED");
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait5Seconds();
				try{
					UtilityMethods.clickElemetJavaSciptExecutor(refreshRecord);
				}
				catch(Exception e){
					System.out.println("NOT ABLE TO REFRESH THE RECORD");
				}
				UtilityMethods.wait7Seconds();
//				new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(inVoiceButtonUnderJournalForLasernetPrint));
//				UtilityMethods.wait3Seconds();	
				UtilityMethods.clickElemetJavaSciptExecutor(inVoiceButtonUnderJournalForLasernetPrint);
				System.out.println("ORDER HAS BEEN INVOICED SUCCESSFULLY 1");
				Reporter.addStepLog("ORDER HAS BEEN INVOICED SUCCESSFULLY ");
				System.out.println("USER HAS CLICKED ON INVOICE BUTTON UNDER JOURNAL 1");
				Reporter.addStepLog("USER HAS CLICKED ON INVOICE BUTTON UNDER JOURNAL");
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait5Seconds();
			
			}
		catch(Exception e){
				System.out.println("PROBLEM IS: "+e.getMessage());
				boolean check = true;
					try{
							if(driver.findElements(By.xpath("(//span[contains(text(),'Sales order') and @class = 'appBarTab-headerLabel allowFlyoutClickPropagation'])[2]")).size()!=0)
						{
			
								new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(salesOrderHeader2));
								salesOrderHeader2.click();
								System.out.println("SALES ORDER BUTTON CLICKED");
								UtilityMethods.waitForPageLoadAndPageReady();
								UtilityMethods.wait5Seconds();
								try{
									UtilityMethods.clickElemetJavaSciptExecutor(refreshRecord);
								}
								catch(Exception i){
									System.out.println("NOT ABLE TO REFRESH THE RECORD");
								}
								UtilityMethods.wait7Seconds();	
								new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(inVoiceButtonUnderJournalForLasernetPrint));
								UtilityMethods.wait7Seconds();
								UtilityMethods.clickElemetJavaSciptExecutor(inVoiceButtonUnderJournalForLasernetPrint);
								System.out.println("ORDER HAS BEEN INVOICED SUCCESSFULLY 2");
								Reporter.addStepLog("ORDER HAS BEEN INVOICED SUCCESSFULLY");
								System.out.println("USER HAS CLICKED ON INVOICE BUTTON UNDER JOURNAL 2");
								Reporter.addStepLog("USER HAS CLICKED ON INVOICE BUTTON UNDER JOURNAL");
								UtilityMethods.waitForPageLoadAndPageReady();
								check = false;
								UtilityMethods.wait5Seconds();
	
						}
						}
						catch(Exception d)
						{
							System.out.println("Problem is: "+e.getMessage());
							new Actions(driver).sendKeys(Keys.ESCAPE).perform();
							UtilityMethods.waitForPageLoadAndPageReady();
							UtilityMethods.wait7Seconds();
						}	
						
				try{
						if(driver.findElements(By.xpath("(//span[contains(text(),'Sales order') and @class = 'appBarTab-headerLabel allowFlyoutClickPropagation'])[2]")).size()!=0 && check == true)
						{
							new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(salesOrderHeader2));
							UtilityMethods.clickElemetJavaSciptExecutor(salesOrderHeader2);
							System.out.println("SALES ORDER BUTTON CLICKED");
							UtilityMethods.waitForPageLoadAndPageReady();
							UtilityMethods.wait5Seconds();
							try{
								UtilityMethods.clickElemetJavaSciptExecutor(refreshRecord);
							}
							catch(Exception i){
								System.out.println("NOT ABLE TO REFRESH THE RECORD");
							}
							UtilityMethods.wait7Seconds();	
							new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(inVoiceButtonUnderJournalForLasernetPrint));
							UtilityMethods.wait7Seconds();
							UtilityMethods.clickElemetJavaSciptExecutor(inVoiceButtonUnderJournalForLasernetPrint);
							System.out.println("ORDER HAS BEEN INVOICED SUCCESSFULLY 3");
							Reporter.addStepLog("ORDER HAS BEEN INVOICED SUCCESSFULLY");
							System.out.println("USER HAS CLICKED ON INVOICE BUTTON UNDER JOURNAL 3");
							Reporter.addStepLog("USER HAS CLICKED ON INVOICE BUTTON UNDER JOURNAL");
							UtilityMethods.waitForPageLoadAndPageReady();
							UtilityMethods.wait5Seconds();
						}
					}
				catch(Exception h){
							if(check==true){
							    System.out.println("PROBLEM IS: "+e.getMessage());
								UtilityMethods.clickElemetJavaSciptExecutor(salesOrderHeader);
								System.out.println("SALES ORDER BUTTON CLICKED");
								UtilityMethods.waitForPageLoadAndPageReady();
								UtilityMethods.wait5Seconds();
								try{
									UtilityMethods.clickElemetJavaSciptExecutor(refreshRecord);
								}
								catch(Exception u){
									System.out.println("NOT ABLE TO REFRESH THE RECORD");
								}
								UtilityMethods.wait7Seconds();	
							//	new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(inVoiceButtonUnderJournalForLasernetPrint));
							//	UtilityMethods.wait7Seconds();
								
								UtilityMethods.clickElemetJavaSciptExecutor(inVoiceButtonUnderJournalForLasernetPrint);
								System.out.println("ORDER HAS BEEN INVOICED SUCCESSFULLY 4");
								Reporter.addStepLog("ORDER HAS BEEN INVOICED SUCCESSFULLY");
								System.out.println("USER HAS CLICKED ON INVOICE BUTTON UNDER JOURNAL 4");
								Reporter.addStepLog("USER HAS CLICKED ON INVOICE BUTTON UNDER JOURNAL");
								UtilityMethods.waitForPageLoadAndPageReady();
								UtilityMethods.wait5Seconds();
							}	
					
					}
				
				}
		
	//	inVoiceButtonUnderJournalForLasernetPrint.click();
		UtilityMethods.wait5Seconds();
		String paymentStatus;
		try{
			paymentStatus = driver.findElement(By.xpath("//input[contains(@id,'CustInvoiceJour_TMVPaymentStatus_input')]")).getAttribute("title");
		}
		catch(Exception e){
		
			try{
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait15Seconds();
				UtilityMethods.wait5Seconds();
				try{
					UtilityMethods.clickElemetJavaSciptExecutor(refreshRecord);
				}
				catch(Exception u){
					System.out.println("NOT ABLE TO REFRESH THE RECORD");
				}
				UtilityMethods.wait5Seconds();
				UtilityMethods.clickElemetJavaSciptExecutor(inVoiceButtonUnderJournalForLasernetPrint);
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait5Seconds();
				paymentStatus = driver.findElement(By.xpath("//input[contains(@id,'CustInvoiceJour_TMVPaymentStatus_input')]")).getAttribute("title");
			}
			catch(Exception d){
		
					paymentStatus = driver.findElement(By.xpath("//input[contains(@id,'_TMVStatus_input')]")).getAttribute("title");
				
			}
		}
		//Customer Balance in ERP comes in Dots
		
		try{
			if(paymentStatus.contains("Posted")){
	
				System.out.println("PAYMENT IS SUCCESSFUL");
				UtilityMethods.validateAssertEqual("Equal", "Equal");
				System.out.println(paymentStatus +" = "+paymentStatus);
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("PAYMENT STATUS :"+paymentStatus);
	
			}
			else{
				
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("PAYMENT STATUS :"+paymentStatus);
				UtilityMethods.validateAssertEqual("Equal", "Not Equal");
			}
		
		}
		catch(ComparisonFailure failure){
			if(driver.findElement(By.xpath("(//input[@name='CustInvoiceJour_TMVPaymentStatus'])[3]")).getAttribute("title").contains("Posted")){
				
				System.out.println("PAYMENT IS SUCCESSFUL");
				UtilityMethods.validateAssertEqual("Equal", "Equal");
				System.out.println(driver.findElement(By.xpath("(//input[@name='CustInvoiceJour_TMVPaymentStatus'])[3]")).getAttribute("title") +" = "+driver.findElement(By.xpath("(//input[@name='CustInvoiceJour_TMVPaymentStatus'])[3]")).getAttribute("title"));
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("PAYMENT STATUS :"+driver.findElement(By.xpath("(//input[@name='CustInvoiceJour_TMVPaymentStatus'])[3]")).getAttribute("title"));
	
			}
			else{
	
				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("PAYMENT STATUS :"+driver.findElement(By.xpath("(//input[@name='CustInvoiceJour_TMVPaymentStatus'])[3]")).getAttribute("title"));
				UtilityMethods.validateAssertEqual("Equal", "Not Equal");
			}
		}
		
		int index = driver.findElement(By.name("CustInvoiceJour_InvoiceNum_Grid")).getAttribute("title").indexOf("Click");
		invoiceNumber = driver.findElement(By.name("CustInvoiceJour_InvoiceNum_Grid")).getAttribute("title").substring(0,index).trim();
		System.out.println("Invoice Number is:"+invoiceNumber);
		Reporter.addStepLog("Invoice Number is:"+invoiceNumber);


	}

	
	public void validationOverInvoiceForCRM() throws Throwable{
		
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		
		new Actions(driver).moveToElement(total).click().build().perform();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		
		System.out.println("INVOICE SUB TOTAL IS: "+invoiceSubTotalInErp.getAttribute("title"));
		if(invoiceSubTotalInErp.getAttribute("title").contains(baseInJournalConfirmation)){
			
			System.out.println("Both invoice and confirmations journals has same base amount with tax");
			Reporter.addStepLog("INVOICE BASE AMOUNT: "+invoiceSubTotalInErp.getAttribute("title")+" CONFIRMATION BASE AMOUNT: "+baseInJournalConfirmation);
			
		}
		else{
			
			System.out.println("Both invoice and confirmations journals has not the same base amount with tax");
		}
		
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("INVOICE SUB TOTAL IS: "+invoiceSubTotalInErp.getAttribute("title"));
		
		System.out.println("INVOICE TOTAL SALES TAX IS: "+invoiceSalesTaxInErp.getAttribute("title"));
		String totalSalesTaxOnInvoiceTotalTab = invoiceSalesTaxInErp.getAttribute("title");
		if(invoiceSalesTaxInErp.getAttribute("title").contains(totalTaxInJournalConfirmation)){
			
			System.out.println("TAX MATCHES: TAX ON INVOICE IS: "+invoiceAmountInErp.getAttribute("title")+"\n TAX ON CONFIRMATION JOURNAL IS: "+totalTaxInJournalConfirmation);
			Reporter.addStepLog("TAX MATCHES: TAX ON INVOICE IS: "+invoiceAmountInErp.getAttribute("title")+"\n TAX ON CONFIRMATION JOURNAL IS: "+totalTaxInJournalConfirmation);

		}
		else{
			
			System.out.println("TAX DOESNOT MATCHES: TAX ON INVOICE IS: "+invoiceAmountInErp.getAttribute("title")+"\n TAX ON CONFIRMATION JOURNAL IS: "+totalTaxInJournalConfirmation);
		}
		Reporter.addStepLog("INVOICE TOTAL SALES TAX IS: "+totalSalesTaxOnInvoiceTotalTab);
		
		System.out.println("INVOICE TOTAL AMOUNT IS: "+invoiceAmountInErp.getAttribute("title"));
		if(invoiceAmountInErp.getAttribute("title").contains(amountWithTaxInJournalConfirmation)){
			
			System.out.println("Both invoice and confirmations journals has same amount with tax");
			
		}
		else{
			
			System.out.println("Both invoice and confirmations journals has not the same amount with tax");
		}
		Reporter.addStepLog("INVOICE TOTAL AMOUNT IS: "+invoiceAmountInErp.getAttribute("title"));
		
		new Actions(driver).moveToElement(closeButton1).click().perform();	
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		
		new Actions(driver).moveToElement(postedSalesTaxButton).click().build().perform();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
	
		for(int i=1; i<=3; i++){
			
			WebElement general = driver.findElement(By.xpath("(//span[contains(text(),'General') and @class='pivot-label'])["+i+"]"));
			UtilityMethods.clickElemetJavaSciptExecutor(general);
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait10Seconds();
			
			if(driver.findElements(By.name("TaxGroups_TaxGroup")).size()!=0){
				String salesTaxGroupOnPostedSalesTaxValue = salesTaxGroupOnPostedSalesTax.getAttribute("title").trim();
				int index = salesTaxGroupOnPostedSalesTaxValue.indexOf("C");
				System.out.println("SALES TAX GROUP ON INVOICE IS: "+salesTaxGroupOnPostedSalesTaxValue.substring(0, index));
	//			Reporter.addStepLog("SALES TAX GROUP ON INVOICE IS: "+salesTaxGroupOnPostedSalesTaxValue.substring(0, index));
				break;
				
			}
			
		}	
		
		String salesTaxGroupOnPostedSalesTaxValue = salesTaxGroupOnPostedSalesTax.getAttribute("title").trim();
		int index = salesTaxGroupOnPostedSalesTaxValue.indexOf("C");
		System.out.println("SALES TAX GROUP ON INVOICE IS: "+salesTaxGroupOnPostedSalesTaxValue.substring(0, index));
	//	Reporter.addStepLog("SALES TAX GROUP ON INVOICE IS: "+salesTaxGroupOnPostedSalesTaxValue.substring(0, index));


		if(salesTaxGroupOnPostedSalesTaxValue.substring(0, index)==null || salesTaxGroupOnPostedSalesTaxValue.substring(0, index)==""){
			System.out.println("SALES TAX GROUP ON INVOICE IS EMPTY");
		}
		else{
			Reporter.addStepLog("SALES TAX GROUP ON INVOICE IS: "+salesTaxGroupOnPostedSalesTaxValue.substring(0, index));
		}
//		if(salesTaxGroupOnPostedSalesTaxValue.substring(0, index).contains(taxCodeInJournalConfirmation)){
//			
//			System.out.println("SALES TAX CODE MATCHES ON CONTRACT AND CONFIRMATION JOURNAL");
//		Reporter.addStepLog("SALES TAX GROUP ON INVOICE IS: "+salesTaxGroupOnPostedSalesTaxValue.substring(0, index));
//	
//		}
//		else{
//			System.out.println("SALES TAX CODE DOES NOT MATCHES ON CONTRACT AND CONFIRMATION JOURNAL");
//		}
//		
//		String taxCodePostedSalesTaxValue = taxCodePostedSalesTax.getAttribute("title").trim();
//		int index1 = taxCodePostedSalesTaxValue.indexOf("C");
//		System.out.println("SALES TAX CODE ON INVOICE IS: "+taxCodePostedSalesTaxValue.substring(0, index1));
//		if(taxCodePostedSalesTaxValue.substring(0, index1).contains(taxCodeInJournalConfirmation)){
//			System.out.println("SALES TAX CODE MATCHES ON CONTRACT AND CONFIRMATION JOURNAL 2ND ONE");
//		}
//
//		Reporter.addStepLog("SALES TAX CODE ON INVOICE IS: "+taxCodePostedSalesTaxValue.substring(0, index1));
//		
		
		driver.navigate().back();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		
		driver.navigate().back();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		
	}
	
	public void validateInoviceInCRM() throws Throwable{
		
		UtilityMethods.switchToFirstTab();
		cmpPage.validateAccountValuesAfterOrderConfirmation();
		UtilityMethods.switchToNewTab();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		
		
	}
	
	public void openingCustomerDetailsAndCreatingTV14B0001Order() throws Throwable{

//		WebDriverWait wait = new WebDriverWait(driver,30);
//
//		Actions action = new Actions(driver);
//		new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(customerOrderNumberValue));
//		action.contextClick(customerOrderNumberValue).perform();
//		UtilityMethods.wait1Seconds();
//		openSalesOrderDetails.click();
//
//		wait.until(ExpectedConditions.visibilityOf(clickSELLbuttonOnCustomerDetailPage));
//		UtilityMethods.wait10Seconds();
//		clickSELLbuttonOnCustomerDetailPage.click();
//		wait.until(ExpectedConditions.visibilityOf(salesOrderButtonOnCustomerDetailPage));
//		salesOrderButtonOnCustomerDetailPage.click();
//		UtilityMethods.waitForPageLoadAndPageReady();
//		wait.until(ExpectedConditions.visibilityOf(salesOrderNumber));
//		
//		
//		salesOrderNumberOnOrder = salesOrderNumber.getText().substring(0,9); 
//		System.out.println("SALES ORDER NUMBER :"+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
//		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
//		Reporter.addStepLog("SALES ORDER NUMBER :"+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
//		itemNumberInputField.click();
		
		UtilityMethods.wait5Seconds();
		System.out.println("SALES QUOTATION NUMBER :"+"<b>"+salesOrderNumberAfterQoutation.getText()+"<b>");
		salesQuotationNumber = salesOrderNumberAfterQoutation.getText().substring(0,8);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES QUOTATION NUMBER :"+"<b>"+salesOrderNumberAfterQoutation.getText().substring(0,10)+"<b>");
		itemNumberInputField.click();

		UtilityMethods.wait1Seconds();

		itemNumberInputField.sendKeys("TV14B0001");
		UtilityMethods.wait7Seconds();
		try{
		perpetualBusinessInputValue.click();
		}
		catch(Exception e)
		{
			perpetualBusinessInputValue1.click();
		}
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait20Seconds();
		String productNameinText;
		try{
			productNameinText = productNameOfFirstOrder.getAttribute("title");
		}
		catch(Exception e){
			productNameinText = productNameOfFirstOrder2.getAttribute("title");
		}

		if(productNameinText.contains("TeamViewer 14 Business / TeamViewer 14 Business")){


			UtilityMethods.validateAssertEqual("Perpetual Business 14 is Added", "Perpetual Business 14 is Added");

		}
		else{

			UtilityMethods.validateAssertEqual("Perpetual Business 14 is Added", "Perpetual Business 14 is NOT Added");
		}

		System.out.println("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
		
		UtilityMethods.wait5Seconds();
		new Actions(driver).moveToElement(saveButton).click().build().perform();;
		UtilityMethods.wait5Seconds();

		/////////////////////////////////////

		if(CRMPage.flagForSalesOrderFromCRM ==  0){
		String isQoutationCreated = qoutationStatusOnERPIfCreated.getAttribute("title");

		if(isQoutationCreated.contains("Create")){

			System.out.println("QOUTATION HAS BEEN SUCCESSFULLY CREATED ON ERP");
		}
		////////////////////////////
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		}
		
		UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.xpath("//span[contains(text(),'Price information')]")));
		new Actions(driver).moveToElement(driver.findElement(By.xpath("//span[contains(text(),'Price information')]"))).click().build().perform();
		// ENTER DISCOUNTS ETC HERE
		UtilityMethods.wait3Seconds();
if(CRMPage.flagForSalesOrderFromCRM == 0){
			
			try{
				priceInfromationAddLine.click();
				}
				catch(NoSuchElementException e){
					
					UtilityMethods.wait3Seconds();
					priceInfromationAddLine.click();
					
				}
		
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait2Seconds();
		
		new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(reasonCodeTVB));
		reasonCodeTVB.clear();
		reasonCodeTVB.sendKeys("17");
		reasonCodeTVB.sendKeys(Keys.TAB);
		UtilityMethods.wait1Seconds();
		if(!discountMethodDropDownTVB.getAttribute("title").contains("Discount percentage")){
			discountMethodDropDownTVB.click();
			UtilityMethods.waitForPageLoadAndPageReady();
		//	new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(discountPercentageMethod));
			UtilityMethods.wait5Seconds();
			discountPercentageMethod.click();
			discountMethodDropDownTVB.sendKeys(Keys.TAB);
		}
		else{
			
		//	System.out.println(discountMethodDropDownTVB.getAttribute("title"));
			discountMethodDropDownTVB.sendKeys(Keys.TAB);
		}
		
		discountPercentage.clear();
		discountPercentage.sendKeys("10");
		discountPercentage.sendKeys(Keys.ENTER);
		UtilityMethods.waitForPageLoadAndPageReady();
	
		}else if(CRMPage.flagForSalesOrderFromCRM == 1){
			
			try{
				priceInfromationAddLine1.click();
				}
				catch(NoSuchElementException e){
					
					UtilityMethods.wait3Seconds();
					priceInfromationAddLine1.click();
					
				}
		
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait2Seconds();
		
		new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(reasonCodeTVB1));
		reasonCodeTVB1.clear();
		reasonCodeTVB1.sendKeys("17");
		reasonCodeTVB1.sendKeys(Keys.TAB);
		UtilityMethods.wait2Seconds();
		if(!discountMethodDropDownTVB1.getAttribute("title").contains("Discount percentage")){
			discountMethodDropDownTVB1.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(discountPercentageMethod));
			UtilityMethods.wait2Seconds();
			discountPercentageMethod.click();
			discountMethodDropDownTVB1.sendKeys(Keys.TAB);
		}
		else{
			
		//	System.out.println(discountMethodDropDownTVB.getAttribute("title"));
			discountMethodDropDownTVB1.sendKeys(Keys.TAB);
		}
		
		discountPercentage1.clear();
		discountPercentage1.sendKeys("10");
		discountPercentage1.sendKeys(Keys.ENTER);
		UtilityMethods.waitForPageLoadAndPageReady();
			
		}
	}

	public void openingCustomerDetailsAndPremiumOrderforCRMFlow() throws Throwable{


		UtilityMethods.wait5Seconds();
		try{
		System.out.println("SALES QUOTATION NUMBER :"+"<b>"+salesOrderNumberAfterQoutation.getText().substring(0,10)+"<b>");
		salesQuotationNumber = salesOrderNumberAfterQoutation.getText().substring(0,10);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("SALES QUOTATION NUMBER :"+"<b>"+salesOrderNumberAfterQoutation.getText().substring(0,10)+"<b>");
		}
		catch(Exception e){
			
			UtilityMethods.clickElemetJavaSciptExecutor(driver.findElement(By.name("LineStripNew")));
			UtilityMethods.wait5Seconds();
			// should look into here}
		}
		if(modification == true){
			
			System.out.println("Modification value is: "+modification);
			new Actions(driver).moveToElement(driver.findElement(By.xpath("(//input[contains(@id,'ItemId_input')])[4]"))).click().perform();

		UtilityMethods.wait3Seconds();
		driver.findElement(By.xpath("(//input[contains(@id,'ItemId_input')])[4]")).sendKeys("TVP0001");
		}
		else{
			itemNumberInputField.click();
			UtilityMethods.wait3Seconds();
			itemNumberInputField.sendKeys("TVP0001");
		}
		
		UtilityMethods.wait5Seconds();

		if(premiumLicenseInputValue.isDisplayed()){

			premiumLicenseInputValue.click();

		}
		
		UtilityMethods.wait3Seconds();
		try{
			productNameinText = productNameOfSalesQoutationOrder.getAttribute("title");
		}
		catch(Exception e){
			productNameinText = productNameOfSalesQoutationOrder2.getAttribute("title");
		}

		try{
			if(productNameinText.equals("TeamViewer Premium / TeamViewer Premium Yearly Subscription")){

				System.out.println("Premium License is Added");
				UtilityMethods.validateAssertEqual("Premium License is Added", "Premium License is Added");

			}
		}
		catch(Exception e){

			System.out.println("Premium License is not Added");
			UtilityMethods.validateAssertEqual("Premium License is Added", "Premium License is not Added");
		}

		System.out.println("USER HAS CREATED A QOUTATION ORDER: "+productNameinText);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);


		UtilityMethods.wait5Seconds();
		UtilityMethods.clickElemetJavaSciptExecutor(saveButton);

		UtilityMethods.wait5Seconds();	
		
		if(modification==false){
			System.out.println("Modification value is: "+modification);
			try{
				String isQoutationCreated = qoutationStatusOnERPIfCreated.getAttribute("title");
				System.out.println("Value is: "+isQoutationCreated);
					if(isQoutationCreated.contains("Create")){
		
						System.out.println("QUOTATION HAS BEEN SUCCESSFULLY CREATED ON ERP");
					}
				productAddOn();
			}
			catch(NoSuchElementException e){
				System.out.println("Flow is against an order");
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait3Seconds();
				UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.xpath("//span[contains(text(),'Price information')]")));
				new Actions(driver).moveToElement(driver.findElement(By.xpath("//span[contains(text(),'Price information')]"))).click().build().perform();
				// ENTER DISCOUNTS ETC HERE
				UtilityMethods.wait3Seconds();
				orderFlowForAddon();
			}
		
		}
	
	}

	public void productAddOn() throws InterruptedException {
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.xpath("//span[contains(text(),'Price information')]")));
		new Actions(driver).moveToElement(driver.findElement(By.xpath("//span[contains(text(),'Price information')]"))).click().build().perform();
		// ENTER DISCOUNTS ETC HERE
		UtilityMethods.wait3Seconds();
		try{
			addline1.click();
		
		}
		catch(NoSuchElementException e){
			addline2.click();
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait2Seconds();
		try{
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(reasonCodeTVB3));
			reasonCodeTVB3.clear();
			reasonCodeTVB3.sendKeys("17");
			reasonCodeTVB3.sendKeys(Keys.TAB);
		}
		catch(Exception e){
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(reasonCodeTVB));
			reasonCodeTVB.clear();
			reasonCodeTVB.sendKeys("17");
			reasonCodeTVB.sendKeys(Keys.TAB);
		}
		try{
		UtilityMethods.wait1Seconds();
		if(!discountMethodDropDownTVB.getAttribute("title").contains("Discount percentage")){
			discountMethodDropDownTVB.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(discountPercentageMethod));
			UtilityMethods.wait2Seconds();
			discountPercentageMethod.click();
			discountMethodDropDownTVB.sendKeys(Keys.TAB);
		}
		else{
			
		//	System.out.println(discountMethodDropDownTVB.getAttribute("title"));
			discountMethodDropDownTVB.sendKeys(Keys.TAB);
		}
		}
		catch(Exception e){
			UtilityMethods.wait1Seconds();
			if(!discountMethodDropDownTVB4.getAttribute("title").contains("Discount percentage")){
				discountMethodDropDownTVB4.click();
				UtilityMethods.waitForPageLoadAndPageReady();
				new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(discountPercentageMethod));
				UtilityMethods.wait2Seconds();
				discountPercentageMethod.click();
				discountMethodDropDownTVB4.sendKeys(Keys.TAB);
			}
			else{
				
			//	System.out.println(discountMethodDropDownTVB.getAttribute("title"));
				discountMethodDropDownTVB4.sendKeys(Keys.TAB);
			}
		}
		try{
			discountPercentage.clear();
			discountPercentage.sendKeys("10");
			discountPercentage.sendKeys(Keys.ENTER);
			UtilityMethods.waitForPageLoadAndPageReady();
		}
		catch(Exception e){
			
			discountPercentage3.clear();
			discountPercentage3.sendKeys("10");
			discountPercentage3.sendKeys(Keys.ENTER);
			UtilityMethods.waitForPageLoadAndPageReady();
		}
		
	}

	public void addMobileAddOns() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		UtilityMethods.scrollToWebElement(driver.findElement(By.xpath("(//span[contains(text(),'Contract')])[1]")));
		try{
			productNameOfSalesQoutationOrder.click();
		}
		catch(Exception e){
			productNameOfSalesQoutationOrder2.click();
		}
	//	new Actions(driver).moveToElement(driver.findElement(By.xpath("(//span[contains(text(),'Contract')])[1]"))).doubleClick(driver.findElement(By.xpath("(//span[contains(text(),'Contract')])[1]"))).build().perform();
		UtilityMethods.wait7Seconds();
		try{
			UtilityMethods.waitForElementEnabled(driver.findElement(By.xpath("//span[contains(text(),'Sell add-ons')]")), 10);
			driver.findElement(By.xpath("//span[contains(text(),'Sell add-ons')]")).click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
		}
		catch(Exception e){
			
			UtilityMethods.clickElemetJavaSciptExecutor(driver.findElement(By.xpath("(//span[contains(text(),'Contract')])[1]")));
			UtilityMethods.wait7Seconds();
			driver.findElement(By.xpath("//span[contains(text(),'Sell add-ons')]")).click();
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		
		new WebDriverWait(driver,20).until(ExpectedConditions.elementToBeClickable(mobileAddOn));
		mobileAddOn.click();
		UtilityMethods.wait3Seconds();
		clickOkForConfirmSalesOrder.click();
		System.out.println("USER HAS SELECTED MOBILE SUPPORT ADD ON");
		Reporter.addStepLog("USER HAS SELECTED MOBILE SUPPORT ADD ON");
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.xpath("//span[contains(text(),'Price information')]")));
		new Actions(driver).moveToElement(driver.findElement(By.xpath("//span[contains(text(),'Price information')]"))).click().build().perform();
		// ENTER DISCOUNTS ETC HERE

		UtilityMethods.wait3Seconds();
		UtilityMethods.scrollToWebElement(driver.findElement(By.xpath("(//span[contains(text(),'Add line')])[3]")));
		UtilityMethods.clickElemetJavaSciptExecutor(driver.findElement(By.xpath("(//span[contains(text(),'Add line')])[3]")));
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		try{
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(reasonCodeMDS2));
		//	reasonCodeMDS2.click();
			reasonCodeMDS2.clear();
			reasonCodeMDS2.sendKeys("17");
			reasonCodeMDS2.sendKeys(Keys.TAB);
		}
		catch(Exception e){
			System.out.println("Reason code failed due to :"+e.getMessage());
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(reasonCodeMDS));
		//	reasonCodeMDS.click();
			reasonCodeMDS.clear();
			reasonCodeMDS.sendKeys("17");
			reasonCodeMDS.sendKeys(Keys.TAB);
		}
		Reporter.addStepLog("REASON CODE IS SELECTED 17 FOR MOBILE ADD ON SUPPORT");
		UtilityMethods.wait2Seconds();

		try{
			discountMethodDropDownMDS.click();
		}
		catch(Exception e){
			discountMethodDropDownMDS2.click();
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(targetAmount));
		UtilityMethods.wait2Seconds();
		targetAmount.click();
		try{
			discountMethodDropDownMDS.sendKeys(Keys.ENTER);
		}
		catch(Exception e){
			discountMethodDropDownMDS2.sendKeys(Keys.ENTER);
		}
		Reporter.addStepLog("TARGET AMOUNT VALUE IS SELECTED IN DISCOUNT PERCENTAGE METHOD");
		float targetAmountForMobile;
		try{
			try{
				targetAmountForMobile = Float.parseFloat(targetAmountValueForMobileAddOn.getAttribute("title"))-20;
			}
			catch(NumberFormatException e){
				targetAmountForMobile = Float.parseFloat(targetAmountValueForMobileAddOn.getAttribute("title").replace(",", "").trim())-50;
			}
			System.out.println("Mobile discounted target amount is "+targetAmountForMobile);
			UtilityMethods.wait2Seconds();
			targetAmountValueForMobileAddOn.clear();
			targetAmountValueForMobileAddOn.sendKeys(Float.toString(targetAmountForMobile));
			targetAmountValueForMobileAddOn.sendKeys(Keys.TAB);
			Reporter.addStepLog("TARGET AMOUNT SELECTED FOR MOBILE ADD ON IS: "+Float.toString(targetAmountForMobile));
		}
		catch(Exception e){
			try{
				targetAmountForMobile = Float.parseFloat(targetAmountValueForMobileAddOn2.getAttribute("title"))-20;
			}
			catch(NumberFormatException k){
				targetAmountForMobile = Float.parseFloat(targetAmountValueForMobileAddOn2.getAttribute("title").replace(",", "").trim())-50;
			}
			System.out.println("Mobile discounted target amount is "+targetAmountForMobile);
			UtilityMethods.wait2Seconds();
			targetAmountValueForMobileAddOn2.clear();
			targetAmountValueForMobileAddOn2.sendKeys(Float.toString(targetAmountForMobile));
			targetAmountValueForMobileAddOn2.sendKeys(Keys.TAB);
			Reporter.addStepLog("TARGET AMOUNT SELECTED FOR MOBILE ADD ON IS: "+Float.toString(targetAmountForMobile));
		}
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		try{
			System.out.println("Cash Discount amount is: "+cashDiscountAmount.getAttribute("title"));
			if(cashDiscountAmount.getAttribute("title").contains("50")){
				
				System.out.println("Cash Discount amount is: "+cashDiscountAmount.getAttribute("title"));
				Reporter.addStepLog("CASH DISCOUNT AMOUNT IS: "+cashDiscountAmount.getAttribute("title"));
				
			}
		}
		catch(Exception e){
			System.out.println("Cash Discount amount is: "+cashDiscountAmount2.getAttribute("title"));
			if(cashDiscountAmount2.getAttribute("title").contains("50")){
				
				System.out.println("Cash Discount amount is: "+cashDiscountAmount2.getAttribute("title"));
				Reporter.addStepLog("CASH DISCOUNT AMOUNT IS: "+cashDiscountAmount2.getAttribute("title"));
				
			}
		}
		
	}
	
	   public void contractCorrectionOfSalesOrder() throws Throwable{
	    	
	    	UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();

			if(driver.findElements(By.xpath("//span[contains(text(),'Open Lasernet setup')]")).size()!=0){
				UtilityMethods.waitForPageLoadAndPageReady();
				
				driver.navigate().back();
				
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait3Seconds();
			}
			try{
			UtilityMethods.scrollToWebElement(contractButtonSalesOrder);
			new Actions(driver).moveToElement(contractButtonSalesOrder).doubleClick(contractButtonSalesOrder).build().perform();
			UtilityMethods.wait3Seconds();
			}
			catch(Exception e){
				driver.navigate().back();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait5Seconds();
				UtilityMethods.scrollToWebElement(contractButtonSalesOrder);
				new Actions(driver).moveToElement(contractButtonSalesOrder).doubleClick(contractButtonSalesOrder).build().perform();
				UtilityMethods.wait3Seconds();
			}
			
			try{
				
				UtilityMethods.clickElemetJavaSciptExecutor(contractCorrectionButtonSalesOrder);
			}
			catch(Exception e){
				
				UtilityMethods.clickElemetJavaSciptExecutor(contractButtonSalesOrder);
				UtilityMethods.wait3Seconds();
				if(driver.findElements(By.xpath("//span[contains(@id,'TMVContractCorrectionBtn_label')]")).size()!=0){
				contractCorrectionButtonSalesOrder.click();
				}
				else
				{
					UtilityMethods.clickElemetJavaSciptExecutor(contractButtonSalesOrder);
					UtilityMethods.wait3Seconds();
					contractCorrectionButtonSalesOrder.click();
				}
			}
			
			UtilityMethods.wait5Seconds();
			yes.click();	
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
	    	System.out.println("USER IS ON CONTRACT CORRECTION SCREEN");
	    	
	    	contractCorrectionSalesTaxGroupInput.clear();
	    	contractCorrectionSalesTaxGroupInput.sendKeys("C-NL-RC");
	    	contractCorrectionSalesTaxGroupInput.sendKeys(Keys.TAB);
	    	System.out.println("USER HAS CHANGED SALES TAX GROUP TO C-NL-RC");
	    	
	    	UtilityMethods.wait5Seconds();
	    	contractCorrectionTaxExemptNumberInput.clear();
	    	contractCorrectionTaxExemptNumberInput.sendKeys("NL812749479B01");
	    	contractCorrectionTaxExemptNumberInput.sendKeys(Keys.TAB);
	    	System.out.println("USER HAS CHANGED TAX EXEMPT NUMBER TO VALID TAX NUMBER");

	    	UtilityMethods.wait7Seconds();
			contractCorrectionAddDiscountLineButton.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait2Seconds();
			try{
				new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(contractCorrectionDiscountLineReasonCode));
			}
			catch(Exception e){
				UtilityMethods.clickElemetJavaSciptExecutor(contractCorrectionAddDiscountLineButton);
				UtilityMethods.waitForPageLoadAndPageReady();
				new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(contractCorrectionDiscountLineReasonCode));
			}
			contractCorrectionDiscountLineReasonCode.clear();
			contractCorrectionDiscountLineReasonCode.sendKeys("17");
			contractCorrectionDiscountLineReasonCode.sendKeys(Keys.TAB);
	    	System.out.println("USER HAS SET DISCOUNT REASON CODE: 17");
			UtilityMethods.wait1Seconds();
			if(!contractCorrectionDiscountMethodDropDown.getAttribute("title").contains("Discount percentage")){
				contractCorrectionDiscountMethodDropDown.click();
				UtilityMethods.waitForPageLoadAndPageReady();
				new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(discountPercentageMethod));
				UtilityMethods.wait2Seconds();
				discountPercentageMethod.click();
				contractCorrectionDiscountMethodDropDown.sendKeys(Keys.TAB);
			}
			else{
				
			//	System.out.println(discountMethodDropDownTVB.getAttribute("title"));
				contractCorrectionDiscountMethodDropDown.sendKeys(Keys.TAB);
			}
			
			System.out.println("USER HAS SET DISCOUNT METHOD CODE: DISCOUNT PERCENTAGE");
			contractCorrectionDiscountPercentage.clear();
			contractCorrectionDiscountPercentage.sendKeys("10");
			contractCorrectionDiscountPercentage.sendKeys(Keys.ENTER);
			System.out.println("USER HAS SET DISCOUNT PERCENTAGE CODE: 10");
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			try{
				UtilityMethods.scrollToWebElement(contractCorrectionDeliveryAddressStreetInput);
				UtilityMethods.wait2Seconds();
				contractCorrectionDeliveryAddressStreetInput.click();
				contractCorrectionDeliveryAddressStreetInput.clear();
				UtilityMethods.wait2Seconds();
				contractCorrectionDeliveryAddressStreetInput.sendKeys("New Address");
				UtilityMethods.wait2Seconds();
				contractCorrectionInvoiceAddressStreetInput.click();
				contractCorrectionInvoiceAddressStreetInput.clear();
				UtilityMethods.wait2Seconds();
				contractCorrectionInvoiceAddressStreetInput.sendKeys("New Address");
				System.out.println("USER HAS EDITED THE DELIVERY AND INVOICE ADDRESSES");
				Reporter.addStepLog("USER HAS EDITED THE DELIVERY AND INVOICE ADDRESSES");
			}
			catch(Exception e){
				contractCorrection_CorrectionTab.click();
				UtilityMethods.wait5Seconds();
				System.out.println("USER HAS CLICKED ON CORRECTION TAB TO UPDATE ADDRESSES");
				if(driver.findElements(By.xpath("//textarea[@name='TMVContractCorrection_BuisnessStreet']")).size()!=0){
					UtilityMethods.scrollToWebElement(contractCorrectionDeliveryAddressStreetInput);
					UtilityMethods.wait2Seconds();
					contractCorrectionDeliveryAddressStreetInput.click();
					contractCorrectionDeliveryAddressStreetInput.clear();
					UtilityMethods.wait2Seconds();
					contractCorrectionDeliveryAddressStreetInput.sendKeys("New Address");
					UtilityMethods.wait2Seconds();
					contractCorrectionInvoiceAddressStreetInput.click();
					contractCorrectionInvoiceAddressStreetInput.clear();
					UtilityMethods.wait2Seconds();
					contractCorrectionInvoiceAddressStreetInput.sendKeys("New Address");
					System.out.println("USER HAS EDITED THE DELIVERY AND INVOICE ADDRESSES");
					Reporter.addStepLog("USER HAS EDITED THE DELIVERY AND INVOICE ADDRESSES");
				}
				else{
					contractCorrection_CorrectionTab.click();
					UtilityMethods.wait2Seconds();
					UtilityMethods.scrollToWebElement(contractCorrectionDeliveryAddressStreetInput);
					UtilityMethods.wait2Seconds();
					contractCorrectionDeliveryAddressStreetInput.click();
					contractCorrectionDeliveryAddressStreetInput.clear();
					UtilityMethods.wait2Seconds();
					contractCorrectionDeliveryAddressStreetInput.sendKeys("New Address");
					UtilityMethods.wait2Seconds();
					contractCorrectionInvoiceAddressStreetInput.click();
					contractCorrectionInvoiceAddressStreetInput.clear();
					UtilityMethods.wait2Seconds();
					contractCorrectionInvoiceAddressStreetInput.sendKeys("New Address");
					System.out.println("USER HAS EDITED THE DELIVERY AND INVOICE ADDRESSES");
					Reporter.addStepLog("USER HAS EDITED THE DELIVERY AND INVOICE ADDRESSES");
				}
					
			}
			
				UtilityMethods.wait10Seconds();
				try{
					contractCorrectionSaveButton.click();
				}
				catch(Exception e){
					contractCorrectionSaveButton2.click();
				}
				UtilityMethods.wait5Seconds();
				contractCorrectionCorrectButton.click();
				UtilityMethods.wait5Seconds();
				yes.click();
			
	    	
	    }
	    
		//Adding Premium Product on Sales Order Page
		public void addPremiumProductOnSalesOrder() throws Throwable{
			
			WebDriverWait wait = new WebDriverWait(driver,30);
			wait.until(ExpectedConditions.visibilityOf(salesOrderNumberAfterQoutation));

			salesOrderNumberOnOrder = salesOrderNumberAfterQoutation.getText().substring(0,9); 
			System.out.println("SALES ORDER NUMBER "+salesOrderNumberAfterQoutation.getText().substring(0,10));
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("SALES ORDER NUMBER :"+"<b>"+salesOrderNumberAfterQoutation.getText().substring(0,10)+"<b>");

			itemNumberInputField.click();
			UtilityMethods.wait1Seconds();

			itemNumberInputField.sendKeys("tvp0001");
			UtilityMethods.wait3Seconds();
			premiumLicenseInputValue.click();

			UtilityMethods.wait15Seconds();
			String productNameinText;
			try{
			wait.until(ExpectedConditions.attributeToBe(productNameOfFirstOrder, "title", "TeamViewer Premium / TeamViewer Premium Yearly Subscription"));
			productNameinText = productNameOfFirstOrder.getAttribute("title");
			productNameOfFirstOrder.click();
			}
			catch(Exception e){
				
				wait.until(ExpectedConditions.attributeToBe(productNameOfFirstOrder2, "title", "TeamViewer Premium / TeamViewer Premium Yearly Subscription"));
				productNameinText = productNameOfFirstOrder2.getAttribute("title");
				productNameOfFirstOrder2.click();
			}
			if(productNameinText.equals("TeamViewer Premium / TeamViewer Premium Yearly Subscription")){


				UtilityMethods.validateAssertEqual("Premium License is Added", "Premium License is Added");
				

			}
			else{

				UtilityMethods.validateAssertEqual("Premium License is Added", "Premium License is NOT Added");
			}

			System.out.println("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS CREATED A SIMPLE ORDER: "+productNameinText);
		
			
		}
	
		public void addAddOnInSalesOrder() throws Throwable{
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			UtilityMethods.scrollToWebElement(contractLineStatusButton);
			contractLineStatusButton.click();
	//		new Actions(driver).moveToElement(driver.findElement(By.xpath("(//span[contains(text(),'Contract')])[2]"))).doubleClick(driver.findElement(By.xpath("(//span[contains(text(),'Contract')])[2]"))).build().perform();
			UtilityMethods.wait7Seconds();
			UtilityMethods.clickElemetJavaSciptExecutor(clickOkForConfirmSalesOrder);
			UtilityMethods.wait5Seconds();
			try{
			driver.findElement(By.xpath("//span[contains(text(),'Sell add-ons')]")).click();
			}
			catch(NoSuchElementException e){
				
				UtilityMethods.scrollToWebElement(contractLineStatusButton);
		//		UtilityMethods.clickElemetJavaSciptExecutor(contractButtonSalesOrder);
				UtilityMethods.wait7Seconds();
				UtilityMethods.clickElemetJavaSciptExecutor(clickOkForConfirmSalesOrder);
				UtilityMethods.wait5Seconds();
				driver.findElement(By.xpath("//span[contains(text(),'Sell add-ons')]")).click();
			}
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			new WebDriverWait(driver,20).until(ExpectedConditions.elementToBeClickable(mobileAddOn));
			mobileAddOn.click();
			UtilityMethods.wait3Seconds();
			UtilityMethods.clickElemetJavaSciptExecutor(clickOkForConfirmSalesOrder);
			
			System.out.println("USER HAS SUCCESSFULLY ADDED ADD ON TO SALES ORDER");
			
			UtilityMethods.wait3Seconds();

			UtilityMethods.clickElemetJavaSciptExecutor(okButtonOnCustomerInformation);
			UtilityMethods.wait5Seconds();
			UtilityMethods.waitForPageLoadAndPageReady();
//			UtilityMethods.waitForPageLoadAndPageReady();
//			UtilityMethods.wait3Seconds();
//			UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.xpath("//span[contains(text(),'Price information')]")));
//			new Actions(driver).moveToElement(driver.findElement(By.xpath("//span[contains(text(),'Price information')]"))).click().build().perform();
//			// ENTER DISCOUNTS ETC HERE
	//
//			UtilityMethods.wait3Seconds();
//			driver.findElement(By.xpath("(//span[contains(text(),'Add line')])[3]")).click();
//			UtilityMethods.waitForPageLoadAndPageReady();
//			UtilityMethods.wait2Seconds();
//			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(reasonCodeMDS));
//			reasonCodeMDS.click();
//			reasonCodeMDS.clear();
//			reasonCodeMDS.sendKeys("17");
//			reasonCodeMDS.sendKeys(Keys.TAB);
//			UtilityMethods.wait2Seconds();
	//
//			discountMethodDropDownMDS.click();
//			UtilityMethods.waitForPageLoadAndPageReady();
//			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(targetAmount));
//			UtilityMethods.wait2Seconds();
//			targetAmount.click();
//			
//			discountMethodDropDownMDS.sendKeys(Keys.ENTER);
	//	
//			float targetAmountForMobile = Float.parseFloat(targetAmountValueForMobileAddOn.getAttribute("title"))-50;
//			System.out.println("Mobile discounted target amount is "+targetAmountForMobile);
//			UtilityMethods.wait2Seconds();
//			targetAmountValueForMobileAddOn.clear();
//			targetAmountValueForMobileAddOn.sendKeys(Float.toString(targetAmountForMobile));
//			targetAmountValueForMobileAddOn.sendKeys(Keys.TAB);
//			
//			UtilityMethods.waitForPageLoadAndPageReady();
//			UtilityMethods.wait7Seconds();
//			
//			System.out.println("Cash Discount amount is: "+cashDiscountAmount.getAttribute("title"));
//			if(cashDiscountAmount.getAttribute("title").contains("50")){
//				
//				System.out.println("Cash Discount amount is: "+cashDiscountAmount.getAttribute("title"));
//				
//			}
			
		}
		
		public void validateValuesInCRMOfInvoice() throws Throwable{
			
			UtilityMethods.switchToFirstTab();
			cmpPage.validateAccountValuesAfterOrderInvoiced();
			
			UtilityMethods.switchToNewTab();
			
			
		}
		
		
		public void verifyCreditNote() throws Throwable{
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			UtilityMethods.scrollToWebElement(contractLine1);
			try{
				if(contractLine1.getAttribute("title").contains("Credit") && contractLine2.getAttribute("title").contains("Credit")){
					System.out.println("CREDIT NOTE ADDED SUCCESSFULLY");
					Reporter.addStepLog("CREDIT NOTE ADDED SUCCESSFULLY");
				}
			}
			catch(Exception e){
				
			}
			
			try{
				if(contractLine3.isDisplayed() && contractLine4.isDisplayed()){
					System.out.println("2 additional lines are created with contract line status = created");
					Reporter.addStepLog("2 additional lines are created with contract line status = created");
				}
			}
			catch(Exception e){
				
			}
			
		}
		
		public void modifyContract() throws Throwable{
			
			try{
				new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(modifyButton));
			}
			catch(TimeoutException e){
				UtilityMethods.waitForPageLoadAndPageReady();
				
				driver.navigate().back();
				
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait3Seconds();
			}
	
			UtilityMethods.waitForPageLoadAndPageReady();
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(modifyButton));
			new Actions(driver).moveToElement(modifyButton).click().perform();
			modification = true;
			
		}
		
		public void cancelContractLine() throws Throwable{
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			try{
				driver.findElement(By.xpath("(//input[@name='ItemName'])[5]")).click();
			//	productNameOfSalesQoutationOrder.click();
			}
			catch(Exception e){
				new Actions(driver).sendKeys(Keys.ESCAPE).perform();;
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait3Seconds();
				driver.findElement(By.xpath("(//input[@name='ItemName'])[5]")).click();
			//	 productNameOfSalesQoutationOrder2.click();
			}
			UtilityMethods.wait3Seconds();
			
			try{
				new Actions(driver).moveToElement(contractButtonSalesOrder).click().perform();
				UtilityMethods.wait5Seconds();
				new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(cancelContractLineButton));
				cancelContractLineButton.click();
			}
			catch(Exception e){
				new Actions(driver).moveToElement(contractButtonSalesOrder2).click().perform();
				UtilityMethods.wait5Seconds();
				new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(cancelContractLineButton));
				cancelContractLineButton.click();
			}
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
		//	new Actions(driver).moveToElement(closeButton).click().perform();
			
			// FUNCTION STARTS FROM HERE
			UtilityMethods.waitForPageLoadAndPageReady();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(driver.findElement(By.name("Fld2_1"))));
			driver.findElement(By.name("Fld2_1")).sendKeys("Contract Updating");
			UtilityMethods.wait2Seconds();
			try{
				System.out.println("CLICKING OPPERTUNITY TOGGLE");
				UtilityMethods.clickElemetJavaSciptExecutor(driver.findElement(By.xpath("//span[@aria-label='Create opportunity in CRM']")));
				UtilityMethods.wait3Seconds();
			}
			catch(Exception e){
				System.out.println("CLICKING OPPERTUNITY TOGGLE FAILED");
				UtilityMethods.wait5Seconds();
			}
			
			driver.findElement(By.name("Fld3_1")).sendKeys("To Update Contract");
			UtilityMethods.wait3Seconds();
			new Actions(driver).moveToElement(clickOkForConfirmSalesOrder).click().perform();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();
			
			
		}
		
		public void cancelContract() throws Throwable{
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			try{
				cancelContractButton.click();
			}
			catch(Exception e){
				new Actions(driver).sendKeys(Keys.ESCAPE).build().perform();
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait3Seconds();
				new Actions(driver).moveToElement(cancelContractButton).click().build().perform();
			}
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
		//	new Actions(driver).moveToElement(closeButton).click().perform();
			
			// FUNCTION STARTS FROM HERE
			UtilityMethods.waitForPageLoadAndPageReady();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(driver.findElement(By.name("Fld2_1"))));
			driver.findElement(By.name("Fld2_1")).sendKeys("Contract Updating");
			UtilityMethods.wait2Seconds();
			try{
				System.out.println("CLICKING OPPERTUNITY TOGGLE");
				UtilityMethods.clickElemetJavaSciptExecutor(driver.findElement(By.xpath("//span[@aria-label='Create opportunity in CRM']")));
				UtilityMethods.wait3Seconds();
			}
			catch(Exception e){
				System.out.println("CLICKING OPPERTUNITY TOGGLE FAILED");
				UtilityMethods.wait5Seconds();
			}
			driver.findElement(By.name("Fld3_1")).sendKeys("To Update Contract");
			UtilityMethods.wait3Seconds();
			new Actions(driver).moveToElement(clickOkForConfirmSalesOrder).click().perform();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();
			
			
		}
		
		
		public void transferOrder() throws Throwable{
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			transferFlow = true;
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(tranferButton));
			try{
				tranferButton.click();
			}
			catch(Exception e){
				new Actions(driver).moveToElement(tranferButton).click().build().perform();
			}
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOf(reseller));
			UtilityMethods.wait3Seconds();
			if(CRMPageSteps.ukFlag==true){
				reseller.sendKeys("20402488");
			}
			else{
				reseller.sendKeys("10000047");
				System.out.println("Reseller Account is 4Real ITSolutions");
				Reporter.addStepLog("Reseller Account is 4Real ITSolutions");
			}
			UtilityMethods.wait2Seconds();
			indirect.sendKeys(CRMPage.accountNumber);
			indirect.sendKeys(Keys.TAB);
//			System.out.println("Indirect Account is 4Real ITSolutions");
//			Reporter.addStepLog("Indirect Account is 4Real ITSolutions");
			UtilityMethods.wait5Seconds();
			clickOkForConfirmSalesOrder.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait20Seconds();
			UtilityMethods.moveToElementWithJSExecutor(invoiceActivationLink);
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			try{
				invoiceActivationLink.click();
			}
			catch(Exception e){
				System.out.println("EXCEPTION IS: "+e.getMessage() );
			}
	
			new Actions(driver).moveToElement(activationLink).sendKeys("teamviewer.automation@systemsltd.com").sendKeys(Keys.TAB).build().perform();
			System.out.println("Activiation Link is teamviewer.automation@systemsltd.com");
			Reporter.addStepLog("Activiation Link is teamviewer.automation@systemsltd.com");
			new Actions(driver).moveToElement(invoiceActivationLink).sendKeys("teamviewer.automation@systemsltd.com").sendKeys(Keys.TAB).build().perform();
			System.out.println("Invoice Activiation Link is teamviewer.automation@systemsltd.com");
			Reporter.addStepLog("Invoice Activiation Link is teamviewer.automation@systemsltd.com");
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			UtilityMethods.clickElemetJavaSciptExecutor(saveButton);
			//new Actions(driver).moveToElement(saveButton).click().build().perform();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			System.out.println("Sales Order Transferred");
			Reporter.addStepLog("Saled Order Transferred");
			
		}
		
		public void consolidateOrders() throws Throwable{
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			if(driver.findElements(By.xpath("//span[contains(text(),'Open Lasernet setup')]")).size()!=0){
				UtilityMethods.waitForPageLoadAndPageReady();
				
				driver.navigate().back();
				
				UtilityMethods.waitForPageLoadAndPageReady();
				UtilityMethods.wait3Seconds();
			}
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			try{
				System.out.println("USER IS CLICKING ON CONSOLIDATE BUTTON");
				consolidateButton.click();
			}
			catch(Exception e){
				System.out.println("USER IS CLICKING ON CONSOLIDATE BUTTON");
				new Actions(driver).moveToElement(consolidateButton).click().perform();
			}
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			if(driver.findElements(By.xpath("//div[contains(@id,'_SalesTable_RowTemplate_Row0')]")).size()==0){
				
				salesOrderSection.click();
				
			}
			if(driver.findElements(By.xpath("//div[contains(@id,'_Lines_RowTemplate_Row0')]")).size()==0){
				
				linesSection.click();
				
			}
			UtilityMethods.wait3Seconds();
			int sizeOfRowsToBeClicked = driver.findElements(By.xpath("//div[@class='markContainer']")).size();
			int j = sizeOfRowsToBeClicked-3;
			for(int i=0;i<=4;i++,j++){
				
				System.out.println("Value of i is: "+i+" And value of j is: "+j);
				try{
					new Actions(driver).moveToElement(driver.findElement(By.xpath("(//div[@class='markContainer'])["+j+"]"))).click().perform();
				//	driver.findElement(By.xpath("(//div[@class='markContainer'])["+j+"]")).click();
					Thread.sleep(3000);
				}
				catch(Exception e){
					
					UtilityMethods.clickElemetJavaSciptExecutor(driver.findElement(By.xpath("(//div[@class='markContainer'])["+j+"]")));
					Thread.sleep(3000);
				}
			}
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait3Seconds();
			try{
				clickOkForConfirmSalesOrder.click();
			}
			catch(Exception e){
				UtilityMethods.clickElemetJavaSciptExecutor(clickOkForConfirmSalesOrder);
			}
		
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(pacLicenseForConsolidation));
			try{
				new Actions(driver).moveToElement(pacLicenseForConsolidation).click().perform();
				UtilityMethods.wait5Seconds();
				
				
			}
			catch(Exception e){
				UtilityMethods.clickElemetJavaSciptExecutor(pacLicenseForConsolidation);
				UtilityMethods.wait5Seconds();
				
			
			}
			
			try{
				
				new Actions(driver).moveToElement(selectPacLicense).click().perform();
				
			}
			catch(Exception e){
				
				UtilityMethods.clickElemetJavaSciptExecutor(selectPacLicense);
			
			}
			
			UtilityMethods.wait5Seconds();
			try{
				new Actions(driver).moveToElement(emailForConsolidation).click().perform();
				UtilityMethods.wait5Seconds();
				
				
			}
			catch(Exception e){
				UtilityMethods.clickElemetJavaSciptExecutor(emailForConsolidation);
				UtilityMethods.wait5Seconds();
				
			}
			
			try{
				new Actions(driver).moveToElement(selectEmail).click().perform();
				
			}
			catch(Exception e){
				UtilityMethods.clickElemetJavaSciptExecutor(selectEmail);
				
				
			}
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			try{
				clickOkForConfirmSalesOrder.click();
			}
			catch(Exception e){
				UtilityMethods.clickElemetJavaSciptExecutor(clickOkForConfirmSalesOrder);
			}
			
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(salesOrderNumber));

			salesOrderNumberOnOrder = salesOrderNumber.getText().substring(0,9); 
			System.out.println("SALES ORDER NUMBER OF CONSOLIDATED ORDER IS:"+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("SALES ORDER NUMBER OF CONSOLIDATED ORDER IS:"+"<b>"+salesOrderNumber.getText().substring(0,10)+"<b>");
			
			consolidation = true;
		}
		
	
		
	public AX365Page(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);

	}




//span[starts-with(@id,'main-product-name')]

}
