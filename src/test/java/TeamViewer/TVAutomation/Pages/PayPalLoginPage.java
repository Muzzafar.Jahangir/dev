package TeamViewer.TVAutomation.Pages;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;

public class PayPalLoginPage extends DriverFactory{


	public PayPalLoginPage(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);

	}

	@FindBy (xpath= "(//span[@class='fake-radio-box'])[2]")
	public WebElement selectPayPalRadioButton;


	@FindBy (xpath= "//input[@name='login_email']")
	public WebElement paypalEmailField;

	@FindBy (xpath= "//input[@name='login_password']")
	public WebElement paypalPasswordField;

	@FindBy (xpath= "//button[@id='btnLogin']")
	public WebElement paypalLoginButton;

	@FindBy (xpath= "//div[@id='button']")
	public WebElement payPalContinueButton;

	@FindBy (xpath= "//input[@id = 'confirmButtonTop']")
	public WebElement verifyAndPayPayPalButton;







	public void selectPayPalPayment() throws Throwable{

		selectPayPalRadioButton.click();

	}




	public void enterCredentialsInPayPal() throws Throwable{

		UtilityMethods.wait60Seconds();
		
		paypalEmailField.clear();
		paypalEmailField.sendKeys("Intdev-buyer@teamviewer.com");
		paypalPasswordField.sendKeys("s7ugeWuc");
		paypalLoginButton.click();
		UtilityMethods.wait10Seconds();

		Actions builder = new Actions(driver);
		Action mouseOverHome = builder
				.moveToElement(payPalContinueButton).build();
		mouseOverHome.perform();
		UtilityMethods.wait3Seconds();
		payPalContinueButton.click();
		UtilityMethods.wait10Seconds();

		verifyAndPayPayPalButton.click();
		UtilityMethods.wait30Seconds();
	}




}
