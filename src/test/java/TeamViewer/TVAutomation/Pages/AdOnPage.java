package TeamViewer.TVAutomation.Pages;

import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;


public class AdOnPage extends DriverFactory {

	//*****************************Business AddOns****************************************

	public static String additionalConcurrentUserCode;
	public static String mobileSupportCode;
	public static String serviceCampCode;
	public static String teamViewerMonitoringCode;
	public static String teamviewerEndPointCode;
	public static String teamviewerBackupCode;
	public static String teamViewerPilotCode;
	public static String licenseName;
	public static String MainProduct;

	private CheckOutPage checkoutPageObject;
		//public static String additionalConcurrentUserCode="TVAD001";
		//public static String mobileSupportCode="TVAD003";
//		public static String serviceCampCode="TVADSSC001";
//		public static String teamViewerMonitoringCode="ITBMA0001";
//		public static String teamviewerEndPointCode="ITBAM0001";
//		public static String teamviewerBackupCode="ITBB0001";
//		public static String teamViewerPilotCode="PILOT001";


	@FindBy (xpath = "((//div[@class='col'])/h4)[1]")

	public WebElement licenseNameForSubject;


	//	@FindBy (xpath = "//button[@value and @data-role]")

	@FindBy (xpath = "//span[contains(text(),'Continue to Payment')]")
	public WebElement Next;
	
	@FindBy (xpath = "//span[contains(text(),'Next Step')]")
	public WebElement NextForQuote;

	@FindBy (xpath = "//*[@class='fake-checkbox-box']")

	public WebElement supportForMobileDevicesCheckboxBusinessLicense;

	@FindBy (xpath = "(//*[@class='select-styled'])[1]")

	public WebElement serviceCampStaffAgentsDropdownBusinessLicense;

	@FindBy (xpath = "//h4[contains(text(),'TeamViewer Monitoring & Asset Management')]")

	public WebElement TeamViewerMonitoringOpenerBusinessLicense;

	@FindBy (xpath = "(//*[@class='select-styled'])[1]")

	public WebElement NumberofMonitorEndpointsChooseBusinessLicense;

	@FindBy (xpath = "(//*[@class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-e'])[4]")

	public WebElement TeamviewerEndPointOpenerBusinessLicense;


	@FindBy (xpath = "(//div[contains(text(),'Please choose')])[2]")

	public WebElement NumberofProtectionEndpointsChooseBusinessLicense;

	@FindBy (xpath = "(//*[@class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-e'])[5]")

	public WebElement TeamviewerBackUpOpenerBusinessLicense;

	@FindBy (xpath = "(//div[contains(text(),'Please choose')])[3]")

	public WebElement StorageSizeChooseBusinessLicense;

	@FindBy (xpath = "(//*[@class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-e'])[6]")

	public WebElement TeamviewerPilotOpenerBusinessLicense;

	@FindBy (xpath = "(//div[contains(text(),'Please choose')])[4]")

	public WebElement NumberofTechniciansChooseBusinessLicense;

	@FindBy (xpath = "//span[@class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-s']")

	public WebElement closerForAddOns;

	@FindBy (xpath = "(//h4[contains(text(),'Additional Concurrent Users ')])[1]")

	public WebElement additionalConcurrentVerifyOnCart;

	@FindBy (xpath = "(//h4[contains(text(),'Support for Mobile Devices ')])[1]")

	public WebElement supportForMobileDevicesVerifyOnCart;

	@FindBy (xpath = "(//h4[contains(text(),'Servicecamp Staff Agents ')])[2]")

	public WebElement serviceCampStaffAgentsVerifyOnCart;

	@FindBy (xpath = "(//h4[contains(text(),'TeamViewer Monitoring & Asset Management')])[2]")

	public WebElement teamViewerMonitoringAndAssetManagementVerifyOnCart;

	@FindBy (xpath = "(//h4[contains(text(),'TeamViewer Endpoint Protection')])[2]")

	public WebElement teamviewerEndPointProtectionVerifyOnCart;		

	@FindBy (xpath = "(//h4[contains(text(),'TeamViewer Backup')])[2]")

	public WebElement teamviewerBackupVerifyOnCart;				

	@FindBy (xpath = "(//h4[contains(text(),'TeamViewer Pilot')])[2]")

	public WebElement teamviewerPilotVerifyOnCart;		
	
	@FindBy(xpath = "(//li[contains(text(),'5')])[1]")
	public WebElement selectValue;
	
	@FindBy(xpath = "(//div[contains(text(),'0') and @class='select-styled'])[1]")
	public WebElement addOn;
	
	@FindBy(xpath = "(//*[@class='fake-checkbox-box'])[2]")
	public WebElement mobileAddOn;
	
	@FindBy(xpath = "(//h4[contains(text(),'TeamViewer')])[1]")
	public WebElement teamViewer;
	
	@FindBy(xpath = "(//h4[contains(text(),'TeamViewer Remote Access')])[1]")
	public WebElement teamRemoteAccess;
	
	@FindBy(xpath = "(//em[@class = 'fake-checkbox-box'])[4]")
	public WebElement teamViewerRemoteAccessChecBox;
	
	@FindBy(xpath = "(//div[contains(text(),'Please choose')])[1]")
	public WebElement teamViewerRemoteAccessDropDown;
	
	@FindBy(xpath = "(//li[contains(text(),'5 Endpoints')])[1]")
	public WebElement teamViewerRemoteAccessDropDownValue;
	
	@FindBy(xpath = "//h4[contains(text(),'TeamViewer Monitoring & Asset Management')]")
	public WebElement teamViewerMonitoringAndAssetManagment;
	
	@FindBy(xpath = "(//li[contains(text(),'10 Endpoints')])[2]")
	public WebElement teamViewerMonitoringAndAssetManagmentDropDownValue;
	
	@FindBy(xpath = "//h4[contains(text(),'TeamViewer Endpoint Protection')]")
	public WebElement teamViewerEndPointProtection;
	
	@FindBy(xpath = "(//li[contains(text(),'25 Endpoints')])[3]")
	public WebElement teamViewerEndPointProtectionDropDownValue;
	
	@FindBy(xpath = "//h4[contains(text(),'TeamViewer Backup')]")
	public WebElement teamViewerBackUp;
	
	@FindBy(xpath = "(//li[contains(text(),'300 GB')])[1]")
	public WebElement teamViewerBackUpDropDownValue;
	
	@FindBy(xpath = "//h4[contains(text(),'TeamViewer Pilot')]")
	public WebElement teamViewerPilot;
	
	@FindBy(xpath = "//li[contains(text(),'2 Technicians')]")
	public WebElement teamViewerPilotDropDownValue;
	
	
	





	//*****************************Premium Corporate AddOns****************************************


	@FindBy (xpath = "//*[@class='fake-checkbox-box']")

	public WebElement additionalConcurrentUsersDropdownPremiumCorporateLicense;

	@FindBy (xpath = "//*[@class='fake-checkbox-box']")

	public WebElement supportForMobileDevicesCheckboxPremiumCorporateLicense;

	@FindBy (xpath = "(//*[@class='select-styled'])[2]")

	public WebElement serviceCampStaffAgentsDropdownPremiumCorporateLicense;

	@FindBy (xpath = "(//*[@class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-e'])[3]")

	public WebElement TeamViewerMonitoringOpenerPremiumCorporateLicense;

	@FindBy (xpath = "(//*[@class='select-styled'])[3]")

	public WebElement NumberofMonitorEndpointsChoosePremiumCorporateLicense;

	@FindBy (xpath = "(//*[@class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-e'])[4]")

	public WebElement TeamviewerEndPointOpenerPremiumCoporateLicense;

	@FindBy (xpath = "(//*[@class='select-styled'])[4]")

	public WebElement NumberofEndpointsChoosePremiumCoporateLicense;

	@FindBy (xpath = "(//*[@class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-e'])[5]")

	public WebElement TeamviewerBackUpOpenerPremiumCoporateLicense;

	@FindBy (xpath = "(//*[@class='select-styled'])[5]")

	public WebElement StorageSizeChoosePremiumCoporateLicense;

	@FindBy (xpath = "(//*[@class='ui-accordion-header-icon ui-icon ui-icon-triangle-1-e'])[6]")

	public WebElement TeamviewerPilotOpenerPremiumCoporateLicense;

	@FindBy (xpath = "(//*[@class='select-styled'])[6]")

	public WebElement NumberofTechniciansChoosePremiumCoporateLicense;






	//***********************************Adding All the Add Ons only for Business License Order****************************************


	public void addMobileDeviceSupportAddOnForBusiness() throws Throwable{
		//Adding Support for Mobile Devices for Business License
		UtilityMethods.waitForPageLoadAndPageReady();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		supportForMobileDevicesCheckboxBusinessLicense.click();
		UtilityMethods.wait7Seconds();
		//Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS SELECTED MOBILE DEVICES SUPPORT");
	}
	public void addAllBusinessLicenseAddOns() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");


		//Adding Support for Mobile Devices
		supportForMobileDevicesCheckboxBusinessLicense.click();
		UtilityMethods.wait7Seconds();


		//Selecting Service Staff Camp Agents
		serviceCampStaffAgentsDropdownBusinessLicense.click();
		UtilityMethods.wait3Seconds();
		List<WebElement> serviceAgentOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[1]/li"));

		for(WebElement option : serviceAgentOptions) {
			if (option.getText().equals("3")) {
				option.click();
			}
		}
		System.out.println("USER HAS SELECTED SERVICE CAMP STAFF AGENTS ");
		UtilityMethods.wait10Seconds();


		//Selecting TeamViewer Monitoring License
		TeamViewerMonitoringOpenerBusinessLicense.click();
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(NumberofMonitorEndpointsChooseBusinessLicense));

		NumberofMonitorEndpointsChooseBusinessLicense.click();

		List<WebElement> monitorOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[2]/li"));

		for(WebElement option : monitorOptions) {
			if (option.getText().startsWith("100")){
				option.click();
			}
		}
		System.out.println("USER HAS SELECTED MONITORING AND ASSET MANAGEMENTS ");
		closerForAddOns.click();
		UtilityMethods.wait10Seconds();


		//Selecting Teamviewer Endpoint Protection

		TeamviewerEndPointOpenerBusinessLicense.click();
		wait.until(ExpectedConditions.visibilityOf(NumberofProtectionEndpointsChooseBusinessLicense));

		NumberofProtectionEndpointsChooseBusinessLicense.click();

		List<WebElement> endpointOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[3]/li"));


		for(WebElement option : endpointOptions) {
			if (option.getText().startsWith("100")){
				option.click();
			}
		}
		System.out.println("USER HAS SELECTED ENDPOINTS PROTECTIONS ");
		closerForAddOns.click();
		UtilityMethods.wait10Seconds();

		//Selecting Teamviewer Backup options
		TeamviewerBackUpOpenerBusinessLicense.click();
		wait.until(ExpectedConditions.visibilityOf(StorageSizeChooseBusinessLicense));

		StorageSizeChooseBusinessLicense.click();

		List<WebElement> backupOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[4]/li"));


		for(WebElement option : backupOptions) {
			if (option.getText().startsWith("100")){
				option.click();
			}
		}

		System.out.println("USER HAS SELECTED TEAMVIEWER BACKUP ");
		closerForAddOns.click();
		UtilityMethods.wait10Seconds();

		//Selecting TeamViewer Pilot options
		TeamviewerPilotOpenerBusinessLicense.click();
		wait.until(ExpectedConditions.visibilityOf(NumberofTechniciansChooseBusinessLicense));

		NumberofTechniciansChooseBusinessLicense.click();

		List<WebElement> pilotOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[5]/li"));

		for(WebElement option : pilotOptions) {
			if (option.getText().startsWith("9")){
				option.click();
			}
		}

		System.out.println("USER HAS SELECTED TEAMVIEWER PILOTS ");
		closerForAddOns.click();
		UtilityMethods.wait7Seconds();

	}



	//**********************************************Adding all Addons In Premium/Corporate License***********************************

	public void addAllPremiumCorporateLicenseAddson() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();


		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		WebDriverWait wait = new WebDriverWait(driver,15);
		UtilityMethods.wait7Seconds();
	//	driver.navigate().refresh();
		//Adding Additional Concurrent Users
//		additionalConcurrentUsersDropdownPremiumCorporateLicense.click();
//		//String url = driver.getCurrentUrl();
//		//driver.navigate().to(url+"/?p=corporate");
//		UtilityMethods.wait5Seconds();
//	//	new Select(additionalConcurrentUsersDropdownPremiumCorporateLicense).selectByValue("5");
//		List<WebElement> additionalConcurrentUsersOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[1]/li"));
//
//
//		for(WebElement option : additionalConcurrentUsersOptions) {
//			if (option.getText().equals("5")) {
//				option.click();
//			}
//		}
//		System.out.println("USER HAS SELECTED 5 ADDITIONAL CONCURRENT USERS");
//		////Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
//		Reporter.addStepLog("5 ADDITIONAL USERS ADDED");
//		wait.until(ExpectedConditions.visibilityOf(additionalConcurrentVerifyOnCart));

		
		//Adding Support for Mobile Devices
		supportForMobileDevicesCheckboxPremiumCorporateLicense.click();
		System.out.println("USER HAS SELECTED SUPPORT FOR MOBILE DEVICES");
		Reporter.addStepLog("SUPPORT FOR MOBILE DEVICES ADD-ON ADDED IN CART");
		wait.until(ExpectedConditions.visibilityOf(supportForMobileDevicesVerifyOnCart));


		//		//Selecting Service Staff Camp Agents
		//		serviceCampStaffAgentsDropdownPremiumCorporateLicense.click();
		//		UtilityMethods.wait1Seconds();
		//		List<WebElement> serviceAgentOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[2]/li"));
		//
		//		for(WebElement option : serviceAgentOptions) {
		//			if (option.getText().equals("6")) {
		//				option.click();
		//			}
		//		}
		//		System.out.println("USER HAS SELECTED SERVICE CAMP STAFF AGENTS");
		//
		//
		//		UtilityMethods.wait5Seconds();
		//
		//
		//		//Selecting TeamViewer Monitoring License
		//		TeamViewerMonitoringOpenerPremiumCorporateLicense.click();
		//		WebDriverWait wait = new WebDriverWait(driver,30);
		//		wait.until(ExpectedConditions.visibilityOf(NumberofMonitorEndpointsChoosePremiumCorporateLicense));
		//
		//		NumberofMonitorEndpointsChoosePremiumCorporateLicense.click();
		//
		//		List<WebElement> monitorOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[3]/li"));
		//
		//		for(WebElement option : monitorOptions) {
		//			if (option.getText().startsWith("100")){
		//				option.click();
		//			}
		//		}
		//		System.out.println("USER HAS SELECTED MONITORING AND ASSET MANAGEMENTS ");
		//
		//
		//		closerForAddOns.click();
		//		UtilityMethods.wait10Seconds();
		//
		//		//Selecting Teamviewer Endpoint Protection
		//
		//		TeamviewerEndPointOpenerPremiumCoporateLicense.click();
		//		wait.until(ExpectedConditions.visibilityOf(NumberofEndpointsChoosePremiumCoporateLicense));
		//
		//		NumberofEndpointsChoosePremiumCoporateLicense.click();
		//
		//		List<WebElement> endpointOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[4]/li"));
		//
		//		for(WebElement option : endpointOptions) {
		//			if (option.getText().startsWith("100")){
		//				option.click();
		//			}
		//		}
		//		System.out.println("USER HAS SELECTED ENDPOINT PROTECTIONS");
		//		closerForAddOns.click();
		//		UtilityMethods.wait10Seconds();
		//
		//		//Selecting Teamviewer Backup options
		//		TeamviewerBackUpOpenerPremiumCoporateLicense.click();
		//		wait.until(ExpectedConditions.visibilityOf(StorageSizeChoosePremiumCoporateLicense));
		//
		//		StorageSizeChoosePremiumCoporateLicense.click();
		//
		//		List<WebElement> backupOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[5]/li"));
		//
		//		for(WebElement option : backupOptions) {
		//			if (option.getText().startsWith("100")){
		//				option.click();
		//			}
		//		}
		//		System.out.println("USER HAS SELECTED TEAMVIEWER BACKUP");
		//		closerForAddOns.click();
		//		UtilityMethods.wait10Seconds();
		//
		//		//Selecting TeamViewer Pilot options
		//		TeamviewerPilotOpenerPremiumCoporateLicense.click();
		//		wait.until(ExpectedConditions.visibilityOf(NumberofTechniciansChoosePremiumCoporateLicense));
		//
		//		NumberofTechniciansChoosePremiumCoporateLicense.click();
		//
		//		List<WebElement> pilotOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[6]/li"));
		//
		//		for(WebElement option : pilotOptions) {
		//			if (option.getText().startsWith("9")){
		//				option.click();
		//			}
		//		}
		//		System.out.println("USER HAS SELECTED TEAMVIEWER PILOTS");
		//		closerForAddOns.click();




		try{

			if(additionalConcurrentVerifyOnCart.isDisplayed()){

				additionalConcurrentUserCode="TVAD001";
				System.out.println("SELECTED PRODUCT IS : "+additionalConcurrentUserCode);

			}

		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED ADDITIONAL CONCURRENT USERS ");

		}




		try{

			if(supportForMobileDevicesVerifyOnCart.isDisplayed()){
				mobileSupportCode = "TVAD003";
				System.out.println("SELECTED PRODUCT IS : "+mobileSupportCode);
				//Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("SELECTED PRODUCT IS : "+mobileSupportCode);
			}

		}
		catch(Exception e){
			System.out.println("USER HAS NOT SELECTED MOBILE DEVICES SUPPORT ");
		}

		try{

			if(serviceCampStaffAgentsVerifyOnCart.isDisplayed()){

				serviceCampCode = "TVADSSC001";
				System.out.println("SELECTED PRODUCT IS : "+serviceCampCode);
				

			}
		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED SERVICE CAMP STAFF AGENTS ");
		}


		try{

			if(teamViewerMonitoringAndAssetManagementVerifyOnCart.isDisplayed()){

				teamViewerMonitoringCode="ITBMA0001";
				System.out.println("SELECTED PRODUCT IS : "+teamViewerMonitoringCode);

			}
		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED TEAMVIEWER MONOTORING AND ASSET MANAGEMENT");
		}



		try{

			if(teamviewerEndPointProtectionVerifyOnCart.isDisplayed()){

				teamviewerEndPointCode = "ITBAM0001";
				System.out.println("SELECTED PRODUCT IS : "+teamviewerEndPointCode);

			}
		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED TEAMVIEWER ENDPOINT PROTECTION");
		}



		try{

			if(teamviewerBackupVerifyOnCart.isDisplayed()){

				teamviewerBackupCode= "ITBB0001";
				System.out.println("SELECTED PRODUCT IS : "+teamviewerBackupCode);
			}
		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED TEAMVIEWER BACKUP PROTECTION");
		}



		try{

			if(teamviewerPilotVerifyOnCart.isDisplayed()){

				teamViewerPilotCode = "PILOT001";
				System.out.println("SELECTED PRODUCT IS : "+teamViewerPilotCode);

			}

		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED TEAMVIEWER PILOT TECHNICIANS");
		}
		
	}
	
	
	
	public void addMonitorEndpointPilotAndBackup() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();


		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,500)");	


		//Selecting TeamViewer Monitoring License
	//	new Select(driver.findElement(By.name("add_update_action_243"))).selectByValue("4");
		TeamViewerMonitoringOpenerBusinessLicense.click();
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(NumberofMonitorEndpointsChooseBusinessLicense));

		NumberofMonitorEndpointsChooseBusinessLicense.click();

		List<WebElement> monitorOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[2]/li"));

		for(WebElement option : monitorOptions) {
			if (option.getText().startsWith("100")){
				option.click();
			}
		}
		System.out.println("USER HAS SELECTED MONITORING AND ASSET MANAGEMENTS ");
		closerForAddOns.click();
		UtilityMethods.wait10Seconds();


		//Selecting Teamviewer Endpoint Protection

		TeamviewerEndPointOpenerBusinessLicense.click();
		UtilityMethods.wait2Seconds();
		wait.until(ExpectedConditions.visibilityOf(NumberofProtectionEndpointsChooseBusinessLicense));

		NumberofProtectionEndpointsChooseBusinessLicense.click();

		List<WebElement> endpointOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[3]/li"));


		for(WebElement option : endpointOptions) {
			if (option.getText().startsWith("100")){
				option.click();
			}
		}
		System.out.println("USER HAS SELECTED ENDPOINTS PROTECTIONS ");
		closerForAddOns.click();
		UtilityMethods.wait10Seconds();

		//Selecting Teamviewer Backup options
		TeamviewerBackUpOpenerBusinessLicense.click();
		UtilityMethods.wait3Seconds();
		
		//wait.until(ExpectedConditions.visibilityOf(StorageSizeChooseBusinessLicense));
		UtilityMethods.wait3Seconds();
		StorageSizeChooseBusinessLicense.click();

		List<WebElement> backupOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[4]/li"));


		for(WebElement option : backupOptions) {
			if (option.getText().startsWith("100")){
				option.click();
			}
		}

		System.out.println("USER HAS SELECTED TEAMVIEWER BACKUP ");
		closerForAddOns.click();
		UtilityMethods.wait10Seconds();

		//Selecting TeamViewer Pilot options
//		TeamviewerPilotOpenerBusinessLicense.click();
//		wait.until(ExpectedConditions.visibilityOf(NumberofTechniciansChooseBusinessLicense));
//
//		NumberofTechniciansChooseBusinessLicense.click();
//
//		List<WebElement> pilotOptions = driver.findElements(By.xpath("(//ul[@class='select-options'])[5]/li"));
//
//		for(WebElement option : pilotOptions) {
//			if (option.getText().startsWith("9")){
//				option.click();
//			}
//		}
//
//		System.out.println("USER HAS SELECTED TEAMVIEWER PILOTS ");
//		closerForAddOns.click();
//		UtilityMethods.wait7Seconds();




		try{

			if(additionalConcurrentVerifyOnCart.isDisplayed()){

				additionalConcurrentUserCode="TVAD001";
				System.out.println("SELECTED PRODUCT IS : "+additionalConcurrentUserCode);

			}

		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED ADDITIONAL CONCURRENT USERS ");

		}




		try{

			if(supportForMobileDevicesVerifyOnCart.isDisplayed()){
				mobileSupportCode = "TVAD003";
				System.out.println("SELECTED PRODUCT IS : "+mobileSupportCode);
			}

		}
		catch(Exception e){
			System.out.println("USER HAS NOT SELECTED MOBILE DEVICES SUPPORT ");
		}

		try{

			if(serviceCampStaffAgentsVerifyOnCart.isDisplayed()){

				serviceCampCode = "TVADSSC001";
				System.out.println("SELECTED PRODUCT IS : "+serviceCampCode);
				

			}
		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED SERVICE CAMP STAFF AGENTS ");
		}


		try{

			if(teamViewerMonitoringAndAssetManagementVerifyOnCart.isDisplayed()){

				teamViewerMonitoringCode="ITBMA0001";
				System.out.println("SELECTED PRODUCT IS : "+teamViewerMonitoringCode);

			}
		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED TEAMVIEWER MONOTORING AND ASSET MANAGEMENT");
		}



		try{

			if(teamviewerEndPointProtectionVerifyOnCart.isDisplayed()){

				teamviewerEndPointCode = "ITBAM0001";
				System.out.println("SELECTED PRODUCT IS : "+teamviewerEndPointCode);

			}
		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED TEAMVIEWER ENDPOINT PROTECTION");
		}



		try{

			if(teamviewerBackupVerifyOnCart.isDisplayed()){

				teamviewerBackupCode= "ITBB0001";
				System.out.println("SELECTED PRODUCT IS : "+teamviewerBackupCode);
			}
		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED TEAMVIEWER BACKUP PROTECTION");
		}



		try{

			if(teamviewerPilotVerifyOnCart.isDisplayed()){

				teamViewerPilotCode = "PILOT001";
				System.out.println("SELECTED PRODUCT IS : "+teamViewerPilotCode);

			}

		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED TEAMVIEWER PILOT TECHNICIANS");
		}
		
	}
	public void routeBack() throws Throwable{


		driver.navigate().to("https://stg:abcd@1234@www.teamviewer.com/en/buy-now/");
		UtilityMethods.wait20Seconds();
	}


	public void refreshBrowser() throws Throwable{

		UtilityMethods.RefreshBrowser();
	}

	public void clicknext() throws Throwable{



		UtilityMethods.waitForPageLoadAndPageReady();
		
		licenseName = licenseNameForSubject.getText();

		System.out.println("Again Getting License Name :"+licenseName);

		if(licenseName.equals("TeamViewer Remote Access")){

			licenseName = "TeamViewer Remote Access License";

		}

		if(licenseName.equals("Single User")){

			licenseName = "TeamViewer Business License";

		}

		if(licenseName.equals("Multi User")){

			licenseName = "TeamViewer Premium License";

		}

		if(licenseName.equals("For Teams")){

			licenseName = "TeamViewer Corporate License";

		}


		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		try
		{
			new Actions(driver).moveToElement(NextForQuote).click().perform();

		}
		catch(Exception e){
			new Actions(driver).moveToElement(Next).click().perform();

		}
		
		Reporter.addStepLog("REDIRECTED TO CHECKOUT PAGE WHEN NEXT BUTTON IS CLICKED");
		System.out.println("Redirected to Checkout page when Next Button is clicked");
		UtilityMethods.wait10Seconds();
		System.out.println("USER IS ENTERING THE BILLINGS DETAILS NOW");

	}

	public void clicknext_staging() throws Throwable{


		licenseName = licenseNameForSubject.getText();

		System.out.println("Again Getting License Name :"+licenseName);

		if(licenseName.equals("TeamViewer Remote Access")){

			licenseName = "TeamViewer Remote Access License";

		}

		if(licenseName.equals("Single User")){

			licenseName = "TeamViewer Business License";

		}

		if(licenseName.equals("Multi User")){

			licenseName = "TeamViewer Premium License";

		}

		if(licenseName.equals("For Teams")){

			licenseName = "TeamViewer Corporate License";

		}


		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");


		UtilityMethods.wait5Seconds();
		driver.navigate().to("https://stg:abcd@1234@www.teamviewer.com/en/buy-now/");
		Next.click();
		//Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CLICKED 'NEXT' BUTTON");
		System.out.println("USER HAS CLICKED NEXT STEP BUTTON ");
		UtilityMethods.wait20Seconds();
		System.out.println("USER IS ENTERING THE BILLINGS DETAILS NOW");

	}
	
	public void addAllPremiumQuoteAddson() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		addOn.click();
		UtilityMethods.wait3Seconds();
		selectValue.click();		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		new Actions(driver).moveToElement(mobileAddOn).click(mobileAddOn).build().perform();
		UtilityMethods.wait7Seconds();
		teamViewer.click();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		try{
			UtilityMethods.scrollToWebElement(teamRemoteAccess);
			teamRemoteAccess.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerRemoteAccessChecBox.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerRemoteAccessDropDown.click();
			UtilityMethods.wait5Seconds();
			teamViewerRemoteAccessDropDownValue.click();
			UtilityMethods.wait5Seconds();
			UtilityMethods.clickElemetJavaSciptExecutor(teamRemoteAccess);
			Reporter.addStepLog("Remote access add-on selected");
		}
		catch(Exception e){
			System.out.println("Remote access add-on missing");
		}
		try{
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerMonitoringAndAssetManagment.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerRemoteAccessDropDown.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerMonitoringAndAssetManagmentDropDownValue.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerMonitoringAndAssetManagment.click();
			Reporter.addStepLog("Teamviewer monitroing and asset management add-on selected");
		}
		catch(Exception e){
			System.out.println("Teamviewer monitroing and asset management add on missing");
		}
		try{
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			UtilityMethods.scrollToWebElement(teamViewerEndPointProtection);
			teamViewerEndPointProtection.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerRemoteAccessDropDown.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerEndPointProtectionDropDownValue.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait7Seconds();
			teamViewerEndPointProtection.click();
			Reporter.addStepLog("Teamviewer End point protection add-on selected");
		}
		catch(Exception e){
			System.out.println("Teamviewer End point protection add on missing");
		}
		try{
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerBackUp.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerRemoteAccessDropDown.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			UtilityMethods.scrollToWebElement(teamViewerBackUpDropDownValue);
			teamViewerBackUpDropDownValue.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerBackUp.click();
			Reporter.addStepLog("Teamviewer back up add-on selected");
		}
		catch(Exception e){
			System.out.println("Teamviewer back up add on missing");
		}
		try{
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerPilot.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerRemoteAccessDropDown.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerPilotDropDownValue.click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait5Seconds();
			teamViewerPilot.click();
			Reporter.addStepLog("Teamviewer pilot add-on selected");
		}
		catch(Exception e){
			System.out.println("Teamviewer pilot add on missing");
		}
		
		try{

			if(additionalConcurrentVerifyOnCart.isDisplayed()){

				additionalConcurrentUserCode="TVAD001";
				System.out.println("SELECTED PRODUCT IS : "+additionalConcurrentUserCode);

			}

		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED ADDITIONAL CONCURRENT USERS ");

		}




		try{

			if(supportForMobileDevicesVerifyOnCart.isDisplayed()){
				mobileSupportCode = "TVAD003";
				System.out.println("SELECTED PRODUCT IS : "+mobileSupportCode);
			}

		}
		catch(Exception e){
			System.out.println("USER HAS NOT SELECTED MOBILE DEVICES SUPPORT ");
		}

		try{

			if(serviceCampStaffAgentsVerifyOnCart.isDisplayed()){

				serviceCampCode = "TVADSSC001";
				System.out.println("SELECTED PRODUCT IS : "+serviceCampCode);
				

			}
		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED SERVICE CAMP STAFF AGENTS ");
		}


		try{

			if(teamViewerMonitoringAndAssetManagementVerifyOnCart.isDisplayed()){

				teamViewerMonitoringCode="ITBMA0001";
				System.out.println("SELECTED PRODUCT IS : "+teamViewerMonitoringCode);

			}
		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED TEAMVIEWER MONOTORING AND ASSET MANAGEMENT");
		}



		try{

			if(teamviewerEndPointProtectionVerifyOnCart.isDisplayed()){

				teamviewerEndPointCode = "ITBAM0001";
				System.out.println("SELECTED PRODUCT IS : "+teamviewerEndPointCode);

			}
		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED TEAMVIEWER ENDPOINT PROTECTION");
		}



		try{

			if(teamviewerBackupVerifyOnCart.isDisplayed()){

				teamviewerBackupCode= "ITBB0001";
				System.out.println("SELECTED PRODUCT IS : "+teamviewerBackupCode);
			}
		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED TEAMVIEWER BACKUP PROTECTION");
		}



		try{

			if(teamviewerPilotVerifyOnCart.isDisplayed()){

				teamViewerPilotCode = "PILOT001";
				System.out.println("SELECTED PRODUCT IS : "+teamViewerPilotCode);

			}

		}
		catch(Exception e){

			System.out.println("USER HAS NOT SELECTED TEAMVIEWER PILOT TECHNICIANS");
		}
		
	
		
	}

	public AdOnPage(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);



	}
}