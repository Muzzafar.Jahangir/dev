package TeamViewer.TVAutomation.Pages;


import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.PropertyReader;
import TeamViewer.TVAutomation.Utils.UtilityMethods;


public class ConfirmationPage extends DriverFactory {

	public static String channelReferenceID;
	public static String paymentCodeOfOrder;

	@FindBy (xpath= "//h1[contains(text(),'Thanks for your purchase!') or contains(text(),'Grazie per il tuo acquisto!') or contains(text(),'Obrigado pela sua compra!') or contains(text(),'Bedankt voor uw aankoop!') or contains(text(),'Merci pour votre achat!') or contains(text(),'Gracias por su compra!')]")
	public WebElement Confirm;
	
	@FindBy (xpath= "//h1[contains(text(),'Thanks for your quote!')]")
	public WebElement ConfirmQuote;

	@FindBy (xpath= "//span[@class='item-row-right' and contains(text(),'PayPal')]")
	public WebElement paymentMethodOnSuccessPageWithTaxPayPal;
	
	@FindBy (xpath= "//span[@class='item-row-right' and contains(text(),'Credit Card')]")
	public WebElement paymentMethodOnSuccessPageWithTaxCreditCard;
	
	@FindBy (xpath= "//span[@class='item-row-right' and contains(text(),'Invoice')]")
	public WebElement paymentMethodOnSuccessPageWithTaxInvoice;
	
	@FindBy (xpath= "(//span[@class='item-row-right'])[2]")
	public WebElement paymentMethodOnSuccessPageWithoutTax;



	@FindBy (xpath= "//input[@name='channel-reference-number']")
	public WebElement getOrderId;


	@FindBy (xpath= "(//*[@class='license-activation-container'])[1]")
	public WebElement Activation;
	
	@FindBy(xpath = "(//a[contains(text(),'Activate License Now')])[1]")
	public WebElement activateLicenseButton;
	



	public void gettingOrderIdFromSuccessPAGE() throws Throwable{

		channelReferenceID = getOrderId.getAttribute("value");
		System.out.println("Channel Reference ID is: "+channelReferenceID);
	//	Reporter.addStepLog("Channel Reference ID is: "+channelReferenceID);
		UtilityMethods.wait5Seconds();
		try{
			
			if(paymentMethodOnSuccessPageWithTaxPayPal.isDisplayed()){
				
				paymentCodeOfOrder = "13042";
				System.out.println("AS PAYMENT IS PAYPAL AND CURRENT TRANSACTION PAYMENT CODE "+paymentCodeOfOrder);
				Reporter.addStepLog("AS PAYMENT IS PAYPAL AND CURRENT TRANSACTION PAYMENT CODE "+paymentCodeOfOrder);
				
			}
			
		}
		catch(NoSuchElementException e)
		{
			
			if(driver.findElements(By.xpath("//span[@class='item-row-right' and contains(text(),'Credit Card')]")).size()!=0){
//			if(paymentMethodOnSuccessPageWithTaxCreditCard.equals("Credit Card")){
				
				paymentCodeOfOrder = "13034";
				System.out.println("AS PAYMENT IS CREDIT CARD AND CURRENT TRANSACTION PAYMENT CODE "+paymentCodeOfOrder);
				Reporter.addStepLog("AS PAYMENT IS CREDIT CARD AND CURRENT TRANSACTION PAYMENT CODE "+paymentCodeOfOrder);
				
				
				
			}	
			else{
				
				paymentCodeOfOrder = "100";
				System.out.println("AS PAYMENT IS INVOICE AND CURRENT TRANSACTION PAYMENT CODE "+paymentCodeOfOrder);
				Reporter.addStepLog("AS PAYMENT IS INVOICE AND CURRENT TRANSACTION PAYMENT CODE "+paymentCodeOfOrder);
				
			
			}
			
		}
		

	}
	
	public void clickActivateLicenseButton() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait15Seconds();
//		new WebDriverWait(driver,30).until(ExpectedConditions.elementSelectionStateToBe(activateLicenseButton, true));
		try{
			UtilityMethods.moveToElementWithJSExecutor(activateLicenseButton);
			activateLicenseButton.click();
		}
		catch(Exception e){
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait15Seconds();
			UtilityMethods.moveToElementWithJSExecutor(activateLicenseButton);
			activateLicenseButton.click();
		}
	
		
	}



	public String getConfirm() throws InterruptedException {
		//UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.waitForElementEnabled(Confirm, 25);
		return Confirm.getText();

	}
	public ConfirmationPage(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);

	}



}