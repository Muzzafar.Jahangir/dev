package TeamViewer.TVAutomation.Pages;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;
import cucumber.api.java.Before;


public class CheckOutPage extends DriverFactory {

	public static String finalXpathTaxValueInString;
	public static String subTotalValue1;
	public static String  orderTotalValue;
	public static String  orderTotalValueForERP;
	public static String subscriptionProductName;
	public static String subscriptionProductCode;
	public static String quantityOfTVMonitoring;
	public static String priceOfTVMonitoring;
	public static String quantityOfTVEndpoint;
	public static String priceOfTVEndpoint;
	public static String quantityOfTVBackup;
	public static String priceOfTVBackUp;
	public static String quantityOfTVPilot;
	public static String priceOfTVPilot;
	public static String priceOfTheSelectedLicense;
	public static String quantityOfAdditionalUsers;
	public static String priceOfAdditionalUsers;
	public static String quantityOfMobileSupporters;
	public static String priceOfMobileSupport;
	public static String currency;

	@FindBy (xpath = "//button[@id='action-primary-continue' and @class='button action continue primary']")
	public WebElement PlaceOrder;	

	@FindBy (css = "img.paypal-button-logo.paypal-button-logo-paypal.paypal-button-logo-gold")
	public WebElement placeOrderButtonForUAT;

	@FindBy (xpath = "//*[contains(text(),'Proceed Payment')]")
	public WebElement proceedPayment;

	@FindBy (xpath = "//strong[@role and @class]")
	public WebElement Voucher;

	@FindBy (xpath = "//label[@for='agreement__1']/span")
	public WebElement Checkbox;

	@FindBy (xpath = "//span[contains(text(),'Invoice')]")
	public WebElement invoicePaymentButton;

	@FindBy (xpath = "//img[@alt='Acceptance Mark']")
	public WebElement payPalPaymentButton;

	@FindBy (xpath = "//input[@name = 'login_email']")
	public WebElement payPalEmailFieldOnPopup;

	@FindBy (id = "password")
	public WebElement payPalPasswordFieldOnPopup;

	@FindBy (id = "btnLogin")
	public WebElement payPalLoginButtonOnPopup;

	@FindBy (id = "button")
	public WebElement continueButtonOnPopup;

	@FindBy (id = "confirmButtonTop")
	public WebElement AgreePayButtonOnPopup;

	@FindBy (xpath = "//span[@data-bind='text: element.error']")
	public WebElement DuplicateEmailError;

	@FindBy (xpath = "//*[@id='discount-messages']")
	public WebElement InValidVoucherError;

	@FindBy (xpath = "//div[text()='Invalid VAT number']")
	public WebElement InValidVATError;

	@FindBy (xpath = "//div[@class='remove-coupon-text']")
	public WebElement ValidVoucher;

	@FindBy (xpath = "//*[@name='firstname']")
	public WebElement FirstName;

	@FindBy (xpath = "//*[@name='lastname']")
	public WebElement LastName;

	@FindBy (xpath = "//*[@name='company']")
	public WebElement Company;

	@FindBy (xpath = "//*[@name='street[0]']")
	public WebElement Address;
	
	@FindBy (xpath = "//*[@name='street']")
	public WebElement Address1;

	@FindBy (xpath = "(//*[@name='vat_id'])")
	public WebElement vatNumber;

	@FindBy (css = "[name='email']")
	public WebElement Email;


	@FindBy (xpath = "(//input[@name = 'email'])[1]")
	public WebElement emailFieldForECOMPayment;

	@FindBy (xpath = "//input[@name = 'invoice_id']")
	public WebElement inVoiceFieldForECOMPayment;

	@FindBy (xpath = "//*[contains(text(),'Next Step')]")
	public WebElement nextStepButtonForECOMPayment;
	
								
	@FindBy (xpath = "(//div[@class='col'])[1]/h4")
	public WebElement licenseName;
	
	@FindBy (xpath = "(//div[@class='col'])[2]/*[1]/span")
	public WebElement priceofTheLicense;
	
	@FindBy(xpath = "(//strong[@class='price'])[1]")
	public WebElement priceofTheLicenseForQuote;
	
	@FindBy (xpath = "(//div[@class='col'])[3]/*[2]")
	public WebElement quantityOfAdditionalConcurrentUsers;
	
	@FindBy (xpath = "(//div[@class='col'])[4]/*[1]")
	public WebElement priceofAdditionalConcurrentUsers;
	
	@FindBy (xpath = "(//div[@class='col'])[4]/*[1]")
	public WebElement priceOfTVmonitor;
	
	
	@FindBy (xpath = "(//div[@class='col'])[5]/p")
	public WebElement quantityOfMobileDevicesSupport;
	
	@FindBy (xpath = "(//div[@class='col'])[6]/*[1]")
	public WebElement priceOfMobileDeviceSupport;
	
	@FindBy (xpath = "(//div[@class='col'])[6]/*[1]")
	public WebElement priceOftvEndpoint;
	
	
	@FindBy (xpath = "(//div[@class='col'])[7]/*[2]")
	public WebElement quantitOfTVBackUp;
	
	
	@FindBy (xpath = "(//div[@class='col'])[8]/*[1]")
	public WebElement priceOfTVBackup;
	
	@FindBy (xpath = "(//div[@class='col'])[9]/*[2]")
	public WebElement quantityofTVPilot;
	
	@FindBy (xpath = "(//div[@class='col'])[10]/*[2]")
	public WebElement priceofTVPilot;
	
	
	
	
	
	



	@FindBy (xpath = "//*[@name='city']")
	public WebElement City;

	@FindBy (xpath = "//*[@name='postcode']")
	public WebElement ZIP;

	@FindBy (xpath = "//*[@name='telephone']")
	public WebElement Phone;

	@FindBy (xpath = "(//*[@name='coupon_code'])")
	public WebElement VoucherNumber;

	@FindBy (css = "#coupon-action")
	public WebElement Apply;

	@FindBy(xpath = "//span[contains(text(),'Credit Card')]")
	public WebElement CreditCard;

//	@FindBy(xpath = "//*[@id = 'first_name']")
//	public WebElement CC_FirstName;
//
//	@FindBy(xpath = "//*[@id = 'last_name']")
//	public WebElement CC_LastName;
//
//	@FindBy(xpath = "//*[@id = 'account_number']")
//	public WebElement CC_Number;
//
//	@FindBy(xpath = "//*[@id = 'card_security_code']")
//	public WebElement CC_CVV;
//
//	@FindBy(xpath = "//*[@name = 'expiration_month']")
//	public WebElement CC_Month;
//
//	@FindBy(xpath = "//*[@name = 'expiration_year']")
//	public WebElement CC_Year;

	@FindBy(xpath = "//*[@id = 'pp-cc-first-name']")
	public WebElement CC_FirstName;
	//*[@id = 'pp-cc-first-name']
	@FindBy(xpath = "//*[@id = 'pp-cc-last-name']")
	public WebElement CC_LastName;

	@FindBy(xpath = "//*[@id = 'pp-cc-account-number']")
	public WebElement CC_Number;

	@FindBy(xpath = "//*[@id = 'pp-cc-cvv']")
	public WebElement CC_CVV;

	@FindBy(xpath = "//*[@id = 'pp-cc-expiration-date']")
	public WebElement CC_Month;

	@FindBy(xpath = "//*[@id = 'pp-cc-expiration-date']")
	public WebElement CC_Year;
	
	//@FindBy(xpath = "//iframe[@class='wirecard-seamless-frame']")
	//public WebElement CC_Iframe;
	
	@FindBy(id = "wirecard-integrated-payment-page-frame")
	public WebElement CC_Iframe;

	@FindBy(xpath = "//*[@name='region_id']")
	public WebElement States;

	@FindBy(xpath = "//*[@id = 'coupon-discount']")
	public WebElement voucherXpath;


	@FindBy(xpath = "//div[@class='subtotal-info']//span[2]")
	public WebElement subTotalXpathIfVoucherExist;

	@FindBy(xpath = "//*[@class='subtotal totals']/*[2]/*[1]")
	public WebElement taxXpathIfVoucherExist;

	@FindBy(xpath = "//*[@class='subtotal totals']/*[2]/*[3]//span")
	public WebElement orderTotalXpathIfVoucherExist;

	@FindBy(xpath = "//*[@class='subtotal totals']/*[2]/*[1]/*")
	public WebElement orderTotalXpathIfVoucherExistButNotTax;

	@FindBy(xpath = "(//*[@class='subtotal totals']/td/*[1])[1]")
	public WebElement taxValueInPercentageIfVoucherExist;

	@FindBy(xpath = "(//*[contains(text(),'Tax') or contains(text(),'Tax') or contains(text(),'Tassa') or contains(text(), 'De Imposto') or contains(text(),'Belasting') or contains(text(),'Taxe')])[1]")
	public WebElement checkIfTaxFieldIsPresent;


	@FindBy(xpath ="//i[@id='userlike-close']")
	public WebElement chatBoxCloseIcon;

	@FindBy(xpath ="//div[@id = 'userlike-eyecatcher-box']")
	public WebElement hidingButtonPlaceOrder;

	@FindBy(xpath ="//h1[contains(text(),'Thanks for your payment.')]")
	public WebElement thanksForYourPayment;

//	@FindBy(css ="img.paypal-button-logo.paypal-button-logo-paypal.paypal-button-logo-gold")
//	public WebElement payPalButtonForUatServer;

	@FindBy(xpath ="//div[@id='paypal-animation-container' and @class='paypal-animation-container-expanded']")
	public WebElement payPalButtonForUatServer;
	
	@FindBy(xpath = "//div[contains(text(),'No invoice matched the given data')]")
	public WebElement popUpText;
	
	@FindBy(id = "paymentlink-forgot")
	public WebElement forgotPasswordLink;
	
	@FindBy(id = "forgot-invoice")
	public WebElement forgotInvoicePopUpField;

	public String removeCurrency(String text){
		String finalString = "";
		
		UtilityMethods.waitForPageLoadAndPageReady();

		int textLength = text.length();

		if(text.contains("€") || text.contains("zł") || text.contains("₩") || text.contains("円") || text.contains("£")){

			finalString  =  text.substring(0, textLength - 2);
			currency = text.substring(textLength - 2, textLength);
			System.out.println(currency);

		}

		if(text.contains("$")){

			finalString = text.substring(4,textLength);
			currency = text.substring(textLength - 2, textLength);
			System.out.println(currency);
		}
		return finalString;

	}

	public String removeDotsandCommas(String text){
		String finalString1 = "";
		String finalString ="";
		UtilityMethods.waitForPageLoadAndPageReady();

		finalString1 = text.replace(",", "");
		finalString = finalString1.replace(".","");
		return finalString;


	}

	public void verifyPaymentIsSuccessful() throws Throwable{

		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(thanksForYourPayment));

		try{

			if(thanksForYourPayment.isDisplayed()){

				UtilityMethods.validateAssertEqual("CARD PAYMENT DONE", "CARD PAYMENT DONE");
				System.out.println("PAYMENT IS SUCCESSFUL");
			}

		}
		catch(Exception e){

			UtilityMethods.validateAssertEqual("CARD PAYMENT DONE", "CARD PAYMENT NOT DONE");
			System.out.println("PAYMENT IS NOT SUCCESSFUL");

		}
		System.out.println("USER IS WAITING 5 MINUTES FOR PAYMENT CREATION IS ERP");
		UtilityMethods.wait10Seconds();
		if(CRMPage.flag == 1){
			
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.close();
			
			driver.switchTo().window(tabs.get(1));
			UtilityMethods.waitForPageLoadAndPageReady();
		}
	}

	public void calculateTheTaxAndTotals() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();



		//************************Calculation for the countries where TAX is applicable**********************************************	
		if(taxXpathIfVoucherExist.isDisplayed() && voucherXpath.isDisplayed() && checkIfTaxFieldIsPresent.isDisplayed()){
			System.out.println("\n");
			
			Reporter.addStepLog("GETTING THE VALUES OF TAX | SUBTOTAL | TOTAL ");
			System.out.println("GETTING THE VALUES OF TAX | SUBTOTAL | TOTAL ");
			System.out.println("\n");

			//Getting Tax Percentage from WebPage
			String valueOfTaxPercentageIs =taxValueInPercentageIfVoucherExist.getText();
			int indexOfpercentage = valueOfTaxPercentageIs.indexOf("%");
			valueOfTaxPercentageIs = valueOfTaxPercentageIs.substring(0,indexOfpercentage);
			double finalTaxPercentageValueIs = Double.parseDouble(valueOfTaxPercentageIs);
			
			Reporter.addStepLog("Tax % OF THIS STORE IS : "+finalTaxPercentageValueIs+"%");
			System.out.println("Tax % OF THIS STORE IS : "+finalTaxPercentageValueIs+"%");
			
			String getCheckOutURL = driver.getCurrentUrl();
			
			
			
			if(getCheckOutURL.contains("jp")){
				
				if(valueOfTaxPercentageIs.contains("10")){
					
					System.out.println("10% TAX IS FOUND FOR JAPAN");
				}
				
				else{
					
					UtilityMethods.validateAssertEqual("10% TAX", "NOT 10% TAX");
				}
					
				
			}
			

			//Getting SubTotal from WebPage
			String firstString = removeCurrency(subTotalXpathIfVoucherExist.getText());
			
			Reporter.addStepLog("SUBTOTAL VALUE SHOWING ON STORE IS :"+firstString+"");
			System.out.println("SUBTOTAL VALUE SHOWING ON STORE IS :"+firstString);

			//******************************SUBTOTAL VALUE**********************************//
			subTotalValue1 = removeDotsandCommas(firstString);
			//******************************SUBTOTAL VALUE**********************************//
			System.out.println("SUBTOTAL VALUE AFTER REMOVING DECIMALS AND COMMAS :"+subTotalValue1);
			Number nFormatFirstString = NumberFormat.getNumberInstance(Locale.FRANCE).parse(subTotalValue1);

			double DTotalValue = nFormatFirstString.floatValue();
			DecimalFormat f = new DecimalFormat("##.00");
			String subTotalValue = f.format(DTotalValue);
			double finalSubTotalValueInDouble  = Double.parseDouble(subTotalValue);	
			Reporter.addStepLog("SUBTOTAL VALUE EXCLUSIVE OF TAX ON STORE IS: "+finalSubTotalValueInDouble+"");
			System.out.println("SUBTOTAL VALUE EXCLUSIVE OF TAX ON STORE IS : "+finalSubTotalValueInDouble);


			//Getting Tax Value from WebPage
			String secondString  = removeCurrency(taxXpathIfVoucherExist.getText());
			String secondString1 = removeDotsandCommas(secondString);


			double finalXpathTaxValueInDouble  = Double.parseDouble(secondString1);
			//*****************************TAX VALUE*******************************//
			finalXpathTaxValueInString=  Double.toString(finalXpathTaxValueInDouble);
			//*********************************************************************//

			
			Reporter.addStepLog("TAX VALUE SHOWING ON STORE IS:"+finalXpathTaxValueInString+"");
			System.out.println("TAX VALUE SHOWING ON STORE IS :"+finalXpathTaxValueInString);


			//Getting Order Total from WebPage
			String thirdString  = removeCurrency(orderTotalXpathIfVoucherExist.getText());
			String thirdString1 = removeDotsandCommas(thirdString);

			Number nFormatThirdString = 	NumberFormat.getNumberInstance(Locale.FRANCE).parse(thirdString1);

			double DOrderTotalValue = nFormatThirdString.floatValue();

			//*****************************ORDER TOTAL*******************************//
			orderTotalValue = f.format(DOrderTotalValue);
			//**********************************************************************//
			double totalofTaxAndSubtotal = finalXpathTaxValueInDouble + finalSubTotalValueInDouble;
			String totalofTaxandSubtotalInString = Double.toString(totalofTaxAndSubtotal);
			
			Reporter.addStepLog("ACTUAL AMOUNT OF SUBTOTAL+TAX ON STORE ="+totalofTaxandSubtotalInString+"");
			System.out.println("ACTUAL AMOUNT OF SUBTOTAL+TAX ON STORE ="+totalofTaxandSubtotalInString);
			
			Reporter.addStepLog("EXPECTED AMOUNT OF SUBTOTAL+TAX ON STORE ="+orderTotalValue+"");
			System.out.println("EXPECTED AMOUNT OF SUBTOTAL+TAX ON STORE ="+orderTotalValue);


			System.out.println("\n");
			System.out.println("***************CHECKING VALIDATIONS HERE*******************");
			System.out.println("\n");



			//Validating the Totals   
			if(orderTotalValue.contains(totalofTaxandSubtotalInString)){

				
				Reporter.addStepLog("EXPECTED ORDER TOTAL VALUE: "+orderTotalValue+" == ACTUAL ORDER TOTAL VALUE: " +totalofTaxandSubtotalInString+"");
				System.out.println("Verify Sum of Products total = Subtotals(Checkout page): Expected order total value: "+orderTotalValue+" Actual order total value " +totalofTaxandSubtotalInString);

			}

			else{
				
				Reporter.addStepLog("VERIFY SUM OF PRODUCTS TOTAL = SUBTOTALS(CHECKOUT PAGE): EXPECTED ORDER VALUE: "+orderTotalValue+ "IS NOT EQUAL TO ACTUAL ORDER VALUE: " +totalofTaxandSubtotalInString);

				System.out.println(totalofTaxandSubtotalInString + " != "+orderTotalValue);
				UtilityMethods.validateAssertEqual("Calculated TOTAL Value is Correct", "Calculated TOTAL Value is NOT Correct");

			}

			//Calculating the Tax Amount
			double taxCalculation =	Math.round(((finalSubTotalValueInDouble*finalTaxPercentageValueIs)/100));
			String finalTaxCalculationValueInString= f.format(taxCalculation);

			Number CalculatedTaxWithoutDecimal = NumberFormat.getNumberInstance(Locale.FRANCE).parse(finalTaxCalculationValueInString);
			double CalculatedTaxWithoutDecimalInDouble = CalculatedTaxWithoutDecimal.floatValue();
			DecimalFormat f1 = new DecimalFormat("#");
			String finalCalculatedTaxWithoutDecimal = f1.format(CalculatedTaxWithoutDecimalInDouble);

		//	Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//	Reporter.addStepLog("CALCULATED TAX VALUE: "+finalCalculatedTaxWithoutDecimal+"");
			System.out.println("CALCULATED TAX VALUE: "+finalCalculatedTaxWithoutDecimal);





			if(finalXpathTaxValueInString.contains(finalCalculatedTaxWithoutDecimal)){


				
				Reporter.addStepLog("VERIFY ORDER TOTAL AFTER SALES TAX %: EXPECTED TAX VALUE: "+ finalTaxCalculationValueInString + "ACTUAL TAX VALUE: "+finalXpathTaxValueInString+"");
				System.out.println("Verify Order Total after Sales Tax % (Should be same as Checkout Page) Expected tax value: "+ finalTaxCalculationValueInString + "Actual tax value: "+finalXpathTaxValueInString+"");
			//	UtilityMethods.validateAssertEqual("Calculated Tax Value IS Correct", "Calculated Tax Value IS Correct");




			}
			else{

				
				Reporter.addStepLog("VERIFY ORDER TOTAL AFTER SALES TAX %: EXPECTED TAX VALUE: "+ finalTaxCalculationValueInString + "IS NOT EQUAL TO ACTUAL TAX VALUE: "+finalXpathTaxValueInString+"");
				System.out.println(finalTaxCalculationValueInString + " != "+finalXpathTaxValueInString);
				UtilityMethods.validateAssertEqual("Calculated Tax Value is Correct", "Calculated Tax Value is NOT Correct");	
			}
		}


		else{

			//*********************************Calculation for The VAT & for the Countries Where Tax is Not Applicable************************
			
			Reporter.addStepLog("NO TAX FOUND");
			System.out.println("NO TAX FOUND");
			finalXpathTaxValueInString ="000";
			System.out.println("\n");
			
			Reporter.addStepLog("GETTING THE VALUES OF SUBTOTAL | TOTAL");
			System.out.println("GETTING THE VALUES OF SUBTOTAL | TOTAL ");
			System.out.println("\n");



			//Getting the Subtotal Value if Tax is not Applicable and work for VAT
			String firstString = removeCurrency(subTotalXpathIfVoucherExist.getText());
			String firstString1 = removeDotsandCommas(firstString);
			Number nFormatFirstString = NumberFormat.getNumberInstance(Locale.FRANCE).parse(firstString1);

			double DTotalValue = nFormatFirstString.floatValue();
			DecimalFormat f = new DecimalFormat("##.00");
			String subTotalValue = f.format(DTotalValue);
			double finalSubTotalValueInDouble  = Double.parseDouble(subTotalValue);	
		
			Reporter.addStepLog("SUBTOTAL VALUE = "+finalSubTotalValueInDouble+"");
			System.out.println("SUBTOTAL VALUE = "+finalSubTotalValueInDouble);


			//Getting Order Total Value if Tax is not Applicable and VAT


			String thirdString  = removeCurrency(orderTotalXpathIfVoucherExistButNotTax.getText());
			String thirdString1 = removeDotsandCommas(thirdString);

			Number nFormatThirdString = NumberFormat.getNumberInstance(Locale.FRANCE).parse(thirdString1);

			double DOrderTotalValue = nFormatThirdString.floatValue();
			String orderTotalValue = f.format(DOrderTotalValue);
			double finalTotalValueIfTaIsNotApplicable = Double.parseDouble(orderTotalValue);
			
			Reporter.addStepLog("ORDER TOTAL VALUE = "+finalTotalValueIfTaIsNotApplicable+"");
			System.out.println("ORDER TOTAL VALUE = "+finalTotalValueIfTaIsNotApplicable);

			System.out.println("\n");
			System.out.println("***************CHECKING VALIDATIONS HERE*******************");
			System.out.println("\n");

			if(orderTotalValue.contains(subTotalValue)){
				
				Reporter.addStepLog("VERIFY SUM OF PRODUCTS TOTAL = SUBTOTALS(CHECKOUT PAGE) SUBTOTALS VALUE: "+subTotalValue + " ORDER TOTAL VALUE "+orderTotalValue+"");
				System.out.println("SUBTOTAL VALUE: "+subTotalValue + " == ORDER TOTAL VALUE: "+orderTotalValue);
			//	UtilityMethods.validateAssertEqual("Subtotal and Total Amounts are Equal", "Subtotal and Total Amounts are Equal");

			}

			else{
				
				Reporter.addStepLog("VERIFY SUM OF PRODUCTS TOTAL = SUBTOTALS(CHECKOUT PAGE) SUBTOTALS VALUE: "+subTotalValue + "IS NOT EQUAL TO ORDER TOTAL VALUE "+orderTotalValue+"");
				UtilityMethods.validateAssertEqual("Subtotal and Total Amounts are Equal", "Subtotal and Total Amounts are Not Equal");

			}

		}






		//	public void calculateTheTaxAndTotalsforKorea() throws Throwable{
		//		UtilityMethods.waitForPageLoadAndPageReady();
		//
		//		System.out.println("\n");
		//		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//	    Reporter.addStepLog("GETTING THE VALUES OF TAX | SUBTOTAL | TOTAL");
		//		System.out.println("GETTING THE VALUES OF TAX | SUBTOTAL | TOTAL ");
		//		System.out.println("\n");
		//		//************************Calculation for the countries where TAX is applicable**********************************************	
		//		if(taxXpathIfVoucherExist.isDisplayed() && voucherXpath.isDisplayed() && checkIfTaxFieldIsPresent.isDisplayed()){
		//
		//			//Getting Tax Percentage from WebPage
		//			String valueOfTaxPercentageIs =taxValueInPercentageIfVoucherExist.getText();
		//			valueOfTaxPercentageIs = valueOfTaxPercentageIs.substring(0,2);
		//			double finalTaxPercentageValueIs = Double.parseDouble(valueOfTaxPercentageIs);
		//
		//			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//			Reporter.addStepLog("Tax % OF THIS STORE IS : "+finalTaxPercentageValueIs+"%");
		//			System.out.println("Tax % OF THIS STORE IS : "+finalTaxPercentageValueIs+"%");
		//
		//			//Getting SubTotal from WebPage
		//			String firstString = removeCurrency(subTotalXpathIfVoucherExist.getText());
		//
		//			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//			Reporter.addStepLog("SUBTOTAL VALUE SHOWING ON STORE IS :"+firstString+"");
		//			System.out.println("SUBTOTAL VALUE SHOWING ON STORE IS :"+firstString);
		//			String firstString1 = removeDotsandCommas(firstString);
		//			System.out.println("SUBTOTAL VALUE AFTER REMOVING DECIMALS AND COMMAS :"+firstString1);
		//			Number nFormatFirstString = NumberFormat.getNumberInstance(Locale.FRANCE).parse(firstString1);
		//			String subTotalValue = nFormatFirstString.toString();
		//			double finalSubTotalValue  = Double.parseDouble(subTotalValue);	
		//			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//			Reporter.addStepLog("SUBTOTAL VALUE EXCLUSIVE OF TAX ON STORE IS : "+finalSubTotalValue+"");
		//			System.out.println("SUBTOTAL VALUE EXCLUSIVE OF TAX ON STORE IS : "+finalSubTotalValue);
		//
		//
		//			//Getting Tax Value from WebPage
		//			String secondString  = removeCurrency(taxXpathIfVoucherExist.getText());
		//			String secondString1 = removeDotsandCommas(secondString);
		//			Number nFormatsecondString = NumberFormat.getNumberInstance(Locale.FRANCE).parse(firstString1);
		//			String finalXpathTaxValueInString=  nFormatsecondString.toString();
		//			double finalXpathTaxValueInDouble = Double.valueOf(finalXpathTaxValueInString);
		//			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//			Reporter.addStepLog("TAX VALUE SHOWING ON STORE IS :"+finalXpathTaxValueInDouble+"");
		//			System.out.println("TAX VALUE SHOWING ON STORE IS :"+finalXpathTaxValueInDouble);
		//
		//
		//			//Getting Order Total from WebPage
		//			String thirdString  = removeCurrency(orderTotalXpathIfVoucherExist.getText());
		//			String thirdString1 = removeDotsandCommas(thirdString);
		//
		//			Number nFormatThirdString = 	NumberFormat.getNumberInstance(Locale.FRANCE).parse(thirdString1);
		//			String orderTotalValue = nFormatThirdString.toString();
		//			//double finalOrderTotalDouble  = Double.parseDouble(orderTotalValue);
		//			double totalofTaxAndSubtotal = finalXpathTaxValueInDouble + finalSubTotalValue;
		//			String totalofTaxandSubtotalInString = Double.toString(totalofTaxAndSubtotal);
		//			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//			Reporter.addStepLog("ACTUAL AMOUNT OF SUBTOTAL+TAX = "+totalofTaxandSubtotalInString+ " ******* EXPECTED AMOUNT OF SUBTOTAL+TAX = "+orderTotalValue+"");
		//			System.out.println("ACTUAL AMOUNT OF SUBTOTAL+TAX = "+totalofTaxandSubtotalInString+ " ******* EXPECTED AMOUNT OF SUBTOTAL+TAX = "+orderTotalValue);
		//
		//			System.out.println("\n");
		//			System.out.println("***************CHECKING VALIDATIONS HERE*******************");
		//			System.out.println("\n");
		//
		//
		//			//Calculating the Totals   
		//			if(orderTotalValue.contains(totalofTaxandSubtotalInString)){
		//				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//				Reporter.addStepLog("EXPECTED ORDER TOTAL AMOUNT: "+orderTotalValue+" == ACTUAL ORDER TOTAL AMOUNT: " +totalofTaxandSubtotalInString+"");
		//				System.out.println("EXPECTED ORDER TOTAL AMOUNT: "+orderTotalValue+" == ACTUAL ORDER TOTAL AMOUNT: " +totalofTaxandSubtotalInString);
		//				UtilityMethods.validateAssertEqual("Calculated TOTAL Value IS Correct", "Calculated TOTAL Value IS Correct");
		//			}
		//
		//			else{
		//				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//				Reporter.addStepLog(totalofTaxandSubtotalInString + " != "+orderTotalValue);
		//				System.out.println(totalofTaxandSubtotalInString + " != "+orderTotalValue);
		//				UtilityMethods.validateAssertEqual("Calculated TOTAL Value is Correct", "Calculated TOTAL Value is NOT Correct");
		//
		//			}
		//
		//			//Calculating the Tax Amount
		//			double taxCalculation =	Math.round(((finalSubTotalValue*finalTaxPercentageValueIs)/100));
		//			DecimalFormat f = new DecimalFormat("##.00");
		//			String finalTaxCalculationValueInString= f.format(taxCalculation);
		//
		//			Number CalculatedTaxWithoutDecimal = NumberFormat.getNumberInstance(Locale.FRANCE).parse(finalTaxCalculationValueInString);
		//			double CalculatedTaxWithoutDecimalInDouble = CalculatedTaxWithoutDecimal.floatValue();
		//			DecimalFormat f1 = new DecimalFormat("#");
		//			String finalCalculatedTaxWithoutDecimal = f1.format(CalculatedTaxWithoutDecimalInDouble);
		//
		//
		//
		//
		//
		//			if(finalXpathTaxValueInString.contains(finalCalculatedTaxWithoutDecimal)){
		//				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//				Reporter.addStepLog("EXPECTED TAX VALUE: "+ finalTaxCalculationValueInString + " == ACTUAL TAX VALUE: "+finalXpathTaxValueInString);
		//				System.out.println("EXPECTED TAX VALUE: "+ finalTaxCalculationValueInString + " == ACTUAL TAX VALUE: "+finalXpathTaxValueInString);
		//				UtilityMethods.validateAssertEqual("Calculated Tax Value IS Correct", "Calculated Tax Value IS Correct");
		//				
		//			}
		//			else{
		//				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//				Reporter.addStepLog(finalTaxCalculationValueInString + " != "+finalXpathTaxValueInString);
		//
		//				System.out.println(finalTaxCalculationValueInString + " != "+finalXpathTaxValueInString);
		//				UtilityMethods.validateAssertEqual("Calculated Tax Value is Correct", "Calculated Tax Value is NOT Correct");	
		//			}
		//		}
		//
		//
		//		else{
		//
		//			//*********************************Calculation for The VAT, for the Countries Where Tax is Not Applicable************************
		//			System.out.println("\n");
		//			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//			Reporter.addStepLog("GETTING THE VALUES OF SUBTOTAL | TOTAL");
		//			System.out.println("GETTING THE VALUES OF SUBTOTAL | TOTAL ");
		//			System.out.println("\n");
		//			finalXpathTaxValueInString ="000";
		//			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//			Reporter.addStepLog("NO TAX FOUND");
		//			System.out.println("NO TAX FOUND");
		//
		//			//Getting the Subtotal Value if Tax is not Applicable and work for VAT
		//			String firstString = removeCurrency(subTotalXpathIfVoucherExist.getText());
		//			subTotalValue1 = removeDotsandCommas(firstString);
		//			Number nFormatFirstString = NumberFormat.getNumberInstance(Locale.FRANCE).parse(subTotalValue1);
		//
		//			double DTotalValue = nFormatFirstString.floatValue();
		//			DecimalFormat f = new DecimalFormat("##.00");
		//			String subTotalValue = f.format(DTotalValue);
		//			double finalSubTotalValueInDouble  = Double.parseDouble(subTotalValue);	
		//			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//			Reporter.addStepLog("SUBTOTAL VALUE = "+finalSubTotalValueInDouble);
		//			System.out.println("SUBTOTAL VALUE = "+finalSubTotalValueInDouble);
		//
		//
		//			//Getting Order Total Value if Tax is not Applicable and VAT
		//
		//
		//			String thirdString  = removeCurrency(orderTotalXpathIfVoucherExistButNotTax.getText());
		//			String thirdString1 = removeDotsandCommas(thirdString);
		//
		//			Number nFormatThirdString = NumberFormat.getNumberInstance(Locale.FRANCE).parse(thirdString1);
		//
		//			double DOrderTotalValue = nFormatThirdString.floatValue();
		//			orderTotalValue = f.format(DOrderTotalValue);
		//			double finalTotalValueIfTaIsNotApplicable = Double.parseDouble(orderTotalValue);
		//			
		//			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//			Reporter.addStepLog("ORDER TOTAL VALUE = "+finalTotalValueIfTaIsNotApplicable);
		//			System.out.println("ORDER TOTAL VALUE = "+finalTotalValueIfTaIsNotApplicable);
		//
		//			System.out.println("\n");
		//			System.out.println("***************CHECKING VALIDATIONS HERE*******************");
		//			System.out.println("\n");
		//			
		//			if(orderTotalValue.contains(subTotalValue)){
		//				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//				Reporter.addStepLog("SUBTOTAL VALUE: "+subTotalValue + " == ORDER TOTAL VALUE: "+orderTotalValue);
		//				System.out.println("SUBTOTAL VALUE: "+subTotalValue + " == ORDER TOTAL VALUE: "+orderTotalValue);
		//				UtilityMethods.validateAssertEqual("Subtotal and Total Amounts are Equal", "Subtotal and Total Amounts are Equal");
		//				
		//			}
		//
		//			else{
		//				Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		//				Reporter.addStepLog("SUBTOTAL AND TOTAL AMOUNTS ARE NOT EQUAL");
		//				UtilityMethods.validateAssertEqual("Subtotal and Total Amounts are Equal", "Subtotal and Total Amounts are Not Equal");
		//
		//			}
		//
		//		}


	}


	public void clickVouch() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");

		Voucher.click();

		UtilityMethods.wait5Seconds();

	}


	public void clickApply() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");

		Apply.click();

		UtilityMethods.wait10Seconds();

	}

	public void enterECOMDataForPayment() throws ElementNotInteractableException, ElementClickInterceptedException, InterruptedException{
		UtilityMethods.waitForPageLoadAndPageReady();
		WebDriverWait wait = new WebDriverWait(driver,60);

		wait.until(ExpectedConditions.elementToBeClickable(nextStepButtonForECOMPayment));
		if(CRMPage.flag==1){
			emailFieldForECOMPayment.sendKeys(CRMPage.emailForLead);
			inVoiceFieldForECOMPayment.sendKeys(AX365Page.invoiceNumber);
		}
		else{
		emailFieldForECOMPayment.sendKeys("teamviewer.automation@systemsltd.com");
		inVoiceFieldForECOMPayment.sendKeys(AX365Page.inVoiceNo);
		}

		
		
		UtilityMethods.wait5Seconds();
		
		try{
			
			
		nextStepButtonForECOMPayment.click();
		
		}
		catch(Exception e){
			
			
		}
		
		
	}
	
	

	
	public void getPricesAndQuantityOfProducts() throws Throwable{
		

		subscriptionProductName = licenseName.getText();
		System.out.println("LICENSE NAME : "+subscriptionProductName);
		

		if(subscriptionProductName.equals("TeamViewer Remote Access")){

			subscriptionProductName = "TeamViewer Remote Access License";
			subscriptionProductCode ="TVR0001";
			System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
			Reporter.addStepLog("SELECTED MAIN PRODUCT IS:"+subscriptionProductCode);

		}

		if(subscriptionProductName.equals("Single User")){

			subscriptionProductName = "TeamViewer Business License";
			subscriptionProductCode ="TVB0001";
			System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
			Reporter.addStepLog("SELECTED MAIN PRODUCT IS:"+subscriptionProductCode);
		}

		if(subscriptionProductName.equals("Multi User")){

			subscriptionProductName = "TeamViewer Premium / TeamViewer Premium Yearly Subscription";
			subscriptionProductCode ="TVP0001";
			System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
			Reporter.addStepLog("SELECTED MAIN PRODUCT IS:"+subscriptionProductCode);
		}

		if(subscriptionProductName.equals("For Teams")){

			subscriptionProductName = "TeamViewer Corporate License";
			subscriptionProductCode ="TVC0001";
			System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
			Reporter.addStepLog("SELECTED MAIN PRODUCT IS:"+subscriptionProductCode);
		}
		
		
		try{
		priceOfTheSelectedLicense =  priceofTheLicense.getText();
		int indexOf€0 = priceOfTheSelectedLicense.indexOf("€");
		priceOfTheSelectedLicense = removeDotsandCommas( priceOfTheSelectedLicense.substring(0,indexOf€0-1));
		System.out.println("PRICE OF SELECTED LICENSE "+priceOfTheSelectedLicense);
		Reporter.addStepLog("PRICE OF SELECTED LICENSE: "+priceOfTheSelectedLicense);
		}
		catch(Exception e){
			
			
		}
		
		
		try{
		quantityOfAdditionalUsers =  quantityOfAdditionalConcurrentUsers.getText(); 
		int indexOfa = quantityOfAdditionalUsers.indexOf("a");
		quantityOfAdditionalUsers = removeDotsandCommas(quantityOfAdditionalUsers.substring(0,indexOfa-1));
		System.out.println("QUANTITY OF ADDITIONAL CONCURRENT USERS "+quantityOfAdditionalUsers);
		Reporter.addStepLog("QUANTITY OF ADDITIONAL CONCURRENT USERS: "+quantityOfAdditionalUsers);
		}
		
		catch(Exception e){
			
			
		}
		
		try{
		priceOfAdditionalUsers =  priceofAdditionalConcurrentUsers.getText();
		int indexOf€ = priceOfAdditionalUsers.indexOf("€");
		priceOfAdditionalUsers =removeDotsandCommas( priceOfAdditionalUsers.substring(0,indexOf€-1));
		System.out.println("PRICE OF ADDITIONAL USERS "+priceOfAdditionalUsers);
		Reporter.addStepLog("PRICE OF ADDITIONAL USERS: "+priceOfAdditionalUsers);
		}
		
		catch(Exception e){
			
			System.out.println("NO PRICE FOUND FOR ADDITIONAL CONCURRENT USERS");
		}
		
		
		
		try{
		quantityOfMobileSupporters =  quantityOfMobileDevicesSupport.getText();
		int indexofX = quantityOfMobileSupporters.indexOf("x");
		quantityOfMobileSupporters = removeDotsandCommas(quantityOfMobileSupporters.substring(0,indexofX-1));
		System.out.println("QUANTITY OF SUPPORT FOR MOBILE DEVICES "+quantityOfMobileSupporters);
		Reporter.addStepLog("QUANTITY OF SUPPORT FOR MOBILE DEVICES: "+quantityOfMobileSupporters);
		}
		
		catch(Exception e){
			
			System.out.println("SUPPORT FOR MOBILE DEVICES IS NOT SELECTED");
		}
		
		try{
		priceOfMobileSupport = priceOfMobileDeviceSupport.getText();
		int indexOf€1 = priceOfMobileSupport.indexOf("€");
		priceOfMobileSupport = removeDotsandCommas(priceOfMobileSupport.substring(0,indexOf€1-1));
		System.out.println("PRICE OF SUPPORT FOR MOBILE DEVICES "+priceOfMobileSupport);
		Reporter.addStepLog("PRICE OF SUPPORT FOR MOBILE DEVICES: "+priceOfMobileSupport);
		}
		
		catch(Exception e){
			
			System.out.println("PRICE OF SUPPORT FOR MOBILE DEVICES");
		}
		
		try{
		orderTotalValueForERP = orderTotalXpathIfVoucherExist.getText();
		int indexOf€2 = orderTotalValueForERP.indexOf("€");
		orderTotalValueForERP =removeDotsandCommas( orderTotalValueForERP.substring(0,indexOf€2-1));
		System.out.println("Verify Sum of Products total: "+orderTotalValueForERP);
		Reporter.addStepLog("VERIFY SUM OF PRODUCTS TOTAL: "+orderTotalValueForERP);
		
		}
		catch(Exception e){
			
			System.out.println("TAX IS NOT APPLICABLE");
		}
		
		
		try{
			
			orderTotalValueForERP = orderTotalXpathIfVoucherExistButNotTax.getText();
			int indexOf€3 = orderTotalValueForERP.indexOf("€");
			orderTotalValueForERP =removeDotsandCommas( orderTotalValueForERP.substring(0,indexOf€3-1));
			System.out.println("Verify Sum of Products total: "+orderTotalValueForERP);
			Reporter.addStepLog("VERIFY SUM OF PRODUCTS TOTAL: "+orderTotalValueForERP);
			
		}
		catch(Exception k){
			
			System.out.println("TAX IS APPLICABLE");
			
			
		}
	
		
		
		
		}
	public void getPricesAndQuantityOfProductsForUSCountries() throws Throwable{
		
		subscriptionProductName = licenseName.getText();
		System.out.println("LICENSE NAME : "+subscriptionProductName);
		

		if(subscriptionProductName.equals("TeamViewer Remote Access")){

			subscriptionProductName = "TeamViewer Remote Access License";
			subscriptionProductCode ="TVR0001";
			System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
			Reporter.addStepLog("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);

		}

		if(subscriptionProductName.equals("Single User")){

			subscriptionProductName = "TeamViewer Business License";
			subscriptionProductCode ="TVB0001";
			System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
			Reporter.addStepLog("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
		}

		if(subscriptionProductName.equals("Multi User")){

			subscriptionProductName = "TeamViewer Premium / TeamViewer Premium Yearly Subscription";
			subscriptionProductCode ="TVP0001";
			System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
			Reporter.addStepLog("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
		}

		if(subscriptionProductName.equals("For Teams")){

			subscriptionProductName = "TeamViewer Corporate License";
			subscriptionProductCode ="TVC0001";
			System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
			Reporter.addStepLog("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
		}
		
		
		
		priceOfTheSelectedLicense =  priceofTheLicense.getText();
		int indexOf€0 = priceOfTheSelectedLicense.indexOf("$");
		priceOfTheSelectedLicense = removeDotsandCommas( priceOfTheSelectedLicense.substring(0,indexOf€0-1));
		System.out.println("PRICE OF SELECTED LICENSE "+priceOfTheSelectedLicense);
		Reporter.addStepLog("PRICE OF SELECTED LICENSE "+priceOfTheSelectedLicense);
		
		
		
		try{
		
		quantityOfTVMonitoring =  quantityOfAdditionalConcurrentUsers.getText(); 
		int indexOfSpace = quantityOfTVMonitoring.indexOf(" ");
		quantityOfTVMonitoring = removeDotsandCommas(quantityOfTVMonitoring.substring(0,indexOfSpace));
		System.out.println("QUANTITY OF TV MONITORING "+quantityOfTVMonitoring);
		Reporter.addStepLog("QUANTITY OF TV MONITORING "+quantityOfTVMonitoring);
		
		}
		
		catch(Exception e){
			
			System.out.println("QUANTITY OF TV MONITORING IS NOT FOUND");
		}
		
		try{
		priceOfTVMonitoring =  priceOfTVmonitor.getText();
		int indexOf€ = priceOfTVMonitoring.indexOf("$");
		priceOfTVMonitoring =removeDotsandCommas( priceOfTVMonitoring.substring(0,indexOf€-1));
		System.out.println("PRICE OF TV MONITORING "+priceOfTVMonitoring);
		Reporter.addStepLog("PRICE OF TV MONITORING "+priceOfTVMonitoring);
		}
		
		catch(Exception e){
			
			System.out.println("PRICE OF TV MONITORING IS NOT FOUND");
			
		}
		
		
		try{
		quantityOfTVEndpoint =  quantityOfMobileDevicesSupport.getText();
		int indexofX = quantityOfTVEndpoint.indexOf(" ");
		quantityOfTVEndpoint = removeDotsandCommas(quantityOfTVEndpoint.substring(0,indexofX));
		System.out.println("QUANTITY OF TV END POINT "+quantityOfTVEndpoint);
		Reporter.addStepLog("QUANTITY OF TV END POINT "+quantityOfTVEndpoint);
		
		}
		
		catch(Exception e){
			
			System.out.println("QUANTITY OF TV ENDPOINT IS NOT FOUND");
			
		}
		
		
		try{
		priceOfTVEndpoint = priceOftvEndpoint.getText();
		int indexOf€1 = priceOfTVEndpoint.indexOf("$");
		priceOfTVEndpoint = removeDotsandCommas(priceOfTVEndpoint.substring(0,indexOf€1-1));
		System.out.println("PRICE OF TV END POINT "+priceOfTVEndpoint);
		Reporter.addStepLog("PRICE OF END POINT "+priceOfTVEndpoint);
		
		}
		
		catch(Exception e){
			
			System.out.println("PRICE OF END POINT NOT FOUND");
		}
		
		try{
		quantityOfTVBackup =  quantitOfTVBackUp.getText();
		int indexofy = quantityOfTVBackup.indexOf(" ");
		quantityOfTVBackup = removeDotsandCommas(quantityOfTVBackup.substring(0,indexofy));
		System.out.println("QUANTITY OF TV BACK UP "+quantityOfTVBackup);
		Reporter.addStepLog("QUANTITY OF TV BACK UP "+quantityOfTVBackup);
		}
		
		catch(Exception e){
			
			System.out.println("QUANTITY OF TV BACK UP NOT FOUND");
		}
		
		try{
		priceOfTVBackUp = priceOfTVBackup.getText();
		int indexOf€2 = priceOfTVBackUp.indexOf("$");
		priceOfTVBackUp = removeDotsandCommas(priceOfTVBackUp.substring(0,indexOf€2-1));
		System.out.println("PRICE OF TV BACK UP "+priceOfTVBackUp);
		Reporter.addStepLog("PRICE OF TV BACK UP "+priceOfTVBackUp);
		
		}
		
		catch(Exception e){
			
			System.out.println("PRICE OF TV BACK UP NOT FOUND");
		}
		
		
		try{
		quantityOfTVPilot =  quantityofTVPilot.getText();
		int indexofy2 = quantityOfTVPilot.indexOf(" ");
		quantityOfTVPilot = removeDotsandCommas(quantityOfTVPilot.substring(0,indexofy2));
		System.out.println("QUANTITY OF TV PIOLT "+quantityOfTVPilot);
		Reporter.addStepLog("QUANTITY OF TV PIOLT "+quantityOfTVPilot);
		}
		
		catch(Exception e){
			
			System.out.println("QUANTITY OF TV PIOLT NOT FOUND");
		}
		
		try{
		priceOfTVPilot = priceofTVPilot.getText();
		int indexOf€3 = priceOfTVPilot.indexOf("$");
		priceOfTVPilot = removeDotsandCommas(priceOfTVPilot.substring(0,indexOf€3-1));
		System.out.println("PRICE OF OF TV PIL0T "+priceOfTVPilot);
		Reporter.addStepLog("PRICE OF OF TV PILOT "+priceOfTVPilot);
		}
		
		catch(Exception e){
			
			System.out.println("PRICE OF TV PILOT NOT FOUND");
		}
		
		
		try{
		orderTotalValueForERP = orderTotalXpathIfVoucherExist.getText();
		int indexOf€4 = orderTotalValueForERP.indexOf("円");
		orderTotalValueForERP =removeDotsandCommas( orderTotalValueForERP.substring(0,indexOf€4-1));
		System.out.println("ORDER TOTAL AMOUNT "+orderTotalValueForERP);
		Reporter.addStepLog("ORDER TOTAL AMOUNT "+orderTotalValueForERP);
		
		}
		catch(Exception e){
			
			System.out.println("TAX IS NOT APPLICABLE");
		}
		
		
		try{
			
			orderTotalValueForERP = orderTotalXpathIfVoucherExistButNotTax.getText();
			int indexOf€5 = orderTotalValueForERP.indexOf("$");
			orderTotalValueForERP =removeDotsandCommas( orderTotalValueForERP.substring(0,indexOf€5-1));
			System.out.println("ORDER TOTAL AMOUNT "+orderTotalValueForERP);
			Reporter.addStepLog("ORDER TOTAL AMOUNT "+orderTotalValueForERP);
			
		}
		catch(Exception k){
			
			System.out.println("TAX IS APPLICABLE");
			
			
		}
		
	}
	public void getPricesAndQuantityOfProductsForJapan() throws Throwable{
		
		subscriptionProductName = licenseName.getText();
	System.out.println("LICENSE NAME : "+subscriptionProductName);
	

	if(subscriptionProductName.equals("TeamViewer Remote Access")){

		subscriptionProductName = "TeamViewer Remote Access License";
		subscriptionProductCode ="TVR0001";
		System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
		Reporter.addStepLog("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);

	}

	if(subscriptionProductName.equals("Single User")){

		subscriptionProductName = "TeamViewer Business License";
		subscriptionProductCode ="TVB0001";
		System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
		Reporter.addStepLog("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
	}

	if(subscriptionProductName.equals("Multi User")){

		subscriptionProductName = "TeamViewer Premium / TeamViewer Premium Yearly Subscription";
		subscriptionProductCode ="TVP0001";
		System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
		Reporter.addStepLog("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
	}

	if(subscriptionProductName.equals("For Teams")){

		subscriptionProductName = "TeamViewer Corporate License";
		subscriptionProductCode ="TVC0001";
		System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
		Reporter.addStepLog("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
	}
	
	
	
	priceOfTheSelectedLicense =  priceofTheLicense.getText();
	int indexOf€0 = priceOfTheSelectedLicense.indexOf("円");
	priceOfTheSelectedLicense = removeDotsandCommas( priceOfTheSelectedLicense.substring(0,indexOf€0-1));
	System.out.println("PRICE OF SELECTED LICENSE "+priceOfTheSelectedLicense);
	Reporter.addStepLog("PRICE OF SELECTED LICENSE "+priceOfTheSelectedLicense);
	
	
	
	try{
	
	quantityOfTVMonitoring =  quantityOfAdditionalConcurrentUsers.getText(); 
	int indexOfSpace = quantityOfTVMonitoring.indexOf(" ");
	quantityOfTVMonitoring = removeDotsandCommas(quantityOfTVMonitoring.substring(0,indexOfSpace));
	System.out.println("QUANTITY OF TV MONITORING "+quantityOfTVMonitoring);
	Reporter.addStepLog("QUANTITY OF TV MONITORING "+quantityOfTVMonitoring);
	
	}
	
	catch(Exception e){
		
		System.out.println("QUANTITY OF TV MONITORING IS NOT FOUND");
	}
	
	try{
	priceOfTVMonitoring =  priceOfTVmonitor.getText();
	int indexOf€ = priceOfTVMonitoring.indexOf("円");
	priceOfTVMonitoring =removeDotsandCommas( priceOfTVMonitoring.substring(0,indexOf€-1));
	System.out.println("PRICE OF TV MONITORING "+priceOfTVMonitoring);
	Reporter.addStepLog("PRICE OF TV MONITORING "+priceOfTVMonitoring);
	}
	
	catch(Exception e){
		
		System.out.println("PRICE OF TV MONITORING IS NOT FOUND");
		
	}
	
	
	try{
	quantityOfTVEndpoint =  quantityOfMobileDevicesSupport.getText();
	int indexofX = quantityOfTVEndpoint.indexOf(" ");
	quantityOfTVEndpoint = removeDotsandCommas(quantityOfTVEndpoint.substring(0,indexofX));
	System.out.println("QUANTITY OF TV END POINT "+quantityOfTVEndpoint);
	Reporter.addStepLog("QUANTITY OF TV END POINT "+quantityOfTVEndpoint);
	
	}
	
	catch(Exception e){
		
		System.out.println("QUANTITY OF TV ENDPOINT IS NOT FOUND");
		
	}
	
	
	try{
	priceOfTVEndpoint = priceOftvEndpoint.getText();
	int indexOf€1 = priceOfTVEndpoint.indexOf("円");
	priceOfTVEndpoint = removeDotsandCommas(priceOfTVEndpoint.substring(0,indexOf€1-1));
	System.out.println("PRICE OF TV END POINT "+priceOfTVEndpoint);
	Reporter.addStepLog("PRICE OF END POINT "+priceOfTVEndpoint);
	
	}
	
	catch(Exception e){
		
		System.out.println("PRICE OF END POINT NOT FOUND");
	}
	
	try{
	quantityOfTVBackup =  quantitOfTVBackUp.getText();
	int indexofy = quantityOfTVBackup.indexOf(" ");
	quantityOfTVBackup = removeDotsandCommas(quantityOfTVBackup.substring(0,indexofy));
	System.out.println("QUANTITY OF TV BACK UP "+quantityOfTVBackup);
	Reporter.addStepLog("QUANTITY OF TV BACK UP "+quantityOfTVBackup);
	}
	
	catch(Exception e){
		
		System.out.println("QUANTITY OF TV BACK UP NOT FOUND");
	}
	
	try{
	priceOfTVBackUp = priceOfTVBackup.getText();
	int indexOf€2 = priceOfTVBackUp.indexOf("円");
	priceOfTVBackUp = removeDotsandCommas(priceOfTVBackUp.substring(0,indexOf€2-1));
	System.out.println("PRICE OF TV BACK UP "+priceOfTVBackUp);
	Reporter.addStepLog("PRICE OF TV BACK UP "+priceOfTVBackUp);
	
	}
	
	catch(Exception e){
		
		System.out.println("PRICE OF TV BACK UP NOT FOUND");
	}
	
	
	try{
	quantityOfTVPilot =  quantityofTVPilot.getText();
	int indexofy2 = quantityOfTVPilot.indexOf(" ");
	quantityOfTVPilot = removeDotsandCommas(quantityOfTVPilot.substring(0,indexofy2));
	System.out.println("QUANTITY OF TV PIOLT "+quantityOfTVPilot);
	Reporter.addStepLog("QUANTITY OF TV PIOLT "+quantityOfTVPilot);
	}
	
	catch(Exception e){
		
		System.out.println("QUANTITY OF TV PIOLT NOT FOUND");
	}
	
	try{
	priceOfTVPilot = priceofTVPilot.getText();
	int indexOf€3 = priceOfTVPilot.indexOf("円");
	priceOfTVPilot = removeDotsandCommas(priceOfTVPilot.substring(0,indexOf€3-1));
	System.out.println("PRICE OF OF TV PIL0T "+priceOfTVPilot);
	Reporter.addStepLog("PRICE OF OF TV PILOT "+priceOfTVPilot);
	}
	
	catch(Exception e){
		
		System.out.println("PRICE OF TV PILOT NOT FOUND");
	}
	
	
	try{
	orderTotalValueForERP = orderTotalXpathIfVoucherExist.getText();
	int indexOf€4 = orderTotalValueForERP.indexOf("円");
	orderTotalValueForERP =removeDotsandCommas( orderTotalValueForERP.substring(0,indexOf€4-1));
	System.out.println("ORDER TOTAL AMOUNT "+orderTotalValueForERP);
	Reporter.addStepLog("ORDER TOTAL AMOUNT "+orderTotalValueForERP);
	
	}
	catch(Exception e){
		
		System.out.println("TAX IS NOT APPLICABLE");
	}
	
	
	try{
		
		orderTotalValueForERP = orderTotalXpathIfVoucherExistButNotTax.getText();
		int indexOf€5 = orderTotalValueForERP.indexOf("円");
		orderTotalValueForERP =removeDotsandCommas( orderTotalValueForERP.substring(0,indexOf€5-1));
		System.out.println("ORDER TOTAL AMOUNT "+orderTotalValueForERP);
		Reporter.addStepLog("ORDER TOTAL AMOUNT "+orderTotalValueForERP);
		
	}
	catch(Exception k){
		
		System.out.println("TAX IS APPLICABLE");
		
		
	}
	
	}
public void getPricesAndQuantityOfProductsForBusinessLicense() throws Throwable{
		
	
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		subscriptionProductName = licenseName.getText();
		System.out.println("LICENSE NAME : "+subscriptionProductName);
		

		if(subscriptionProductName.equals("TeamViewer Remote Access")){

			subscriptionProductName = "TeamViewer Remote Access License";
			subscriptionProductCode ="TVR0001";
			System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
			Reporter.addStepLog("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);

		}

		if(subscriptionProductName.equals("Single User")){

			subscriptionProductName = "TeamViewer Business License";
			subscriptionProductCode ="TVB0001";
			System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
			Reporter.addStepLog("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
		}

		if(subscriptionProductName.equals("Multi User") || subscriptionProductName.equals("TeamViewer Premium")){

			subscriptionProductName = "TeamViewer Premium / TeamViewer Premium Yearly Subscription";
			subscriptionProductCode ="TVP0001";
			System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
			Reporter.addStepLog("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
		}

		if(subscriptionProductName.equals("For Teams")){

			subscriptionProductName = "TeamViewer Corporate License";
			subscriptionProductCode ="TVC0001";
			System.out.println("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
			Reporter.addStepLog("SELECTED MAIN PRODUCT IS :"+subscriptionProductCode);
		}
		
		
		try{
		priceOfTheSelectedLicense =  priceofTheLicense.getText();
		System.out.println("Price of license is: "+priceOfTheSelectedLicense);
		int indexOf₩0 = priceOfTheSelectedLicense.indexOf("€");
		priceOfTheSelectedLicense = removeDotsandCommas( priceOfTheSelectedLicense.substring(0,indexOf₩0-1));
		System.out.println("PRICE OF SELECTED LICENSE "+priceOfTheSelectedLicense);
		Reporter.addStepLog("PRICE OF SELECTED LICENSE "+priceOfTheSelectedLicense);
		}
		catch(NoSuchElementException e){
			
			priceOfTheSelectedLicense = priceofTheLicenseForQuote.getText();
			System.out.println("Price of license is: "+priceOfTheSelectedLicense);
			int indexOf₩0 = priceOfTheSelectedLicense.indexOf("€");
			priceOfTheSelectedLicense = removeDotsandCommas( priceOfTheSelectedLicense.substring(0,indexOf₩0-1));
			System.out.println("PRICE OF SELECTED LICENSE "+priceOfTheSelectedLicense);
			Reporter.addStepLog("PRICE OF SELECTED LICENSE "+priceOfTheSelectedLicense);
			
		}
		
		
		try{
		
		quantityOfTVMonitoring =  quantityOfAdditionalConcurrentUsers.getText(); 
		int indexOfSpace = quantityOfTVMonitoring.indexOf(" ");
		quantityOfTVMonitoring = removeDotsandCommas(quantityOfTVMonitoring.substring(0,indexOfSpace));
		System.out.println("QUANTITY OF TV MONITORING "+quantityOfTVMonitoring);
		Reporter.addStepLog("QUANTITY OF TV MONITORING "+quantityOfTVMonitoring);
		
		}
		
		catch(Exception e){
			
			System.out.println("QUANTITY OF TV MONITORING IS NOT FOUND");
		}
		
		try{
		priceOfTVMonitoring =  priceOfTVmonitor.getText();
		int indexOf€ = priceOfTVMonitoring.indexOf("€");
		priceOfTVMonitoring =removeDotsandCommas( priceOfTVMonitoring.substring(0,indexOf€-1));
		System.out.println("PRICE OF TV MONITORING "+priceOfTVMonitoring);
		Reporter.addStepLog("PRICE OF TV MONITORING "+priceOfTVMonitoring);
		}
		
		catch(Exception e){
			
			System.out.println("PRICE OF TV MONITORING IS NOT FOUND");
			
		}
		
		
		try{
		quantityOfTVEndpoint =  quantityOfMobileDevicesSupport.getText();
		int indexofX = quantityOfTVEndpoint.indexOf(" ");
		quantityOfTVEndpoint = removeDotsandCommas(quantityOfTVEndpoint.substring(0,indexofX));
		System.out.println("QUANTITY OF TV END POINT "+quantityOfTVEndpoint);
		Reporter.addStepLog("QUANTITY OF TV END POINT "+quantityOfTVEndpoint);
		
		}
		
		catch(Exception e){
			
			System.out.println("QUANTITY OF TV ENDPOINT IS NOT FOUND");
			
		}
		
		
		try{
		priceOfTVEndpoint = priceOftvEndpoint.getText();
		int indexOf€1 = priceOfTVEndpoint.indexOf("€");
		priceOfTVEndpoint = removeDotsandCommas(priceOfTVEndpoint.substring(0,indexOf€1-1));
		System.out.println("PRICE OF TV END POINT "+priceOfTVEndpoint);
		Reporter.addStepLog("PRICE OF END POINT "+priceOfTVEndpoint);
		
		}
		
		catch(Exception e){
			
			System.out.println("PRICE OF END POINT NOT FOUND");
		}
		
		try{
		quantityOfTVBackup =  quantitOfTVBackUp.getText();
		int indexofy = quantityOfTVBackup.indexOf(" ");
		quantityOfTVBackup = removeDotsandCommas(quantityOfTVBackup.substring(0,indexofy));
		System.out.println("QUANTITY OF TV BACK UP "+quantityOfTVBackup);
		Reporter.addStepLog("QUANTITY OF TV BACK UP "+quantityOfTVBackup);
		}
		
		catch(Exception e){
			
			System.out.println("QUANTITY OF TV BACK UP IS NOT SHOWING ");
		}
		
		try{
		priceOfTVBackUp = priceOfTVBackup.getText();
		int indexOf€2 = priceOfTVBackUp.indexOf("€");
		priceOfTVBackUp = removeDotsandCommas(priceOfTVBackUp.substring(0,indexOf€2-1));
		System.out.println("PRICE OF TV BACK UP "+priceOfTVBackUp);
		Reporter.addStepLog("PRICE OF TV BACK UP "+priceOfTVBackUp);
		
		}
		
		catch(Exception e){
			
			System.out.println(e.getMessage());
		}
		
		
		try{
		quantityOfTVPilot =  quantityofTVPilot.getText();
		int indexofy2 = quantityOfTVPilot.indexOf(" ");
		quantityOfTVPilot = removeDotsandCommas(quantityOfTVPilot.substring(0,indexofy2));
		System.out.println("QUANTITY OF TV PIOLT "+quantityOfTVPilot);
		Reporter.addStepLog("QUANTITY OF TV PIOLT "+quantityOfTVPilot);
		}
		
		catch(Exception e){
			
			System.out.println(e.getMessage());
		}
		
		try{
		priceOfTVPilot = priceofTVPilot.getText();
		int indexOf€3 = priceOfTVPilot.indexOf("€");
		priceOfTVPilot = removeDotsandCommas(priceOfTVPilot.substring(0,indexOf€3-1));
		System.out.println("PRICE OF OF TV PIOLT "+priceOfTVPilot);
		Reporter.addStepLog("PRICE OF OF TV PIOLT "+priceOfTVPilot);
		}
		
		catch(Exception e){
			
			System.out.println(e.getMessage());
		}
		
		
		try{
		orderTotalValueForERP = orderTotalXpathIfVoucherExist.getText();
		int indexOf€4 = orderTotalValueForERP.indexOf("€");
		orderTotalValueForERP =removeDotsandCommas( orderTotalValueForERP.substring(0,indexOf€4-1));
		System.out.println("ORDER TOTAL AMOUNT "+orderTotalValueForERP);
		Reporter.addStepLog("ORDER TOTAL AMOUNT "+orderTotalValueForERP);
		
		}
		catch(Exception e){
			
			System.out.println("TAX IS NOT APPLICABLE");
		}
		
		
		try{
			
			orderTotalValueForERP = orderTotalXpathIfVoucherExistButNotTax.getText();
			int indexOf€5 = orderTotalValueForERP.indexOf("€");
			orderTotalValueForERP =removeDotsandCommas( orderTotalValueForERP.substring(0,indexOf€5-1));
			System.out.println("ORDER TOTAL AMOUNT "+orderTotalValueForERP);
			Reporter.addStepLog("ORDER TOTAL AMOUNT "+orderTotalValueForERP);
			
		}
		catch(Exception k){
			
			System.out.println("TAX IS APPLICABLE");
			
			
		}
	
		
		
		
		}

	public void clickCheckBox() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,2000)");


		Checkbox.click(); 
		Reporter.addStepLog("USER HAS CLICKED CHECKBOX");
		UtilityMethods.wait5Seconds();


	}








	public void clickPO() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,2000)");
		UtilityMethods.wait10Seconds();

		try{
			new WebDriverWait(driver,10).until(ExpectedConditions.elementToBeClickable(PlaceOrder));
			UtilityMethods.clickElemetJavaSciptExecutor(PlaceOrder);
			Reporter.addStepLog("USER HAS CLICKED PLACE ORDER BUTTON");
	
		}
		catch(Exception e){
			
			System.out.println("Problem is : "+e.getMessage());
			UtilityMethods.waitForPageLoadAndPageReady();
			new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//iframe[contains(@title,'ppbutton')]")));
			driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@title,'ppbutton')]")));
	
			UtilityMethods.waitForElementDisplayed(payPalButtonForUatServer, 30);
			new Actions(driver).doubleClick(payPalButtonForUatServer).perform();

			Reporter.addStepLog("USER HAS CLICKED PAYPAL BUTTON");
			
			driver.switchTo().defaultContent();
			UtilityMethods.wait3Seconds();
			UtilityMethods.waitForPageLoadAndPageReady();
			
		}
		
		Reporter.addStepLog("USER HAS CLICKED 'PLACE ORDER' BUTTON");
		System.out.println("USER HAS CLICKED PLACE ORDER BUTTON");
		UtilityMethods.wait30Seconds();

	}




	public void clickInvoicePaymentButton() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");

		new Actions(driver).moveToElement(invoicePaymentButton).click(invoicePaymentButton).build().perform();
		Reporter.addStepLog("USER SELECTS 'INVOICE' AS PAYMENT METHOD");
		System.out.println("USER HAS SELECTED INVOICE AS PAYMENT METHOD ");
		UtilityMethods.wait3Seconds();
	
	}


	public void clickCreditCardRadioButton() throws Throwable{

		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(CreditCard));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		UtilityMethods.wait3Seconds();
		CreditCard.click();
	
		Reporter.addStepLog("USER HAS SELECTED 'CREDIT CARD' AS PAYMENT METHOD");
		System.out.println("USER HAS SELECTED CREDIT CARD AS PAYMENT METHOD ");
		UtilityMethods.wait10Seconds();
	
		Reporter.addStepLog("USER IS ENTERING 'CREDIT CARD' INFORMATION");
		System.out.println("USER IS ENTERING THE CREDIT CARD INFORMATION ");
	}

	public void clickProceedButton() throws Throwable{

		try{

			if(proceedPayment.isDisplayed()){

				proceedPayment.click();
				System.out.println("USER HAS CLICKED ON PROCESSING PAYMENT BUTTON");
			}
		}
		catch(Exception e){

			System.out.println("PROCEED PAYMENT BUTTON DOESN'T EXIST");
		}


	}
	public void clickPayPalButton() throws Throwable{

		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(payPalPaymentButton));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,2000)", "");
		UtilityMethods.wait2Seconds();
		payPalPaymentButton.click();
		UtilityMethods.wait3Seconds();
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING 'PAYPAL' INFORMATION");
		System.out.println("USER IS ENTERING 'PAYPAL' INFORMATION ");

	}

	
	public void enterPayPalCredentialsForERP() throws Throwable{
		
		//When Paypal Opens in a window
		UtilityMethods.wait5Seconds();
		

		ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(newTab);
		UtilityMethods.wait5Seconds();
		driver.switchTo().window(newTab.get(2));
		UtilityMethods.wait5Seconds();
		System.out.println(driver.getTitle());
		
		WebDriverWait wait = new WebDriverWait(driver,120);
			
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@name = 'login_email']"))));
			driver.findElement(By.xpath("//input[@name = 'login_email']")).clear();
			driver.findElement(By.xpath("//input[@name = 'login_email']")).sendKeys("Intdev-buyer@teamviewer.com");
			driver.findElement(By.xpath("//button[@id = 'btnNext']")).click();
			UtilityMethods.wait5Seconds();
			payPalPasswordFieldOnPopup.sendKeys("s7ugeWuc");
			payPalLoginButtonOnPopup.click();
		



		//Clicking Continue Button

		UtilityMethods.wait15Seconds();
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath("//button[contains(text(),'Continue')]"))).click().build().perform();
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		
		//Clicking AgreeAndPay Button
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@id='confirmButtonTop']"))));
		jse.executeScript("window.scrollBy(0,2000)", "");
		UtilityMethods.wait3Seconds();
		driver.findElement(By.xpath("//input[@id='confirmButtonTop']")).click();

		//Switching To Parent Handle
		driver.switchTo().window(newTab.get(1));
		System.out.println(driver.getTitle());
		UtilityMethods.wait10Seconds();
		
		
		
		
		
		//When PayPal Opens in a Page
//		WebDriverWait wait = new WebDriverWait(driver,120);
//		try{
//
//			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@name = 'login_email']"))));
//			driver.findElement(By.xpath("//input[@name = 'login_email']")).clear();
//			driver.findElement(By.xpath("//input[@name = 'login_email']")).sendKeys("Intdev-buyer@teamviewer.com");
//			driver.findElement(By.xpath("//button[@id = 'btnNext']")).click();
//			UtilityMethods.wait5Seconds();
//			payPalPasswordFieldOnPopup.sendKeys("s7ugeWuc");
//			payPalLoginButtonOnPopup.click();
//		}
//		catch(Exception e){}
//	
//		UtilityMethods.wait15Seconds();
//		Actions action = new Actions(driver);
//		action.moveToElement(driver.findElement(By.xpath("//button[contains(text(),'Continue')]"))).click().build().perform();
//		
//		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@id='confirmButtonTop']"))));
//		driver.findElement(By.xpath("//input[@id='confirmButtonTop']")).click();
	
	}
	public void enterPayPalCredentials() throws Throwable{
		UtilityMethods.wait10Seconds();
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS ENTERING PAYPAL CREDENTIALS");
		String parentHandle = driver.getWindowHandle();

		ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());

		for(String currentTab : newTab){

			if(!currentTab.equals(parentHandle)){

				driver.switchTo().window(currentTab);
				System.out.println(driver.getTitle());
			}

		}	

		UtilityMethods.wait10Seconds();

		try{
			driver.findElement(By.xpath("//a[contains(text(),'Accept Cookies')]")).click();
		}
		catch(Exception e){
			System.out.println("Cookie not found");
		}
		WebDriverWait wait = new WebDriverWait(driver,120);
		try{

			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@name = 'login_email']"))));
			driver.findElement(By.xpath("//input[@name = 'login_email']")).clear();
			driver.findElement(By.xpath("//input[@name = 'login_email']")).sendKeys("Intdev-buyer@teamviewer.com");
			driver.findElement(By.xpath("//button[@id = 'btnNext']")).click();
			UtilityMethods.waitForPageLoadAndPageReady();
			UtilityMethods.wait10Seconds();
			payPalPasswordFieldOnPopup.sendKeys("s7ugeWuc");
			payPalLoginButtonOnPopup.click();
		}
		catch(Exception e){

			System.out.println("USER IS ALREADY LOGGED IN TO PAYPAL");

		}

		UtilityMethods.wait20Seconds();

		//Clicking Continue Button
		if(driver.findElements(By.xpath("//a[contains(text(),'Not now')]")).size()!=0){
			
			driver.findElement(By.xpath("//a[contains(text(),'Not now')]")).click();
			UtilityMethods.waitForPageLoadAndPageReady();
			
		}
			
	//	Actions action = new Actions(driver);
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
//		driver.findElement(By.xpath("//span[contains(text(),'TestBank')]")).click();
		JavascriptExecutor jse5 = (JavascriptExecutor)driver;
		jse5.executeScript("window.scrollBy(0,2000)", "");
		UtilityMethods.wait2Seconds();
//		UtilityMethods.moveToElementWithJSExecutor(driver.findElement(By.xpath("//button[contains(text(),'Continue')]")));
//		UtilityMethods.clickElemetJavaSciptExecutor(driver.findElement(By.xpath("//button[contains(text(),'Continue')]")));
		new Actions(driver).moveToElement(driver.findElement(By.xpath("//button[contains(text(),'Continue')]"))).click().build().perform();
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,2000)", "");

		try{
			driver.findElement(By.xpath("//a[contains(text(),'Accept Cookies')]")).click();
		}
		catch(Exception e){
			System.out.println("Cookie not found");
		}
		
		//Clicking AgreeAndPay Button
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@id='confirmButtonTop']"))));
		
//		JavascriptExecutor jse2 = (JavascriptExecutor)driver;
//		jse2.executeScript("window.scrollBy(0,2000)", "");
		UtilityMethods.wait3Seconds();
		driver.findElement(By.xpath("//input[@id='confirmButtonTop']")).click();

		//Switching To Parent Handle
		driver.switchTo().window(parentHandle);
		System.out.println(driver.getTitle());
		UtilityMethods.wait10Seconds();

	}



	public String getDuplicateEmailError() throws InterruptedException {
		UtilityMethods.waitForElementEnabled(DuplicateEmailError, 10);
		return DuplicateEmailError.getText();
	}


	public String getInValidVoucherError() throws InterruptedException {
		UtilityMethods.waitForElementEnabled(InValidVoucherError, 10);
		return InValidVoucherError.getText();


	}

	public String getInvalidVATError() throws InterruptedException {
		UtilityMethods.waitForElementEnabled(InValidVATError, 10);
		return InValidVATError.getText();

	}

	public String getValidVoucher() throws InterruptedException {
		UtilityMethods.waitForElementEnabled(ValidVoucher, 20);
		return ValidVoucher.getText();

	}
	
	public void verifyErrorPopUp() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
		new Actions(driver).moveToElement(popUpText).click(popUpText).build().perform();
		if(popUpText.isDisplayed()){
			System.out.println("ERROR POP UP FOR ENTERING DETAIL AGAIN IN ECOM APPEARS");
		}
		else{
			
			System.out.println("ERROR POP UP FOR ENTERING DETAIL AGAIN IN ECOM DOES NOT APPEAR");
			UtilityMethods.validateAssertEqual("ERROR POP UP FOR ENTERING DETAIL AGAIN IN ECOM APPEARS", "ERROR POP UP FOR ENTERING DETAIL AGAIN IN ECOM DOES NOT APPEAR");
		}
		
	
	}
	
	public void getURLandVerify() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait10Seconds();
		new WebDriverWait(driver,60).until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[contains(text(),'Credit Card')]"))));
		if(driver.findElement(By.xpath("//span[contains(text(),'Credit Card')]")).isDisplayed()){
			String url = driver.getCurrentUrl();
			System.out.println(url);
			if(url.contains("nl")){
				
				System.out.println("Neitherlands payment URL opens");
				
			}
			else{
				
				UtilityMethods.validateAssertEqual("URL OF NEITHERLANDS FOR PAYMENT", "URL OF OTHER COUNTRY FOR PAYMENT");
				
			}
		
		}
		
		
		
	}


	public CheckOutPage(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);

	}

	public void enterTextInTextBox(WebElement element, String text) throws InterruptedException {
		UtilityMethods.waitForPageLoadAndPageReady();
		//UtilityMethods.waitForElementClickable(element);
		element.sendKeys(text);

	}
	
	
}



