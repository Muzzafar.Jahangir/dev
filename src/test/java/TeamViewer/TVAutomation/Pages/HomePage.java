package TeamViewer.TVAutomation.Pages;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;


public class HomePage extends DriverFactory {

	public static String countryName="";
	@FindBy(xpath = "(//*[@type='submit'])[1]")
	public WebElement remoteAccessSubscriptionButtonForStaging;

	@FindBy(xpath = "(//a[contains(text(),'Buy now')])[2]")
	public WebElement remoteAccessSubscriptionButtonForProduction;

	@FindBy(xpath = "(//a[contains(text(),'Buy now')])[1]")
	public WebElement remoteAccessSubscriptionButtonForSpain;


	//@FindBy (xpath = "(//*[@type='submit'])[2]")
	@FindBy (xpath = "(//a[contains(text(),'Buy now')])[2]")
	public WebElement businessLicenseButton;



	@FindBy (xpath = "(//a[contains(text(),'Buy now')])[3]")
	public WebElement premiumLicenseButton;


	@FindBy (xpath = "(//a[@class='button buy-now-cta for-teams corporate'])[1]")
	public WebElement corporateLicenseButton;


	@FindBy (xpath = "//*[@id='onetrust-accept-btn-handler']")
	public WebElement cookie;
	
	
	@FindBy (xpath = "//span[@data-overlay-class= 'overlay-fade']")
	public WebElement changeTheLanguageIcon;
	
	
	@FindBy (xpath = "//select[@name = 'language-selector']")
	public WebElement clickChangeLanguageDropdown;
	
	
	@FindBy (xpath = "(//option[contains(text(),'English')])[1]")
	public WebElement selectEnglishLanguage;
	
	
	
	@FindBy (xpath = "//button[@type= 'submit']")
	public WebElement clickSaveButton;
	
	@FindBy (xpath = "(//a[contains(text(),'Buy now')])[1]")
	public WebElement buyNowButton;
	
	@FindBy(xpath = "//span[contains(text(),'Request Quote')]")
	public WebElement requestQuote;
	
	@FindBy(xpath = "(//h4[contains(text(),'TeamViewer')])[1]")
	public WebElement teamViewer;
	
	@FindBy(id = "prod_drpdwn")
	public WebElement selectPremiumProduct;


	public void changeLanguageandOpenWebShop() throws Throwable{
//		
//		changeTheLanguageIcon.click();
//		
//		UtilityMethods.wait3Seconds();
//		
//		
//		clickChangeLanguageDropdown.click();
//		
//		UtilityMethods.wait2Seconds();
//		
//		selectEnglishLanguage.click();
//		
//		UtilityMethods.wait2Seconds();
//		
//		clickSaveButton.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		
		buyNowButton.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait7Seconds();
	}


	public void clickRemoteAccessSubscription() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,500)");

	//	UtilityMethods.scrollToWebElement(remoteAccessSubscriptionButtonForProduction);
		System.out.println("SCRIPT IS RUNNING FOR PRODUCTION");
		remoteAccessSubscriptionButtonForProduction.click();


		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CLICKED ON LICENSE BUTTON");
		System.out.println("USER HAS CLICKED ON LICENSE BUTTON");

		UtilityMethods.wait20Seconds();

	}
	
	public void clickPremiumProduct() throws Throwable{
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOf(teamViewer));
		teamViewer.click();
		UtilityMethods.wait3Seconds();
		selectPremiumProduct.click();
		new Select(selectPremiumProduct).selectByVisibleText("TeamViewer Premium");
		
		
		
	
		
	}

	public void clickRemoteAccessSubscriptionForSpain() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,500)");
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER HAS CLICKED ON LICENSE BUTTON");
		System.out.println("USER HAS CLICKED ON LICENSE BUTTON");
		remoteAccessSubscriptionButtonForSpain.click();

		UtilityMethods.wait10Seconds();

	}

	public void clickBusinessLicenseButton() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,200)");

	//	businessLicenseButton.click();
		driver.navigate().to("https://uat.teamviewer.com/en-nl/checkout/cart/index/productsku/TVB0001_000000089/qty/1/?p=business");
		System.out.println("USER HAS CLICKED ON LICENSE BUTTON");
		UtilityMethods.wait10Seconds();
	}


	public void clickPremiumLicenseButton() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();
//		UtilityMethods.scrollToWebElement(premiumLicenseButton);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,200)");
		//new Actions(driver).moveToElement(premiumLicenseButton).click(premiumLicenseButton).build().perform();
		driver.navigate().to("https://uat.service.teamviewer.com/en-be/checkout/cart/index/productsku/TVP0001_000000093/qty/1/?p=premium");
//		premiumLicenseButton.click();
		System.out.println("USER HAS CLICKED ON LICENSE BUTTON");

		UtilityMethods.wait10Seconds();
	}

	public void clickCorporateLicenseButton() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,300)");
		System.out.println("User has clicked on License button");
		System.out.println(corporateLicenseButton.getAttribute("href"));
		String url = corporateLicenseButton.getAttribute("href");
		int index = url.indexOf("teamviewer");
		String url2 = url.substring(index, url.length());
		
//		System.out.println(url2);
		String urlToAdd = corporateLicenseButton.getAttribute("href").substring(0, index)+"service."+url2;
		System.out.println(urlToAdd);
//		System.out.println(urlToAdd+url2);
		UtilityMethods.waitForPageLoadAndPageReady();
	//	driver.get(urlToAdd);
		driver.navigate().to("https://uat:abcd@1234@uat.service.teamviewer.com/en-nl/checkout/cart/index/productsku/TVC0001_000000091/qty/1/?p=corporate");
	//	driver.navigate().to("https://uat:abcd@1234@https://uat.service.teamviewer.com/en-nl/checkout/cart/");
	//	UtilityMethods.clickElemetJavaSciptExecutor(corporateLicenseButton);

		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
	}
	

	public void clickCookie() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		String storeUrl = driver.getCurrentUrl();
		System.out.println(storeUrl);
		int storeUrlLength = storeUrl.length();
		String finalCountryName = storeUrl.substring(50, 56);
		System.out.println(finalCountryName+storeUrlLength);
		
		if(finalCountryName.contains("fr")){

			countryName= "France";
			System.out.println(countryName);
		}
		else if(finalCountryName.contains("nl")){

			countryName= "Netherlands";
			System.out.println(countryName);
		}
		else if(finalCountryName.contains("it")){

			countryName= "Italy";
			System.out.println(countryName);
		}
		else if(finalCountryName.contains("be")){

			countryName= "Belgium";
		}
		else if(finalCountryName.contains("bg")){

			countryName= "Bulgaria";
		}
		else if(finalCountryName.contains("es")){

			countryName= "Spain";
		}
		else if(finalCountryName.contains("lu")){

			countryName= "Luxembourg";
		}
		else if(finalCountryName.contains("al")){

			countryName= "Albania";
		}
		else if(finalCountryName.contains("ee")){

			countryName= "Estonia";
		}
		else if(finalCountryName.contains("pl")){

			countryName= "Poland";
		}
		else if(finalCountryName.contains("pt")){

			countryName= "Portugal";
		}
		else if(finalCountryName.contains("pa")){

			countryName= "Panama";
		}
		else if(finalCountryName.contains("gb")){

			countryName= "United Kingdom";
		}
		else if(finalCountryName.contains("ec")){

			countryName= "Ecuador";
		}
		else if(finalCountryName.contains("uy")){

			countryName= "Uruguay";
		}
		else if(finalCountryName.contains("ie")){

			countryName= "Ireland";
		}
		else if(finalCountryName.contains("ar")){

			countryName= "Argentina";
		}
		else{
			System.out.println("Country not identified");
		}
		
		UtilityMethods.wait5Seconds();
		try{
			if(cookie.isDisplayed()){
				cookie.click();
			//	Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
				Reporter.addStepLog("ACCEPTED THE COOKIE");
				System.out.println("EATING THE COOKIE");
			}


		}
		catch(Exception e){

			System.out.println("COOKIE NOT FOUND!!!");
			
		}
		UtilityMethods.wait5Seconds();
	}



	public HomePage(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);

	}
}