package TeamViewer.TVAutomation.Pages;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;

public class GmailLoginPage extends DriverFactory {
	
	@FindBy (xpath = "(//*[@class='h-c-header__nav-li-link '])[4]")
	public WebElement EmailsignIn;
	
	@FindBy (xpath = "//*[@name='identifier']")
	public WebElement emailFieldLogin;
	
	@FindBy (xpath = "//*[@name='password']")
	public WebElement passwordFieldLogin;
	
	@FindBy (xpath = "//span[contains(text(),'Next')]")
	public WebElement nextButton;
	
	@FindBy (xpath = "(//span[contains(text(),'Your TeamViewer order confirmation') or contains(text(),'Su confirmación de pedido TeamViewer') or contains(text(),'Sua confirmação de ordem TeamViewer') or contains(text(),'Confirmation de votre commande TeamViewer') or contains(text(),'Uw TeamViewer-bestellingsbevestiging')])[2]")
	public WebElement clickEmailItem;
	
	@FindBy (xpath = "(//*[@title='Inbox'])[1]")
	public WebElement goToInbox;
	
	
	
	@FindBy (xpath = "(//span[contains(text(),'How to activate your TeamViewer license - Customer Number ')])[2]")
	public WebElement clickEmailItemActivation;
	
	@FindBy (xpath = "//a[contains(text(),'Activate now.')]")
	public WebElement activateLink;
	
	@FindBy (xpath = "(//span[@role='checkbox'])[1]")
	public WebElement selectAllEmails;
	
	@FindBy (xpath = "(//div[@data-tooltip='Archive'])[1]")
	public WebElement addToArchive;
	
	
	public void archiveTheEmails() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		selectAllEmails.click();
		UtilityMethods.wait3Seconds();
		addToArchive.click();
		
		
	}
	
	
	
	
	
	
	public void clickEmailItem() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		clickEmailItem.click();
		UtilityMethods.wait5Seconds();
		goToInbox.click();
		
		
	}
	
	
	public void clickEmailItemActivation() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		clickEmailItemActivation.click();
		UtilityMethods.wait3Seconds();
		activateLink.click();
		UtilityMethods.wait3Seconds();
			
		
	}
	
	public void archiveEmails() throws Throwable{
		
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(3));
		UtilityMethods.wait3Seconds();
		goToInbox.click();
		UtilityMethods.wait1Seconds();
		selectAllEmails.click();
		UtilityMethods.wait1Seconds();
		addToArchive.click();
		
	}

	
	
	public void clickSignIn() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		EmailsignIn.click();
		
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		UtilityMethods.wait30Seconds();
		
	}
	
	public void clickNextButton() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		nextButton.click();
		UtilityMethods.waitForPageLoadAndPageReady();
		

	}
	
	public void EnterEMail() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		driver.findElement(By.xpath("//*[@name='identifier']"));
		driver.findElement(By.xpath("//*[@name='identifier']")).sendKeys("qa.tester.lead1");
		UtilityMethods.waitForPageLoadAndPageReady();
	}
	
	public void EnterPassword() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();

		driver.findElement(By.xpath("//*[@name='password']"));
		driver.findElement(By.xpath("//*[@name='password']")).sendKeys("k!@#$%^&*(");
		UtilityMethods.waitForPageLoadAndPageReady();
	}
	
	
	public GmailLoginPage(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);
			
	}
	
	

}
