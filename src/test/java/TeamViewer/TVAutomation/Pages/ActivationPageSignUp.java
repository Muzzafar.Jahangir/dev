package TeamViewer.TVAutomation.Pages;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cucumber.listener.Reporter;

import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.UtilityMethods;


public class ActivationPageSignUp extends DriverFactory {
	
	public static String email;
	public static String password;


	@FindBy (xpath= "//a[contains(text(),'Create an account now')]")
	public WebElement createAnAccountNow;


	@FindBy (xpath= "//*[@id='Email']")
	public WebElement emailFieldOnSignUp;

	@FindBy (xpath= "//*[@id='Password']")
	public WebElement passwordFieldOnSignUp;

	@FindBy (xpath= "//*[@id='DisplayName']")
	public WebElement UserName;

	@FindBy (xpath= "//div[contains(text(),'This e-mail address is already in use')]")
	public WebElement emailAlreadyExist;

	@FindBy (xpath= "//a[contains(text(),'Sign In')]")
	public WebElement againSigningInButton;

	@FindBy (xpath= "//input[@id='UserName']")
	public WebElement emailFieldOnLogin;

	@FindBy (xpath= "//input[@id='Password']")
	public WebElement passWordFieldOnLogin;

	@FindBy (id= "btn-sign-in")
	public WebElement getStartedButton;

	@FindBy(xpath = "//a[contains(text(),'Create an account now')]")
	public WebElement newAccountLink;
	
	@FindBy(xpath = "//button[contains(text(),'Sign Up')]")
	public WebElement signUpButton;
	
	@FindBy(xpath = "//button[contains(text(),'Sign Up')]")
	public WebElement signUp;
	
	@FindBy(id = "UserName")
	public WebElement userName;
	
	@FindBy(id = "Password")
	public WebElement inputPassword;
	
	@FindBy(xpath = "//button[contains(text(),'Ignore and continue this time')]")
	public WebElement ignoreButton;


	public void clickActivation() throws Throwable{
		UtilityMethods.waitForPageLoadAndPageReady();
		createAnAccountNow.click();


	}

	public void enterSignUpData() throws Throwable{

		
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(2));

		UtilityMethods.waitForElementDisplayed(emailFieldOnSignUp, 30);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS CREATING AN ACCOUNT ON TEAMVIEWER MANAGEMENT CONSOLE");

		emailFieldOnSignUp.sendKeys("teamviewer.automation@systemsltd.com");
		UtilityMethods.wait1Seconds();

		UserName.sendKeys("NalTest");
		UtilityMethods.wait1Seconds();
		passwordFieldOnSignUp.sendKeys("k123456789");
		UtilityMethods.wait1Seconds();


		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,2000)");

		signUpButton.click();
	
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));

		try{

			UtilityMethods.wait3Seconds();

			if(emailAlreadyExist.isDisplayed()){

				UtilityMethods.wait1Seconds();
				
				System.out.println("YOUR ACCOUNT ON TEAMVIEWER ALREADY EXIST");	

			}
			
		}

		catch(Exception e){
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS SIGNED UP TO TEAMVIEWER MANAGEMENT CONSOLE SUCCESSFULLY");
			System.out.println("USER HAS SIGNED UP TO TEAMVIEWER MANAGEMENT CONSOLE SUCCESSFULLY");
		}
		
		

	}
	
	public void enterSignUpDataForMCOPortal() throws Throwable{

		
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		driver.switchTo().window(tabs.get(1));
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait2Seconds();
		
	//	signUpButton.click();
		newAccountLink.click();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		
		UtilityMethods.waitForElementDisplayed(emailFieldOnSignUp, 30);
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
		Reporter.addStepLog("USER IS CREATING AN ACCOUNT ON TEAMVIEWER MANAGEMENT CONSOLE");

		DateFormat df = new SimpleDateFormat("ddMMyyHHmmss");
	       Date dateobj = new Date();
	      
	      String emailIncrementer = df.format(dateobj);
	      System.out.println(emailIncrementer);
	       email = "teamviewerautomationsystems+"+emailIncrementer+"@gmail.com";
	       password = "testing@123";
	       
	       
		emailFieldOnSignUp.sendKeys(email);
		UtilityMethods.wait1Seconds();

		UserName.sendKeys("NAL MULLER");
		UtilityMethods.wait1Seconds();
		passwordFieldOnSignUp.sendKeys("testing@123");
		UtilityMethods.wait3Seconds();
		driver.findElement(By.id("EulaCheckBox")).click();
		
		UtilityMethods.moveToElementWithJSExecutor(signUp);
		signUp.click();
		UtilityMethods.wait10Seconds();
		Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));

		try{

			UtilityMethods.wait3Seconds();

			if(emailAlreadyExist.isDisplayed()){

				UtilityMethods.wait1Seconds();
				
				System.out.println("YOUR ACCOUNT ON TEAMVIEWER ALREADY EXIST");	

			}
			
		}

		catch(Exception e){
			Reporter.loadXMLConfig(new File("src\\test\\resources\\features\\extent-config.xml"));
			Reporter.addStepLog("USER HAS SIGNED UP TO TEAMVIEWER MANAGEMENT CONSOLE SUCCESSFULLY");
			System.out.println("USER HAS SIGNED UP TO TEAMVIEWER MANAGEMENT CONSOLE SUCCESSFULLY");
		}
		
		

	}
	
	public void switchAndLoginMCOPortal() throws Throwable{

		
		UtilityMethods.waitForPageLoadAndPageReady();
		String url = "https://uat.teamviewer.com/en-nl/dashboard";
		((JavascriptExecutor)driver).executeScript("window.open('about:blank','_blank')");
		UtilityMethods.CloseCurrentTabAndOpenNewTab(url);
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait5Seconds();
		
		System.out.println(email);
		System.out.println(password);
		try{
		userName.sendKeys(email);
		UtilityMethods.wait2Seconds();
		inputPassword.sendKeys(password);
		UtilityMethods.wait2Seconds();
		getStartedButton.click();
		
		UtilityMethods.waitForPageLoadAndPageReady();
		UtilityMethods.wait3Seconds();
		if(ignoreButton.isDisplayed()){
			
			ignoreButton.click();
			
		}
		
		}
		catch(Exception e){
			System.out.println("USER ALREADY LOGGED IN");
			
		}

	}



	public ActivationPageSignUp(WebDriver driver) throws Exception {
		PageFactory.initElements(driver, this);

	}



}
