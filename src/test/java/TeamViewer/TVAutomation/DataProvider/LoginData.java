package TeamViewer.TVAutomation.DataProvider;

import static TeamViewer.TVAutomation.Utils.DataPool.readExcelData;

public class LoginData {

	String excelSheetName;
	String excelSheetTab;
	String caseID;

	public LoginData(String excelSheetName, String excelSheetTab, String caseID) {
		this.excelSheetName = excelSheetName;
		this.excelSheetTab = excelSheetTab;
		this.caseID = caseID;
	}

	public String getUsername() throws Throwable {
		return readExcelData(excelSheetTab, caseID).get(TestData.USER_COLUMN_HEADING).toString();
	}

	public String getPassword() throws Throwable {
		return readExcelData(excelSheetTab, caseID).get(TestData.PASSWORD_COLUMN_HEADING).toString();
	}

	public String getName() throws Throwable {
		return readExcelData(excelSheetTab, caseID).get("name").toString();
	}
}
