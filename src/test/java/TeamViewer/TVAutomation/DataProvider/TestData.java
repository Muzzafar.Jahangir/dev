package TeamViewer.TVAutomation.DataProvider;

/**
 * Created by Farhan Sheikh on July 24 2017
 */
public class TestData {

	public static final String EXCEL_FILE_NAME = "Testdata_TV";
	public static final String EXCEL_FILE_EXTENSION = ".xlsx";

	
	public static final String ADD_DATA_SHEET = "AddData";
	public static final String URLS_SHEET = "url";
	public static final String ADD_DATA_SHEET_CRM = "AddCRMOppertunityData";
	public static final String ADD_CUSTOMER_DATA_SHEET_CRM = "AddCRMCustomerData";
	
	public final static String MANDATORY_FIELD_ERROR_MESSAGE_1 = "Please provide valid value for field";
	public final static String MANDATORY_FIELD_ERROR_MESSAGE_2 = "!";
	public static long SELENIUM_RESULTS_MIN;
	
	public interface InformationMessage {
		public final static String GIFT_CARD_ZERO_BALANCE = "Your Gift Card amount was $0.00, Please provide a different Gift Card having sufficient amount to process the order!";
		public final static String GIFT_CARD_LESS_BALANCE = "Your Gift Card did not have enough amount, Please provide a different Gift Card!";
		public final static String ELEMENT_NOT_FOUND_EXCEPTION = " was not visible on UI for some reason, so application was not able to interact with it. ";
		public final static String TEST_CASE_FAILED_MESSAGE = "Test Case failed at Step : ";
		public final static String TIME_OUT_EXCEPTION_MESSAGE = "Execution did not complete in given time. ";
		public final static String ASSERT_EQUAL_MESSAGE = " is not equal to the ";
		public final static String ASSERT_NOT_EQUAL_MESSAGE = " is equal to the ";
		public final static String DOUBLE_QUOTES = "\"";
		public final static String DOUBLE_QUOTES_END = "\"!";
		public final static String WISHLIST_NOT_EMPTY_BAG_MESSAGE = "Your wishlist bag is not empty!";
		public final static String SHOPPING_BAG_NOT_EMPTY_BAG_MESSAGE = "Your shopping bag is not empty!";
		public final static String ITEM_BAG_SIZE_NOT_M_MESSAGE = "Selected Bag Item size is not 'M'!";
		public final static String ITEM_BAG_SIZE_NOT_L_MESSAGE = "Selected Bag Item size is not 'L'!";
		public final static String BAG_ORDER_SUMMARY_ITEM_TOTAL_COUNT_MESSAGE = "Bag Order Summary did not showing the Items Total count!";
		public final static String CHECKOUT_BUTTON_NOT_SHOWING_MESSAGE = "Checkout button is not showing on UI!";
		public final static String PAYPAL_BUTTON_NOT_SHOWING_MESSAGE = "PayPal button is not showing on UI!";
		public final static String WISHLIST_ITEM_SIZE_NOT_M_MESSAGE = "Selected Wishlist Item size is not 'M'!";
		public final static String WISHLIST_ITEM_SIZE_NOT_L_MESSAGE = "Selected Wishlist Item size is not 'L'!";
		public final static String SEARCH_ITEM_NOT_MATCHED_FIRST_BAG_ITEM = "Search item did not matched with the First Bag Item!";
		public final static String SEARCH_ITEM_NOT_MATCHED_SECOND_BAG_ITEM = "Search item did not matched with the Second Bag Item!";
		public final static String BAG_SIZE_NOT_EQUAL_2 = "Bag size is not equal to 2!";
		public final static String TOTAL_BAG_SIZE_NOT_SHOWING_TOTAL_ITEM_COUNT_MESSAGE = "Total bag size is not showing the total item count!";
		public final static String PAGE_HEADING_TITLE_NOT_MY_BAG_MESSAGE = "Page heading Title is not 'My Bag'!";
		public final static String WISHLIST_TITLE_NOT_WISHLIST_MESSAGE = "Wishlist Title is not 'Wishlist'!";
		public final static String URL_NOT_ENDS_CARD_MESSAGE = "Current URL did not ends with 'cart'!";
		public final static String FINAL_ACTUAL_TOTAL_NOT_EQUAL_TOTAL_DIFFERENCE = "Final Actual Total is not equal to Total Difference!";
		public final static String FINAL_ACTUAL_TOTAL_EQUAL_TOTAL_DIFFERENCE = "Final Actual Total is equal to Total Difference!";
		public final static String EXPECTED_SUB_TOTAL_NOT_EQUAL_MESSAGE = "Expected Sub Total according to Item Quantity is not equal to Updated Sub Total!";
		public final static String EXPECTED_SUB_TOTAL_EQUAL_MESSAGE = "Expected Sub Total according to Item Quantity is equal to Updated Sub Total!";
		public final static String EXPECTED_SUB_TOTAL_NOT_EQUAL_UPDATED_SUB_MESSAGE = "Expected Sub Total is not equal to Updated Sub Total!";
		public final static String EXPECTED_SUB_TOTAL_EQUAL_UPDATED_SUB_MESSAGE = "Expected Sub Total is equal to Updated Sub Total!";
		public final static String ZERO_BALANCE_MESSAGE = "Your order has no balance, so no payment method is necessary to complete this order.";
	}
	
	// login_credentials sheet columns
		public static final String USER_COLUMN_HEADING = "User";
		public static final String PASSWORD_COLUMN_HEADING = "Password";
	
	// AddData sheet columns

	public final static String FIRST_NAME_COLUMN = "FirstName";
	public final static String LAST_NAME_COLUMN = "LastName";
	public final static String COMPANY_COLUMN = "Company";
	public final static String VAT_COLUMN = "VAT";
	public final static String EMAIL_COLUMN = "Email";
	public final static String CITY_COLUMN = "City";
	public final static String ADDRESS_COLUMN = "Address";
	public final static String ZIP_COLUMN = "ZIP";
	public final static String PHONE_COLUMN = "Phone";
	public final static String VOUCHER_COLUMN = "Voucher";
	public final static String ORDER_SUCCESS = "OrderSuccessText";
	public final static String CREDIT_CARD_NUMBER="CreditCard Number" ;
	public final static String CREDIT_CARD_MONTH = "Month" ;
	public final static String CREDIT_CARD_YEAR = "Year";
	public final static String CREDIT_CARD_CVV = "CVV";
	public final static String COUNTRY = "Country";
	


	public final static String URL_COLUMN = "urlName";
	
	
	// createAccount sheet key values
	public static final String CREATE_JP_DATA = "JP";
	public static final String CREATE_KR_DATA = "KR";
	public static final String CREATE_PL_DATA = "PL";
	public static final String CREATE_NL_DATA = "NL";
	public static final String CREATE_PLT_DATA = "PLT";
	public static final String CREATE_AL_DATA = "AL";
	public static final String CREATE_PT_DATA = "PT";
	public static final String CREATE_UY_DATA = "UY";
	public static final String CREATE_PA_DATA = "PA";
	public static final String CREATE_BG_DATA = "BG";
	public static final String CREATE_EE_DATA = "EE";
	public static final String CREATE_PR_DATA = "PR";
	public static final String CREATE_EC_DATA = "EC";
	public static final String CREATE_BE_DATA = "BE";
	public static final String CREATE_CR_DATA = "CR";
	public static final String CREATE_MT_DATA = "MT";
	public static final String CREATE_DM_DATA = "DM";
	public static final String CREATE_FR_DATA = "FR";
	public static final String CREATE_ES_DATA = "ES";
	public static final String CREATE_LU_DATA = "LU";
	public static final String CREATE_IT_DATA = "IT";
	public static final String CREATE_NL_DATA_AMERICAN = "NLA";
	public static final String CREATE_NL_DATA_MASTER = "NLM";
	public static final String CREATE_AR_DATA = "AR";
	public static final String CREATE_ART_DATA = "ART";
	public static final String CREATE_UK_DATA = "GB";
	public static final String CREATE_UKT_DATA = "GBT";
	public static final String CREATE_IR_DATA = "IR";
	
	
	public static final String NL_URL = "NL";
	public static final String BE_URL= "BE";
	
	public static final String SIGN_UP_DATA_2_KEY = "signUpDataConfirmPasswordLT5";
	public static final String SIGN_UP_DATA_3_KEY = "mismatchedPasswordAndConfirmPassword";
	public static final String SIGN_UP_DATA_4_KEY = "passwordGreaterThan20";
	public static final String SIGN_UP_DATA_5_KEY = "confirmPasswordGreaterThan20";
	
	//CRM LEAD Related data 
	public static final String SALUTATION = "Salutation";
	public static final String STREET = "Street";
	public static final String ZIP_CODE = "ZIP/Postal Code";
	public static final String CITY = "City";
	public static final String TAX_NUMBER = "Tax Number Provided";
	public static final String TAX_NUMBER_VALUE = "Tax Number";
	public static final String SOURCE = "Source";
	public static final String PRODUUCT_LINE = "Product Line";
	public static final String BUSINESS_PHONE = "Business phone";
	public static final String NAME_FIRST = "First Name";
	public static final String NAME_LAST = "Last Name";
	public static final String TOPIC = "Topic";
	public static final String EMAIL = "Email";
	public static final String COMPANY_NAME = "Company Name";
	public static final String COUNTRYCRM = "Country";
	
	//CRM CUSTOMER RELATED DATA
	public static final String FIRST_NAME = "FirstName";
	public static final String COUNTRY_NAME = "Country";
	
	// * All Email related constants are define here
	public interface Email {
		public final static String EMAIL_RESULT_SUBJECT = "Smoke Test";
		public final static String EMAIL_RESULT_SUBJECT1 = " Regression Testing - ERP";
		public final static String EMAIL_SEND_FROM_LABEL = "NoReply-TeamViewer";
		public final static String EMAIL_FROM = "emailFrom";
		public final static String EMAIL_TO = "emailTo";
		public final static String EMAIL_CC = "emailCC";
		public final static String EMAIL_SMTP_HOST_SERVER = "smtpHostServer";
		public final static String EMAIL_SMTP_MAIL_SERVER = "smtpMailServer";
		public final static String SCENARIO_LABEL = "Scenario";
		public final static String SCENARIO_OUTLINE_LABEL = "Scenario Outline";
		public final static String BACKGROUND_LABEL = "Background";
		public final static String FAILED_STATUS = "failed";
		public final static String PASS_STATUS = "PASS";
		public final static String FAIL_STATUS = "FAIL";
	
}
}