package TeamViewer.TVAutomation;

import java.io.IOException;
import javax.mail.MessagingException;
import org.junit.runner.RunWith;
import TeamViewer.TVAutomation.DataProvider.TestData;
import TeamViewer.TVAutomation.Utils.DriverFactory;
import TeamViewer.TVAutomation.Utils.SendEmailSetup.SendEmail;
import cucumber.api.CucumberOptions;


@RunWith(ExtendedCucumberRunner.class)
@CucumberOptions(
		
		
	//********************SCRIPTING AND STAGING****************************
		
		tags = {"@EcomQuotationFlow,@EcomInvoiceFlow"},
		
		
		//*******************PRODUCTION********************
		
	//	 tags = {"@RemoteAccessValidationsOnProductionFrance"},
		//tags = {"@RemoteAccessValidationsOnProductionItaly"},
	//	tags = {"@RemoteAccessValidationsOnProductionLuxembourg"},
		//tags = {"@RemoteAccessValidationsOnProductionSpain"},
				
		

		features = { "src/test/resources/features/" }, plugin = { "pretty", "html:target/cucumber-html-report-SanityForTeamViewer", "com.cucumber.listener.ExtentCucumberFormatter:target/extent-reports/TeamViewerAutomationTestExecutionReport.html",
				"json:target/cucumber-json-report-SanityForTeamViewer.json", "rerun:target/rerun-SanityForTeamViewer.txt" })

public class RunAll_SanityForTeamViewer extends DriverFactory{
	
	
	@BeforeSuite
	public static void setUp() {
		
		System.out.println("SUITE HAS BEEN STARTED");
		
		
	}

	@AfterSuite
	public static void tearDown() throws MessagingException, IOException {
		System.out.println("Test Suite Execution Completed.");
		
		//PRODUCTION
		SendEmail.sendEmailBody(TestData.Email.EMAIL_RESULT_SUBJECT, "cucumber-json-report-SanityForTeamViewer");
		
		//Internal ERP
		//SendEmail.sendEmailBody(TestData.Email.EMAIL_RESULT_SUBJECT1, "cucumber-json-report-SanityForTeamViewer");
		// for Production
       quitDriver(); 
	}
	
}