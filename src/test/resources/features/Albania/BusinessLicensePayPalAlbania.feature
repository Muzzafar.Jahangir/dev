#Created by Muhammad Naveed Khan and Naqash Zafar on May 24, 2019
Feature: Business License PayPal - Albania

  Background: 
    Given User access to Albania store
    And I Close Cookie 
    
     @TCIDSanity @TCIDAlbaniaPaypal @TCIDAlbania @TCIDBusinessLicense @TCIDBusinessLicenseBasic @t1
  Scenario: Verify purchase business license of Team Viewer for Albania
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Albania data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 
 	
 	
 	 @TCIDSanity @TCIDAlbaniaPaypal @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Albania
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Albania data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 
 	
 	
 	
 	

 	
 	