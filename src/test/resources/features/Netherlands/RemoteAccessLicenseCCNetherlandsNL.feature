#Created by Muhammad Naveed Khan and Naqash Zafar on April, 26 2019
Feature: Remote Access License CC - Netherlands

  Background: 
    Given User access to Netherlands NL store
    And I Close Cookie
    
   @TCIDSanity  @TCIDNetherlands @TCIDFranceRemoteDemo1 @TCIDRemoteLicense
  Scenario: Verify Checkout Process for Team Viewer for Netherlands NL
 	When I Click on Remote Access Subscription Button
 	 Then I Click on Next button
	And I enter Netherlands data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
	And I Activate License
	And I Enter Credentials For Login on TV Activation Link
	And I Login Email
	#And I Access Gmail
	#And I Login Gmail
	#And I archive Emails
	
	@TCIDNetherlandsVAT @TCIDSanity @TCIDRemoteLicense
  Scenario: Verify Checkout Process for Team Viewer for Netherlands NL using VAT
 	When I Click on Remote Access Subscription Button
 	Then I Click on Next button
 	 And  I enter Netherlands VAT
	And I enter Netherlands data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	And I Activate License
 	And I Enter Credentials For Login on TV Activation Link
	And I Login Email