#Created by Muhammad Naveed Khan and Naqash Zafar on April 23, 2019
Feature: Business Licence PayPal - Netherlands

  Background: 
    Given User access to Netherlands store
    And I Close Cookie
    
  @TCIDSanity  @TCIDNetherlands @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Netherlands
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Netherlands data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	  @TCIDSanity  @TCIDNetherlands @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Netherlands
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Netherlands data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	

 	  @TCIDSanity @TCIDNetherlandsVAT  @TCIDNetherlands @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Netherlands using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And  I enter Netherlands VAT
	And I enter Netherlands data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	  @TCIDSanity @TCIDNetherlandsVAT  @TCIDNetherlands @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Netherlands using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And  I enter Netherlands VAT
	And I enter Netherlands data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	