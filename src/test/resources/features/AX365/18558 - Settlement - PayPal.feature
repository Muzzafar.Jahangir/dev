#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Business License Payment with Credit Card is successful - 18554

  Background: 
    Given User access to D365
     
    
  @TCIDInvoiceSettlementPP @TCIDSanity
  Scenario: Verify Business License Payment with Credit Card is successful - 18554
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Business license order in ERP
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I verify contract is invoiced 
 	And I open invoice under journal for getting invoice number
 	And I Switch To ECOM
 	And I Enter Details in ECOM to process
 	And I select paypal as payment method
 	And I click On Proceed Next Button
 	And I enter paypal credentials for ERP Flow
 	And I validate payment is successful
 	And I Switch To ERP again
 	And I open customer and check payment settlement against invoice of CC
 	
 	
 	
 	
 	
 	
 	