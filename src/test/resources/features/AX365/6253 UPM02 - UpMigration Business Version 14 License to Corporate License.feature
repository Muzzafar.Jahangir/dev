#Created by Muhammad Naveed Khan and Naqash Zafar on May 30 , 2019
Feature: Verify Migration TV14C0001 To TVC0001

  Background: 
    Given User access to D365
     
    
  @TCIDMigrationTV14CtoTVC @TCIDSanity
  Scenario: Verify Migration TV14C0001 To TVC0001 in Dynamics 365
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create TV14C0001 Perpetual order in ERP
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I migrate TV14C0001 To TVC0001
	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I validate TV14C0001 To TVC0001 migration
 	And I Cancel Migration Contract On ERP
 
 	
 	