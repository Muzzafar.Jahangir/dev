#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Cancel Contract In ERP

  Background: 
    Given User access to D365
     
    
  @TCIDCreditNoteForCancellation-19380 @TCIDDemoD365 @TCIDSanity @CancelContract
  Scenario: Verify order contract is successfully cancelled in Dynamics 365
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create TV14B0001 Perpetual order in ERP
 	And I get values of tax subtotal and totals for Internal Created Order
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I Cancel Perpetual Contract On ERP
 	And I confirm sales order and invoice in ERP
 	And I open journal invoice for voucher checking
 	
 	
 	