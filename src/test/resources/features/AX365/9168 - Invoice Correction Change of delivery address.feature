#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Invoice Correction for Delivery Address

  Background: 
    Given User access to D365
     
    
  @TCIDInvoiceCorrectionForDeliveryAddress @TCIDSanity
  Scenario: Verify Invoice Correction for Delivery Address
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Business license order in ERP
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I correct the invoice and changing the customer delivery address
 	And I resend the invoice for implementing the changes
 	And I verify delivery address after resending the invoice
 	And I open journal invoice for voucher checking and lasernet printing
 	And I verify delivery address after resending the invoice
 	
 	
 	
 	