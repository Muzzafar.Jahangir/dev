#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Switch Premium License To Corporate License In ERP

  Background: 
    Given User access to D365
     
    
  @TCID6213 @TCIDSanity
  Scenario: Verify Premium License is successfully switched with discount to Corporate License in Dynamics 365
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Premium license order in ERP
 	And I get values of tax subtotal and totals for Internal Created Order
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I switch from Premium To Corporate
 	And I complete order in ERP
 	And I again confirm sales order and invoice in ERP for Switch Order
 	And I verify discount on corporate is applied
 	And I validate Business To Corporate switching
 	And I open journal invoice for corporate discount checking
 	And I verify discounted price available in lasernet preview for corporate
 	
 	
 	