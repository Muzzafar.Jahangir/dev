#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Order Totals In ERP

  Background: 
    Given User access to D365
     
    
  @TCIDDemoD3650 @TCIDSanity
  Scenario: Verify totals of purchases are correct in Dynamics 365
 	When I Login ERP
 	And I Search Order in ERP
 	And I Verify Purchases in ERP
 	And I Verify Totals of Order from ECOM to ERP
 	