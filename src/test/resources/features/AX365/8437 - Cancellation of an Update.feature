 #Created by Muhammad Naveed Khan and Naqash Zafar on May 30 , 2019
Feature: Verify Update TV13B0001 To TV14B0001

  Background: 
    Given User access to D365
     
    
  @TCIDTV13BtoTV14BUpdate-8437 @TCIDSanity
  Scenario: Verify Update TV13B0001 To TV14B0001
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create TV13B0001 Perpetual order in ERP
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I update TV13B0001 To TV14B0001
	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I validate TV13B0001 To TV14B0001 update
 	And I Cancel Perpetual Contract On ERP
 	And I confirm sales order and invoice in ERP
 	And I validate TV13B0001 line is activated back
 
 	
 	