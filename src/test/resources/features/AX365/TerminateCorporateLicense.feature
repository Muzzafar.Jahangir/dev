#Created by Muhammad Naveed Khan and Naqash Zafar on May 5 , 2019
Feature: Verify Terminate Corporate License in ERP

  Background: 
    Given User access to D365
     
    
  @TCIDCorporateL @TCIDSanity
  Scenario: Verify Corporate License is terminated successfully
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Corporate license order in ERP
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I terminate contract in ERP
 	And I verify terminatation of contract in ERP