#Created Naqash Zafar on July  25 , 2019
Feature: Verify Business License Invoice Payment is PAID successfully - 18555


# USE NETHERLANDS VPN TO ROUTE FOR STAGING AND INVOICE LANGUAGE MUST BE fr FOR THAT USER
  Background: 
    Given User access to D365
     
    
  @TCIDPaymentBusinesslicense-18555 @TCIDSanity
  Scenario: Business License Invoice Payment is PAID successfully
 	When I Login ERP
 	And I Search Customer in ERP
 	And I create Business license order in ERP
 	And I complete order in ERP
 	And I confirm sales order and invoice in ERP
 	And I verify contract is invoiced
 	And I open invoice under journal for getting invoice number
 	And I Switch To ECOM
 	And I Close Cookie
 	And I Enter Details in ECOM to process
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data for Invoice payment on staging
 	And I validate payment is successful
 	And I Open New Tab and Run The Job For Posting Of Status
 	And I open customers page and search relevant customer
 	And I Open Customer Transactions And Validate the Settlements
 	And I Search For Settled Invoice and Check The Offset account
 	
 	
 	
 	