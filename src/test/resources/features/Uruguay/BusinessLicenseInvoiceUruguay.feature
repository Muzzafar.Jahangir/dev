#Created by Muhammad Naveed Khan and Naqash Zafar on April 24, 2019
Feature: Business Licence Invoice - Uruguay

  Background: 
    Given User access to Uruguay store
    And I Close Cookie 
    
  @TCID03 @TCIDSanity @TCIDUruguay @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Uruguay
    When I Click on Business License Button US Region
    Then I Click on Next button
	And I enter Uruguay data
	And I select the State
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 @TCID04 @TCIDSanity @TCIDUruguay @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Uruguay
    When I Click on Business License Button US Region
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Uruguay data
	And I select the State
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 
 	

 	
 	