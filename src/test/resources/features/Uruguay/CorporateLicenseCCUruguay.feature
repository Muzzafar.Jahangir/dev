#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate License CC - Uruguay

  Background: 
    Given User access to Uruguay store
    And I Close Cookie 
    
   @TCID05 @TCIDSanity @TCIDUruguay @TCIDCorporateLicenseCC @TCIDCorporateLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Uruguay
    When I Click on Corporate License Button US Region
    Then I Click on Next button
	And I enter Uruguay data
	And I select the State
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	@TCID06  @TCIDSanity @TCIDUruguay @TCIDCorporateLicenseCC @TCIDCorporateLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Uruguay
    When I Click on Corporate License Button US Region
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Uruguay data
	And I select the State
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement