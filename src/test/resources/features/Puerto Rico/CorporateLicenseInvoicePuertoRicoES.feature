#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate License Invoice - Puerto Rico

  Background: 
    Given User access to Puerto Rico ES store
    And I Close Cookie 
    
     @TCIDSanity @TCIDPuertoRico @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Puerto Rico ES
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Puerto Rico data
	And I select the State
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	 @TCID10101  @TCIDSanity @TCIDPuertoRico @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Puerto Rico ES
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Puerto Rico data
	And I select the State
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement