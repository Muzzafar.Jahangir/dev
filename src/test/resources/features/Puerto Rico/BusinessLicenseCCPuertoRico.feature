#Created by Muhammad Naveed Khan and Naqash Zafar on April 24, 2019
Feature: Business Licence CC - Puerto Rico

  Background: 
    Given User access to Puerto Rico store
    And I Close Cookie 
    
     @TCIDSanity @TCIDPuertoRico @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer Puerto Rico
    When I Click on Business License Button US Region
    Then I Click on Next button
	And I enter Puerto Rico data
	And I select the State
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	     @TCIDSanity @TCIDPuertoRico @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer Puerto Rico
    When I Click on Business License Button US Region
    Then I Click on Next button
    And I add All the Business License AddOns
	And I enter Puerto Rico data
	And I select the State
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	

 	
 	