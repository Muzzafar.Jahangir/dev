#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Premium License CC - Portugal

  Background: 
    Given User Access to Portugal PT store
    And I Close Cookie 
    
    @TCIDSanity @TCIDPortugal @TCIDPremiumLicenseCC @TCIDPremiumLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Portugal PT
    When I Click on Premium License Button
    Then I Click on Next button
	And I enter Portugal data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDPortugal @TCIDPremiumLicenseCC @TCIDPremiumLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Portugal PT
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Portugal data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	    @TCIDSanity @TCIDPortugalVAT @TCIDPortugal @TCIDPremiumLicenseCC @TCIDPremiumLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Portugal PT using VAT
    When I Click on Premium License Button
    Then I Click on Next button
     And I enter Portugal VAT
	And I enter Portugal data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDPortugalVAT @TCIDPortugal @TCIDPremiumLicenseCC @TCIDPremiumLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Portugal PT using VAT
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
     And I enter Portugal VAT
	And I enter Portugal data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement