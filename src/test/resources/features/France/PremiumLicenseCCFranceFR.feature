#Created by Muhammad Naveed Khan and Naqash Zafar on May 2, 2019
Feature: Premium License CC - France FR

  Background: 
    Given User access to France FR store
    And I Close Cookie 
    
    @TCIDSanity @TCIDFrance @TCIDPremiumLicenseCC @TCIDPremiumLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for France  FR
    When I Click on Premium License Button
    Then I Click on Next button
	And I enter France data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDFrance @TCIDPremiumLicenseCC @TCIDPremiumLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for France FR
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter France data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	    @TCIDSanity @TCIDFranceVAT @TCIDPremiumLicenseCC @TCIDPremiumLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for France  FR using VAT
    When I Click on Premium License Button
    Then I Click on Next button
    And I enter France VAT
	And I enter France data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDFranceVAT @TCIDPremiumLicenseCC @TCIDPremiumLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for France FR using VAT
    When I Click on Premium License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter France VAT
	And I enter France data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
