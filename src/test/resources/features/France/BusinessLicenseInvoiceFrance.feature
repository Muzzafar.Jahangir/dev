#Created by Muhammad Naveed Khan and Naqash Zafar on May 2, 2019
Feature: Business Licence Invoice - France

  Background: 
    Given User access to France store
    And I Close Cookie
    
    
  @TCIDSanity  @TCIDFrance @TCIDBusinessLicense @TCIDBusinessLicenseBasic @TCIDBusinessLicenseFranceDemo
  Scenario: Verify purchase business license of Team Viewer for France
    When I Click on Business License Button
    Then I Click on Next button
	And I enter France data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	
 	@TCIDSanity  @TCIDFrance @TCIDBusinessLicense @TCIDBusinessLicenseFranceMobileDeviceAddOn
  Scenario: Verify purchase business license with Mobile Device Support Addon of Team Viewer for France
    When I Click on Business License Button
    #And I add Support for Mobile Devices AddsOn For Business License
    #Then I Click on Next button
	#And I enter France data
 	#And I select invoice as payment method
 	#And I select check box
 	#And Validating the Calculations
 	#And I Place Order
 	#And I verify Order Placement
 	#And I Login Email
	#And I SignUp for TV account
	#And I Validate License is Added
	#And I delete Account of TeamViewer
	#And I clear Inbox
	#Given User access to D365
	#When I Login ERP
 	#And I Search Order in ERP
 	#And I Verify Order in ERP
 	#And I Verify Totals of Order in ERP
 	
 	
 	
 	  @TCIDSanity  @TCIDFrance @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for France
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter France data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	


	  @TCIDSanity  @TCIDFranceVAT @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for France using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter France VAT
	And I enter France data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	  @TCIDSanity  @TCIDFranceVAT @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for France using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter France VAT
	And I enter France data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	