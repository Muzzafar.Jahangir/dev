#Created by Muhammad Naveed Khan and Naqash Zafar on May 3, 2019
Feature: Corporate License PayPal - Italy IT

  Background: 
    Given User access to Italy IT store
    And I Close Cookie 
    
     @TCIDSanity @TCIDItaly @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Italy - IT
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Italy data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	 @TCID10101  @TCIDSanity @TCIDItaly @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Italy IT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Italy data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	     @TCIDSanity @TCIDItalyVAT @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Italy - IT using VAT
    When I Click on Corporate License Button
    Then I Click on Next button
    And I enter Italy VAT
	And I enter Italy data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	  @TCIDSanity @TCIDItalyVAT @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Italy IT using VAT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter Italy VAT
	And I enter Italy data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement