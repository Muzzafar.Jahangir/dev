#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Premium License Invoice - Panama

  Background: 
    Given User access to Panama store
    And I Close Cookie 
    
   @TCIDSanity @TCIDPanama @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Premium license of Team Viewer for Panama
    When I Click on Premium License Button US Region
    Then I Click on Next button
	And I enter Panama data
	And I select the State
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	   @TCIDSanity @TCIDPanama @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Premium license of Team Viewer for Panama
    When I Click on Premium License Button US Region
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Panama data
	And I select the State
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement