#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate License Invoice - Belgium FR

  Background: 
    Given User access to Belgium FR store
    And I Close Cookie 
    
     @TCIDSanity @TCIDBelgium @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Belgium FR
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	 @TCID10101  @TCIDSanity @TCIDBelgium @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Belgium FR
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	     @TCIDSanity @TCIDBelgium @TCIDBusinessLicense @TCIDBusinessLicenseBasic @TCIDBelgiumVAT
  Scenario: Verify purchase Corporate license of Team Viewer for Belgium FR using VAT
    When I Click on Corporate License Button
    Then I Click on Next button
    And I enter Belgium VAT
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	 @TCID10101  @TCIDSanity @TCIDBelgium @TCIDBusinessLicense @TCIDBusinessLicenseAddOns @TCIDBelgiumVAT
  Scenario: Verify purchase Corporate license of Team Viewer for Belgium FR using VAT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter Belgium VAT
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement