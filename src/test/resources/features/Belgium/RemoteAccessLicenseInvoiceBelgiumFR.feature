#Created by Muhammad Naveed Khan on August 13, 2018
Feature: Remote Access License Invoice - Belgium FR

  Background: 
    Given User access to Belgium FR store
    And I Close Cookie
        @TCIDSanity @TCIDBelgium @TCIDRemoteLicense
  Scenario: Verify CheckOut Process for Team Viewer for Belgium FR
	  
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	#And I Activate License
 	#And I Enter Credentials For Login on TV Activation Link
	#And I Login Email
 	
 	      @TCIDBelgiumVAT @TCIDSanity @TCIDBelgium @TCIDRemoteLicense
  Scenario: Verify CheckOut Process for Team Viewer for Belgium FR using VAT
	  
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
     And I enter Belgium VAT
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	#And I Activate License
 	#And I Enter Credentials For Login on TV Activation Link
	#And I Login Email
 	

 	
 	