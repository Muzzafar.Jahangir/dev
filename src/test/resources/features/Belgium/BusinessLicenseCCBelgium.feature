#Created by Muhammad Naveed Khan and Naqash Zafar on April 24, 2019
Feature: Business Licence CC - Belgium

  Background: 
    Given User access to Belgium store
    And I Close Cookie 
    
   @TCIDSanity @TCIDBelgium @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Belgium
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 @TCIDSanity @TCIDBelgium @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Belgium
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	
 	@TCIDTestVAT   @TCIDSanity @TCIDBelgiumVAT @TCIDBelgium @TCIDBusinessLicenseCC @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Belgium using VAT
    When I Click on Business License Button 
    Then I Click on Next button
   	And I enter Belgium VAT
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 @TCIDSanity @TCIDBelgium @TCIDBusinessLicenseCC @TCIDBusinessLicenseAddOns @TCIDBelgiumVAT
  Scenario: Verify purchase business license of Team Viewer for Belgium using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Belgium VAT
	And I enter Belgium data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement