#Created by Muhammad Naveed Khan on August 13, 2018
Feature: Remote Access License Invoice - Belgium NL

  Background: 
    Given User access to Belgium NL store
    And I Close Cookie
  @TCIDSanity @TCIDBelgium @TCIDRemoteLicense
  Scenario: Verify CheckOut Process for Team Viewer for Belgium NL
	  
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 @TCIDBelgiumVAT @TCIDSanity @TCIDBelgium @TCIDRemoteLicense
  Scenario: Verify CheckOut Process for Team Viewer for Belgium NL using VAT
	  
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
     And I enter Belgium VAT
	And I enter Belgium data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement

 	
 	