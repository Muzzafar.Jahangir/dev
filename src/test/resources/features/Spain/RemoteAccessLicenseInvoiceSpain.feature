#Created by Muhammad Naveed Khan and Naqash Zafar on May 14, 2019
Feature: Remote Access License Invoice - Spain

  Background: 
    Given User access to Spain store
    
  
  @RemoteAccessValidationsOnProductionSpain
  Scenario: Verify CheckOut Process for Team Viewer for Spain
	When I Change Langauage and Open WebShop
    When I Click on Remote Access Subscription Button
    And I Close Cookie 
    Then I Click on Next button
	And I enter Spain data
    And I select invoice as payment method
 	And I select check box
	And Validating the Calculations
	And I Place Order
    And I verify Order Placement
 	And I Login Email
	And I SignUp for TV account
	And I Validate License is Added
	And I delete Account of TeamViewer
 	

 	
 	
 	

 	
 	