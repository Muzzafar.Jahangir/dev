#Created by Muhammad Naveed Khan and Naqash Zafar on May 3, 2019
Feature: Corporate License Invoice - Spain ES

  Background: 
    Given User access to Spain ES store
    And I Close Cookie 
    
     @TCIDSanity @TCIDSpain @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Spain - ES
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Spain data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	 @TCID10101  @TCIDSanity @TCIDSpain @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Spain ES
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Spain data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	    @TCIDSanity @TCIDSpain @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Spain - ES using VAT
    When I Click on Corporate License Button
    Then I Click on Next button
    And I enter Spain VAT
	And I enter Spain data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	 @TCID10101  @TCIDSanity @TCIDSpain @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Spain ES using VAT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter Spain VAT
	And I enter Spain data
 	And I select invoice as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement