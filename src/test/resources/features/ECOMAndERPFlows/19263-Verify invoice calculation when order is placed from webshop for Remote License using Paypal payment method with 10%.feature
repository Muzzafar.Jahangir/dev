#Created by Muhammad Naveed Khan and Naqash Zafar on Aug 05, 2019
Feature:  Verify invoice calculation when order is placed from webshop for Remote License using Paypal payment method with Tax 10%
  Background: 
    Given User access to Japan store
 	
 	@CorporateLicenseFlowJapan-19263
  Scenario:  Verify invoice calculation when order is placed from webshop for Remote License using Paypal payment method with Tax 10%
   	
   	
    And I Close Cookie
  	#And I Refresh Browser # Dont include it for Japan
   	Then I Click on Next button
	And I enter Japan data
	And I select the State
	And I get quantity and Prices of Products Japan
	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	And I Get Order Id and Payment Method From Success Page
 	And I Switch To ERP for Order Verification
 	When I Login ERP
 	And I Search Channel Reference ID in ERP
 	And I open the sales order and verify products purchased in ERP
 	And I verify quantity of purchased products in ERP
 	And I verify amount of purchased products in ERP
 	And I verify customer information in General Tab
 	And I verify customer information in SetUp Tab
 	And I verify customer information in Price And Information Tab
 	And I verify customer information in Financials And Dimensions Tab
 	And I open new tab and get the number of Main Account and Sales Tax using Sales Tax Code and Sales Tax Group
 	And I get values of tax subtotal and totals for verification of ECOM to ERP flow
 	And I confirm sales order and invoice in ERP
 	And I open journal invoice
 	And I open journal invoice for voucher checking and lasernet printing
 	And I open customers page and search relevant customer
 	And I Open Customer Transactions And Validate the Settlements
 	And I Search For Settled Invoice
 	
 	And I open Data Recognition form and search order
 	And I run the journal job
 	And I open journal
 	Then I validate values and post and validate journal
 	
 	
 	
 