Feature: Verify that payment link functionality is working and payment done through PayPal payment method successfully


# USE NETHERLANDS VPN TO ROUTE FOR STAGING AND INVOICE LANGUAGE MUST BE fr FOR THAT USER
  Background: 
    Given User access to Netherlands store for staging
     
    
  @TCIDPaymentBusinesslicense-19657 @TCIDSanity
  Scenario: Business License Invoice Payment is PAID successfully
  
   When I Click on Corporate License Button
   And I Close Cookie
   Then I Refresh Browser
   And I add All Premium/Corporate License AddOns
   And I Click on Next button
   And I enter Netherlands data
   And I get quantity and Prices of Products
   And I select invoice as payment method
   And I select check box
   And Validating the Calculations
   And I Place Order
   And I verify Order Placement
   And I Get Order Id and Payment Method From Success Page
   
 	And I Switch To ERP for Order Verification  
 	And I Login ERP
 	And I Search Channel Reference ID in ERP
 	And I open the sales order and verify products purchased in ERP
 	And I verify quantity of purchased products in ERP
 	And I verify amount of purchased products in ERP
 	And I verify customer information in General Tab
 	And I verify customer information in SetUp Tab
 	And I verify customer information in Price And Information Tab
 	And I verify customer information in Financials And Dimensions Tab
 	And I open new tab and get the number of Main Account and Sales Tax using Sales Tax Code and Sales Tax Group
 	And I get values of tax subtotal and totals for verification of ECOM to ERP flow
 	And I confirm sales order and invoice in ERP
 	And I verify contract is invoiced
 	And I open invoice under journal for getting invoice number
 	And I Switch To ECOM
 	And I Close Cookie
 	And I Enter Details in ECOM to process
 	And I select paypal as payment method
 	And I Place Order
 	And I enter PayPal Credentials
 	And I validate payment is successful
 	And I Open New Tab and Run The Job For Posting Of Status
 	And I open customers page and search relevant customer
 	And I Open Customer Transactions And Validate the Settlements
 	And I Search For Settled Invoice
 	And I Verify Offset account for PayPal
 
 	
 	