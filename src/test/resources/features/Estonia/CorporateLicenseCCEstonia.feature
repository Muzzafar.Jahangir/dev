#Created by Muhammad Naveed Khan and Naqash Zafar on April 26, 2019
Feature: Corporate License CC - Estonia

  Background: 
    Given User access to Estonia store
    And I Close Cookie 
    
    @TCIDSanity @TCIDEstonia @TCIDCorporateLicenseCC @TCIDCorporateLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Estonia
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Estonia data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	    @TCIDSanity @TCIDEstonia @TCIDCorporateLicenseCC @TCIDCorporateLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Estonia
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Estonia data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	    @TCIDSanity @TCIDEstoniaVAT @TCIDCorporateLicenseCC @TCIDCorporateLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Estonia using VAT
    When I Click on Corporate License Button
    Then I Click on Next button
    And I enter Estonia VAT
	And I enter Estonia data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	    @TCIDSanity @TCIDEstoniaVAT @TCIDCorporateLicenseCC @TCIDCorporateLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Estonia using VAT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter Estonia VAT
	And I enter Estonia data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I verify Order Placement