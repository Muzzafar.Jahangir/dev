#Created by Muhammad Naveed Khan and Naqash Zafar on April 30, 2019
Feature: Remote Access License CC - Poland

  Background: 
    Given User access to Poland store
    And I Close Cookie
    
  @TCID1113 @TCIDSanity
  Scenario: Verify CheckOut Process for Team Viewer for Poland EN 
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
	And I enter Poland data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And I Place Order
 	And I verify Order Placement
 	
 	
 	
 	@TCIDSanity @TCIDPoland @TCIDPolandVAT
  Scenario: Verify CheckOut Process for Team Viewer for Poland EN using VAT
    When I Click on Remote Access Subscription Button
    Then I Click on Next button
    And I enter Poland VAT
	And I enter Poland data
 	And I select Credit Card as Payment Method
 	And I enter Credit Card Data
 	And I select check box
 	And I Place Order
 	And I verify Order Placement
 

 	
 	