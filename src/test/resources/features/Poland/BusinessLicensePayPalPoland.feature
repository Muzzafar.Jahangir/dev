#Created by Muhammad Naveed Khan and Naqash Zafar on April 23, 2019
Feature: Business License PayPal - Poland

  Background: 
    Given User access to Poland store
    And I Close Cookie 
    
    @TCIDSanity @TCIDPoland @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Poland
    When I Click on Business License Button
    Then I Click on Next button
	And I enter Poland data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 
 	
 	 @TCIDSanity @TCIDPoland @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Poland
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
	And I enter Poland data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	@TCIDPolandVAT  @TCIDSanity @TCIDPoland @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase business license of Team Viewer for Poland using VAT
    When I Click on Business License Button
    Then I Click on Next button
    And I enter Poland VAT
	And I enter Poland data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 
 	
 	@TCIDPolandVAT @TCIDSanity @TCIDPoland @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase business license of Team Viewer for Poland using VAT
    When I Click on Business License Button
    And I add All the Business License AddOns
    Then I Click on Next button
    And I enter Poland VAT
	And I enter Poland data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement

 	
 	