#Created by Muhammad Naveed Khan on August 13, 2018
Feature: Request a Catalog

  Background: 
    Given User Access to NL EN application

 @TCID3333
  Scenario: Verify CheckOut Process for Team Viewer using Voucher
    When I Click on Multi User button
    And I add mobile devices
 	Then I Click on Next button
 	And I enter NL data
 	And I enter NL Email
 	And I select payment
 	And I add voucher
 	And I enter NL Voucher
 	And I Apply Voucher
 	Then I verify valid Voucher appeared
 	And I select check box
 	
 	
