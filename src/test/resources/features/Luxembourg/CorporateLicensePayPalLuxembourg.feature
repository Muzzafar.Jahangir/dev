#Created by Muhammad Naveed Khan and Naqash Zafar on May 3, 2019
Feature: Corporate License PayPal - Lexumbourg

  Background: 
    Given User access to Luxembourg store
    And I Close Cookie 
    
     @TCIDSanity @TCIDLexumbourg @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Lexumbourg
    When I Click on Corporate License Button
    Then I Click on Next button
	And I enter Luxembourg data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	 @TCID10101  @TCIDSanity @TCIDLexumbourg @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Luxembourg
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
	And I enter Luxembourg data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	     @TCIDSanity @TCIDLexumbourg @TCIDBusinessLicense @TCIDBusinessLicenseBasic
  Scenario: Verify purchase Corporate license of Team Viewer for Lexumbourg using VAT
    When I Click on Corporate License Button
    Then I Click on Next button
    And I enter Luxembourg VAT
	And I enter Luxembourg data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement
 	
 	
 	 @TCID10101  @TCIDSanity @TCIDLexumbourg @TCIDBusinessLicense @TCIDBusinessLicenseAddOns
  Scenario: Verify purchase Corporate license of Team Viewer for Luxembourg using VAT
    When I Click on Corporate License Button
    And I add All Premium/Corporate License AddOns
    Then I Click on Next button
    And I enter Luxembourg VAT
	And I enter Luxembourg data
 	And I select paypal as payment method
 	And I select check box
 	And Validating the Calculations
 	And I Place Order
 	And I enter PayPal Credentials
 	And I verify Order Placement